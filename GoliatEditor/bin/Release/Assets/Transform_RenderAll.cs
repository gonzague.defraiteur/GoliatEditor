public static void RenderAll(Camera cam)
		{
			roots = Transform.roots;
			//string vs, fs;
			if (testPhysic)
			{
				Physic.Init();
				littleBox = new Transform(Vector3.up * 5, Quaternion.identity, Vector3.one);
				groundBox = new Transform(Vector3.zero, Quaternion.identity, Vector3.one);
				littleBox.addComponent<BoxCollider>();
				BoxCollider collider = littleBox.getComponent<BoxCollider>();
				collider.size = new Vector3(1, 1, 1);
				RigidBody body = collider.addComponent<RigidBody>();
				body.useGravity = true;
				body.mass = 1;

				BoxCollider groundCollider = groundBox.addComponent<BoxCollider>();
				groundCollider.size = new Vector3(10, 1, 10);
				_roots.Add(littleBox);
				_roots.Add(groundBox);
				testPhysic = true;
			}
			//Shader.Bind(null);
			//Shader shader = new Shader();
			baseColor = Color.LightGray.gl();
			GL.ClearColor(Color.SlateGray.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			GL.CullFace(CullFaceMode.FrontAndBack);
			//DrawCircle(0.5f, 50, Vector3.zero, Quaternion.identity, Vector3.one, Color.White);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			//OpenTK.OpenGL.Lightv(OpenTK.OpenGL.Enums.LightName.Light0, OpenTK.OpenGL.Enums.LightParameter.Position, light_position);
			//GL.Light(LightName.Light0, LightParameter.Ambient, light_ambient);
			GL.Light(LightName.Light0, LightParameter.Diffuse, mat_specular);
			//GL.Light(LightName.FragmentLight0Sgix , LightParameter.SpotDirection, Color.Indigo.gl());
			Matrix GlobalTransformation = Matrix.Identity;
			
			/*if (testShader == null)
			{
				testShader = new Shader(vtest, ftest);
				
			}*/
			//Shader.Bind(testShader);

			//testShader.SetVariable("pixel_threshold", 500000);
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			//GL.LoadIdentity();
			//Matrix4 lookat2;
			//lookat2 = cameraTransform.worldToLocalMatrix.gl();
			//lookat2.Transpose();
			//GL.LoadMatrix(ref lookat2);


			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Color.Blue.gl());
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			GL.LineWidth(0.5f);
			if (cam != null)
			{
				DrawCircle(0.26f, 50, Vector3.zero, Quaternion.identity, Vector3.one * cam.transform.position.magnitude, Color.Blue);
				DrawCircle(0.26f, 50, Vector3.zero, new Quaternion(new Vector3(90, 0, 0)), Vector3.one * cam.transform.position.magnitude, Color.SeaGreen * 1.3f);
				DrawCircle(0.26f, 50, Vector3.zero, new Quaternion(new Vector3(0, 90, 0)), Vector3.one * cam.transform.position.magnitude, Color.Red);
				//DrawCircle(7, 50, Vector3.zero, Quaternion.identity, Vector3.one, Color.White);

				DrawCircle(0.26f, 50, Vector3.zero, cam.transform.rotation.inverse, Vector3.one * cam.transform.position.magnitude, Color.White);
				DrawCircle(0.30f, 50, Vector3.zero, cam.transform.rotation.inverse, Vector3.one * cam.transform.position.magnitude, Color.White);
				//DrawPlane(Vector2.one * 15, Vector3.zero, Quaternion.identity, Vector3.one, Color.Red);
			}
			//DrawBox(new Vector3(1, 0.1f, 2) * 5, Vector3.zero, Quaternion.identity, Vector3.one, Color.Gray);
			//DrawCircle(5, 50, Vector3.zero, new Quaternion(new Vector3(0, 45, 0)), Vector3.one, Color.Green);
			//DrawCircle(5, 50, Vector3.zero, new Quaternion(new Vector3(0, 135, 0)), Vector3.one, Color.Green);
			for (int i = 0; i < Debug.debugRays.Count; i++)
			{
				GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Lines);
				GL.Vertex3(Debug.debugRays[i].origin.gl());
				GL.Vertex3(Debug.debugRays[i].end.gl());
				GL.End();
			}
			for (int i = 0; i < GameBehaviour.GameBehaviours.Count; i++ )
			{
				if (GameBehaviour.GameBehaviours[i] is BoxCollider)
				{
					BoxCollider collider = (BoxCollider)GameBehaviour.GameBehaviours[i];
					DrawBox(collider.size, collider.transform.position, Quaternion.identity, Vector3.one, Color.Gray);
				}
			}
			for (int i = 0; i < roots.Count; i++)
			{
				Animation anim = roots[i].getComponent<Animation>();
				if (anim != null)
				{
					anim.Update();
				}
				RecursiveRender(roots[i], 0, GlobalTransformation, cam);
				GL.BindTexture(TextureTarget.Texture2D, 0);
			}
			RecursiveRender(arrowsTransform, 0, GlobalTransformation, cam);
		}
		public static bool debug = false;
		
		private static void RecursiveRender(Transform node, int mode, Matrix parentTransformation, Camera cam)
		{
			if (node == null)
				return;
			Matrix GlobalTransformation = parentTransformation * node.transformation;
			//OpenTK.Matrix4 GlobalTransformation = Matrix4.Identity;
			
			if (!node.isActive)
				return;
			if (node.getComponent<Terrain>() != null)
			{
				Color4 Transparent = Color.SlateGray.gl();
				GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Transparent);
			}
			Shader.UnBind();
			if (node.isActive)
			{
				//Debug.Log("OK");
				Matrix4 m = node.transformation.gl();

				m.Transpose();
				if (node.isSelected && mode == 0)
				{
					if (node.getComponent<MeshFilter>() != null)
						GL.PushName(node.GetHashCode());
					RecursiveRender(node, mode + 1, GlobalTransformation, cam);
					Color4 Transparent = new Color4(0, 55, 0, 55);
					GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Transparent);
					GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
					GL.LineWidth(0.01f);
					GL.Enable(EnableCap.LineStipple);
					unchecked
					{
						GL.LineStipple(1, (short)(0x3F07F / 2));
					}
				}
				else if (node.isSelected)
				{
					if (node.getComponent<Terrain>() != null)
					{
						/*if (!File.Exists(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "/testterrain/tmp.bmp")))
						{
							if (test == null)
								test = node.getComponent<Terrain>().heightMap;
							//							Texture heightMap = node.getComponent<Terrain>().heightMap;
							//						heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "/testterrain/tmp.bmp"));
							//node.getComponent<Terrain>().heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "/testterrain/tmp.bmp"));
						}*/
						//Texture tmp = node.getComponent<Terrain>().heightMap;
						//Console.WriteLine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
						//tmp.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "testterrain/tmp.bmp"));
						//tmp.UnBind();
						Vector3 impact = node.getComponent<Terrain>().mouseImpact;
						OpenTK.Vector2 mousepos = new OpenTK.Vector2(impact.x, impact.z);

						Shader.terrainShader.SetVariable("mouseposition", mousepos.X, mousepos.Y);
						Shader.terrainShader.SetVariable("node_matrix", m);

						Matrix4 view = Camera.current.transform.transformation.inverse.gl();
						view.Transpose();
						Shader.terrainShader.SetVariable("view_matrix", view);
						//Shader.terrainShader.SetVariable("heightMap", node.getComponent<Terrain>().heightMap.glID);
						Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, node.getComponent<Terrain>().heightMap.glID));
						GL.ActiveTexture(TextureUnit.Texture0);

						Renderer.Call(() => GL.UseProgram(Shader.terrainShader.Program));
						Renderer.Call(() => GL.Uniform1(GL.GetUniformLocation(Shader.terrainShader.Program, "heightmap"), TextureUnit.Texture0 - TextureUnit.Texture0));
						Shader.Bind(Shader.terrainShader);

					}
					GL.Disable(EnableCap.LineStipple);
					GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
				}
				else if (node.getComponent<MeshFilter>() != null)
				{
					//Console.WriteLine("this hash code:" + node.GetHashCode());
					//Console.WriteLine("in behaviours type:" + GameBehaviour.GameBehaviours[node.GetHashCode()].GetType());
					GL.PushName(node.GetHashCode());
					GL.Disable(EnableCap.LineStipple);
					GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
				}

				GL.PushMatrix();
				GL.MultMatrix(ref m);
				//Color4 layerColor = FromIntColor((uint)node.GetHashCode());µ
				if (node.getComponent<Goliat.SphereCollider>() != null)
				{

					;
				}
				if (node.getComponent<Goliat.BoxCollider>() != null)
				{
					;
					//Console.WriteLine("do i use gravity?" + node.getComponent<BoxCollider>().jrigidbody.AffectedByGravity);
					//Console.WriteLine("size:" + node.getComponent<BoxCollider>().size);
					//DrawBox(node.getComponent<Goliat.BoxCollider>().size, node.position, node.rotation, Vector3.one, Color.Gray);
				}
				if (node.getComponent<Goliat.MeshFilter>() != null )
				{

					Assimp.Scene scene = node.m_model;
					List<Goliat.MeshFilter> meshs = node.getComponents<Goliat.MeshFilter>();
					Animation anim = node.root.getComponent<Animation>();
					if (meshs == null)
					{
						Console.WriteLine("meshs null");
					}
					if (meshs[0].sharedMesh == null)
					{
						Console.WriteLine("sharedMesh null");
					}
					if (meshs[0].sharedMesh.vertices == null)
					{
						Console.WriteLine("vertices null");
					}
					int len = meshs[0].sharedMesh.vertices.Length;
					List<Vector3>[] verticesWeights = new List<Vector3>[len];
					List<Vector3>[] normalsWeights = new List<Vector3>[len];
					Vector3[] vertices = (Vector3[])meshs[0].sharedMesh.vertices.Clone();
					Vector3[] normals = (Vector3[])meshs[0].sharedMesh.normals.Clone();
				
					if (meshs[0].GetType().IsSameOrSubclass(typeof(Goliat.SkinnedMeshFilter)))
					{
						SkinnedMeshFilter filter = (SkinnedMeshFilter)meshs[0];
						
						
						Mesh mesh = filter.sharedMesh;
						Dictionary<string, Matrix> finalMatrixs = new Dictionary<string, Matrix>();
						string guid = filter.sharedMesh.guid;
						if (!debug)
						{
							//Console.WriteLine("found skinned mesh 0");
							debug = true;
						}
						//Console.WriteLine("hey0.0");
						if (filter == null)
						{
							Console.WriteLine("filter null");
							
						}
						//Console.WriteLine("hey0.0.1");
						/*if (filter.bones == null)
						{
							Console.WriteLine("filter bones null");
						}*/
						//Console.WriteLine("hey0.0.2");
						for (int i = 0; i < filter.bones.Count; i++)
						{
							Bone bone = filter.bones[i];
							Matrix finalMatrix = bone.localToRootMatrix * bone.skins[guid].offsetMatrix;
							List<VertexWeight> weights = bone.skins[guid].weights;
							for (int j = 0; j < weights.Count; j++)
							{
								if (verticesWeights[weights[j].index] == null)
								{
									verticesWeights[weights[j].index] = new List<Vector3>();
									normalsWeights[weights[j].index] = new List<Vector3>();
								}
								verticesWeights[weights[j].index].Add((finalMatrix * mesh.vertices[weights[j].index]) * weights[j].weight);
								Vector4 normal = new Vector4(mesh.normals[weights[j].index].x, mesh.normals[weights[j].index].y, mesh.normals[weights[j].index].z, 0);
								normal = finalMatrix.multiplyPoint(normal);
								normalsWeights[weights[j].index].Add(Vector3.Lerp(mesh.normals[weights[j].index], new Vector3(normal.x, normal.y, normal.z), weights[j].weight));
							}
						}
						for (int i = 0; i < len; i++)
						{
							if (verticesWeights[i] == null)
							{
								continue;
							}
							Vector3 medianVert = Vector3.zero;
							Vector3 medianNormal = Vector3.zero;
							int count = verticesWeights[i].Count;
							for (int j = 0; j < count; j++)
							{
								medianVert += verticesWeights[i][j];
								medianNormal += verticesWeights[i][j];
							}
							vertices[i] = medianVert;
							normals[i] = medianNormal;
						}
					}
					

					foreach (Goliat.MeshFilter myFilter in meshs)
					{
						if (mode == 1 || !node.isSelected)
						{
							GL.Enable(EnableCap.LineStipple);
							GL.LineWidth(1.0f);
							
							foreach (Vector3[] line in myFilter.sharedMesh.debugLines)
							{
								BeginMode faceMode = BeginMode.Lines;
								GL.Begin(faceMode);

								GL.Vertex3(line[0].gl());
								GL.Vertex3(line[1].gl());
								GL.End();
							}
						}
						Goliat.Mesh myMesh = myFilter.sharedMesh;
						Assimp.Mesh mesh = myMesh.importVersion;
						Goliat.MeshRenderer myRenderer = node.getComponent<Goliat.MeshRenderer>();
						if (myRenderer == null && node.getComponent<Terrain>() == null)
						{
							Debug.Log("renderer null");
						}
						

						if (myMesh.hasNormals)
						{
							GL.Enable(EnableCap.Lighting);
						}
						else
						{
							GL.Disable(EnableCap.Lighting);
						}

						/*bool hasColors = mesh.HasVertexColors(0);
						if (myMesh.hasColors)
						{
							GL.Enable(EnableCap.ColorMaterial);
						}
						else
						{
							GL.Disable(EnableCap.ColorMaterial);
						}*/
						//bool hasTexCoords = myMesh.hasTextureCoords(0);

						if (myMesh.faces != null)
						{
							foreach (Face face in myMesh.faces)
							{
								if (myRenderer != null && myRenderer.materials != null && (!node.isSelected || mode == 1))
								{
									myRenderer.materials[face.materialIndex].Bind();
								}
								BeginMode faceMode;
								switch (face.indices.Count())
								{
									case 1:
										faceMode = BeginMode.Points;
										break;
									case 2:
										faceMode = BeginMode.Lines;
										break;
									case 3:
										faceMode = BeginMode.Triangles;
										break;
									default:
										faceMode = BeginMode.Polygon;
										break;
								}
								GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);
								for (int i = 0; i < face.indices.Count(); i++)
								{
									int indice = face.indices[i];
									GL.Color4(baseColor);
									/*if (myMesh.hasColors)
									{
										Color4 vertColor = FromIntColor(0);
										if (hasColors)
											vertColor = FromColor(mesh.VertexColorChannels[0][indice]);

										GL.Color4(vertColor);
									}*/
									if (myMesh.hasNormals)
									{
										//OpenTK.Vector3 normal = FromVector(mesh.Normals[indice]);
										if (myMesh.normals != null)
										{
											OpenTK.Vector3 normal = normals[indice].gl();
											GL.Normal3(normal);
										}
									}
									if (myMesh.hasTextureCoords)
									{
										//OpenTK.Vector3 uvw = FromVector(mesh.TextureCoordinateChannels[0][indice]);
										OpenTK.Vector2 uv = myMesh.uvs[indice].gl();
										GL.TexCoord2(uv.X, uv.Y);
									}

									OpenTK.Vector3 pos = vertices[indice].gl();
									//if (node.getComponent<Terrain>() != null && node.isSelected)
									//Shader.terrainShader.SetVariable("vertex_pos", pos.X, pos.Y , pos.Z, 1.0f );
									GL.Vertex3(pos);
								}
								//Console.WriteLine("hooooo");
								GL.End();
							}
						}
						else
						{
							Debug.Log("faces null");
						}

					}

				}
				if (mode == 0 && node.getComponent<MeshFilter>() != null)
					GL.PopName();

				if (node.children != null && mode == 0)
				{
					for (int i = 0; i < node.children.Count; i++)
					{
						if (node.children[i] != null)
							RecursiveRender(node.children[i], mode, GlobalTransformation, cam);
					}
				}
				GL.PopMatrix();
			}

		}