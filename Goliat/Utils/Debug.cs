﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;
using System.IO;

namespace Goliat
{
	public class Debug
	{
		public static String console = "";
		public static void Output(String str)
		{
			console += "\n";
			console += str + "\n";
			if (console.Length > 3000)
			{
				console.Remove(0, 500);
			}
		}
		private static List<Ray> _debugRays = new List<Ray>();
		public static List<Ray> debugRays
		{
			get
			{
				return ((List<Ray>)_debugRays.Clone());
			}
			private set
			{
				_debugRays = value;
			}
		}
		public static void DrawRay(Ray ray)
		{
			_debugRays.Add(ray);
		}

		public static void DrawLine(Vector3 p1, Vector3 p2)
		{
			_debugRays.Add(new Ray(p1, p1 - p2));
		}

		private static List<Vector4> _debugSpheres = new List<Vector4>();
		public static List<Vector4> debugSpheres
		{
			get
			{
				return ((List<Vector4>)_debugSpheres.Clone());
			}
			private set
			{
				_debugSpheres = value;
			}
		}
		public static void DrawSphere(Vector3 position, float radius)
		{
			debugSpheres.Add(new Vector4(position.x, position.y, position.z, radius));
		}
		public static void ClearDebug()
		{
			_debugRays = new List<Ray>();
			_debugSpheres = new List<Vector4>();
		}
		public static void Log(string filePath, string str)
		{
			StackTrace trace = new StackTrace(true);
			//for (int i = 0; i < trace.FrameCount; i++)
			//{
			// Note that high up the call stack, there is only
			// one stack frame.
			string console = "";
			try
			{
				if (trace.GetFrame(1) != null && trace.GetFrame(1).GetMethod() != null)
				{
					Type type = trace.GetFrame(1).GetMethod().DeclaringType;
					if (type.Assembly == Assembly.GetExecutingAssembly())
					{
						console += "\n";
						console += trace.GetFrame(1).GetFileName() + "; l";
						console += trace.GetFrame(1).GetFileLineNumber() + " - c";
						console += trace.GetFrame(1).GetFileColumnNumber() + " : ";
					}
				}
			}
			catch (Exception e)
			{
				;
			}
			File.AppendAllText(filePath, console + str + "\n");
		}
		public static void Log(String str)
		{

			StackTrace trace = new StackTrace(true);
			//for (int i = 0; i < trace.FrameCount; i++)
			//{
			// Note that high up the call stack, there is only
			// one stack frame.
			Type type = trace.GetFrame(1).GetMethod().DeclaringType;
			if (type.Assembly == Assembly.GetExecutingAssembly())
			{
				console += "\n";
				console += trace.GetFrame(1).GetFileName() + "; l";
				console += trace.GetFrame(1).GetFileLineNumber() + " - c";
				console += trace.GetFrame(1).GetFileColumnNumber() + " : ";
			}
			else
			{
				try
				{
					UserScript check = null;
					if (UserScript.scriptDictionary.TryGetValue(type.Assembly, out check))
					{
						console += "\n";
						console += check.scriptPath + "; l";
						console += trace.GetFrame(1).GetFileLineNumber() + " - c";
						console += trace.GetFrame(1).GetFileColumnNumber() + " : ";
					}
					else
					{
						console += "\n";
						console += trace.GetFrame(1).GetFileName() + "; l";
						console += trace.GetFrame(1).GetFileLineNumber() + " - c";
						console += trace.GetFrame(1).GetFileColumnNumber() + " : ";
					}
					//				UserScript check = UserScript.Find(type.Name);


				}
				catch
				{
					console += "\n";
					console += trace.GetFrame(1).GetFileName() + "; l";
					console += trace.GetFrame(1).GetFileLineNumber() + " - c";
					console += trace.GetFrame(1).GetFileColumnNumber() + " : ";
				}
				//}
			}
			//}

			//console += (StackTrace.GetFrame(1).GetMethod().Name);
			console += str + "\n";
			if (console.Length > 3000)
			{
				console.Remove(0, 500);
			}

		}
	}
}
