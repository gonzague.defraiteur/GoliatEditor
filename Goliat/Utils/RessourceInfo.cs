﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using TextureType = Assimp.TextureType;
using TextureSlot = Assimp.TextureSlot;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Color = Goliat.Color;
namespace Goliat
{
	[Goliat.Serializable]
	public class TextureInfo : RessourceInfo
	{
		//public TextureType textureType = TextureType.;
		public bool alphaFromGrayScale = false;
		public bool alphaIsTransparency = true;
		public TextureWrapMode wrapMode = TextureWrapMode.Repeat;
		public Range anisoLevel = new Range(1, 0, 16);
		public Bitmap imageBitmap = null;
		public override void reimport()
		{
			Texture tmp = Texture.FromInfo(this);
			tmp.guid = this.guid;

		}
	}

	[Goliat.Serializable]
	public class ShaderInfo : RessourceInfo
	{
		public Dictionary<string, object> shaderVariables = new Dictionary<string, object>();
		public string vertexShaderSource = "";
		public string fragmentShaderSource = "";
		public override void reimport()
		{
			Shader tmp = Shader.FromInfo(this);
			tmp.info = this;
			tmp.guid = this.guid;
			tmp.updateDictionary();
		}
	}

	[Goliat.Serializable]
	public class MaterialInfo : RessourceInfo
	{
		public List<ShaderInfo> shaders;
		//public Dictionary<string, object> shaderVariables = new Dictionary<string,object>();
		public Material material = null;
	}

	[Goliat.Serializable]
	public class RessourceInfo
	{
		public string guid = "";
		public string filePath = "";
		public virtual void reimport()
		{
			;
		}

	}



	[Goliat.Serializable]
	public class MeshImportInfo
	{
		public float scaleFactor = 0.01f;
		public MeshCompressionType meshCompression = MeshCompressionType.Off;
		public bool Read_Write_Enabled = false;
		public bool optimizeMesh = true;
		public bool importBlendShapes = true;
		public bool generateColliders = false;
		public bool generateLightMapUvs = false;
	}


	[Goliat.Serializable]
	public class NormalsAndTangentsInfo
	{
		public NormalImportType normals = NormalImportType.Import;
		public TangentImportType tangents = TangentImportType.Import;
		public bool splitTangents = true;
		public bool keepQuads = false;
	}


	[Goliat.Serializable]
	public class MeshMaterialInfo
	{
		public bool importMaterials = true;
		public MaterialNamingType materialNaming = MaterialNamingType.ByBaseTextureName;
		public MaterialSearchType materialSearch = MaterialSearchType.RecursiveUp;
	}

	/*[Goliat.Serializable]
	public class MeshInfo : RessourceInfo
	{
		private List<int> hierarchyPath = new List<int>();
		private int siblingIndex = 0;
		public Mesh mesh = null;
		public void update()
		{
			if (this.mesh != null)
				this.mesh.updateDictionary();
			else
				this.reimport();
		}
		public override void reimport()
		{
			mesh = Mesh.ImportFromInfo(this, hierarchyPath, siblingIndex);
		}
	}*/

	[Goliat.Serializable]
	public class SceneInfo : RessourceInfo
	{
		public List<MeshInfo> meshs = new List<MeshInfo>();
		public List<MaterialInfo> materials = new List<MaterialInfo>();
		public List<TextureInfo> textures = new List<TextureInfo>();
		public List<SceneInfo> nodes = new List<SceneInfo>();
		public override void reimport()
		{
			for (int i = 0; i < meshs.Count; i++)
			{
				;	//meshs[i].reimport();
			}
			for (int i = 0; i < materials.Count; i++)
			{
				materials[i].reimport();
			}
			for (int i = 0; i < textures.Count; i++)
			{
				textures[i].reimport();
			}
			for (int i = 0; i < nodes.Count; i++)
			{
				nodes[i].reimport();
			}
		}
	}

	[Goliat.Serializable]
	public class ImportInfo : RessourceInfo
	{

		public MeshImportInfo meshes = new MeshImportInfo();
		public NormalsAndTangentsInfo normalsANDtangents = new NormalsAndTangentsInfo();
		public MeshMaterialInfo materials = new MeshMaterialInfo();
	}

}
