﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Goliat
{



	public class TransformCollection
	{
		private List<Transform> _children;
		private Transform _container;
		public int Count
		{
			get
			{
				if (this._children == null)
				{
					this._children = new List<Transform>();
				}
				return (_children.Count);
			}
		}
		public Transform this[int index]
		{
			get
			{
				return (this._children[index]);
			}
			set
			{
				this._children[index].parent = null;
				this._children[index] = value;
				this._children[index].parent = this._container;
			}
		}
		public void Add(Transform tr)
		{
			this._children.Add(tr);
			if (tr.parent != _container)
				tr.parent = _container;
		}
		public void Add(Transform tr, bool keepTransform)
		{
			this._children.Add(tr);
			if (tr.parent != _container)
			{
				tr.Attach(_container, keepTransform);
			}
		}
		public bool Contains(Transform tr)
		{
			for (int i = 0; i < this._children.Count; i++)
			{
				if (this._children[i] == tr)
				{
					return (true);
				}
			}
			return (false);
		}
		public void Remove(Transform tr)
		{

			if (this.Contains(tr))
			{
				tr.parent = null;
			}
			this._children.Remove(tr);
		}
		public void RemoveOnly(Transform tr)
		{
			this._children.Remove(tr);
		}
		public TransformCollection(Transform container)
		{
			this._children = new List<Transform>();
			this._container = container;
			container.children = this;
		}
		public TransformCollection()
		{
			this._children = new List<Transform>();
			this._container = null;
		}
	}
}
