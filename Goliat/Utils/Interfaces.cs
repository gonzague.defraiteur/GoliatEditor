﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goliat
{
	
	public interface IRestorable
	{
		void OnSave();
		void OnRestore();
	}

	public interface IRegenerable
	{
		void OnClose(Goliat.Serialization.Serializer.SerializationInfo info);
		void OnRegen(Goliat.Serialization.Serializer.SerializationInfo info);
	}

	public interface ISerializable
	{
		void OnSerialize(Goliat.Serialization.Serializer.SerializationInfo info);
		void OnDeserialize(Goliat.Serialization.Serializer.SerializationInfo info);
	}
	public interface IEditable
	{
		void OnInspectorGUI();
	}
}
