﻿namespace Goliat
{

	[System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct)]
	public sealed class Serializable : System.Attribute
	{

	}


	[System.AttributeUsage(System.AttributeTargets.Field | System.AttributeTargets.Property)]
	public sealed class Serialized : System.Attribute
	{

	}

	[System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct | System.AttributeTargets.Field | System.AttributeTargets.Property)]
	public sealed class HideInInspector : System.Attribute
	{

	}

	[System.AttributeUsage(System.AttributeTargets.Field | System.AttributeTargets.Property)]
	public sealed class NonSerialized : System.Attribute
	{

	}

}