using System;
using System.Xml;
using System.Collections.Generic;

public class ColladaDocument
{
	public static ColladaDocument ImportFromFile(string fileName)
	{
		XmlDocument doc = new XmlDocument();
		doc.Load(fileName);
		var res = new ColladaDocument(doc);
		return (res);
	}
	public override string ToString()
	{
		return (this.Text);
	}
	public ColladaDocument(XmlDocument node)
	{
		this.Text = node.InnerText;
		if (node.ChildNodes != null)
		{
			foreach (XmlNode subNode in node.ChildNodes)
			{
				if (subNode.Name == "xml")
				{
					if (this.xmlElementList == null)
					{
						this.xmlElementList = new List<XmlElement>();
					}
					this.xmlElementList.Add(new XmlElement(subNode));
				}
				else if (subNode.Name == "COLLADA")
				{
					if (this.COLLADAElementList == null)
					{
						this.COLLADAElementList = new List<COLLADAElement>();
					}
					this.COLLADAElementList.Add(new COLLADAElement(subNode));
				}
				if (subNode.Name == "#text")
				{
					this.Text = subNode.Value;
				}
			}
		}
	}
	public string Text = "";
	public List<XmlElement> xmlElementList = null;
	public XmlElement xml
	{
		get
		{
			if (xmlElementList == null || xmlElementList.Count == 0)
				return(null);
			return (xmlElementList[0]);
		}
	}
	public List<COLLADAElement> COLLADAElementList = null;
	public COLLADAElement COLLADA
	{
		get
		{
			if (COLLADAElementList == null || COLLADAElementList.Count == 0)
				return(null);
			return (COLLADAElementList[0]);
		}
	}
	public class XmlElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public XmlElement(XmlNode node)
		{
			this.Text = node.InnerText;
		}
		public string Text = "";
	}
	public class COLLADAElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public COLLADAElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "xmlns")
					{
						this.xmlns = new XmlnsAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "version")
					{
						this.version = new VersionAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "asset")
					{
						if (this.assetElementList == null)
						{
							this.assetElementList = new List<AssetElement>();
						}
						this.assetElementList.Add(new AssetElement(subNode));
					}
					else if (subNode.Name == "library_animations")
					{
						if (this.library_animationsElementList == null)
						{
							this.library_animationsElementList = new List<Library_animationsElement>();
						}
						this.library_animationsElementList.Add(new Library_animationsElement(subNode));
					}
					else if (subNode.Name == "library_animation_clips")
					{
						if (this.library_animation_clipsElementList == null)
						{
							this.library_animation_clipsElementList = new List<Library_animation_clipsElement>();
						}
						this.library_animation_clipsElementList.Add(new Library_animation_clipsElement(subNode));
					}
					else if (subNode.Name == "library_images")
					{
						if (this.library_imagesElementList == null)
						{
							this.library_imagesElementList = new List<Library_imagesElement>();
						}
						this.library_imagesElementList.Add(new Library_imagesElement(subNode));
					}
					else if (subNode.Name == "library_materials")
					{
						if (this.library_materialsElementList == null)
						{
							this.library_materialsElementList = new List<Library_materialsElement>();
						}
						this.library_materialsElementList.Add(new Library_materialsElement(subNode));
					}
					else if (subNode.Name == "library_effects")
					{
						if (this.library_effectsElementList == null)
						{
							this.library_effectsElementList = new List<Library_effectsElement>();
						}
						this.library_effectsElementList.Add(new Library_effectsElement(subNode));
					}
					else if (subNode.Name == "library_geometries")
					{
						if (this.library_geometriesElementList == null)
						{
							this.library_geometriesElementList = new List<Library_geometriesElement>();
						}
						this.library_geometriesElementList.Add(new Library_geometriesElement(subNode));
					}
					else if (subNode.Name == "library_controllers")
					{
						if (this.library_controllersElementList == null)
						{
							this.library_controllersElementList = new List<Library_controllersElement>();
						}
						this.library_controllersElementList.Add(new Library_controllersElement(subNode));
					}
					else if (subNode.Name == "library_visual_scenes")
					{
						if (this.library_visual_scenesElementList == null)
						{
							this.library_visual_scenesElementList = new List<Library_visual_scenesElement>();
						}
						this.library_visual_scenesElementList.Add(new Library_visual_scenesElement(subNode));
					}
					else if (subNode.Name == "scene")
					{
						if (this.sceneElementList == null)
						{
							this.sceneElementList = new List<SceneElement>();
						}
						this.sceneElementList.Add(new SceneElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public XmlnsAttr xmlns = null;
		public VersionAttr version = null;
		public List<AssetElement> assetElementList = null;
		public AssetElement asset
		{
			get
			{
				if (assetElementList == null || assetElementList.Count == 0)
					return(null);
				return (assetElementList[0]);
			}
		}
		public List<Library_animationsElement> library_animationsElementList = null;
		public Library_animationsElement library_animations
		{
			get
			{
				if (library_animationsElementList == null || library_animationsElementList.Count == 0)
					return(null);
				return (library_animationsElementList[0]);
			}
		}
		public List<Library_animation_clipsElement> library_animation_clipsElementList = null;
		public Library_animation_clipsElement library_animation_clips
		{
			get
			{
				if (library_animation_clipsElementList == null || library_animation_clipsElementList.Count == 0)
					return(null);
				return (library_animation_clipsElementList[0]);
			}
		}
		public List<Library_imagesElement> library_imagesElementList = null;
		public Library_imagesElement library_images
		{
			get
			{
				if (library_imagesElementList == null || library_imagesElementList.Count == 0)
					return(null);
				return (library_imagesElementList[0]);
			}
		}
		public List<Library_materialsElement> library_materialsElementList = null;
		public Library_materialsElement library_materials
		{
			get
			{
				if (library_materialsElementList == null || library_materialsElementList.Count == 0)
					return(null);
				return (library_materialsElementList[0]);
			}
		}
		public List<Library_effectsElement> library_effectsElementList = null;
		public Library_effectsElement library_effects
		{
			get
			{
				if (library_effectsElementList == null || library_effectsElementList.Count == 0)
					return(null);
				return (library_effectsElementList[0]);
			}
		}
		public List<Library_geometriesElement> library_geometriesElementList = null;
		public Library_geometriesElement library_geometries
		{
			get
			{
				if (library_geometriesElementList == null || library_geometriesElementList.Count == 0)
					return(null);
				return (library_geometriesElementList[0]);
			}
		}
		public List<Library_controllersElement> library_controllersElementList = null;
		public Library_controllersElement library_controllers
		{
			get
			{
				if (library_controllersElementList == null || library_controllersElementList.Count == 0)
					return(null);
				return (library_controllersElementList[0]);
			}
		}
		public List<Library_visual_scenesElement> library_visual_scenesElementList = null;
		public Library_visual_scenesElement library_visual_scenes
		{
			get
			{
				if (library_visual_scenesElementList == null || library_visual_scenesElementList.Count == 0)
					return(null);
				return (library_visual_scenesElementList[0]);
			}
		}
		public List<SceneElement> sceneElementList = null;
		public SceneElement scene
		{
			get
			{
				if (sceneElementList == null || sceneElementList.Count == 0)
					return(null);
				return (sceneElementList[0]);
			}
		}
	}
	public class XmlnsAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public XmlnsAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class VersionAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public VersionAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class AssetElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public AssetElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "contributor")
					{
						if (this.contributorElementList == null)
						{
							this.contributorElementList = new List<ContributorElement>();
						}
						this.contributorElementList.Add(new ContributorElement(subNode));
					}
					else if (subNode.Name == "created")
					{
						if (this.createdElementList == null)
						{
							this.createdElementList = new List<CreatedElement>();
						}
						this.createdElementList.Add(new CreatedElement(subNode));
					}
					else if (subNode.Name == "modified")
					{
						if (this.modifiedElementList == null)
						{
							this.modifiedElementList = new List<ModifiedElement>();
						}
						this.modifiedElementList.Add(new ModifiedElement(subNode));
					}
					else if (subNode.Name == "unit")
					{
						if (this.unitElementList == null)
						{
							this.unitElementList = new List<UnitElement>();
						}
						this.unitElementList.Add(new UnitElement(subNode));
					}
					else if (subNode.Name == "up_axis")
					{
						if (this.up_axisElementList == null)
						{
							this.up_axisElementList = new List<Up_axisElement>();
						}
						this.up_axisElementList.Add(new Up_axisElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<ContributorElement> contributorElementList = null;
		public ContributorElement contributor
		{
			get
			{
				if (contributorElementList == null || contributorElementList.Count == 0)
					return(null);
				return (contributorElementList[0]);
			}
		}
		public List<CreatedElement> createdElementList = null;
		public CreatedElement created
		{
			get
			{
				if (createdElementList == null || createdElementList.Count == 0)
					return(null);
				return (createdElementList[0]);
			}
		}
		public List<ModifiedElement> modifiedElementList = null;
		public ModifiedElement modified
		{
			get
			{
				if (modifiedElementList == null || modifiedElementList.Count == 0)
					return(null);
				return (modifiedElementList[0]);
			}
		}
		public List<UnitElement> unitElementList = null;
		public UnitElement unit
		{
			get
			{
				if (unitElementList == null || unitElementList.Count == 0)
					return(null);
				return (unitElementList[0]);
			}
		}
		public List<Up_axisElement> up_axisElementList = null;
		public Up_axisElement up_axis
		{
			get
			{
				if (up_axisElementList == null || up_axisElementList.Count == 0)
					return(null);
				return (up_axisElementList[0]);
			}
		}
	}
	public class ContributorElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ContributorElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "author")
					{
						if (this.authorElementList == null)
						{
							this.authorElementList = new List<AuthorElement>();
						}
						this.authorElementList.Add(new AuthorElement(subNode));
					}
					else if (subNode.Name == "authoring_tool")
					{
						if (this.authoring_toolElementList == null)
						{
							this.authoring_toolElementList = new List<Authoring_toolElement>();
						}
						this.authoring_toolElementList.Add(new Authoring_toolElement(subNode));
					}
					else if (subNode.Name == "comments")
					{
						if (this.commentsElementList == null)
						{
							this.commentsElementList = new List<CommentsElement>();
						}
						this.commentsElementList.Add(new CommentsElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<AuthorElement> authorElementList = null;
		public AuthorElement author
		{
			get
			{
				if (authorElementList == null || authorElementList.Count == 0)
					return(null);
				return (authorElementList[0]);
			}
		}
		public List<Authoring_toolElement> authoring_toolElementList = null;
		public Authoring_toolElement authoring_tool
		{
			get
			{
				if (authoring_toolElementList == null || authoring_toolElementList.Count == 0)
					return(null);
				return (authoring_toolElementList[0]);
			}
		}
		public List<CommentsElement> commentsElementList = null;
		public CommentsElement comments
		{
			get
			{
				if (commentsElementList == null || commentsElementList.Count == 0)
					return(null);
				return (commentsElementList[0]);
			}
		}
	}
	public class AuthorElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public AuthorElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Authoring_toolElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Authoring_toolElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class CommentsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public CommentsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class CreatedElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public CreatedElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class ModifiedElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ModifiedElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class UnitElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public UnitElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "meter")
					{
						this.meter = new MeterAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public MeterAttr meter = null;
		public NameAttr name = null;
	}
	public class MeterAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public MeterAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class NameAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public NameAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Up_axisElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Up_axisElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Library_animationsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_animationsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "animation")
					{
						if (this.animationElementList == null)
						{
							this.animationElementList = new List<AnimationElement>();
						}
						this.animationElementList.Add(new AnimationElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<AnimationElement> animationElementList = null;
		public AnimationElement animation
		{
			get
			{
				if (animationElementList == null || animationElementList.Count == 0)
					return(null);
				return (animationElementList[0]);
			}
		}
	}
	public class AnimationElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public AnimationElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "source")
					{
						if (this.sourceElementList == null)
						{
							this.sourceElementList = new List<SourceElement>();
						}
						this.sourceElementList.Add(new SourceElement(subNode));
					}
					else if (subNode.Name == "sampler")
					{
						if (this.samplerElementList == null)
						{
							this.samplerElementList = new List<SamplerElement>();
						}
						this.samplerElementList.Add(new SamplerElement(subNode));
					}
					else if (subNode.Name == "channel")
					{
						if (this.channelElementList == null)
						{
							this.channelElementList = new List<ChannelElement>();
						}
						this.channelElementList.Add(new ChannelElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public List<SourceElement> sourceElementList = null;
		public SourceElement source
		{
			get
			{
				if (sourceElementList == null || sourceElementList.Count == 0)
					return(null);
				return (sourceElementList[0]);
			}
		}
		public List<SamplerElement> samplerElementList = null;
		public SamplerElement sampler
		{
			get
			{
				if (samplerElementList == null || samplerElementList.Count == 0)
					return(null);
				return (samplerElementList[0]);
			}
		}
		public List<ChannelElement> channelElementList = null;
		public ChannelElement channel
		{
			get
			{
				if (channelElementList == null || channelElementList.Count == 0)
					return(null);
				return (channelElementList[0]);
			}
		}
	}
	public class IdAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public IdAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class SourceElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SourceElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float_array")
					{
						if (this.float_arrayElementList == null)
						{
							this.float_arrayElementList = new List<Float_arrayElement>();
						}
						this.float_arrayElementList.Add(new Float_arrayElement(subNode));
					}
					else if (subNode.Name == "technique_common")
					{
						if (this.technique_commonElementList == null)
						{
							this.technique_commonElementList = new List<Technique_commonElement>();
						}
						this.technique_commonElementList.Add(new Technique_commonElement(subNode));
					}
					else if (subNode.Name == "technique")
					{
						if (this.techniqueElementList == null)
						{
							this.techniqueElementList = new List<TechniqueElement>();
						}
						this.techniqueElementList.Add(new TechniqueElement(subNode));
					}
					else if (subNode.Name == "Name_array")
					{
						if (this.Name_arrayElementList == null)
						{
							this.Name_arrayElementList = new List<Name_arrayElement>();
						}
						this.Name_arrayElementList.Add(new Name_arrayElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public List<Float_arrayElement> float_arrayElementList = null;
		public Float_arrayElement float_array
		{
			get
			{
				if (float_arrayElementList == null || float_arrayElementList.Count == 0)
					return(null);
				return (float_arrayElementList[0]);
			}
		}
		public List<Technique_commonElement> technique_commonElementList = null;
		public Technique_commonElement technique_common
		{
			get
			{
				if (technique_commonElementList == null || technique_commonElementList.Count == 0)
					return(null);
				return (technique_commonElementList[0]);
			}
		}
		public List<TechniqueElement> techniqueElementList = null;
		public TechniqueElement technique
		{
			get
			{
				if (techniqueElementList == null || techniqueElementList.Count == 0)
					return(null);
				return (techniqueElementList[0]);
			}
		}
		public List<Name_arrayElement> Name_arrayElementList = null;
		public Name_arrayElement Name_array
		{
			get
			{
				if (Name_arrayElementList == null || Name_arrayElementList.Count == 0)
					return(null);
				return (Name_arrayElementList[0]);
			}
		}
	}
	public class Float_arrayElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Float_arrayElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "count")
					{
						this.count = new CountAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public CountAttr count = null;
	}
	public class CountAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public CountAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Technique_commonElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Technique_commonElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "accessor")
					{
						if (this.accessorElementList == null)
						{
							this.accessorElementList = new List<AccessorElement>();
						}
						this.accessorElementList.Add(new AccessorElement(subNode));
					}
					else if (subNode.Name == "instance_material")
					{
						if (this.instance_materialElementList == null)
						{
							this.instance_materialElementList = new List<Instance_materialElement>();
						}
						this.instance_materialElementList.Add(new Instance_materialElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<AccessorElement> accessorElementList = null;
		public AccessorElement accessor
		{
			get
			{
				if (accessorElementList == null || accessorElementList.Count == 0)
					return(null);
				return (accessorElementList[0]);
			}
		}
		public List<Instance_materialElement> instance_materialElementList = null;
		public Instance_materialElement instance_material
		{
			get
			{
				if (instance_materialElementList == null || instance_materialElementList.Count == 0)
					return(null);
				return (instance_materialElementList[0]);
			}
		}
	}
	public class AccessorElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public AccessorElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "source")
					{
						this._source = new _sourceAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "count")
					{
						this.count = new CountAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "stride")
					{
						this.stride = new StrideAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "param")
					{
						if (this.paramElementList == null)
						{
							this.paramElementList = new List<ParamElement>();
						}
						this.paramElementList.Add(new ParamElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public _sourceAttr _source = null;
		public CountAttr count = null;
		public StrideAttr stride = null;
		public List<ParamElement> paramElementList = null;
		public ParamElement param
		{
			get
			{
				if (paramElementList == null || paramElementList.Count == 0)
					return(null);
				return (paramElementList[0]);
			}
		}
	}
	public class _sourceAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public _sourceAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class StrideAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public StrideAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class ParamElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ParamElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "type")
					{
						this.type = new TypeAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public NameAttr name = null;
		public TypeAttr type = null;
	}
	public class TypeAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public TypeAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Instance_materialElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Instance_materialElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "symbol")
					{
						this.symbol = new SymbolAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "target")
					{
						this.target = new TargetAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "bind_vertex_input")
					{
						if (this.bind_vertex_inputElementList == null)
						{
							this.bind_vertex_inputElementList = new List<Bind_vertex_inputElement>();
						}
						this.bind_vertex_inputElementList.Add(new Bind_vertex_inputElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public SymbolAttr symbol = null;
		public TargetAttr target = null;
		public List<Bind_vertex_inputElement> bind_vertex_inputElementList = null;
		public Bind_vertex_inputElement bind_vertex_input
		{
			get
			{
				if (bind_vertex_inputElementList == null || bind_vertex_inputElementList.Count == 0)
					return(null);
				return (bind_vertex_inputElementList[0]);
			}
		}
	}
	public class SymbolAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public SymbolAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class TargetAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public TargetAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Bind_vertex_inputElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Bind_vertex_inputElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "semantic")
					{
						this.semantic = new SemanticAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "input_semantic")
					{
						this.input_semantic = new Input_semanticAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "input_set")
					{
						this.input_set = new Input_setAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public SemanticAttr semantic = null;
		public Input_semanticAttr input_semantic = null;
		public Input_setAttr input_set = null;
	}
	public class SemanticAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public SemanticAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Input_semanticAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public Input_semanticAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Input_setAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public Input_setAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class TechniqueElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public TechniqueElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "profile")
					{
						this.profile = new ProfileAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "sid")
					{
						this.sid = new SidAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "pre_infinity")
					{
						if (this.pre_infinityElementList == null)
						{
							this.pre_infinityElementList = new List<Pre_infinityElement>();
						}
						this.pre_infinityElementList.Add(new Pre_infinityElement(subNode));
					}
					else if (subNode.Name == "post_infinity")
					{
						if (this.post_infinityElementList == null)
						{
							this.post_infinityElementList = new List<Post_infinityElement>();
						}
						this.post_infinityElementList.Add(new Post_infinityElement(subNode));
					}
					else if (subNode.Name == "blinn")
					{
						if (this.blinnElementList == null)
						{
							this.blinnElementList = new List<BlinnElement>();
						}
						this.blinnElementList.Add(new BlinnElement(subNode));
					}
					else if (subNode.Name == "mirrorU")
					{
						if (this.mirrorUElementList == null)
						{
							this.mirrorUElementList = new List<MirrorUElement>();
						}
						this.mirrorUElementList.Add(new MirrorUElement(subNode));
					}
					else if (subNode.Name == "mirrorV")
					{
						if (this.mirrorVElementList == null)
						{
							this.mirrorVElementList = new List<MirrorVElement>();
						}
						this.mirrorVElementList.Add(new MirrorVElement(subNode));
					}
					else if (subNode.Name == "wrapU")
					{
						if (this.wrapUElementList == null)
						{
							this.wrapUElementList = new List<WrapUElement>();
						}
						this.wrapUElementList.Add(new WrapUElement(subNode));
					}
					else if (subNode.Name == "wrapV")
					{
						if (this.wrapVElementList == null)
						{
							this.wrapVElementList = new List<WrapVElement>();
						}
						this.wrapVElementList.Add(new WrapVElement(subNode));
					}
					else if (subNode.Name == "repeatU")
					{
						if (this.repeatUElementList == null)
						{
							this.repeatUElementList = new List<RepeatUElement>();
						}
						this.repeatUElementList.Add(new RepeatUElement(subNode));
					}
					else if (subNode.Name == "repeatV")
					{
						if (this.repeatVElementList == null)
						{
							this.repeatVElementList = new List<RepeatVElement>();
						}
						this.repeatVElementList.Add(new RepeatVElement(subNode));
					}
					else if (subNode.Name == "offsetU")
					{
						if (this.offsetUElementList == null)
						{
							this.offsetUElementList = new List<OffsetUElement>();
						}
						this.offsetUElementList.Add(new OffsetUElement(subNode));
					}
					else if (subNode.Name == "offsetV")
					{
						if (this.offsetVElementList == null)
						{
							this.offsetVElementList = new List<OffsetVElement>();
						}
						this.offsetVElementList.Add(new OffsetVElement(subNode));
					}
					else if (subNode.Name == "amount")
					{
						if (this.amountElementList == null)
						{
							this.amountElementList = new List<AmountElement>();
						}
						this.amountElementList.Add(new AmountElement(subNode));
					}
					else if (subNode.Name == "extra")
					{
						if (this.extraElementList == null)
						{
							this.extraElementList = new List<ExtraElement>();
						}
						this.extraElementList.Add(new ExtraElement(subNode));
					}
					else if (subNode.Name == "spec_level")
					{
						if (this.spec_levelElementList == null)
						{
							this.spec_levelElementList = new List<Spec_levelElement>();
						}
						this.spec_levelElementList.Add(new Spec_levelElement(subNode));
					}
					else if (subNode.Name == "emission_level")
					{
						if (this.emission_levelElementList == null)
						{
							this.emission_levelElementList = new List<Emission_levelElement>();
						}
						this.emission_levelElementList.Add(new Emission_levelElement(subNode));
					}
					else if (subNode.Name == "faceted")
					{
						if (this.facetedElementList == null)
						{
							this.facetedElementList = new List<FacetedElement>();
						}
						this.facetedElementList.Add(new FacetedElement(subNode));
					}
					else if (subNode.Name == "double_sided")
					{
						if (this.double_sidedElementList == null)
						{
							this.double_sidedElementList = new List<Double_sidedElement>();
						}
						this.double_sidedElementList.Add(new Double_sidedElement(subNode));
					}
					else if (subNode.Name == "wireframe")
					{
						if (this.wireframeElementList == null)
						{
							this.wireframeElementList = new List<WireframeElement>();
						}
						this.wireframeElementList.Add(new WireframeElement(subNode));
					}
					else if (subNode.Name == "face_map")
					{
						if (this.face_mapElementList == null)
						{
							this.face_mapElementList = new List<Face_mapElement>();
						}
						this.face_mapElementList.Add(new Face_mapElement(subNode));
					}
					else if (subNode.Name == "helper")
					{
						if (this.helperElementList == null)
						{
							this.helperElementList = new List<HelperElement>();
						}
						this.helperElementList.Add(new HelperElement(subNode));
					}
					else if (subNode.Name == "start_time")
					{
						if (this.start_timeElementList == null)
						{
							this.start_timeElementList = new List<Start_timeElement>();
						}
						this.start_timeElementList.Add(new Start_timeElement(subNode));
					}
					else if (subNode.Name == "end_time")
					{
						if (this.end_timeElementList == null)
						{
							this.end_timeElementList = new List<End_timeElement>();
						}
						this.end_timeElementList.Add(new End_timeElement(subNode));
					}
					else if (subNode.Name == "frame_rate")
					{
						if (this.frame_rateElementList == null)
						{
							this.frame_rateElementList = new List<Frame_rateElement>();
						}
						this.frame_rateElementList.Add(new Frame_rateElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public ProfileAttr profile = null;
		public SidAttr sid = null;
		public List<Pre_infinityElement> pre_infinityElementList = null;
		public Pre_infinityElement pre_infinity
		{
			get
			{
				if (pre_infinityElementList == null || pre_infinityElementList.Count == 0)
					return(null);
				return (pre_infinityElementList[0]);
			}
		}
		public List<Post_infinityElement> post_infinityElementList = null;
		public Post_infinityElement post_infinity
		{
			get
			{
				if (post_infinityElementList == null || post_infinityElementList.Count == 0)
					return(null);
				return (post_infinityElementList[0]);
			}
		}
		public List<BlinnElement> blinnElementList = null;
		public BlinnElement blinn
		{
			get
			{
				if (blinnElementList == null || blinnElementList.Count == 0)
					return(null);
				return (blinnElementList[0]);
			}
		}
		public List<MirrorUElement> mirrorUElementList = null;
		public MirrorUElement mirrorU
		{
			get
			{
				if (mirrorUElementList == null || mirrorUElementList.Count == 0)
					return(null);
				return (mirrorUElementList[0]);
			}
		}
		public List<MirrorVElement> mirrorVElementList = null;
		public MirrorVElement mirrorV
		{
			get
			{
				if (mirrorVElementList == null || mirrorVElementList.Count == 0)
					return(null);
				return (mirrorVElementList[0]);
			}
		}
		public List<WrapUElement> wrapUElementList = null;
		public WrapUElement wrapU
		{
			get
			{
				if (wrapUElementList == null || wrapUElementList.Count == 0)
					return(null);
				return (wrapUElementList[0]);
			}
		}
		public List<WrapVElement> wrapVElementList = null;
		public WrapVElement wrapV
		{
			get
			{
				if (wrapVElementList == null || wrapVElementList.Count == 0)
					return(null);
				return (wrapVElementList[0]);
			}
		}
		public List<RepeatUElement> repeatUElementList = null;
		public RepeatUElement repeatU
		{
			get
			{
				if (repeatUElementList == null || repeatUElementList.Count == 0)
					return(null);
				return (repeatUElementList[0]);
			}
		}
		public List<RepeatVElement> repeatVElementList = null;
		public RepeatVElement repeatV
		{
			get
			{
				if (repeatVElementList == null || repeatVElementList.Count == 0)
					return(null);
				return (repeatVElementList[0]);
			}
		}
		public List<OffsetUElement> offsetUElementList = null;
		public OffsetUElement offsetU
		{
			get
			{
				if (offsetUElementList == null || offsetUElementList.Count == 0)
					return(null);
				return (offsetUElementList[0]);
			}
		}
		public List<OffsetVElement> offsetVElementList = null;
		public OffsetVElement offsetV
		{
			get
			{
				if (offsetVElementList == null || offsetVElementList.Count == 0)
					return(null);
				return (offsetVElementList[0]);
			}
		}
		public List<AmountElement> amountElementList = null;
		public AmountElement amount
		{
			get
			{
				if (amountElementList == null || amountElementList.Count == 0)
					return(null);
				return (amountElementList[0]);
			}
		}
		public List<ExtraElement> extraElementList = null;
		public ExtraElement extra
		{
			get
			{
				if (extraElementList == null || extraElementList.Count == 0)
					return(null);
				return (extraElementList[0]);
			}
		}
		public List<Spec_levelElement> spec_levelElementList = null;
		public Spec_levelElement spec_level
		{
			get
			{
				if (spec_levelElementList == null || spec_levelElementList.Count == 0)
					return(null);
				return (spec_levelElementList[0]);
			}
		}
		public List<Emission_levelElement> emission_levelElementList = null;
		public Emission_levelElement emission_level
		{
			get
			{
				if (emission_levelElementList == null || emission_levelElementList.Count == 0)
					return(null);
				return (emission_levelElementList[0]);
			}
		}
		public List<FacetedElement> facetedElementList = null;
		public FacetedElement faceted
		{
			get
			{
				if (facetedElementList == null || facetedElementList.Count == 0)
					return(null);
				return (facetedElementList[0]);
			}
		}
		public List<Double_sidedElement> double_sidedElementList = null;
		public Double_sidedElement double_sided
		{
			get
			{
				if (double_sidedElementList == null || double_sidedElementList.Count == 0)
					return(null);
				return (double_sidedElementList[0]);
			}
		}
		public List<WireframeElement> wireframeElementList = null;
		public WireframeElement wireframe
		{
			get
			{
				if (wireframeElementList == null || wireframeElementList.Count == 0)
					return(null);
				return (wireframeElementList[0]);
			}
		}
		public List<Face_mapElement> face_mapElementList = null;
		public Face_mapElement face_map
		{
			get
			{
				if (face_mapElementList == null || face_mapElementList.Count == 0)
					return(null);
				return (face_mapElementList[0]);
			}
		}
		public List<HelperElement> helperElementList = null;
		public HelperElement helper
		{
			get
			{
				if (helperElementList == null || helperElementList.Count == 0)
					return(null);
				return (helperElementList[0]);
			}
		}
		public List<Start_timeElement> start_timeElementList = null;
		public Start_timeElement start_time
		{
			get
			{
				if (start_timeElementList == null || start_timeElementList.Count == 0)
					return(null);
				return (start_timeElementList[0]);
			}
		}
		public List<End_timeElement> end_timeElementList = null;
		public End_timeElement end_time
		{
			get
			{
				if (end_timeElementList == null || end_timeElementList.Count == 0)
					return(null);
				return (end_timeElementList[0]);
			}
		}
		public List<Frame_rateElement> frame_rateElementList = null;
		public Frame_rateElement frame_rate
		{
			get
			{
				if (frame_rateElementList == null || frame_rateElementList.Count == 0)
					return(null);
				return (frame_rateElementList[0]);
			}
		}
	}
	public class ProfileAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public ProfileAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class SidAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public SidAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Pre_infinityElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Pre_infinityElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Post_infinityElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Post_infinityElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class BlinnElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public BlinnElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "ambient")
					{
						if (this.ambientElementList == null)
						{
							this.ambientElementList = new List<AmbientElement>();
						}
						this.ambientElementList.Add(new AmbientElement(subNode));
					}
					else if (subNode.Name == "diffuse")
					{
						if (this.diffuseElementList == null)
						{
							this.diffuseElementList = new List<DiffuseElement>();
						}
						this.diffuseElementList.Add(new DiffuseElement(subNode));
					}
					else if (subNode.Name == "specular")
					{
						if (this.specularElementList == null)
						{
							this.specularElementList = new List<SpecularElement>();
						}
						this.specularElementList.Add(new SpecularElement(subNode));
					}
					else if (subNode.Name == "shininess")
					{
						if (this.shininessElementList == null)
						{
							this.shininessElementList = new List<ShininessElement>();
						}
						this.shininessElementList.Add(new ShininessElement(subNode));
					}
					else if (subNode.Name == "reflective")
					{
						if (this.reflectiveElementList == null)
						{
							this.reflectiveElementList = new List<ReflectiveElement>();
						}
						this.reflectiveElementList.Add(new ReflectiveElement(subNode));
					}
					else if (subNode.Name == "reflectivity")
					{
						if (this.reflectivityElementList == null)
						{
							this.reflectivityElementList = new List<ReflectivityElement>();
						}
						this.reflectivityElementList.Add(new ReflectivityElement(subNode));
					}
					else if (subNode.Name == "transparent")
					{
						if (this.transparentElementList == null)
						{
							this.transparentElementList = new List<TransparentElement>();
						}
						this.transparentElementList.Add(new TransparentElement(subNode));
					}
					else if (subNode.Name == "transparency")
					{
						if (this.transparencyElementList == null)
						{
							this.transparencyElementList = new List<TransparencyElement>();
						}
						this.transparencyElementList.Add(new TransparencyElement(subNode));
					}
					else if (subNode.Name == "index_of_refraction")
					{
						if (this.index_of_refractionElementList == null)
						{
							this.index_of_refractionElementList = new List<Index_of_refractionElement>();
						}
						this.index_of_refractionElementList.Add(new Index_of_refractionElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<AmbientElement> ambientElementList = null;
		public AmbientElement ambient
		{
			get
			{
				if (ambientElementList == null || ambientElementList.Count == 0)
					return(null);
				return (ambientElementList[0]);
			}
		}
		public List<DiffuseElement> diffuseElementList = null;
		public DiffuseElement diffuse
		{
			get
			{
				if (diffuseElementList == null || diffuseElementList.Count == 0)
					return(null);
				return (diffuseElementList[0]);
			}
		}
		public List<SpecularElement> specularElementList = null;
		public SpecularElement specular
		{
			get
			{
				if (specularElementList == null || specularElementList.Count == 0)
					return(null);
				return (specularElementList[0]);
			}
		}
		public List<ShininessElement> shininessElementList = null;
		public ShininessElement shininess
		{
			get
			{
				if (shininessElementList == null || shininessElementList.Count == 0)
					return(null);
				return (shininessElementList[0]);
			}
		}
		public List<ReflectiveElement> reflectiveElementList = null;
		public ReflectiveElement reflective
		{
			get
			{
				if (reflectiveElementList == null || reflectiveElementList.Count == 0)
					return(null);
				return (reflectiveElementList[0]);
			}
		}
		public List<ReflectivityElement> reflectivityElementList = null;
		public ReflectivityElement reflectivity
		{
			get
			{
				if (reflectivityElementList == null || reflectivityElementList.Count == 0)
					return(null);
				return (reflectivityElementList[0]);
			}
		}
		public List<TransparentElement> transparentElementList = null;
		public TransparentElement transparent
		{
			get
			{
				if (transparentElementList == null || transparentElementList.Count == 0)
					return(null);
				return (transparentElementList[0]);
			}
		}
		public List<TransparencyElement> transparencyElementList = null;
		public TransparencyElement transparency
		{
			get
			{
				if (transparencyElementList == null || transparencyElementList.Count == 0)
					return(null);
				return (transparencyElementList[0]);
			}
		}
		public List<Index_of_refractionElement> index_of_refractionElementList = null;
		public Index_of_refractionElement index_of_refraction
		{
			get
			{
				if (index_of_refractionElementList == null || index_of_refractionElementList.Count == 0)
					return(null);
				return (index_of_refractionElementList[0]);
			}
		}
	}
	public class AmbientElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public AmbientElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "color")
					{
						if (this.colorElementList == null)
						{
							this.colorElementList = new List<ColorElement>();
						}
						this.colorElementList.Add(new ColorElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<ColorElement> colorElementList = null;
		public ColorElement color
		{
			get
			{
				if (colorElementList == null || colorElementList.Count == 0)
					return(null);
				return (colorElementList[0]);
			}
		}
	}
	public class ColorElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ColorElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class DiffuseElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public DiffuseElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "texture")
					{
						if (this.textureElementList == null)
						{
							this.textureElementList = new List<TextureElement>();
						}
						this.textureElementList.Add(new TextureElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<TextureElement> textureElementList = null;
		public TextureElement texture
		{
			get
			{
				if (textureElementList == null || textureElementList.Count == 0)
					return(null);
				return (textureElementList[0]);
			}
		}
	}
	public class TextureElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public TextureElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "texture")
					{
						this._texture = new _textureAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "texcoord")
					{
						this.texcoord = new TexcoordAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "extra")
					{
						if (this.extraElementList == null)
						{
							this.extraElementList = new List<ExtraElement>();
						}
						this.extraElementList.Add(new ExtraElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public _textureAttr _texture = null;
		public TexcoordAttr texcoord = null;
		public List<ExtraElement> extraElementList = null;
		public ExtraElement extra
		{
			get
			{
				if (extraElementList == null || extraElementList.Count == 0)
					return(null);
				return (extraElementList[0]);
			}
		}
	}
	public class _textureAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public _textureAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class TexcoordAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public TexcoordAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class ExtraElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ExtraElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "technique")
					{
						if (this.techniqueElementList == null)
						{
							this.techniqueElementList = new List<TechniqueElement>();
						}
						this.techniqueElementList.Add(new TechniqueElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<TechniqueElement> techniqueElementList = null;
		public TechniqueElement technique
		{
			get
			{
				if (techniqueElementList == null || techniqueElementList.Count == 0)
					return(null);
				return (techniqueElementList[0]);
			}
		}
	}
	public class SpecularElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SpecularElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "color")
					{
						if (this.colorElementList == null)
						{
							this.colorElementList = new List<ColorElement>();
						}
						this.colorElementList.Add(new ColorElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<ColorElement> colorElementList = null;
		public ColorElement color
		{
			get
			{
				if (colorElementList == null || colorElementList.Count == 0)
					return(null);
				return (colorElementList[0]);
			}
		}
	}
	public class ShininessElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ShininessElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float")
					{
						if (this._floatElementList == null)
						{
							this._floatElementList = new List<_floatElement>();
						}
						this._floatElementList.Add(new _floatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<_floatElement> _floatElementList = null;
		public _floatElement _float
		{
			get
			{
				if (_floatElementList == null || _floatElementList.Count == 0)
					return(null);
				return (_floatElementList[0]);
			}
		}
	}
	public class _floatElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public _floatElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class ReflectiveElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ReflectiveElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "color")
					{
						if (this.colorElementList == null)
						{
							this.colorElementList = new List<ColorElement>();
						}
						this.colorElementList.Add(new ColorElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<ColorElement> colorElementList = null;
		public ColorElement color
		{
			get
			{
				if (colorElementList == null || colorElementList.Count == 0)
					return(null);
				return (colorElementList[0]);
			}
		}
	}
	public class ReflectivityElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ReflectivityElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float")
					{
						if (this._floatElementList == null)
						{
							this._floatElementList = new List<_floatElement>();
						}
						this._floatElementList.Add(new _floatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<_floatElement> _floatElementList = null;
		public _floatElement _float
		{
			get
			{
				if (_floatElementList == null || _floatElementList.Count == 0)
					return(null);
				return (_floatElementList[0]);
			}
		}
	}
	public class TransparentElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public TransparentElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "opaque")
					{
						this.opaque = new OpaqueAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "color")
					{
						if (this.colorElementList == null)
						{
							this.colorElementList = new List<ColorElement>();
						}
						this.colorElementList.Add(new ColorElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public OpaqueAttr opaque = null;
		public List<ColorElement> colorElementList = null;
		public ColorElement color
		{
			get
			{
				if (colorElementList == null || colorElementList.Count == 0)
					return(null);
				return (colorElementList[0]);
			}
		}
	}
	public class OpaqueAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public OpaqueAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class TransparencyElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public TransparencyElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float")
					{
						if (this._floatElementList == null)
						{
							this._floatElementList = new List<_floatElement>();
						}
						this._floatElementList.Add(new _floatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<_floatElement> _floatElementList = null;
		public _floatElement _float
		{
			get
			{
				if (_floatElementList == null || _floatElementList.Count == 0)
					return(null);
				return (_floatElementList[0]);
			}
		}
	}
	public class Index_of_refractionElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Index_of_refractionElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float")
					{
						if (this._floatElementList == null)
						{
							this._floatElementList = new List<_floatElement>();
						}
						this._floatElementList.Add(new _floatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<_floatElement> _floatElementList = null;
		public _floatElement _float
		{
			get
			{
				if (_floatElementList == null || _floatElementList.Count == 0)
					return(null);
				return (_floatElementList[0]);
			}
		}
	}
	public class MirrorUElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MirrorUElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class MirrorVElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MirrorVElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class WrapUElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public WrapUElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class WrapVElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public WrapVElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class RepeatUElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public RepeatUElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class RepeatVElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public RepeatVElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class OffsetUElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public OffsetUElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class OffsetVElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public OffsetVElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class AmountElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public AmountElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Spec_levelElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Spec_levelElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float")
					{
						if (this._floatElementList == null)
						{
							this._floatElementList = new List<_floatElement>();
						}
						this._floatElementList.Add(new _floatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<_floatElement> _floatElementList = null;
		public _floatElement _float
		{
			get
			{
				if (_floatElementList == null || _floatElementList.Count == 0)
					return(null);
				return (_floatElementList[0]);
			}
		}
	}
	public class Emission_levelElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Emission_levelElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "float")
					{
						if (this._floatElementList == null)
						{
							this._floatElementList = new List<_floatElement>();
						}
						this._floatElementList.Add(new _floatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<_floatElement> _floatElementList = null;
		public _floatElement _float
		{
			get
			{
				if (_floatElementList == null || _floatElementList.Count == 0)
					return(null);
				return (_floatElementList[0]);
			}
		}
	}
	public class FacetedElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public FacetedElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Double_sidedElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Double_sidedElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class WireframeElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public WireframeElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Face_mapElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Face_mapElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class HelperElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public HelperElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "bounding_min")
					{
						if (this.bounding_minElementList == null)
						{
							this.bounding_minElementList = new List<Bounding_minElement>();
						}
						this.bounding_minElementList.Add(new Bounding_minElement(subNode));
					}
					else if (subNode.Name == "bounding_max")
					{
						if (this.bounding_maxElementList == null)
						{
							this.bounding_maxElementList = new List<Bounding_maxElement>();
						}
						this.bounding_maxElementList.Add(new Bounding_maxElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<Bounding_minElement> bounding_minElementList = null;
		public Bounding_minElement bounding_min
		{
			get
			{
				if (bounding_minElementList == null || bounding_minElementList.Count == 0)
					return(null);
				return (bounding_minElementList[0]);
			}
		}
		public List<Bounding_maxElement> bounding_maxElementList = null;
		public Bounding_maxElement bounding_max
		{
			get
			{
				if (bounding_maxElementList == null || bounding_maxElementList.Count == 0)
					return(null);
				return (bounding_maxElementList[0]);
			}
		}
	}
	public class Bounding_minElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Bounding_minElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Bounding_maxElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Bounding_maxElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Start_timeElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Start_timeElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class End_timeElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public End_timeElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Frame_rateElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Frame_rateElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Name_arrayElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Name_arrayElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "count")
					{
						this.count = new CountAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public CountAttr count = null;
	}
	public class SamplerElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SamplerElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "input")
					{
						if (this.inputElementList == null)
						{
							this.inputElementList = new List<InputElement>();
						}
						this.inputElementList.Add(new InputElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public List<InputElement> inputElementList = null;
		public InputElement input
		{
			get
			{
				if (inputElementList == null || inputElementList.Count == 0)
					return(null);
				return (inputElementList[0]);
			}
		}
	}
	public class InputElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public InputElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "semantic")
					{
						this.semantic = new SemanticAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "source")
					{
						this._source = new _sourceAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "offset")
					{
						this.offset = new OffsetAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "set")
					{
						this.set = new SetAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public SemanticAttr semantic = null;
		public _sourceAttr _source = null;
		public OffsetAttr offset = null;
		public SetAttr set = null;
	}
	public class OffsetAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public OffsetAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class SetAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public SetAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class ChannelElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ChannelElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "source")
					{
						this._source = new _sourceAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "target")
					{
						this.target = new TargetAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public _sourceAttr _source = null;
		public TargetAttr target = null;
	}
	public class Library_animation_clipsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_animation_clipsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "animation_clip")
					{
						if (this.animation_clipElementList == null)
						{
							this.animation_clipElementList = new List<Animation_clipElement>();
						}
						this.animation_clipElementList.Add(new Animation_clipElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<Animation_clipElement> animation_clipElementList = null;
		public Animation_clipElement animation_clip
		{
			get
			{
				if (animation_clipElementList == null || animation_clipElementList.Count == 0)
					return(null);
				return (animation_clipElementList[0]);
			}
		}
	}
	public class Animation_clipElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Animation_clipElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "start")
					{
						this.start = new StartAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "end")
					{
						this.end = new EndAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "instance_animation")
					{
						if (this.instance_animationElementList == null)
						{
							this.instance_animationElementList = new List<Instance_animationElement>();
						}
						this.instance_animationElementList.Add(new Instance_animationElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public StartAttr start = null;
		public EndAttr end = null;
		public List<Instance_animationElement> instance_animationElementList = null;
		public Instance_animationElement instance_animation
		{
			get
			{
				if (instance_animationElementList == null || instance_animationElementList.Count == 0)
					return(null);
				return (instance_animationElementList[0]);
			}
		}
	}
	public class StartAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public StartAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class EndAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public EndAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Instance_animationElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Instance_animationElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "url")
					{
						this.url = new UrlAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public UrlAttr url = null;
	}
	public class UrlAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public UrlAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class Library_imagesElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_imagesElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "image")
					{
						if (this.imageElementList == null)
						{
							this.imageElementList = new List<ImageElement>();
						}
						this.imageElementList.Add(new ImageElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<ImageElement> imageElementList = null;
		public ImageElement image
		{
			get
			{
				if (imageElementList == null || imageElementList.Count == 0)
					return(null);
				return (imageElementList[0]);
			}
		}
	}
	public class ImageElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ImageElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "init_from")
					{
						if (this.init_fromElementList == null)
						{
							this.init_fromElementList = new List<Init_fromElement>();
						}
						this.init_fromElementList.Add(new Init_fromElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public NameAttr name = null;
		public List<Init_fromElement> init_fromElementList = null;
		public Init_fromElement init_from
		{
			get
			{
				if (init_fromElementList == null || init_fromElementList.Count == 0)
					return(null);
				return (init_fromElementList[0]);
			}
		}
	}
	public class Init_fromElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Init_fromElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Library_materialsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_materialsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "material")
					{
						if (this.materialElementList == null)
						{
							this.materialElementList = new List<MaterialElement>();
						}
						this.materialElementList.Add(new MaterialElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<MaterialElement> materialElementList = null;
		public MaterialElement material
		{
			get
			{
				if (materialElementList == null || materialElementList.Count == 0)
					return(null);
				return (materialElementList[0]);
			}
		}
	}
	public class MaterialElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MaterialElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "instance_effect")
					{
						if (this.instance_effectElementList == null)
						{
							this.instance_effectElementList = new List<Instance_effectElement>();
						}
						this.instance_effectElementList.Add(new Instance_effectElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public NameAttr name = null;
		public List<Instance_effectElement> instance_effectElementList = null;
		public Instance_effectElement instance_effect
		{
			get
			{
				if (instance_effectElementList == null || instance_effectElementList.Count == 0)
					return(null);
				return (instance_effectElementList[0]);
			}
		}
	}
	public class Instance_effectElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Instance_effectElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "url")
					{
						this.url = new UrlAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public UrlAttr url = null;
	}
	public class Library_effectsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_effectsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "effect")
					{
						if (this.effectElementList == null)
						{
							this.effectElementList = new List<EffectElement>();
						}
						this.effectElementList.Add(new EffectElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<EffectElement> effectElementList = null;
		public EffectElement effect
		{
			get
			{
				if (effectElementList == null || effectElementList.Count == 0)
					return(null);
				return (effectElementList[0]);
			}
		}
	}
	public class EffectElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public EffectElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "profile_COMMON")
					{
						if (this.profile_COMMONElementList == null)
						{
							this.profile_COMMONElementList = new List<Profile_COMMONElement>();
						}
						this.profile_COMMONElementList.Add(new Profile_COMMONElement(subNode));
					}
					else if (subNode.Name == "extra")
					{
						if (this.extraElementList == null)
						{
							this.extraElementList = new List<ExtraElement>();
						}
						this.extraElementList.Add(new ExtraElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public NameAttr name = null;
		public List<Profile_COMMONElement> profile_COMMONElementList = null;
		public Profile_COMMONElement profile_COMMON
		{
			get
			{
				if (profile_COMMONElementList == null || profile_COMMONElementList.Count == 0)
					return(null);
				return (profile_COMMONElementList[0]);
			}
		}
		public List<ExtraElement> extraElementList = null;
		public ExtraElement extra
		{
			get
			{
				if (extraElementList == null || extraElementList.Count == 0)
					return(null);
				return (extraElementList[0]);
			}
		}
	}
	public class Profile_COMMONElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Profile_COMMONElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "newparam")
					{
						if (this.newparamElementList == null)
						{
							this.newparamElementList = new List<NewparamElement>();
						}
						this.newparamElementList.Add(new NewparamElement(subNode));
					}
					else if (subNode.Name == "technique")
					{
						if (this.techniqueElementList == null)
						{
							this.techniqueElementList = new List<TechniqueElement>();
						}
						this.techniqueElementList.Add(new TechniqueElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<NewparamElement> newparamElementList = null;
		public NewparamElement newparam
		{
			get
			{
				if (newparamElementList == null || newparamElementList.Count == 0)
					return(null);
				return (newparamElementList[0]);
			}
		}
		public List<TechniqueElement> techniqueElementList = null;
		public TechniqueElement technique
		{
			get
			{
				if (techniqueElementList == null || techniqueElementList.Count == 0)
					return(null);
				return (techniqueElementList[0]);
			}
		}
	}
	public class NewparamElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public NewparamElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "sid")
					{
						this.sid = new SidAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "surface")
					{
						if (this.surfaceElementList == null)
						{
							this.surfaceElementList = new List<SurfaceElement>();
						}
						this.surfaceElementList.Add(new SurfaceElement(subNode));
					}
					else if (subNode.Name == "sampler2D")
					{
						if (this.sampler2DElementList == null)
						{
							this.sampler2DElementList = new List<Sampler2DElement>();
						}
						this.sampler2DElementList.Add(new Sampler2DElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public SidAttr sid = null;
		public List<SurfaceElement> surfaceElementList = null;
		public SurfaceElement surface
		{
			get
			{
				if (surfaceElementList == null || surfaceElementList.Count == 0)
					return(null);
				return (surfaceElementList[0]);
			}
		}
		public List<Sampler2DElement> sampler2DElementList = null;
		public Sampler2DElement sampler2D
		{
			get
			{
				if (sampler2DElementList == null || sampler2DElementList.Count == 0)
					return(null);
				return (sampler2DElementList[0]);
			}
		}
	}
	public class SurfaceElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SurfaceElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "type")
					{
						this.type = new TypeAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "init_from")
					{
						if (this.init_fromElementList == null)
						{
							this.init_fromElementList = new List<Init_fromElement>();
						}
						this.init_fromElementList.Add(new Init_fromElement(subNode));
					}
					else if (subNode.Name == "format")
					{
						if (this.formatElementList == null)
						{
							this.formatElementList = new List<FormatElement>();
						}
						this.formatElementList.Add(new FormatElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public TypeAttr type = null;
		public List<Init_fromElement> init_fromElementList = null;
		public Init_fromElement init_from
		{
			get
			{
				if (init_fromElementList == null || init_fromElementList.Count == 0)
					return(null);
				return (init_fromElementList[0]);
			}
		}
		public List<FormatElement> formatElementList = null;
		public FormatElement format
		{
			get
			{
				if (formatElementList == null || formatElementList.Count == 0)
					return(null);
				return (formatElementList[0]);
			}
		}
	}
	public class FormatElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public FormatElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Sampler2DElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Sampler2DElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "source")
					{
						if (this.sourceElementList == null)
						{
							this.sourceElementList = new List<SourceElement>();
						}
						this.sourceElementList.Add(new SourceElement(subNode));
					}
					else if (subNode.Name == "wrap_s")
					{
						if (this.wrap_sElementList == null)
						{
							this.wrap_sElementList = new List<Wrap_sElement>();
						}
						this.wrap_sElementList.Add(new Wrap_sElement(subNode));
					}
					else if (subNode.Name == "wrap_t")
					{
						if (this.wrap_tElementList == null)
						{
							this.wrap_tElementList = new List<Wrap_tElement>();
						}
						this.wrap_tElementList.Add(new Wrap_tElement(subNode));
					}
					else if (subNode.Name == "minfilter")
					{
						if (this.minfilterElementList == null)
						{
							this.minfilterElementList = new List<MinfilterElement>();
						}
						this.minfilterElementList.Add(new MinfilterElement(subNode));
					}
					else if (subNode.Name == "magfilter")
					{
						if (this.magfilterElementList == null)
						{
							this.magfilterElementList = new List<MagfilterElement>();
						}
						this.magfilterElementList.Add(new MagfilterElement(subNode));
					}
					else if (subNode.Name == "mipfilter")
					{
						if (this.mipfilterElementList == null)
						{
							this.mipfilterElementList = new List<MipfilterElement>();
						}
						this.mipfilterElementList.Add(new MipfilterElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<SourceElement> sourceElementList = null;
		public SourceElement source
		{
			get
			{
				if (sourceElementList == null || sourceElementList.Count == 0)
					return(null);
				return (sourceElementList[0]);
			}
		}
		public List<Wrap_sElement> wrap_sElementList = null;
		public Wrap_sElement wrap_s
		{
			get
			{
				if (wrap_sElementList == null || wrap_sElementList.Count == 0)
					return(null);
				return (wrap_sElementList[0]);
			}
		}
		public List<Wrap_tElement> wrap_tElementList = null;
		public Wrap_tElement wrap_t
		{
			get
			{
				if (wrap_tElementList == null || wrap_tElementList.Count == 0)
					return(null);
				return (wrap_tElementList[0]);
			}
		}
		public List<MinfilterElement> minfilterElementList = null;
		public MinfilterElement minfilter
		{
			get
			{
				if (minfilterElementList == null || minfilterElementList.Count == 0)
					return(null);
				return (minfilterElementList[0]);
			}
		}
		public List<MagfilterElement> magfilterElementList = null;
		public MagfilterElement magfilter
		{
			get
			{
				if (magfilterElementList == null || magfilterElementList.Count == 0)
					return(null);
				return (magfilterElementList[0]);
			}
		}
		public List<MipfilterElement> mipfilterElementList = null;
		public MipfilterElement mipfilter
		{
			get
			{
				if (mipfilterElementList == null || mipfilterElementList.Count == 0)
					return(null);
				return (mipfilterElementList[0]);
			}
		}
	}
	public class Wrap_sElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Wrap_sElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Wrap_tElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Wrap_tElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class MinfilterElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MinfilterElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class MagfilterElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MagfilterElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class MipfilterElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MipfilterElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Library_geometriesElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_geometriesElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "geometry")
					{
						if (this.geometryElementList == null)
						{
							this.geometryElementList = new List<GeometryElement>();
						}
						this.geometryElementList.Add(new GeometryElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<GeometryElement> geometryElementList = null;
		public GeometryElement geometry
		{
			get
			{
				if (geometryElementList == null || geometryElementList.Count == 0)
					return(null);
				return (geometryElementList[0]);
			}
		}
	}
	public class GeometryElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public GeometryElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "mesh")
					{
						if (this.meshElementList == null)
						{
							this.meshElementList = new List<MeshElement>();
						}
						this.meshElementList.Add(new MeshElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public NameAttr name = null;
		public List<MeshElement> meshElementList = null;
		public MeshElement mesh
		{
			get
			{
				if (meshElementList == null || meshElementList.Count == 0)
					return(null);
				return (meshElementList[0]);
			}
		}
	}
	public class MeshElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MeshElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "source")
					{
						if (this.sourceElementList == null)
						{
							this.sourceElementList = new List<SourceElement>();
						}
						this.sourceElementList.Add(new SourceElement(subNode));
					}
					else if (subNode.Name == "vertices")
					{
						if (this.verticesElementList == null)
						{
							this.verticesElementList = new List<VerticesElement>();
						}
						this.verticesElementList.Add(new VerticesElement(subNode));
					}
					else if (subNode.Name == "triangles")
					{
						if (this.trianglesElementList == null)
						{
							this.trianglesElementList = new List<TrianglesElement>();
						}
						this.trianglesElementList.Add(new TrianglesElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<SourceElement> sourceElementList = null;
		public SourceElement source
		{
			get
			{
				if (sourceElementList == null || sourceElementList.Count == 0)
					return(null);
				return (sourceElementList[0]);
			}
		}
		public List<VerticesElement> verticesElementList = null;
		public VerticesElement vertices
		{
			get
			{
				if (verticesElementList == null || verticesElementList.Count == 0)
					return(null);
				return (verticesElementList[0]);
			}
		}
		public List<TrianglesElement> trianglesElementList = null;
		public TrianglesElement triangles
		{
			get
			{
				if (trianglesElementList == null || trianglesElementList.Count == 0)
					return(null);
				return (trianglesElementList[0]);
			}
		}
	}
	public class VerticesElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public VerticesElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "input")
					{
						if (this.inputElementList == null)
						{
							this.inputElementList = new List<InputElement>();
						}
						this.inputElementList.Add(new InputElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public List<InputElement> inputElementList = null;
		public InputElement input
		{
			get
			{
				if (inputElementList == null || inputElementList.Count == 0)
					return(null);
				return (inputElementList[0]);
			}
		}
	}
	public class TrianglesElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public TrianglesElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "material")
					{
						this._material = new _materialAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "count")
					{
						this.count = new CountAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "input")
					{
						if (this.inputElementList == null)
						{
							this.inputElementList = new List<InputElement>();
						}
						this.inputElementList.Add(new InputElement(subNode));
					}
					else if (subNode.Name == "p")
					{
						if (this.pElementList == null)
						{
							this.pElementList = new List<PElement>();
						}
						this.pElementList.Add(new PElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public _materialAttr _material = null;
		public CountAttr count = null;
		public List<InputElement> inputElementList = null;
		public InputElement input
		{
			get
			{
				if (inputElementList == null || inputElementList.Count == 0)
					return(null);
				return (inputElementList[0]);
			}
		}
		public List<PElement> pElementList = null;
		public PElement p
		{
			get
			{
				if (pElementList == null || pElementList.Count == 0)
					return(null);
				return (pElementList[0]);
			}
		}
	}
	public class _materialAttr
	{
		public override string ToString()
		{
			return (this.Value);
		}
		private string _Name = "";
		public string Name
		{
			get
			{
				return (this._Name);
			}
		}
		private string _Value = "";
		public string Value
		{
			get
			{
				return (this._Value);
			}
		}
		public _materialAttr(string name, string value)
		{
			this._Name = name;
			this._Value = value;
		}
	}
	public class PElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public PElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Library_controllersElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_controllersElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "controller")
					{
						if (this.controllerElementList == null)
						{
							this.controllerElementList = new List<ControllerElement>();
						}
						this.controllerElementList.Add(new ControllerElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<ControllerElement> controllerElementList = null;
		public ControllerElement controller
		{
			get
			{
				if (controllerElementList == null || controllerElementList.Count == 0)
					return(null);
				return (controllerElementList[0]);
			}
		}
	}
	public class ControllerElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public ControllerElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "skin")
					{
						if (this.skinElementList == null)
						{
							this.skinElementList = new List<SkinElement>();
						}
						this.skinElementList.Add(new SkinElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public List<SkinElement> skinElementList = null;
		public SkinElement skin
		{
			get
			{
				if (skinElementList == null || skinElementList.Count == 0)
					return(null);
				return (skinElementList[0]);
			}
		}
	}
	public class SkinElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SkinElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "source")
					{
						this._source = new _sourceAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "bind_shape_matrix")
					{
						if (this.bind_shape_matrixElementList == null)
						{
							this.bind_shape_matrixElementList = new List<Bind_shape_matrixElement>();
						}
						this.bind_shape_matrixElementList.Add(new Bind_shape_matrixElement(subNode));
					}
					else if (subNode.Name == "source")
					{
						if (this.sourceElementList == null)
						{
							this.sourceElementList = new List<SourceElement>();
						}
						this.sourceElementList.Add(new SourceElement(subNode));
					}
					else if (subNode.Name == "joints")
					{
						if (this.jointsElementList == null)
						{
							this.jointsElementList = new List<JointsElement>();
						}
						this.jointsElementList.Add(new JointsElement(subNode));
					}
					else if (subNode.Name == "vertex_weights")
					{
						if (this.vertex_weightsElementList == null)
						{
							this.vertex_weightsElementList = new List<Vertex_weightsElement>();
						}
						this.vertex_weightsElementList.Add(new Vertex_weightsElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public _sourceAttr _source = null;
		public List<Bind_shape_matrixElement> bind_shape_matrixElementList = null;
		public Bind_shape_matrixElement bind_shape_matrix
		{
			get
			{
				if (bind_shape_matrixElementList == null || bind_shape_matrixElementList.Count == 0)
					return(null);
				return (bind_shape_matrixElementList[0]);
			}
		}
		public List<SourceElement> sourceElementList = null;
		public SourceElement source
		{
			get
			{
				if (sourceElementList == null || sourceElementList.Count == 0)
					return(null);
				return (sourceElementList[0]);
			}
		}
		public List<JointsElement> jointsElementList = null;
		public JointsElement joints
		{
			get
			{
				if (jointsElementList == null || jointsElementList.Count == 0)
					return(null);
				return (jointsElementList[0]);
			}
		}
		public List<Vertex_weightsElement> vertex_weightsElementList = null;
		public Vertex_weightsElement vertex_weights
		{
			get
			{
				if (vertex_weightsElementList == null || vertex_weightsElementList.Count == 0)
					return(null);
				return (vertex_weightsElementList[0]);
			}
		}
	}
	public class Bind_shape_matrixElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Bind_shape_matrixElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class JointsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public JointsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "input")
					{
						if (this.inputElementList == null)
						{
							this.inputElementList = new List<InputElement>();
						}
						this.inputElementList.Add(new InputElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<InputElement> inputElementList = null;
		public InputElement input
		{
			get
			{
				if (inputElementList == null || inputElementList.Count == 0)
					return(null);
				return (inputElementList[0]);
			}
		}
	}
	public class Vertex_weightsElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Vertex_weightsElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "count")
					{
						this.count = new CountAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "input")
					{
						if (this.inputElementList == null)
						{
							this.inputElementList = new List<InputElement>();
						}
						this.inputElementList.Add(new InputElement(subNode));
					}
					else if (subNode.Name == "vcount")
					{
						if (this.vcountElementList == null)
						{
							this.vcountElementList = new List<VcountElement>();
						}
						this.vcountElementList.Add(new VcountElement(subNode));
					}
					else if (subNode.Name == "v")
					{
						if (this.vElementList == null)
						{
							this.vElementList = new List<VElement>();
						}
						this.vElementList.Add(new VElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public CountAttr count = null;
		public List<InputElement> inputElementList = null;
		public InputElement input
		{
			get
			{
				if (inputElementList == null || inputElementList.Count == 0)
					return(null);
				return (inputElementList[0]);
			}
		}
		public List<VcountElement> vcountElementList = null;
		public VcountElement vcount
		{
			get
			{
				if (vcountElementList == null || vcountElementList.Count == 0)
					return(null);
				return (vcountElementList[0]);
			}
		}
		public List<VElement> vElementList = null;
		public VElement v
		{
			get
			{
				if (vElementList == null || vElementList.Count == 0)
					return(null);
				return (vElementList[0]);
			}
		}
	}
	public class VcountElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public VcountElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class VElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public VElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Library_visual_scenesElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Library_visual_scenesElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "visual_scene")
					{
						if (this.visual_sceneElementList == null)
						{
							this.visual_sceneElementList = new List<Visual_sceneElement>();
						}
						this.visual_sceneElementList.Add(new Visual_sceneElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<Visual_sceneElement> visual_sceneElementList = null;
		public Visual_sceneElement visual_scene
		{
			get
			{
				if (visual_sceneElementList == null || visual_sceneElementList.Count == 0)
					return(null);
				return (visual_sceneElementList[0]);
			}
		}
	}
	public class Visual_sceneElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Visual_sceneElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "node")
					{
						if (this.nodeElementList == null)
						{
							this.nodeElementList = new List<NodeElement>();
						}
						this.nodeElementList.Add(new NodeElement(subNode));
					}
					else if (subNode.Name == "extra")
					{
						if (this.extraElementList == null)
						{
							this.extraElementList = new List<ExtraElement>();
						}
						this.extraElementList.Add(new ExtraElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public NameAttr name = null;
		public List<NodeElement> nodeElementList = null;
		public NodeElement node
		{
			get
			{
				if (nodeElementList == null || nodeElementList.Count == 0)
					return(null);
				return (nodeElementList[0]);
			}
		}
		public List<ExtraElement> extraElementList = null;
		public ExtraElement extra
		{
			get
			{
				if (extraElementList == null || extraElementList.Count == 0)
					return(null);
				return (extraElementList[0]);
			}
		}
	}
	public class NodeElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public NodeElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "id")
					{
						this.id = new IdAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "name")
					{
						this.name = new NameAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "sid")
					{
						this.sid = new SidAttr(attribute.Name, attribute.Value);
					}
					else if (attributeName == "type")
					{
						this.type = new TypeAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "matrix")
					{
						if (this.matrixElementList == null)
						{
							this.matrixElementList = new List<MatrixElement>();
						}
						this.matrixElementList.Add(new MatrixElement(subNode));
					}
					else if (subNode.Name == "node")
					{
						if (this.nodeElementList == null)
						{
							this.nodeElementList = new List<NodeElement>();
						}
						this.nodeElementList.Add(new NodeElement(subNode));
					}
					else if (subNode.Name == "extra")
					{
						if (this.extraElementList == null)
						{
							this.extraElementList = new List<ExtraElement>();
						}
						this.extraElementList.Add(new ExtraElement(subNode));
					}
					else if (subNode.Name == "instance_controller")
					{
						if (this.instance_controllerElementList == null)
						{
							this.instance_controllerElementList = new List<Instance_controllerElement>();
						}
						this.instance_controllerElementList.Add(new Instance_controllerElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public IdAttr id = null;
		public NameAttr name = null;
		public SidAttr sid = null;
		public TypeAttr type = null;
		public List<MatrixElement> matrixElementList = null;
		public MatrixElement matrix
		{
			get
			{
				if (matrixElementList == null || matrixElementList.Count == 0)
					return(null);
				return (matrixElementList[0]);
			}
		}
		public List<NodeElement> nodeElementList = null;
		public NodeElement node
		{
			get
			{
				if (nodeElementList == null || nodeElementList.Count == 0)
					return(null);
				return (nodeElementList[0]);
			}
		}
		public List<ExtraElement> extraElementList = null;
		public ExtraElement extra
		{
			get
			{
				if (extraElementList == null || extraElementList.Count == 0)
					return(null);
				return (extraElementList[0]);
			}
		}
		public List<Instance_controllerElement> instance_controllerElementList = null;
		public Instance_controllerElement instance_controller
		{
			get
			{
				if (instance_controllerElementList == null || instance_controllerElementList.Count == 0)
					return(null);
				return (instance_controllerElementList[0]);
			}
		}
	}
	public class MatrixElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public MatrixElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "sid")
					{
						this.sid = new SidAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public SidAttr sid = null;
	}
	public class Instance_controllerElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Instance_controllerElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "url")
					{
						this.url = new UrlAttr(attribute.Name, attribute.Value);
					}
				}
			}
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "skeleton")
					{
						if (this.skeletonElementList == null)
						{
							this.skeletonElementList = new List<SkeletonElement>();
						}
						this.skeletonElementList.Add(new SkeletonElement(subNode));
					}
					else if (subNode.Name == "bind_material")
					{
						if (this.bind_materialElementList == null)
						{
							this.bind_materialElementList = new List<Bind_materialElement>();
						}
						this.bind_materialElementList.Add(new Bind_materialElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public UrlAttr url = null;
		public List<SkeletonElement> skeletonElementList = null;
		public SkeletonElement skeleton
		{
			get
			{
				if (skeletonElementList == null || skeletonElementList.Count == 0)
					return(null);
				return (skeletonElementList[0]);
			}
		}
		public List<Bind_materialElement> bind_materialElementList = null;
		public Bind_materialElement bind_material
		{
			get
			{
				if (bind_materialElementList == null || bind_materialElementList.Count == 0)
					return(null);
				return (bind_materialElementList[0]);
			}
		}
	}
	public class SkeletonElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SkeletonElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
	}
	public class Bind_materialElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Bind_materialElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "technique_common")
					{
						if (this.technique_commonElementList == null)
						{
							this.technique_commonElementList = new List<Technique_commonElement>();
						}
						this.technique_commonElementList.Add(new Technique_commonElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<Technique_commonElement> technique_commonElementList = null;
		public Technique_commonElement technique_common
		{
			get
			{
				if (technique_commonElementList == null || technique_commonElementList.Count == 0)
					return(null);
				return (technique_commonElementList[0]);
			}
		}
	}
	public class SceneElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public SceneElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (subNode.Name == "instance_visual_scene")
					{
						if (this.instance_visual_sceneElementList == null)
						{
							this.instance_visual_sceneElementList = new List<Instance_visual_sceneElement>();
						}
						this.instance_visual_sceneElementList.Add(new Instance_visual_sceneElement(subNode));
					}
					if (subNode.Name == "#text")
					{
						this.Text = subNode.Value;
					}
				}
			}
		}
		public string Text = "";
		public List<Instance_visual_sceneElement> instance_visual_sceneElementList = null;
		public Instance_visual_sceneElement instance_visual_scene
		{
			get
			{
				if (instance_visual_sceneElementList == null || instance_visual_sceneElementList.Count == 0)
					return(null);
				return (instance_visual_sceneElementList[0]);
			}
		}
	}
	public class Instance_visual_sceneElement
	{
		public override string ToString()
		{
			return (this.Text);
		}
		public Instance_visual_sceneElement(XmlNode node)
		{
			this.Text = node.InnerText;
			if (node.Attributes != null)
			{
				foreach (XmlNode attribute in node.Attributes)
				{
					string attributeName = attribute.Name;
					string attributeValue = attribute.Value;
					if (attributeName == "url")
					{
						this.url = new UrlAttr(attribute.Name, attribute.Value);
					}
				}
			}
		}
		public string Text = "";
		public UrlAttr url = null;
	}
}
