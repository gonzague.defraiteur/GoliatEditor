﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Security.Policy;
using System.Security;
using System.Security.Permissions;
using FastMember;
using Microsoft.CodeAnalysis;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms.Integration;
using Goliat;
using Goliat.Serialization;
using StackTrace = System.Diagnostics.StackTrace;
namespace Goliat.Engine.GUI
{
	public class InspectorGUI
	{
		public class InspectorField
		{
			public InspectorCard parent;
			public UIElement control;
			public InspectorField()
			{
				;
			}
			public InspectorField(InspectorCard card)
			{
				this.parent = card;
				;
			}
			private void UpdateValue(object sender, System.Windows.RoutedEventArgs e)
			{
				this.parent.Read();
			}
			private void UpdateValue(object sender, Goliat.Controls.ItemSelectedEventArgs e)
			{
				this.parent.Read();
			}
			public class ToggleField : InspectorField
			{
				public bool value
				{
					get
					{
						Console.WriteLine("getvalue toggle");
						return ((bool)((CheckBox)control).IsChecked);
						;
					}
					set
					{
						Console.WriteLine("setvalue");
						((CheckBox)control).IsChecked = value;
					}
				}
				public ToggleField(bool number, InspectorCard parent) : base(parent)
				{
					control = new CheckBox();
					((CheckBox)control).Unchecked += this.UpdateValue;
					((CheckBox)control).Checked += this.UpdateValue;
					this.value = number;
				}
			}
			public class IntField : InspectorField
			{
				public long value
				{
					get
					{
						return (((TextBox)control).Text.ConvertString<long>());
						;
					}
					set
					{
						((TextBox)control).Text = value.ToString();
					}
				}
				public IntField(long number, InspectorCard parent) : base(parent)
				{
					control = new TextBox();
					((TextBox)control).TextChanged += this.UpdateValue;
					this.value = number;
				}
			}
			public class FloatField : InspectorField
			{
				public float value
				{
					get
					{
						return (((TextBox)control).Text.ConvertString<float>());
						;
					}
					set
					{
						((TextBox)control).Text = value.ToString();
					}
				}
				public FloatField(float number, InspectorCard parent) : base(parent)
				{
					control = new TextBox();
					((TextBox)control).TextChanged += this.UpdateValue;
					this.value = number;
				}
			}
			public class NumberSlider : InspectorField
			{
				public float value
				{
					get
					{
						return ((float)((Slider)control).Value);
						;
					}
					set
					{
						((Slider)control).Value = (double)value;
					}
				}
				public NumberSlider(float number, float min, float max, InspectorCard parent) : base(parent)
				{
					control = new Slider();
					((Slider)control).ValueChanged += this.UpdateValue;
					this.value = number;
				}
			}
			public class BehaviourField : InspectorField
			{

			}
			public class RessourceField : InspectorField
			{
				public Type RessourceType;
				public SharedResource value
				{
					get
					{
						return ((SharedResource)((Goliat.Controls.PickBox)control).Asset);
						//return (null);
						//return ((float)((Slider)control).Value);
						;
					}
					set
					{
						((Goliat.Controls.PickBox)control).Asset = value;
						;
						//((Slider)control).Value = (double)value;
					}
				}
				public RessourceField(SharedResource ressource, Type type, InspectorCard parent) : base(parent)
				{
					control = new Goliat.Controls.PickBox(type);
					((Goliat.Controls.PickBox)control).ItemSelected += this.UpdateValue;
					((Goliat.Controls.PickBox)control).Asset = ressource;
					//control = new Slider();
					//((Slider)control).ValueChanged += this.UpdateValue;
					this.value = ressource;
					this.RessourceType = type;
				}
			}
			public class CustomObjectField : InspectorField
			{
				public object value
				{
					get
					{
						
						return (((Expander)control).Tag);
						;
					}
					set
					{
						CustomProperty root = new CustomProperty(null, "", value);
						control = (Expander)InspectorTools.generateExpander(root);
					}
				}
				public CustomObjectField(object customObj, InspectorCard parent) : base(parent)
				{
					control = new Expander();
					this.value = customObj;
				}
			}
			public class LabelField : InspectorField
			{

			}
			public class TextField : InspectorField
			{
				public string value
				{
					get
					{
						return (((TextBox)control).Text);
						;
					}
					set
					{
						((TextBox)control).Text = value;
					}
				}
				public TextField(string value)
				{
					control = new TextBox();
					((TextBox)control).TextChanged += this.UpdateValue;
					this.value = value;
				}
			}
			public class TextArea : InspectorField
			{
				public string value
				{
					get
					{
						RichTextBox richTextBox1 = (RichTextBox)control;
						return (new TextRange(richTextBox1.Document.ContentStart, richTextBox1.Document.ContentEnd).Text);
						//return (((RichTextBox)control).Text);
						;
					}
					set
					{
						RichTextBox richTextBox1 = (RichTextBox)control;
						richTextBox1.Document.Blocks.Clear();
						richTextBox1.Document.Blocks.Add(new Paragraph(new Run(value)));
					}
				}
				public TextArea(string value, InspectorCard parent) : base(parent)
				{
					control = new RichTextBox();
					((RichTextBox)control).TextChanged += this.UpdateValue;
					this.value = value;
				}
			}
			public class EnumField : InspectorField
			{
				
				public Enum value
				{
					get
					{
						return (Enum)((ComboBoxItem)((ComboBox)control).SelectedItem).Tag;
					}
					set
					{
						foreach (ComboBoxItem item in ((ComboBox)control).Items)
						{
							if ((Enum)item.Tag == (Enum)value)
							{
								((ComboBox)control).SelectedItem = item;
							}
						}
					}
				}
				public EnumField(Enum value, InspectorCard parent) : base(parent)
				{
					control = new ComboBox();
					ComboBox box = (ComboBox)control;
					foreach (var val in Enum.GetValues(value.GetType()))
					{
						ComboBoxItem item = new ComboBoxItem();
						item.Content = Enum.GetName(value.GetType(), val);
						item.Tag = val;
						box.Items.Add(item);
						;
					}
					box.SelectionChanged += this.UpdateValue;
				}
			}
			public class Vector2Field : InspectorField
			{
				private Vector2 _value = Vector2.zero;
				public Vector2 value
				{
					get
					{
						Console.WriteLine("getvalue");
						Vector2 res = Vector2.zero;
						res.x = ((TextBox)((Grid)control).Children[0]).Text.ConvertString<float>();
						res.y = ((TextBox)((Grid)control).Children[1]).Text.ConvertString<float>();
						return (res);
						;
					}
					set
					{
						((TextBox)((Grid)control).Children[0]).Text = value.x.ToString();
						((TextBox)((Grid)control).Children[1]).Text = value.y.ToString();
					}
				}
				public Vector2Field(Vector2 number, InspectorCard parent) : base(parent)
				{
					control = new Grid();
					Grid grid = (Grid)control;
					TextBox box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 0);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 1);
					grid.Children.Add(box);
					this.value = number;
				}
			}
			public class Vector3Field : InspectorField
			{
				public Vector3 value
				{
					get
					{
						Console.WriteLine("getvalue v3");
						Vector3 res = Vector3.zero;
						res.x = ((TextBox)((Grid)control).Children[0]).Text.ConvertString<float>();
						res.y = ((TextBox)((Grid)control).Children[1]).Text.ConvertString<float>();
						res.z = ((TextBox)((Grid)control).Children[2]).Text.ConvertString<float>();
						return (res);
						;
					}
					set
					{
						Console.WriteLine("setvalue v3");
							if (((TextBox)((Grid)control).Children[0]) == null)
							{
								Console.WriteLine("i got null child");
							}
						((TextBox)((Grid)control).Children[0]).Text = (value.x == 0 ? "0" : value.x.ToString());
						((TextBox)((Grid)control).Children[1]).Text = (value.y == 0 ? "0" : value.y.ToString());
						((TextBox)((Grid)control).Children[2]).Text = (value.z == 0 ? "0" : value.z.ToString());
					}
				}
				public Vector3Field(Vector3 number, InspectorCard parent) : base(parent)
				{

					control = new Grid();
					Grid grid = (Grid)control;
					grid.Width = 150;
					grid.HorizontalAlignment = HorizontalAlignment.Left;
					ColumnDefinition col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					TextBox box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 0);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 1);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 2);
					grid.Children.Add(box);
					this.value = number;
				}
			}
			public class Vector4Field : InspectorField
			{
				private Vector4 _value = Vector4.zero;
				public Vector4 value
				{
					get
					{
						Console.WriteLine("getvalue");
						Vector4 res = Vector4.zero;
						res.x = ((TextBox)((Grid)control).Children[0]).Text.ConvertString<float>();
						res.y = ((TextBox)((Grid)control).Children[1]).Text.ConvertString<float>();
						res.z = ((TextBox)((Grid)control).Children[2]).Text.ConvertString<float>();
						res.w = ((TextBox)((Grid)control).Children[3]).Text.ConvertString<float>();
						return (res);
						;
					}
					set
					{
						Console.WriteLine("setvalue");
						((TextBox)((Grid)control).Children[0]).Text = value.x.ToString();
						((TextBox)((Grid)control).Children[1]).Text = value.y.ToString();
						((TextBox)((Grid)control).Children[2]).Text = value.z.ToString();
						((TextBox)((Grid)control).Children[3]).Text = value.w.ToString();
					}
				}
				public Vector4Field(Vector4 number, InspectorCard parent) : base(parent)
				{
					control = new Grid();
					Grid grid = (Grid)control;
					grid.Width = 150;
					grid.HorizontalAlignment = HorizontalAlignment.Left;
					ColumnDefinition col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					TextBox box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 0);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 1);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 2);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 3);
					grid.Children.Add(box);
					this.value = number;
				}
			}

			public class QuaternionField : InspectorField
			{
				private Quaternion _value = Quaternion.identity;
				public Quaternion value
				{
					get
					{
						Quaternion res = Quaternion.identity;
						res.x = ((TextBox)((Grid)control).Children[0]).Text.ConvertString<float>();
						res.y = ((TextBox)((Grid)control).Children[1]).Text.ConvertString<float>();
						res.z = ((TextBox)((Grid)control).Children[2]).Text.ConvertString<float>();
						res.w = ((TextBox)((Grid)control).Children[3]).Text.ConvertString<float>();
						return (res);
						;
					}
					set
					{
						((TextBox)((Grid)control).Children[0]).Text = value.x.ToString();
						((TextBox)((Grid)control).Children[1]).Text = value.y.ToString();
						((TextBox)((Grid)control).Children[2]).Text = value.z.ToString();
						((TextBox)((Grid)control).Children[3]).Text = value.w.ToString();
					}
				}
				public QuaternionField(Quaternion number, InspectorCard parent) : base(parent)
				{
					control = new Grid();
					Grid grid = (Grid)control;
					grid.Width = 150;
					grid.HorizontalAlignment = HorizontalAlignment.Left;
					ColumnDefinition col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					TextBox box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 0);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 1);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 2);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 3);
					grid.Children.Add(box);
					this.value = number;
				}
			}
			public class ButtonField : InspectorField
			{
				private ButtonHandler _handler = null;
				public ButtonHandler handler
				{
					get
					{
						return (this._handler);
					}
					set
					{
						if (this._handler != value)
						{
							
							if (value != null)
							{
								if (this._handler != null)
								{
									((Button)control).Click -= this._handler.Clicked;
								}
								((Button)control).Click += value.Clicked;
							}
						}
						this._handler = value;
					}
				}
				public ButtonField(string buttonName, ButtonHandler handler, InspectorCard parent) : base(parent)
				{
					control = new Button();
					Button button = ((Button)control);
					//button.Width = 45;
					//button.Height = 25;
					button.Content = buttonName;
					//((Button)control).
					this.handler = handler;
					//if (handler != null)
						//((Button)control).Click += handler.Clicked;
				}
			}
			public class EulerField : InspectorField
			{
				private Quaternion _value = Quaternion.identity;
				public Quaternion value
				{
					get
					{
						Console.WriteLine("getvalue euler");
						Quaternion res = Quaternion.identity;
						Vector3 euler = res.eulerAngles;
						euler.x = ((TextBox)((Grid)control).Children[0]).Text.ConvertString<float>();
						euler.y = ((TextBox)((Grid)control).Children[1]).Text.ConvertString<float>();
						euler.z = ((TextBox)((Grid)control).Children[2]).Text.ConvertString<float>();
						res.eulerAngles = euler;
						Console.WriteLine("fin getvalue");
						Console.WriteLine("res euler:" + euler);
						Console.WriteLine("res quaternion:" + res);
						return (res);
						;
					}
					set
					{
						Console.WriteLine("setvalue");

						((TextBox)((Grid)control).Children[0]).Text = value.eulerAngles.x.ToString();
						((TextBox)((Grid)control).Children[1]).Text = value.eulerAngles.y.ToString();
						((TextBox)((Grid)control).Children[2]).Text = value.eulerAngles.z.ToString();
					}
				}
				public EulerField(Quaternion number, InspectorCard parent) : base(parent)
				{
					control = new Grid();
					Grid grid = (Grid)control;
					grid.Width = 150;
					grid.HorizontalAlignment = HorizontalAlignment.Left;
					ColumnDefinition col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					col = new ColumnDefinition();
					col.Width = new GridLength(1, GridUnitType.Star);
					grid.ColumnDefinitions.Add(col);
					TextBox box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 0);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 1);
					grid.Children.Add(box);
					box = new TextBox();
					((TextBox)box).TextChanged += this.UpdateValue;
					Grid.SetColumn(box, 2);
					grid.Children.Add(box);
					this.value = number;
				}
			}
		}


		public class EditableObjectInfo
		{
			public InspectorCard card = null;
			public object obj;
			public EditableObjectInfo(object obj, InspectorCard card)
			{
				this.card = card;
				this.obj = obj;
				;
			}
		}
		public class InspectorCard
		{
			public int _index = 0;
			public UIElement root = new Grid();
			public UIElement current = null;
			public Dictionary<string, InspectorField> Fields = new Dictionary<string, InspectorField>();
			public IEditable sourceObj = null;
			public InspectorCard()
			{
				current = root;
				//((Grid)current).Width = new GridLength(1, GridUnitType.Auto);
			}
			public object getValue(string fieldName) // si il s agit d une checkbox, il retourne le bool to string, et pour le reste, il retourne la string.
			{
				return (Fields.ContainsKey(fieldName) ? Fields[fieldName] : null);
			}
			public void AppendField(string fieldName, InspectorField field)
			{
				Console.WriteLine("Append field:" + fieldName);
				Fields.AddOrRetrieve(fieldName, field);
				if (current.GetType() == typeof(Grid))
				{
					
					Console.WriteLine("adding field:" + fieldName);
					Grid grid = (Grid)current;
					RowDefinition def = new RowDefinition();
					def.Height = new GridLength(1, GridUnitType.Auto);
					ColumnDefinition widthDef = new ColumnDefinition();
					widthDef.Width = new GridLength(1, GridUnitType.Auto);
					grid.ColumnDefinitions.Add(widthDef);
					grid.RowDefinitions.Add(def);
					
					if (fieldName != "")
					{
						Grid subGrid = new Grid();
						ColumnDefinition def2 = new ColumnDefinition();
						def2.Width = new GridLength(100, GridUnitType.Pixel);
						subGrid.ColumnDefinitions.Add(def2);
						def2 = new ColumnDefinition();
						def2.Width = new GridLength(1, GridUnitType.Auto);
						subGrid.ColumnDefinitions.Add(def2);
						if (!(field is InspectorField.ButtonField))
						{
							Label label = new Label();
							label.Background = (Color.LightGray * 1.0002f).Window().Brush();
							label.Content = fieldName;
							Grid.SetColumn(label, 0);


						

							subGrid.Children.Add(label);
						}
						Border border = new Border();
						border.Child = field.control;
						border.CornerRadius = new CornerRadius(5);
						border.BorderThickness = new Thickness(5);
						subGrid.Background = (Color.LightGray * 1.0002f).Window().Brush();
						border.BorderBrush = Color.SlateGray.Window().Brush();
						Grid.SetColumn(border, 1);
						subGrid.Children.Add(border);

						border = new Border();
						border.Child = subGrid;
						border.BorderThickness = new Thickness(5);
						border.BorderBrush = (Color.LightGray * 0.9998f).Window().Brush();
						Grid.SetRow(border, _index);
						Grid.SetColumn(border, 0);
						grid.Children.Add(border);
						
					}
					else
					{

						Grid.SetRow(field.control, _index);
						Grid.SetColumn(field.control, 0);
						grid.Children.Add(field.control);
					}
					_index++;
				}
			}
			public void Map(IEditable obj)
			{
				Console.WriteLine("MAP");
				currentCard = this;
				((Grid)root).Tag = new EditableObjectInfo(obj, this);
				MapType(obj.GetType());
				/*Console.WriteLine("heyyyohhh");
				MappingMode last = State;
				State = MappingMode.Mapping;
				obj.OnInspectorGUI();
				State = last;*/
				this.sourceObj = obj;
				Console.WriteLine("MAP0");
			}
			public void Populate(IEditable obj)
			{
				Console.WriteLine("WRITE");
				currentCard = this;
				((Grid)root).Tag = new EditableObjectInfo(obj, this);
				//MapType(obj.GetType());
				Console.WriteLine("heyyyohhh");
				MappingMode last = State;
				State = MappingMode.Write;
				obj.OnInspectorGUI();
				State = last;
				this.sourceObj = obj;
				Console.WriteLine("WRITE0");
			}
			public void Read()
			{
				Console.WriteLine("READ");
				if (sourceObj == null)
					return;
				currentCard = this;
				MappingMode last = State;
				State = MappingMode.Read;
				sourceObj.OnInspectorGUI();
				State = last;
				Console.WriteLine("READ0");
			}
		}
		private enum MappingMode
		{
			Mapping,
			Read,
			Write
		}
		private static MappingMode State = MappingMode.Mapping;
		private static Type current = null; // cest pas un type courrant c'est une InspectorCard current.
		//
		private static InspectorCard currentCard = null;
		private static bool mapping = false;
		private static Dictionary<Type, InspectorCard> mappedTypes = new Dictionary<Type, InspectorCard>();
		/*public static void ReadCard(InspectorCard card, IEditable obj)
		{
			MappingMode last = State;
			State = MappingMode.Read;
			currentCard = card;
			obj.OnInspectorGUI();
			State = last;
		}
		public static void WriteCard(InspectorCard card, IEditable obj)
		{
			MappingMode last = State;
			State = MappingMode.Write;
			currentCard = card;
			obj.OnInspectorGUI();
			State = last;
		}*/
		public static InspectorCard MapType(Type type)
		{

			//InspectorCard currentCard = new InspectorCard();
			MappingMode last = State;
			State = MappingMode.Mapping;
			Console.WriteLine("mapping:" + type);
			object instance = type.CreateDefault();
			Console.WriteLine("ohh??");
			current = type;
			if (type.GetInterfaces().Contains(typeof(IEditable)))
			{
				Console.WriteLine("is ieditable");
				current = type;
				((IEditable)instance).OnInspectorGUI();
			}
			State = last;
			return (currentCard);
		}
		//
		public static InspectorCard GetCard(Type type)
		{
			current = type;
			mappedTypes.AddOrReplace(type, new InspectorCard());
			return (mappedTypes.ContainsKey(type) ? mappedTypes[type] : MapType(type));
		}


		//se recomporte comme avec un objet standard (créé un expander et peuple de façon standard).
		public static object CustomObjectField(string label, object value)
		{
			// c tt bete: le root de la property c est la value.
			// on gere tout pareil qu'en inspecteur standard, sauf que c'est simplement pas lié a un behaviour.
			//
			// ici pas encore compris comment on va le mapper.
			;
			return (value);
		}
		public static bool Toggle(string label, bool value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.ToggleField(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.ToggleField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return (((InspectorField.ToggleField)(card.Fields[label])).value);
			}
			return (value);
		}

		public static int IntField(string label, int value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.IntField(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.IntField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((int)((InspectorField.IntField)(card.Fields[label])).value);
			}
			return (value);
		}
		public static long LongField(string label, long value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.IntField(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.IntField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return (((InspectorField.IntField)(card.Fields[label])).value);
			}
			return (value);
		}
		public static float FloatField(string label, float value)
		{

			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.FloatField(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.FloatField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((int)((InspectorField.FloatField)(card.Fields[label])).value);
			}
			return (value);
		}
		public static T BehaviourField<T>(string label, T value) where T : GameBehaviour
		{

			return (value);
		}
		public static T RessourceField<T>(string label, T value) where T : SharedResource
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				/* Texture)
				{
					;
				}*/
				card.AppendField(label, new InspectorField.RessourceField(((SharedResource)value), typeof(T), card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.RessourceField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((T)((InspectorField.RessourceField)(card.Fields[label])).value);
			}
			return (value);
		}
		/*public static T BehaviourCombinedField<T>(string label, T value) where T : GameBehaviour
		{
			return (value);
		}
		public static T RessourceCombinedField<T>(string label, T value) where T : SharedResource
		{
			return (value);
		}*/
		public static float Slider(string label, float value, float min, float max)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.NumberSlider(value, min, max, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.NumberSlider)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((float)((InspectorField.NumberSlider)(card.Fields[label])).value);
			}
			return (value);
		}
		public static void LabelField(string label)
		{
			return;
		}
		public static string PassWordField(string label, string value)
		{
			return (value);
		}
		public static string TextField(string label, string value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.TextField(value));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.TextField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((string)((InspectorField.TextField)(card.Fields[label])).value);
			}
			return (value);
		}
		public static string TextArea(string label, string value)
		{
			return (value);
		}
		public static Enum EnumField(string label, Enum value)
		{
			return (value);
		}

		public static Vector2 Vector2Field(string label, Vector2 value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.Vector2Field(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.Vector2Field)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((Vector2)((InspectorField.Vector2Field)(card.Fields[label])).value);
			}
			return (value);
		}
		public static Vector3 Vector3Field(string label, Vector3 value)
		{
			Console.WriteLine("Vector3Field:" + State.ToString());
			if (State == MappingMode.Mapping)
			{
				Console.WriteLine("im appending field");
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.Vector3Field(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.Vector3Field)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((Vector3)((InspectorField.Vector3Field)(card.Fields[label])).value);
			}
			return (value);
		}
		public static Vector4 Vector4Field(string label, Vector4 value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.Vector4Field(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.Vector4Field)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((Vector4)((InspectorField.Vector4Field)(card.Fields[label])).value);
			}
			return (value);
		}
		public static Quaternion QuaternionField(string label, Quaternion value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.QuaternionField(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.QuaternionField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((Quaternion)((InspectorField.QuaternionField)(card.Fields[label])).value);
			}
			return (value);
		}
		public static void Button(string label, ButtonHandler myButton)
		{

			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.ButtonField(label, myButton, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.ButtonField)(card.Fields[label])).handler = myButton;
				}
			}
			//throw new NotImplementedException();
		}

		public static Quaternion EulerField(string label, Quaternion value)
		{
			if (State == MappingMode.Mapping)
			{
				InspectorCard card = currentCard;
				card.AppendField(label, new InspectorField.EulerField(value, card));
			}
			if (State == MappingMode.Write)
			{
				InspectorCard card = currentCard;
				if (card.Fields.ContainsKey(label))
				{
					((InspectorField.EulerField)(card.Fields[label])).value = value;
				}
			}
			if (State == MappingMode.Read)
			{
				InspectorCard card = currentCard;
				return ((Quaternion)((InspectorField.EulerField)(card.Fields[label])).value);
			}
			return (value);
		}
		public static Color ColorField(string label, Color value)
		{
			return (Color.White);
		}
		public static Rect RectField(string label, Rect value)
		{
			return (value);
		}
		public static Bounds BoundsField(string label, Bounds value)
		{
			;
			return (value);
		}
		public static void BeginExpandableGroup(string header)
		{
			;
		}
		public static void EndExpandableGroup()
		{
			;
		}
		public static void BeginScrollView()
		{
			;
		}
		public static void EndScrollView()
		{
			;
		}
		public static void Space()
		{
			;
		}





	}
}
