﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Security.Policy;
using System.Security;
using System.Security.Permissions;
using FastMember;
using Microsoft.CodeAnalysis;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms.Integration;
using Goliat;
using Goliat.Serialization;
using System;

namespace Goliat.Engine.GUI
{

	public class InspectorTools
	{
		public static Expander generateExpander(CustomProperty root)
		{

			Expander res = new Expander();
			res.Header = root.name;
			//Console.WriteLine("HEader:" + res.Header);
			//res.IsExpanded = false;
			//Console.WriteLine("header:" + res.Header);
			//res.Content = "yoyo";

			res.Tag = root;
			res.Expanded += Property_Expand;
			if (root.expanded)
			{
				Console.WriteLine("IM EXPANDED WTF??");
				//res.IsExpanded = true;
			}
			return (res);
		}
		public static UIElement generateProperty(CustomProperty root)
		{

			if (root.value.isNumber() || root.value is bool || root.value is string)
			{
				Grid res = new Grid();
				Label name = new Label();
				name.Content = root.name;
				Grid.SetColumn(name, 0);
				res.Children.Add(name);
				TextBox value = new TextBox();
				value.Width = 45;
				value.Height = 20;
				value.Tag = root;
				value.Text = root.value.ToString();
				value.TextChanged += Property_ChangedValue;
				Grid.SetColumn(value, 1);
				res.Children.Add(value);
				ColumnDefinition def = new ColumnDefinition();
				def.Width = new GridLength(0, GridUnitType.Auto);

				ColumnDefinition def2 = new ColumnDefinition();
				def2.Width = new GridLength(0, GridUnitType.Auto);
				res.ColumnDefinitions.Add(def);
				res.ColumnDefinitions.Add(def2);
				return (res);
			}
			else
			{
				//Console.WriteLine("generating expander:");
				return (generateExpander(root));
			}
		}
		public static void Property_Expand(object sender, System.Windows.RoutedEventArgs e)
		{
			//((Expander)sender).IsExpanded = true;
			CustomProperty root = (CustomProperty)((Expander)sender).Tag;
			//root.expanded = true;
			if (root.sons == null || root.sons.Count == 0)
			{
				root.generateSons();
			}
			if (root.sons == null || root.sons.Count == 0)
			{
				Console.WriteLine("sons null???");
				return;
			}

			Expander current = ((Expander)sender);
			Console.WriteLine("expanding:" + (current.Header));
			Grid description = new Grid();

			current.Content = description;
			for (int i = 0; i < root.sons.Count; i++)
			{
				Console.WriteLine("son name:" + root.sons[i].name);
				UIElement son = generateProperty(root.sons[i]);

				Grid.SetRow(son, i);
				RowDefinition def = new RowDefinition();
				def.Height = new GridLength(1, GridUnitType.Auto);
				description.RowDefinitions.Add(def);
				description.Children.Add(son);
				;
			}
			e.Handled = true;
			//((Expander)sender).IsExpanded = true;


			/*if (root.value.IsValueType() && (root.value is string || root.value is bool || root.value.isNumber()))
			{
				;
			}
			else
			{
				;
			}*/
		}
		public static void Property_ChangedValue(object sender, System.Windows.RoutedEventArgs e)
		{
			;
		}
		public static Expander generateExpander(CustomProperty root, bool expanded)
		{
			Expander res = new Expander();
			res.Header = root.name;
			Console.WriteLine("header:" + res.Header);
			res.Tag = root;
			//res.IsExpanded = false;
			res.Expanded += Property_Expand;
			if (expanded || root.expanded)
			{
				res.IsExpanded = true;
			}
			return (res);
		}
	}
}
