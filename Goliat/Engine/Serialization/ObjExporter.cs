﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat;
using System.IO;

namespace Goliat
{
	public class ObjExporter
	{

		public static void ExportObj(Mesh mesh, string filePath)
		{
			string stringMesh = MeshToString(mesh);
			File.WriteAllText(filePath, stringMesh);
		}
		/*public static void Parse(Mesh mesh, string meshString, ref int index)
		{
			;
		}*/
		public static Vector3 ParseVector3(string meshString, ref int index)
		{
			Vector3 res = Vector3.zero;
			try
			{
				string line = StringParser.GetNextLine(meshString, ref index);
				//line = StringParser.Replace(line, "\n", "");
				//string trimmed = StringParser.Replace(line, )

				List<string> splitted = StringParser.Split(line, " ");
				for (int i = 0; i < splitted.Count; i++)
				{

					//Debug.Log(Serializer.CustomProperty.logPath, "splitted[" + i + "] = ;" + splitted[i] + ";");
					res[i] = (float)System.Convert.ChangeType(StringParser.Replace(splitted[i], ".", ","), typeof(float));
				}
			}
			catch (Exception e)
			{
				;//Debug.Log(Serializer.CustomProperty.logPath,"mesh exception:" + e.ToString());
			}
			return (res);
		}
		public static string ParseEndOfLine(string meshString, ref int index)
		{
			string res = StringParser.GetNextLine(meshString, ref index);
			return (res);
		}
		public static Vector2 ParseVector2(string meshString, ref int index)
		{

			Vector2 res = Vector2.zero;
			try
			{
				string line = StringParser.GetNextLine(meshString, ref index);
				//line = StringParser.Replace(line, "\n", "");
				//string trimmed = StringParser.Replace(line, )

				List<string> splitted = StringParser.Split(line, " ");
				for (int i = 0; i < splitted.Count; i++)
				{
					res[i] = (float)System.Convert.ChangeType(StringParser.Replace(splitted[i], ".", ","), typeof(float));

				}
			}
			catch (Exception e)
			{
				;//Debug.Log(Serializer.CustomProperty.logPath, "mesh exception:" + e.ToString());
			}
			return (res);
		}
		public static Face ParseFace(string meshString, ref int index)
		{

			Face res = new Face();
			try
			{
				string line = StringParser.GetNextLine(meshString, ref index);
				//line = StringParser.Replace(line, "\n", "");
				//string trimmed = StringParser.Replace(line, )
				//Debug.Log(Serializer.CustomProperty.logPath, "here is a face:" + line + "\n");
				List<string> splitted = StringParser.Split(line, " ");
				res.indices = new int[splitted.Count];
				//Debug.Log(Serializer.CustomProperty.logPath, "and its values:");
				for (int i = 0; i < splitted.Count; i++)
				{
					res.indices[i] = (int)System.Convert.ChangeType(splitted[i], typeof(int)) - 1;
					//Debug.Log(Serializer.CustomProperty.logPath, "" + res.indices[i]);
				}
				//Debug.Log(Serializer.CustomProperty.logPath, "end");
			}
			catch (Exception e)
			{
				;//Debug.Log(Serializer.CustomProperty.logPath, "mesh exception?" + e.ToString());
			}
			return (res);
		}
		public static Mesh StringToMesh(string meshString)
		{
			//Debug.Log(Serializer.CustomProperty.logPath, "here mesh:" + meshString);
			Mesh res = new Mesh();
			int i = 0;
			int materialIndex = 0;
			List<string> materialNames = new List<string>();
			List<Vector2> uvs = new List<Vector2>();
			List<Vector3> normals = new List<Vector3>();
			List<Vector3> vertices = new List<Vector3>();
			List<Face> faces = new List<Face>();
			try
			{
				//foreach (string line in File.ReadAllLines(path))
				//Debug.Log(Serializer.CustomProperty.logPath, "obj here:" + meshString);
				while (i < meshString.Length)
				{
					if (StringParser.CheckOccurence(meshString, "usemtl", ref i))
					{
						i = i + 1;
						string materialName = ParseEndOfLine(meshString, ref i);
						if (!materialNames.Contains(materialName))
						{
							materialNames.Add(materialName);
						}
						for (int j = 0; j < materialNames.Count; j++)
						{
							if (materialName == materialNames[j])
							{
								materialIndex = j;
							}
						}
					}
					//Debug.Log(Serializer.CustomProperty.logPath, "line:" + StringParser.GetNextLine(meshString, i));
					if (StringParser.CheckOccurence(meshString, "vt", ref i))
					{
						i = i + 1;

						Vector2 tmp = ParseVector2(meshString, ref i);
						tmp.y = 1 - tmp.y;
						uvs.Add(tmp);
						//Debug.Log(Serializer.CustomProperty.logPath, "found uv:" + uvs[uvs.Count - 1]);
					}
					else if (StringParser.CheckOccurence(meshString, "vn", ref i))
					{
						i = i + 1;
						normals.Add(ParseVector3(meshString, ref i));
						//Debug.Log(Serializer.CustomProperty.logPath, "found normal:" + normals[normals.Count - 1]);
					}
					else if (StringParser.CheckOccurence(meshString, "v", ref i))
					{
						i = i + 1;
						vertices.Add(ParseVector3(meshString, ref i));
						//Debug.Log(Serializer.CustomProperty.logPath, "found vertice:" + vertices[vertices.Count - 1]);
					}
					else if (StringParser.CheckOccurence(meshString, "f", ref i))
					{
						i = i + 1;
						Face face = ParseFace(meshString, ref i);
						face.materialIndex = materialIndex;
						faces.Add(face);
						//Debug.Log(Serializer.CustomProperty.logPath, "found face:" + faces[faces.Count - 1]);
					}
					else
					{
						StringParser.GetNextLine(meshString, ref i);
					}
				}
			}
			catch (Exception e)
			{
				;//Debug.Log(Serializer.CustomProperty.logPath, "mesh exception:" + e.ToString());
			}
			res.vertices = vertices.ToArray();
			res.normals = normals.ToArray();
			res.faces = faces.ToArray();
			res.uvs = uvs.ToArray();
			return (res);
		}

		public static string MeshToString(Mesh m)
		{
			//			Vector3 s = t.localScale;
			//		Vector3 p = t.localPosition;
			//	Quaternion r = t.localRotation;

			string currentMat = "material0";
			int numVertices = 0;
			/*Mesh m = mf.sharedMesh;
			if (!m)
			{
				return "####Error####";
			}*/
			//Material[] mats = mf.renderer.sharedMaterials;
			StringBuilder sb = new StringBuilder();
			sb.Append("g\n");


			foreach (Vector3 vv in m.vertices)
			{
				Vector3 v = vv;//t.TransformPoint(vv);
				numVertices++;
				string tmp = string.Format("v {0} {1} {2}\n", v.x, v.y, v.z);
				tmp = StringParser.Replace(tmp, ",", ".");
				sb.Append(tmp);
			}
			sb.Append("\n");
			foreach (Vector3 nn in m.normals)
			{
				Vector3 v = nn;
				string tmp = string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z);
				tmp = StringParser.Replace(tmp, ",", ".");
				sb.Append(tmp);//string.Format("vn {0} {1} {2}\n", -v.x, -v.y, v.z));
			}
			sb.Append("\n");
			foreach (Vector2 v in m.uvs)
			{
				string tmp = string.Format("vt {0} {1}\n", v.x, (-v.y) + 1);
				tmp = StringParser.Replace(tmp, ",", ".");
				sb.Append(tmp);//string.Format("vn {0} {1} {2}\n", -v.x, -v.y, v.z));
				//sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));

			}
			for (int i = 0; i < m.faces.Length; i++)
			{
				string facemat = "material" + m.faces[i].materialIndex;
				if (facemat != currentMat)
				{
					sb.Append("usemtl " + facemat);
					currentMat = facemat;
				}
				sb.Append("f ");
				for (int j = 0; j < m.faces[i].indices.Length; j++)
				{
					sb.Append("" + (m.faces[i].indices[j] + 1));
					//sb.Append("/" + (m.faces[i].indices[j] + 1));
					//sb.Append("/" + (m.faces[i].indices[j] + 1));
					if (j != m.faces[i].indices.Length - 1)
					{
						sb.Append(" ");
					}
					else
					{
						sb.Append("\n");
					}
					//						sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
					//		triangles[i] + 1 + StartIndex, triangles[i + 1] + 1 + StartIndex, triangles[i + 2] + 1 + StartIndex));
				}
			}

			//StartIndex += numVertices;
			return sb.ToString();
		}
	}
}
