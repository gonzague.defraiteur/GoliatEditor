﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Goliat
{
	public class Time
	{
		private static Stopwatch sw = new Stopwatch();
		private static float _deltaTime = 0;
		public static float deltaTime
		{
			get
			{
				return (_deltaTime);
			}
		}
		private static float lastTime = time;
		public static float time
		{
			get
			{
				return ((float)(DateTime.UtcNow.Subtract(DateTime.Today).TotalMilliseconds));
			}
			set
			{
				;
			}
		}
		public static float iter = 0;
		public static void update()
		{
			StackTrace stackTrace = new StackTrace();
			iter++;
			if (iter >= 10)
			{
				iter = 0;
				sw.Stop();
				double timeslice = sw.Elapsed.TotalMilliseconds;
				_deltaTime = (float)timeslice / 8000;
				sw.Reset();
				sw.Start();
			}
		}
	}
}
