﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat;
using Vector3D = Assimp.Vector3D;
using System.IO;
using System.Runtime.InteropServices;
using Assimp;

//http://thomasdiewald.com/blog/?p=1888 pour mesh colliders, perfecto.

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using Config = OpenTK.Configuration;
using Utilities = OpenTK.Platform.Utilities;


using Vector3 = Goliat.Vector3;
using Quaternion = Goliat.Quaternion;
using Matrix = Goliat.Matrix;
using Color = Goliat.Color;
using Goliat.Serialization;
using Goliat.Engine.GUI;
using InspectorCard = Goliat.Engine.GUI.InspectorGUI.InspectorCard;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using RigidBody = Jitter.Dynamics.RigidBody;
namespace Goliat
{
	public class ImpactInfo
	{
		public List<Transform> hits = new List<Transform>();
		public List<float> distances = new List<float>();
		public List<Vector3> points = new List<Vector3>();
	}
	public enum Space
	{
		self, world
	}

	public class GameObject : GameBehaviour
	{

	}

	public class BlendShape
	{
		public float value
		{
			get;
			set;
		}
		public string name = "";
		public Vector3[] fullShape = null;

	}

	public class BlendShapes
	{
		private Dictionary<string, BlendShape> blendShapes = new Dictionary<string,BlendShape>(); 
		public float this[int index]
		{
			get
			{
				float res = 0;
				try
				{
					res = ((BlendShape)this.blendShapes.ElementAt(index).Value).value;
				}
				catch (Exception e)
				{
					//LogException()
				}
				return (res);
			}
			set
			{
				this.blendShapes[this.blendShapes.ElementAt(index).Key].value = value;
			}
		}
		public float this[string name]
		{
			get
			{
				return ((((BlendShape)this.blendShapes.TryGetValueOrNull(name))).value);
			}
			set
			{
				BlendShape tmp = (((BlendShape)this.blendShapes.TryGetValueOrNull(name)));
				if (tmp != null)
				{
					tmp.value = value;
				}
			}
		}
		public BlendShape getSnapShot(int blendShapeIndex, float value)
		{
			;
			return (null);
		}
		public BlendShape getSnapShot(string blendShapeName, float value)
		{
			;
			return (null);
		} ///yeyeye trop dark x)

	}

	[Goliat.Serializable]
	[System.Serializable]
	public class SkinnedMeshFilter : MeshFilter
	{
		private void AddBonesRecursively(Transform root)
		{
			for (int i = 0; i < root.children.Count; i++)
			{
				Console.WriteLine("root got childs");
				if (root.children[i].GetType().IsSameOrSubclass(typeof(Bone)))
				{
					Console.WriteLine("found bone");

					if (root.children[i].GetType().IsSameOrSubclass(typeof(Bone)) && ((Bone)root.children[i]).skins.ContainsKey(this.sharedMesh.guid))
					{
						Console.WriteLine("found matching bone");
						this._bones.Add((Bone)root.children[i]);
					}
					else
					{
						Console.WriteLine("my guid:" + this.sharedMesh.guid);
						foreach (SkinInfo info in ((Bone)root.children[i]).skins.Values)
						{
							Console.WriteLine("found skin for:" + info.meshGuid);
						}
					}
				}
				AddBonesRecursively(root.children[i]);
			}
		}
		[Goliat.NonSerialized]
		private List<Bone> _bones = null;
		[Goliat.NonSerialized]
		public List<Bone> bones
		{
			get
			{
				if (this.sharedMesh == null)
				{
					return (null);
				}
				if (this._bones == null)
				{
					//Console.WriteLine("Constructing bones");
					this._bones = new List<Bone>();
					if (this.GetComponent<Transform>() == null)
					{
						Console.WriteLine("transform null");
						for (int i = 0; i < this.components.Count; i++)
						{
							Console.WriteLine("this component:" + this.components[i]);
						}
					}
					Transform root = this.GetComponent<Transform>().root;
					this.AddBonesRecursively(root);
				}
				return ((List<Bone>)_bones.Clone());
			}
			private set
			{
				this._bones = value;
			}
		}
 		public Bounds bounds
		{
			get
			{
				return (null);
			}
		}
		public int quality = 4;
		public bool updateWhenOffscreen = true;
		public Mesh BakeMesh()
		{
			return (null);
		}
		public SkinnedMeshFilter()
		{
			;
		}
		public BlendShapes blendShapes;
	} // faut lui trouver un autre nom?
	//
	// du coup: si on veut ça, on fait un dico <string, List<SkinnedMeshFilter>>
	// un autre <string, Bone>
	/*
	 * 
	 * on remplit les deux au fur et a mesure.
	 * et fuck si c est pas beau.
	 * 
	 * a la fin on reset les dicos de chacun.
	 * 
	 */

	public class SkinInfo
	{
		public List<VertexWeight>	weights = new List<VertexWeight>();
		public List<int>			meshSibling = new List<int>();
		public Matrix				offsetMatrix = Matrix.Identity;
		public string meshGuid = "";
		public Bone			bone = null;


		private SkinnedMeshFilter	_filter = null;
		private SkinnedMeshFilter getFilter()
		{
			Transform current = this.bone.root;
			List<int> sibling = (List<int>)meshSibling.Clone();
			while (sibling.Count > 0)
			{
				current = current.children[sibling[0]];
				sibling.RemoveAt(0);
			}
			return (current.GetComponent<SkinnedMeshFilter>());
		}
		[Goliat.NonSerialized]
		public SkinnedMeshFilter	filter
		{
			get
			{
				if (_filter == null)
				{
					_filter = this.getFilter();
				}
				return (this._filter);
			}
		}

		public SkinInfo(string guid, List<VertexWeight> weights, List<int> meshSibling, Matrix offsetMatrix, Bone parent)
		{
			this.meshGuid = guid;
			this.weights = (List<VertexWeight>)weights.Clone();
			this.meshSibling = (List<int>)meshSibling.Clone();
			this.offsetMatrix = offsetMatrix;
			this.bone = parent;
		}
	}
	[Goliat.Serializable]
	[System.Serializable]
	public class Bone : Transform, IRegenerable
	{
		[Goliat.NonSerialized]
		public Dictionary<string, SkinInfo> skins = new Dictionary<string, SkinInfo>(); // guid, float[];
		public SkinInfo getSkinInfo(SkinnedMeshFilter filter)
		{
			if (filter == null || filter.sharedMesh == null)
				return (null);
			return (skins.ContainsKey(filter.sharedMesh.guid) ? skins[filter.sharedMesh.guid] : null);
		}

		public void OnClose(Goliat.Serialization.Serializer.SerializationInfo info)
		{
			return;
			//int total = 0;
			List<int> counts = new List<int>();
			int total = 0;
			int offsetLength = skins.Keys.Count * 16;
			int totalSibling = 0;
			float[] offsets = new float[offsetLength];
			foreach (string key in skins.Keys)
			{
				SkinInfo sinfo = skins[key];
				counts.Add(sinfo.weights.Count);

				total += sinfo.weights.Count;
				totalSibling += sinfo.meshSibling.Count + 1;
			}
			float[] weights = new float[total];
			float[] siblings = new float[totalSibling];
			int j = 0;
			int k = 0;
			int current = 0;
			string[] keys = new string[skins.Keys.Count];
			foreach (string key in skins.Keys)
			{
				SkinInfo sinfo = skins[key];
				float[] array = skins[key].offsetMatrix.array;
				for (int i = 0; i < array.Length; i++)
				{
					offsets[(k * 16) + i] = array[i];
				}
				siblings[current] = sinfo.meshSibling.Count;
				current++;
				for (int i = 0; i < sinfo.meshSibling.Count; i++)
				{
					siblings[current + i] = sinfo.meshSibling[i];
				}
				current += sinfo.meshSibling.Count;
				offsets.Append(skins[key].offsetMatrix.array);
				keys[k] = key;
				for (int i = 0; i < sinfo.weights.Count; i++)
				{
					weights[j] = sinfo.weights[i].index;
					weights[j + 1] = sinfo.weights[i].weight;
					j += 2;
				}
				k++;
			}

			info.AddValue("keys", keys);
			//total += counts.Count * 16;
			info.AddValue("skinCounts", counts);
			//

			info.AddValue("weights", weights);

			info.AddValue("offsets", offsets);
			info.AddValue("siblings", siblings);
			// sibling: 4 0 1 2 3 5 0 1 2 3 4 ...

			//float[] weights = new float[];

			//info.AddValue();
			;
		}
		public void OnRegen(Goliat.Serialization.Serializer.SerializationInfo info)
		{
			return;
			//this.name = info.GetValue("name");
			dynamic keys = info.GetValue("keys");
			dynamic skinCounts = info.GetValue("skinCounts");
			dynamic weights = info.GetValue("weights");
			dynamic offsets = info.GetValue("offsets");
			dynamic siblings = info.GetValue("siblings");

			int weightoffset = 0;
			int offsetmatrixsoffset = 0;
			int j = 0;
			int currentSibling = 0;
			foreach (string key in keys)
			{
				int siblingLen = siblings[currentSibling];
				currentSibling++;
				List<int> sibling = new List<int>();
				for (int i = 0; i < siblingLen; i++)
				{
					sibling.Add(siblings[i + currentSibling]);
				}
				currentSibling += siblingLen;
				List<VertexWeight> skinweights = new List<VertexWeight>();
				for (int i = 0; i < skinCounts[j]; i++)
				{
					skinweights.Add(new VertexWeight(weights[weightoffset + (i * 2)], weights[(i * 2) + 1]));
				}
				weightoffset += skinCounts[j];
				Matrix mat = Matrix.Identity;
				for (int i = 0; i < 16; i++)
				{
					mat[i] = offsets[i + offsetmatrixsoffset];
				}
				offsetmatrixsoffset += 16;
				this.skins.Add(key, new SkinInfo(key, skinweights, sibling, mat, this));
				j++;
				;
			}
		}

		[Goliat.NonSerialized]
		public Bone rootBone
		{
			get
			{
				if (this.parent != null && this.parent.GetType() == this.GetType())
				{
					return (((Bone)this.parent).rootBone);
				}
				else
				{
					return (this);
				}
			}
		}

		[Goliat.NonSerialized]
		public Matrix localToRootMatrix
		{
			get
			{
				Matrix res = new Matrix(4, 4);
				if (this.parent != null && this.parent.GetType().IsSameOrSubclass(typeof(Bone)))
					res = ((Bone)this.parent).localToRootMatrix * (this.transformation);
				else
					res = (this.transformation);
				return (res);
			}
			set
			{
				;
			}
		}
		public bool drawBone = false;
		public readonly Vector3 originalPosition = Vector3.zero;
		public readonly Quaternion originalRotation = Quaternion.identity;
		public readonly Vector3 originalScale = Vector3.one;
		public Bone(SceneBone boneInfo) : base((Scene)boneInfo)
		{
			this.originalPosition = this.localPosition;
			this.originalRotation = this.localRotation;
			this.originalScale = this.localScale;
			foreach (MeshInfo key in boneInfo.offsetMatrixs.Keys)
			{
				string guid = key.mesh.guid;
				List<VertexWeight> weights = boneInfo.weights[key];

				List<int> sibling = key.sceneNode.getSibling();
				Matrix offsetMatrix = boneInfo.offsetMatrixs[key];
				Bone parent = this;
				this.skins.Add(guid, new SkinInfo(guid, weights, sibling, offsetMatrix, parent));
			}
		}
		public Bone()
		{
			this.originalPosition = this.localPosition;
			this.originalRotation = this.localRotation;
			this.originalScale = this.localScale;
		}
	}

	public class ButtonHandler
	{
		//public delegate void ButtonClicked();
		public System.Windows.RoutedEventHandler Clicked;
	}

	[Goliat.Serializable]
	[System.Serializable]
	public class Transform : GameBehaviour, IRestorable, IEditable, ISerializable
	{
		public void DebugMyID(object sender, System.Windows.RoutedEventArgs e)
		{
			Console.WriteLine("MY ID:" + this.id);
		}
		public ButtonHandler myButton;
		public Texture myTexture = null;
		public void OnInspectorGUI()
		{
			this._localPosition = InspectorGUI.Vector3Field("Local Position:", this._localPosition);
			this._localScale = InspectorGUI.Vector3Field("Local Scale:", this._localScale);
			this._localRotation = InspectorGUI.EulerField("Local Rotation:", this._localRotation);
			this.isActive = InspectorGUI.Toggle("is Active:", this.isActive);
			this.myTexture = InspectorGUI.RessourceField<Texture>("texture?", this.myTexture);
			if (this.myButton == null)
			{
				this.myButton = new ButtonHandler();
				this.myButton.Clicked += DebugMyID;
			}
			InspectorGUI.Button("Some Button:", this.myButton);
			Console.WriteLine("isactive:" + this.isActive);
			this._updateFromLocal();
		}
		private bool _isActive = true;
		public bool isActive
		{
			get
			{
				return (this._isActive);
			}
			set
			{
				this._isActive = value;
			}
		}

		public void OnSave()
		{
			;
		}
		public void OnSerialize(Serializer.SerializationInfo info)
		{
			//Console.WriteLine("on serialize:" + this.name);
			List<Transform> childrens = new List<Transform>();
			for (int i = 0; i < this.children.Count; i++)
			{
				childrens.Add(this.children[i]);
			}
			info.AddValue("name", this.name);
			//Console.WriteLine("adding children:");
			info.AddValue("children", childrens);
			//Console.WriteLine("children added");
			//Console.WriteLine("adding components");
			info.AddValue("components", this.components);
			//Console.WriteLine("components added");
			//Console.WriteLine("adding parent for:" + this.name);
			//Console.WriteLine("witch is null?" + (_parent == null));
			info.AddValue("parent", this._parent);
			//Console.WriteLine("parent added");
			info.AddValue("rotation", this._localRotation);
			info.AddValue("position", this._localPosition);
			info.AddValue("scale", this._localScale);
			Console.WriteLine("components:");
			
		}
		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			//Console.WriteLine("on deserialize");
			dynamic childrens = info.GetValue("children");
			dynamic components = info.GetValue("components");
			dynamic parent = info.GetValue("parent");
			dynamic rotation = info.GetValue("rotation");
			dynamic position = info.GetValue("position");
			dynamic scale = info.GetValue("scale");
			dynamic name = info.GetValue("name");
			this.name = name;
			this.children = new TransformCollection();
			this.components = components;
			this._parent = parent;
			this._localRotation = rotation;
			this._localScale = scale;
			this._localPosition = position;
			this._updateFromLocal();
			for (int i = 0; i < childrens.Count; i++)
			{
				this.children.Add(childrens[i]);
			}
			for (int i = 0; i < this.components.Count; i++)
			{
				Console.WriteLine("found:" + this.components[i] + "; witch got:");
				for (int j = 0; j < this.components[i].components.Count; j++)
				{
					Console.WriteLine("" + this.components[i].components[j]);
				}
			}
			this.OnRestore();
		}
		public void OnRestore()
		{
			if (!Transform.Transforms.Contains(this))
			{
				Transform.Transforms.Add(this);
			}
		}

		private static List<Transform> _roots = new List<Transform>();
		public static List<Transform> roots
		{
			get
			{
				if (_roots == null)
				{
					_roots = new List<Transform>();
				}
				return ((List<Transform>)Transform._roots.Clone());
			}
			set
			{
				Transform._roots = value;
			}
		}
		private static Color4 baseColor = Color.LightGray.gl();
		private static Transform arrowsTransform = Transform.zero;
		public static bool testPhysic = true;
		public static Transform groundBox = null;
		public static Transform littleBox = null;
		public static System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
		public static bool physicInited = false;
		public static List<TestCollider> testPhysicEntities = new List<TestCollider>();
		public static Jitter.World world = null;
		public class TestCollider
		{
			public Vector3 position
			{
				get
				{
					return (this.rigidbody.Position.goliat());
				}
			}
			public Quaternion rotation
			{
				get
				{
					return (this.rigidbody.Orientation.goliat().quaternion);
				}
			}
			public Vector3 size = Vector3.one;
			public Jitter.Dynamics.RigidBody rigidbody = null;
			public TestCollider(Vector3 size, bool isStatic = false, bool useGravity = true, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
			{
				if (position == default(Vector3))
				{
					position = Vector3.zero;
				}
				if (rotation == default(Quaternion))
				{
					rotation = Quaternion.identity;
				}
				BoxShape shape = new BoxShape(size.jitter());

				this.rigidbody = new Jitter.Dynamics.RigidBody(shape);
				this.rigidbody.Position = position.jitter();
				//this.rigidbody.Orientation = rotation.orthogonalMatrix.jitter;
				this.rigidbody.AffectedByGravity = useGravity;
				this.rigidbody.IsStatic = isStatic;
				world.AddBody(this.rigidbody);
				this.size = size;
				;
			}

		}
		public static void RenderATriangle(Vector3 a, Vector3 b, Vector3 c)
		{
			GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);
			Vector3 normal = Vector3.GenerateNormal(new Vector3[] { a, b, c });
			GL.Vertex3(a.gl());
			GL.Normal3(normal.gl());
			GL.Color4(Color.Blue.gl());

			GL.Vertex3(b.gl());
			GL.Normal3(normal.gl());
			GL.Color4(Color.Blue.gl());

			GL.Vertex3(c.gl());
			GL.Normal3(normal.gl());
			GL.Color4(Color.Blue.gl());

			GL.End();
		}
		public static bool switchSphereTest = false;
		public static void TestRenderAllToBuffer()
		{
			int fb;
			int color_rb;
			int depth_rb;
			GL.Ext.GenFramebuffers(1, out fb);// .GenFramebuffersEXT(1, &fb);
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, fb);
			//Create and attach a color buffer
			GL.Ext.GenRenderbuffers(1, out color_rb);
			//We must bind color_rb before we call glRenderbufferStorageEXT
			GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, color_rb);
			//The storage format is RGBA8
			GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, RenderbufferStorage.Rgba8, 256, 256);//.RenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_RGBA8, 256, 256);
																												//Attach color buffer to FBO
			GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, RenderbufferTarget.RenderbufferExt, color_rb);// GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, color_rb);
																																									  //-------------------------
			GL.Ext.GenRenderbuffers(1, out depth_rb);
			GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, depth_rb);// .BindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth_rb);
			GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, RenderbufferStorage.DepthComponent24, 256, 256);
			//-------------------------
			//Attach depth buffer to FBO
			GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, RenderbufferTarget.RenderbufferExt, depth_rb);
			//-------------------------
			//Does the GPU support current FBO configuration?
			FramebufferErrorCode status;
			status = GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt); //glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
			switch (status)
			{
				case FramebufferErrorCode.FramebufferCompleteExt:
					Console.WriteLine("OK");
					break;
				default:
					Console.WriteLine("FRAME BUFFER ERROR:" + status.ToString());
					//HANDLE_THE_ERROR;
					break;
			}
			//-------------------------
			//and now you can render to the FBO (also called RenderBuffer)
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, fb);
			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			//-------------------------
			GL.Viewport(0, 0, 256, 256);
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(0.0, 256.0, 0.0, 256.0, -1.0, 1.0);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();
			//-------------------------
			GL.Disable(EnableCap.Texture2D);
			GL.Disable(EnableCap.Blend);
			GL.Enable(EnableCap.DepthTest);
			//-------------------------
			//**************************
			//RenderATriangle, {0.0, 0.0}, {256.0, 0.0}, {256.0, 256.0}
			//Read http://www.opengl.org/wiki/VBO_-_just_examples
			//RenderATriangle();
			//-------------------------
			RenderATriangle(Vector3.zero, new Vector3(256, 0, 0), new Vector3(256, 256, 0));
			byte[] pixels = new byte[4 * 4 * 4];

			GL.ReadPixels<byte>(0, 0, 4, 4, PixelFormat.Bgra, PixelType.Byte, pixels);
			for (int i = 0; i < pixels.Length / 4; i++)
			{
				byte b = pixels[i * 4];
				byte g = pixels[(i * 4) + 1];
				byte r = pixels[(i * 4) + 2];
				byte a = pixels[(i * 4) + 3];
				Console.WriteLine("index:" + i + "; Color:" + new Color(r, g, b, a) + ";");
			}
			//pixels 0, 1, 2 should be white
			//pixel 4 should be black
			//----------------
			//Bind 0, which means render to back buffer
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
			GL.Ext.DeleteRenderbuffers(1, color_rb.pointer());
			GL.Ext.DeleteRenderbuffers(1, depth_rb.pointer());
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
			GL.Ext.DeleteFramebuffers(1, fb.pointer());
		}
		private static FrameBufferObject tmp = null;

		public static List<int> GetSelection(Rect rect, Camera cam)
		{
			List<int> res = new List<int>();
			Color[,] block = GetSelectionBlock(rect, cam);
			for (int i = 0; i < block.GetLength(0); i++)
			{
				for (int j = 0; j < block.GetLength(1); j++)
				{
					if (block[i, j] != Color.Black)
					{
						res.Add(block[i, j].rgba);
					}
				}
			}
			return (res);
		}

		public static Color[,] GetSelectionBlock(Rect rect, Camera cam)
		{
			List<int> res = new List<int>();
			Texture selectionBuffer = GetSelectionBuffer(cam);
			Color[,] block = selectionBuffer.getPixelRect(new Vector2(rect.x, rect.y), new Vector2(rect.width, rect.height));
			return (block);
		}
		private static FrameBufferObject selectionBuffer = null;
		private static bool renderingSelection = false;
		public static Texture GetSelectionBuffer(Camera cam)
		{
			if (tmp == null)
				tmp = new FrameBufferObject(cam.width, cam.height);
			else
			{
				tmp.resize(cam.width, cam.height);
			}
			tmp.simplyBind();
			Shader oneColorShader = Shader.oneColorShader;
			GL.ClearColor(Color.Red.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			GL.CullFace(CullFaceMode.FrontAndBack);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			GL.Light(LightName.Light0, LightParameter.Diffuse, mat_specular);
			Matrix GlobalTransformation = Matrix.Identity;
			GL.FrontFace(FrontFaceDirection.Ccw);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Color.LightGray.gl());
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			GL.LineWidth(0.5f);
			Shader.Bind(oneColorShader);
			//oneColorShader.SetVariable("color", Color.Red);
			renderingSelection = true;
			RenderAll(cam);
			renderingSelection = false;
			Shader.UnBind();
			tmp.simplyUnbind();
			try
			{
				tmp.texture.save("C:/tmp/testframebuffer.png", System.Drawing.RotateFlipType.RotateNoneFlipY);
			}
			catch (Exception e)
			{
				;
			}
			RenderAll(cam);
			return (tmp.texture);
			;
		}
		public static void TestFrameBuffer(Camera cam)
		{
			if (tmp == null)
				tmp = new FrameBufferObject(cam.width, cam.height);
			else
			{
				tmp.resize(cam.width, cam.height);
			}
			tmp.simplyBind();
			Shader uvShader = Shader.uvShader;

			baseColor = Color.LightGray.gl();
			GL.ClearColor(Color.Aquamarine.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			GL.CullFace(CullFaceMode.FrontAndBack);

			//DrawCircle(0.5f, 50, Vector3.zero, Quaternion.identity, Vector3.one, Color.White);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			//OpenTK.OpenGL.Lightv(OpenTK.OpenGL.Enums.LightName.Light0, OpenTK.OpenGL.Enums.LightParameter.Position, light_position);
			//GL.Light(LightName.Light0, LightParameter.Ambient, light_ambient);
			GL.Light(LightName.Light0, LightParameter.Diffuse, mat_specular);
			//GL.Light(LightName.FragmentLight0Sgix , LightParameter.SpotDirection, Color.Indigo.gl());
			Matrix GlobalTransformation = Matrix.Identity;

			/*if (testShader == null)
			{
				testShader = new Shader(vtest, ftest);
				
			}*/
			//Shader.Bind(testShader);

			//testShader.SetVariable("pixel_threshold", 500000);
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			//GL.LoadIdentity();
			//Matrix4 lookat2;
			//lookat2 = cameraTransform.worldToLocalMatrix.gl();
			//lookat2.Transpose();
			//GL.LoadMatrix(ref lookat2);


			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Color.Blue.gl());
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			GL.LineWidth(0.5f);

			//Renderer.Call(() => GL.UseProgram(uvShader.Program));
			Shader.Bind(uvShader);
			RenderAll(cam);
			//Goliat.Utils.DrawUtils.DrawBox(Vector3.one, Vector3.zero, Quaternion.identity, Vector3.one, Color.Red, true);
			Shader.UnBind();
			//RenderATriangle(Vector3.zero, Vector2.up, Vector2.right);
			Debug.Log("Saved texture plzz!!");
			//GL.Flush();
			tmp.simplyUnbind();
			try
			{
				tmp.texture.save("C:/tmp/testframebuffer.png", System.Drawing.RotateFlipType.RotateNoneFlipY);
			}
			catch (Exception e)
			{
				;
			}
			RenderAll(cam);
			
			//resTexture = tmp.texture;
			return;
		}
		public static void RenderAll(Camera cam)
		{
			
			roots = Transform.roots;
			//string vs, fs;
			if (testPhysic && (!physicInited || sw.ElapsedMilliseconds > 1000))
			{
				if (!physicInited)
				{
					//Scene
					//string inputFilePath = "C:/tmp/monster.dae";
					//Scene.ClassFromXMLGenerator gen = Scene.ClassFromXMLGenerator.Parse("C:/tmp/monster.dae", "Collada");
					//string outputFilePath = "C:/Users/Octogon/Documents/Visual Studio 2013/Projects/GoliatEditor/Goliat/Collada.cs";
					//ColladaDocument doc = ColladaDocument.ImportFromFile(inputFilePath);
					//Console.WriteLine(doc.COLLADA.library_geometries.geometry.mesh.source.float_array.Text);
					//Console.WriteLine("count:" + doc.COLLADA.library_geometries.geometry.mesh.source.float_array.count);
					Console.WriteLine("done float array");
					//System.IO.File.WriteAllText(outputFilePath, gen.stringDoc);
					//Scene.ImportCollada("C:/tmp/monster.dae");
					Console.WriteLine("HEYYOUUU");
					Vector3 test = new Vector3(10, 24, 20);
					Quaternion rot = new Quaternion(new Vector3(90, 0, 0));
					test = test * rot;
					Console.WriteLine("new vector:" + test);
					Matrix testMat = rot.orthogonalMatrix;
					Console.WriteLine("inverted matrix0:" + testMat.GeneralizedInverse());
					Console.WriteLine("inverted matrix1" + testMat.inverseMatrix());
					//Gwen.Control.Button button = new Gwen.Control.Button()
					Physic.Init();
					groundBox = new Transform(Vector3.zero, Quaternion.identity, Vector3.one);
					BoxCollider groundCollier = groundBox.AddComponent<BoxCollider>();
					groundCollier.drawMesh = true;
					groundCollier.size = new Vector3(30f, 0.5f, 30f);
					_roots.Add(groundBox);
					/*Quaternion test = new Quaternion(new Vector3(90, 47, 72));
					Console.WriteLine("TEST QUATERNION:" + test);
					Console.WriteLine("TEST QUATERNION MATRIX:" + test.orthogonalMatrix);
					Console.WriteLine("TEST QUATERNION MATRIX QUATERNION:" + test.orthogonalMatrix.quaternion);
					Console.WriteLine("TEST QUATERNION MATRIX QUATERNION EULER ANGLES:" + test.orthogonalMatrix.quaternion.eulerAngles);
					Jitter.LinearMath.JMatrix matrix = test.orthogonalMatrix.jitter;
					StringBuilder matrixToString = new StringBuilder();
					matrixToString.Append(matrix.M11 + ", " + matrix.M12 + ", " + matrix.M13 + ";\n");
					matrixToString.Append(matrix.M21 + ", " + matrix.M22 + ", " + matrix.M23 + ";\n");
					matrixToString.Append(matrix.M31 + ", " + matrix.M32 + ", " + matrix.M33 + ";\n");
					Console.WriteLine("JITTER MATRIX:" + matrixToString.ToString());
					Console.WriteLine("WITH PASSING BY JITTER:" + test.orthogonalMatrix.jitter.goliat().quaternion.eulerAngles);
					*//*Jitter.Collision.CollisionSystem collisionSystem = new Jitter.Collision.CollisionSystemSAP();
					world = new Jitter.World(collisionSystem);
					world.Gravity = new Vector3(0, -10, 0).jitter();
					testPhysicEntities = new List<TestCollider>();
					testPhysicEntities.Add(new TestCollider(new Vector3(1f, 1f, 1f), true, false));*/
					physicInited = true;
					testPhysic = true;
					Goliat.Utils.DrawUtils.DrawSphere(5, 10, 5, Vector3.zero, Quaternion.identity, Vector3.one, Color.Red, true, true);
				}
				else
				{
					if (!switchSphereTest)
					{
						Vector3 position = Vector3.Random(Vector3.zero, Vector3.right + Vector3.forward);
						Quaternion rotation = new Quaternion(Vector3.Random(Vector3.zero, Vector3.one * 25));
						Transform littleBox = new Transform(position + (Vector3.up * 5), rotation, Vector3.one);
						//Console.WriteLine("rotation:" + rotation);
						BoxCollider collider = littleBox.AddComponent<BoxCollider>();
						collider.size = Vector3.one * 1.4f;
						RigidBody body = collider.AddComponent<RigidBody>();
						_roots.Add(littleBox);
						switchSphereTest = true;
					}
					else
					{
						Vector3 position = Vector3.Random(Vector3.zero, Vector3.right + Vector3.forward);
						Quaternion rotation = new Quaternion(Vector3.Random(Vector3.zero, Vector3.one * 25));
						Transform littleBox = new Transform(position + (Vector3.up * 5), rotation, Vector3.one);
						//Console.WriteLine("rotation:" + rotation);
						SphereCollider collider = littleBox.AddComponent<SphereCollider>();
						collider.radius = 1.4f;
						RigidBody body = collider.AddComponent<RigidBody>();
						_roots.Add(littleBox);
						switchSphereTest = false;
					}
					//collider.size = Vector3.one;
											//body.mass = 1;
											//body.useGravity = true;
											//body.mass = 1;

					//testPhysicEntities.Add(new TestCollider(Vector3.one, false, true, 5 * Vector3.up, Quaternion.identity));
					sw.Stop();
					sw.Reset();
					sw.Restart();

				}

			}
			if (world != null)
			{
				float coolFuckinLittleTardStep = 1.0f / 100.0f;
				for (float start = 0f; start < Time.deltaTime; start += coolFuckinLittleTardStep)
				{
					world.Step(coolFuckinLittleTardStep, false);
				}
			}

			//Goliat.Utils.DrawUtils.DrawSphere(5, 10, 5, Vector3.zero, Quaternion.identity, Vector3.one, Color.Red, true, false);
			Physic.Update();
			//Shader.Bind(null);
			//Shader shader = new Shader();
			baseColor = Color.LightGray.gl();
			GL.ClearColor(Color.Aquamarine.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			GL.CullFace(CullFaceMode.FrontAndBack);
		
			//DrawCircle(0.5f, 50, Vector3.zero, Quaternion.identity, Vector3.one, Color.White);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			//OpenTK.OpenGL.Lightv(OpenTK.OpenGL.Enums.LightName.Light0, OpenTK.OpenGL.Enums.LightParameter.Position, light_position);
			//GL.Light(LightName.Light0, LightParameter.Ambient, light_ambient);
			GL.Light(LightName.Light0, LightParameter.Diffuse, mat_specular);
			//GL.Light(LightName.FragmentLight0Sgix , LightParameter.SpotDirection, Color.Indigo.gl());
			Matrix GlobalTransformation = Matrix.Identity;
			
			/*if (testShader == null)
			{
				testShader = new Shader(vtest, ftest);
				
			}*/
			//Shader.Bind(testShader);

			//testShader.SetVariable("pixel_threshold", 500000);
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			//GL.LoadIdentity();
			//Matrix4 lookat2;
			//lookat2 = cameraTransform.worldToLocalMatrix.gl();
			//lookat2.Transpose();
			//GL.LoadMatrix(ref lookat2);


			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Color.Blue.gl());
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			GL.LineWidth(0.5f);
			
			//DrawBox(new Vector3(1, 0.1f, 2) * 5, Vector3.zero, Quaternion.identity, Vector3.one, Color.Gray);
			//DrawCircle(5, 50, Vector3.zero, new Quaternion(new Vector3(0, 45, 0)), Vector3.one, Color.Green);
			//DrawCircle(5, 50, Vector3.zero, new Quaternion(new Vector3(0, 135, 0)), Vector3.one, Color.Green);
			for (int i = 0; i < Debug.debugRays.Count; i++)
			{
				GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Lines);
				GL.Vertex3(Debug.debugRays[i].origin.gl());
				GL.Vertex3(Debug.debugRays[i].end.gl());
				GL.End();
			}
			for (int i = 0; i < testPhysicEntities.Count; i++)
			{
				TestCollider collider = testPhysicEntities[i];
				//throw new System.Exception("???");
				Goliat.Utils.DrawUtils.DrawBox(collider.size, collider.position, collider.rotation, Vector3.one, Color.DarkMagenta, true);
			}
			for (int i = 0; i < GameBehaviour.GameBehaviours.Count; i++ )
			{
				if (GameBehaviour.GameBehaviours[i] is BoxCollider)
				{
					BoxCollider collider = (BoxCollider)GameBehaviour.GameBehaviours[i];
					if (renderingSelection)
						Shader.oneColorShader.SetVariable("color", Color.FromRgba(GameBehaviour.GameBehaviours[i].id));
					if (!(GameBehaviour.GameBehaviours[i].transform.isSelected))
					{
						Goliat.Utils.DrawUtils.DrawBox(collider.size, collider.transform.position, collider.transform.rotation, Vector3.one, Color.DarkMagenta, true);
					}
					else
					{
						//Console.WriteLine("IM SELECTED????");
						Goliat.Utils.DrawUtils.DrawBox(collider.size, collider.transform.position, collider.transform.rotation, Vector3.one, Color.White, true);
					}

				}
				else if (GameBehaviour.GameBehaviours[i] is SphereCollider)
				{
					SphereCollider collider = (SphereCollider)GameBehaviour.GameBehaviours[i];
					if (renderingSelection)
						Shader.oneColorShader.SetVariable("color", Color.FromRgba(GameBehaviour.GameBehaviours[i].id));
					
					if (!(GameBehaviour.GameBehaviours[i].transform.isSelected))
					{
						Goliat.Utils.DrawUtils.DrawSphere(collider.radius, 6, 6, collider.transform.position, collider.transform.rotation, Vector3.one, Color.SeaGreen, true);
					}
					else
					{
						//Console.WriteLine("IM SELECTED????");
						Goliat.Utils.DrawUtils.DrawSphere(collider.radius, 6, 6, collider.transform.position, collider.transform.rotation, Vector3.one, Color.White, true);
					}
				}
				if (renderingSelection)
					Shader.oneColorShader.SetVariable("color", Color.Black);
			}
			//Goliat.Utils.DrawUtils.DrawSphere(5, 26, 15, Vector3.zero, Quaternion.identity, Vector3.one, Color.Red, true, false);
			for (int i = 0; i < roots.Count; i++)
			{
				Animation anim = roots[i].GetComponent<Animation>();
				if (anim != null)
				{
					anim.Update();
				}
				RecursiveRender(roots[i], 0, GlobalTransformation, cam);
				GL.BindTexture(TextureTarget.Texture2D, 0);
			}
			RecursiveRender(arrowsTransform, 0, GlobalTransformation, cam);
		}
		public static bool debug = false;
		
		private static void RecursiveRender(Transform node, int mode, Matrix parentTransformation, Camera cam)
		{
			if (node == null)
				return;
			Matrix GlobalTransformation = parentTransformation * node.transformation;
			//OpenTK.Matrix4 GlobalTransformation = Matrix4.Identity;
			
			if (!node.isActive)
				return;
			if (node.GetComponent<Terrain>() != null)
			{
				Color4 Transparent = Color.SlateGray.gl();
				GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Transparent);
			}
			//Shader.UnBind();
			if (node.isActive)
			{
				//Debug.Log("OK");
				Matrix4 m = node.transformation.gl();

				m.Transpose();
				if (node.isSelected && mode == 0)
				{
					if (node.GetComponent<MeshFilter>() != null)
						GL.PushName(node.GetHashCode());
					RecursiveRender(node, mode + 1, GlobalTransformation, cam);
					Color4 Transparent = new Color4(0, 55, 0, 55);
					GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Transparent);
					GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
					GL.LineWidth(0.01f);
					GL.Enable(EnableCap.LineStipple);
					unchecked
					{
						GL.LineStipple(1, (short)(0x3F07F / 2));
					}
				}
				else if (node.isSelected)
				{
					if (node.GetComponent<Terrain>() != null)
					{
						/*if (!File.Exists(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "/testterrain/tmp.bmp")))
						{
							if (test == null)
								test = node.getComponent<Terrain>().heightMap;
							//							Texture heightMap = node.getComponent<Terrain>().heightMap;
							//						heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "/testterrain/tmp.bmp"));
							//node.getComponent<Terrain>().heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "/testterrain/tmp.bmp"));
						}*/
						//Texture tmp = node.getComponent<Terrain>().heightMap;
						//Console.WriteLine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
						//tmp.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "testterrain/tmp.bmp"));
						//tmp.UnBind();
						Vector3 impact = node.GetComponent<Terrain>().mouseImpact;
						OpenTK.Vector2 mousepos = new OpenTK.Vector2(impact.x, impact.z);

						Shader.terrainShader.SetVariable("mouseposition", mousepos.X, mousepos.Y);
						Shader.terrainShader.SetVariable("node_matrix", m);

						Matrix4 view = Camera.current.transform.transformation.inverse.gl();
						view.Transpose();
						Shader.terrainShader.SetVariable("view_matrix", view);
						//Shader.terrainShader.SetVariable("heightMap", node.getComponent<Terrain>().heightMap.glID);
						Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, node.GetComponent<Terrain>().heightMap.glID));
						GL.ActiveTexture(TextureUnit.Texture0);

						Renderer.Call(() => GL.UseProgram(Shader.terrainShader.Program));
						Renderer.Call(() => GL.Uniform1(GL.GetUniformLocation(Shader.terrainShader.Program, "heightmap"), TextureUnit.Texture0 - TextureUnit.Texture0));
						Shader.Bind(Shader.terrainShader);

					}
					GL.Disable(EnableCap.LineStipple);
					GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
				}
				else if (node.GetComponent<MeshFilter>() != null)
				{
					//Console.WriteLine("this hash code:" + node.GetHashCode());
					//Console.WriteLine("in behaviours type:" + GameBehaviour.GameBehaviours[node.GetHashCode()].GetType());
					GL.PushName(node.GetHashCode());
					if (renderingSelection)
						Shader.oneColorShader.SetVariable("color", Color.FromRgba(node.GetHashCode()));
					GL.Disable(EnableCap.LineStipple);
					GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
				}

				GL.PushMatrix();
				GL.MultMatrix(ref m);
				//Color4 layerColor = FromIntColor((uint)node.GetHashCode());µ
				if (node.GetComponent<Goliat.SphereCollider>() != null)
				{

					;
				}
				if (node.GetComponent<Goliat.BoxCollider>() != null)
				{
					;
					//Console.WriteLine("do i use gravity?" + node.getComponent<BoxCollider>().jrigidbody.AffectedByGravity);
					//Console.WriteLine("size:" + node.getComponent<BoxCollider>().size);
					//DrawBox(node.getComponent<Goliat.BoxCollider>().size, node.position, node.rotation, Vector3.one, Color.Gray);
				}
				if (node.GetComponent<Goliat.MeshFilter>() != null )
				{

					Assimp.Scene scene = node.m_model;
					List<Goliat.MeshFilter> meshs = node.getComponents<Goliat.MeshFilter>();
					Animation anim = node.root.GetComponent<Animation>();
					if (meshs == null)
					{
						Console.WriteLine("meshs null");
					}
					if (meshs[0].sharedMesh == null)
					{
						Console.WriteLine("sharedMesh null");
					}
					if (meshs[0].sharedMesh.vertices == null)
					{
						Console.WriteLine("vertices null");
					}
					int len = meshs[0].sharedMesh.vertices.Length;
					List<Vector3>[] verticesWeights = new List<Vector3>[len];
					List<Vector3>[] normalsWeights = new List<Vector3>[len];
					Vector3[] vertices = (Vector3[])meshs[0].sharedMesh.vertices.Clone();
					Vector3[] normals = (Vector3[])meshs[0].sharedMesh.normals.Clone();
				
					if (meshs[0].GetType().IsSameOrSubclass(typeof(Goliat.SkinnedMeshFilter)))
					{
						SkinnedMeshFilter filter = (SkinnedMeshFilter)meshs[0];
						
						
						Mesh mesh = filter.sharedMesh;
						Dictionary<string, Matrix> finalMatrixs = new Dictionary<string, Matrix>();
						string guid = filter.sharedMesh.guid;
						if (!debug)
						{
							//Console.WriteLine("found skinned mesh 0");
							debug = true;
						}
						//Console.WriteLine("hey0.0");
						if (filter == null)
						{
							Console.WriteLine("filter null");
							
						}
						//Console.WriteLine("hey0.0.1");
						/*if (filter.bones == null)
						{
							Console.WriteLine("filter bones null");
						}*/
						//Console.WriteLine("hey0.0.2");
						for (int i = 0; i < filter.bones.Count; i++)
						{
							Bone bone = filter.bones[i];
							Matrix finalMatrix = bone.localToRootMatrix * bone.skins[guid].offsetMatrix;
							List<VertexWeight> weights = bone.skins[guid].weights;
							for (int j = 0; j < weights.Count; j++)
							{
								if (verticesWeights[weights[j].index] == null)
								{
									verticesWeights[weights[j].index] = new List<Vector3>();
									normalsWeights[weights[j].index] = new List<Vector3>();
								}
								verticesWeights[weights[j].index].Add((finalMatrix * mesh.vertices[weights[j].index]) * weights[j].weight);
								Vector4 normal = new Vector4(mesh.normals[weights[j].index].x, mesh.normals[weights[j].index].y, mesh.normals[weights[j].index].z, 0);
								normal = finalMatrix.multiplyPoint(normal);
								normalsWeights[weights[j].index].Add(Vector3.Lerp(mesh.normals[weights[j].index], new Vector3(normal.x, normal.y, normal.z), weights[j].weight));
							}
						}
						for (int i = 0; i < len; i++)
						{
							if (verticesWeights[i] == null)
							{
								continue;
							}
							Vector3 medianVert = Vector3.zero;
							Vector3 medianNormal = Vector3.zero;
							int count = verticesWeights[i].Count;
							for (int j = 0; j < count; j++)
							{
								medianVert += verticesWeights[i][j];
								medianNormal += verticesWeights[i][j];
							}
							vertices[i] = medianVert;
							normals[i] = medianNormal;
						}
					}
					

					foreach (Goliat.MeshFilter myFilter in meshs)
					{
						if (mode == 1 || !node.isSelected)
						{
							GL.Enable(EnableCap.LineStipple);
							GL.LineWidth(1.0f);
							
							foreach (Vector3[] line in myFilter.sharedMesh.debugLines)
							{
								BeginMode faceMode = BeginMode.Lines;
								GL.Begin(faceMode);
								GL.Vertex3(line[0].gl());
								GL.Vertex3(line[1].gl());
								GL.End();
							}
						}
						Goliat.Mesh myMesh = myFilter.sharedMesh;
						Assimp.Mesh mesh = myMesh.importVersion;
						Goliat.MeshRenderer myRenderer = node.GetComponent<Goliat.MeshRenderer>();
						if (myRenderer == null && node.GetComponent<Terrain>() == null)
						{
							Debug.Log("renderer null");
						}
						

						if (myMesh.hasNormals)
						{
							GL.Enable(EnableCap.Lighting);
						}
						else
						{
							GL.Disable(EnableCap.Lighting);
						}

						/*bool hasColors = mesh.HasVertexColors(0);
						if (myMesh.hasColors)
						{
							GL.Enable(EnableCap.ColorMaterial);
						}
						else
						{
							GL.Disable(EnableCap.ColorMaterial);
						}*/
						//bool hasTexCoords = myMesh.hasTextureCoords(0);

						if (myMesh.faces != null)
						{
							foreach (Face face in myMesh.faces)
							{
								if (myRenderer != null && myRenderer.materials != null && (!node.isSelected || mode == 1))
								{
									myRenderer.materials[face.materialIndex].Bind();
								}
								BeginMode faceMode;
								switch (face.indices.Count())
								{
									case 1:
										faceMode = BeginMode.Points;
										break;
									case 2:
										faceMode = BeginMode.Lines;
										break;
									case 3:
										faceMode = BeginMode.Triangles;
										break;
									default:
										faceMode = BeginMode.Polygon;
										break;
								}
								GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);
								for (int i = 0; i < face.indices.Count(); i++)
								{
									int indice = face.indices[i];
									GL.Color4(baseColor);
									/*if (myMesh.hasColors)
									{
										Color4 vertColor = FromIntColor(0);
										if (hasColors)
											vertColor = FromColor(mesh.VertexColorChannels[0][indice]);

										GL.Color4(vertColor);
									}*/
									if (myMesh.hasNormals)
									{
										//OpenTK.Vector3 normal = FromVector(mesh.Normals[indice]);
										if (myMesh.normals != null)
										{
											OpenTK.Vector3 normal = normals[indice].gl();
											GL.Normal3(normal);
										}
									}
									if (myMesh.hasTextureCoords)
									{
										//OpenTK.Vector3 uvw = FromVector(mesh.TextureCoordinateChannels[0][indice]);
										OpenTK.Vector2 uv = myMesh.uvs[indice].gl();
										GL.TexCoord2(uv.X, uv.Y);
									}

									OpenTK.Vector3 pos = vertices[indice].gl();
									//if (node.getComponent<Terrain>() != null && node.isSelected)
									//Shader.terrainShader.SetVariable("vertex_pos", pos.X, pos.Y , pos.Z, 1.0f );
									GL.Vertex3(pos);
								}
								//Console.WriteLine("hooooo");
								GL.End();
							}
						}
						else
						{
							Debug.Log("faces null");
						}
					}
				}
				if (mode == 0 && node.GetComponent<MeshFilter>() != null)
				{
					if (renderingSelection)
						Shader.oneColorShader.SetVariable("color", Color.FromRgba(0));
					GL.PopName();
				}
				if (node.children != null && mode == 0)
				{
					for (int i = 0; i < node.children.Count; i++)
					{
						if (node.children[i] != null)
							RecursiveRender(node.children[i], mode, GlobalTransformation, cam);
					}
				}
				GL.PopMatrix();
			}

		}
		private static Transform appendAsset(Assimp.Scene model)
		{
			Assimp.Node current = model.RootNode;
			return (appendNodeRecursive(current, null, model));
		}


		private static Transform appendNodeRecursive(Assimp.Node node, Transform current, Assimp.Scene m_model)
		{
			Transform tr = getTransform(node, current, m_model);
			Transform children = null;
			if (current == null)
				children = tr;
			else
				children = current.children[current.children.Count - 1];
			if (node.Children != null)
			{
				for (int i = 0; i < node.Children.Count; i++)
				{
					appendNodeRecursive(node.Children[i], children, m_model);
				}
			}
			return (tr);
		}

		private static Transform getTransform(Assimp.Node node, Transform parent, Assimp.Scene m_model)
		{
			Transform tr = new Transform(Goliat.Vector3.zero, Goliat.Quaternion.identity, Goliat.Vector3.one);
			if (parent == null && !_roots.Contains(tr))
			{
				_roots.Add(tr);
			}
			tr.m_model = m_model;
			tr.parent = parent;
			tr.name = node.Name;
			Vector3D scaling;
			Vector3D translation;
			Assimp.Quaternion rotation;

			node.Transform.Decompose(out scaling, out rotation, out translation);
			tr.localRotation = new Goliat.Quaternion(rotation);
			tr.localPosition = new Goliat.Vector3(translation);
			tr.localScale = new Goliat.Vector3(scaling);
			for (int i = 0; i < node.MeshIndices.Count; i++)
			{
				Goliat.MeshFilter tmp = tr.AddComponent<Goliat.MeshFilter>();
				tmp.sharedMesh = new Mesh();
				Goliat.MeshRenderer renderer = tr.AddComponent<Goliat.MeshRenderer>();
				renderer.materials = new List<Goliat.Material>();
				Assimp.Mesh mesh = m_model.Meshes[node.MeshIndices[i]];
				Assimp.Material mat = m_model.Materials[mesh.MaterialIndex];
				for (int j = 0; j < mat.GetMaterialTextures(TextureType.Diffuse).Length; j++)
				{
					renderer.materials.Add(new Goliat.Material(mat, j));
				}
				if (mat.GetMaterialTextures(TextureType.Diffuse).Length == 0)
				{
					renderer.materials.Add(new Goliat.Material(mat, 0));
				}
				if (tmp.sharedMesh == null)
					tmp.sharedMesh = new Mesh();
				tmp.sharedMesh.Assimp_Import(m_model.Meshes[node.MeshIndices[i]], m_model);
			}
			return (tr);
		}
		[System.NonSerializedAttribute]
		private static Assimp.AssimpContext importer = new Assimp.AssimpContext();
		private static Transform AppendAsset(String path)
		{
			if (File.Exists(path))
			{
				Assimp.Scene m_model = importer.ImportFile(path, PostProcessPreset.TargetRealTimeFast);
				return (appendAsset(m_model));
			}
			else
			{
				Debug.Log("mince");
			}
			return (null);
		}
		public override void Update()
		{
			if (this.GetComponent<RigidBody>() != null && this.GetComponent<RigidBody>().initialized)
			{
				this.transform._position = this.GetComponent<RigidBody>().position;
				this.transform._rotation = this.GetComponent<RigidBody>().rotation;
				//this.transform._rotation = this.GetComponent<RigidBody>().rotation;
				this._updateFromWorld(false);
			}
		}
		public static Transform ImportFromFile(String path)
		{
			string meta = Path.ChangeExtension(path, ".meta");
			if (File.Exists(meta))
			{
				SharedResource.MappedRessourceInfo info = (SharedResource.MappedRessourceInfo)Serializer.UnSerialize(meta);
				Scene scn = (Scene)info.Regenerate();
				Transform tr = scn.generateTransform();
				tr.AddToScene();
				return (tr);
			}
			return (AppendAsset(path));
		}


		private static Transform appendAsset(Goliat.Scene model)
		{
			Goliat.Scene current = model;
			return (appendNodeRecursive(current, null));
		}


		private static Transform appendNodeRecursive(Goliat.Scene node, Transform current)//, Assimp.Scene m_model)
		{
			Transform tr = getTransform(node, current);
			Transform children = null;
			if (current == null)
				children = tr;
			else
				children = current.children[current.children.Count - 1];
			if (node.children != null)
			{
				for (int i = 0; i < node.children.Count; i++)
				{
					appendNodeRecursive(node.children[i], children);
				}
			}
			return (tr);
		}

		private static Transform getTransform(Goliat.Scene node, Transform parent)//, Goliat.Scene m_model)
		{
			Transform tr = new Transform(Goliat.Vector3.zero, Goliat.Quaternion.identity, Goliat.Vector3.one);
			if (parent == null && !_roots.Contains(tr))
			{
				_roots.Add(tr);
			}
			tr.parent = parent;
			tr.name = node.name;


		
			tr.localRotation = node.localRotation;
			tr.localPosition = node.localPosition;
			tr.localScale = node.localScale;
			for (int i = 0; i < node.meshs.Count; i++)
			{
				Goliat.MeshFilter tmp = tr.AddComponent<Goliat.MeshFilter>();
				tmp.sharedMesh = node.meshs[i].mesh;
				Goliat.MeshRenderer renderer = tr.AddComponent<Goliat.MeshRenderer>();
				renderer.materials = (List<Material>)node.meshs[i].materials.Clone();
				if (tmp.sharedMesh == null)
					tmp.sharedMesh = new Mesh();
			}
			return (tr);
		}
		public void Attach(Transform parent, bool keepTransform)
		{
			if (!keepTransform)
			{
				this._parent = parent;
				parent.children.Add(this, keepTransform);
			}
			else
			{
				this.parent = parent;
			}

		}
		public Transform(Goliat.Scene scn)
		{
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this._localPosition = Vector3.zero;
			this._localRotation = Quaternion.identity;
			this._localScale = Vector3.one;
			this.localRotation = scn.localRotation;
			this.localPosition = scn.localPosition;
			this.localScale = scn.localScale;
			this.name = scn.name;
			//Debug.Log("number");
			//Console.WriteLine("one");
			if (scn.animation != null)
			{
				Animation anim = new Animation(scn.animation);
				this.AddComponent(anim);
			}
			for (int i = 0; i < scn.children.Count; i++)
			{
				Transform tmp;
				if (scn.children[i] is SceneBone)
				{
					tmp = new Bone((SceneBone)scn.children[i]);
				}
				else
				{
					tmp = new Transform(scn.children[i]);
				}
				tmp.Attach(this, false);
			}
			MeshFilter tmp2 = new MeshFilter();
			MeshRenderer renderer = this.AddComponent<MeshRenderer>();
			for (int i = 0; i < scn.meshs.Count; i++)
			{
				for (int j = 0; j < scn.meshs[i].materials.Count; j++)
				{
					if (scn.meshs[i].materials[j] == null)
					{
						;
					}
					else
					{
						renderer.materials.Add(scn.meshs[i].materials[j]);
						;
					}
				}
				if (scn.meshs[i].isSkinned)
				{
					tmp2 = new SkinnedMeshFilter();
				}
			}
			if (scn.meshs.Count > 0)
			{
				tmp2.sharedMesh = scn.meshs[0].mesh;
				this.AddComponent(tmp2);
			}
			if (this.GetComponent<MeshFilter>() != null)
			{
				Console.WriteLine("i got a mesh filter at least :/");
			}

		}
		public static Transform ImportFromFile(String path, bool addToScene)
		{
			string meta = Path.ChangeExtension(path, ".meta");
			if (File.Exists(meta))
			{
				SharedResource.MappedRessourceInfo info = (SharedResource.MappedRessourceInfo)Serializer.UnSerialize(meta);
				Scene scn = (Scene)info.Regenerate();
				Transform tr = scn.generateTransform();
				tr.AddToScene();
			}
			Transform res = AppendAsset(path);
			if (!addToScene)
			{
				roots.Remove(res);
			}
			return (res);
		}
		private void PickMatrix(double x, double y, double deltax, double deltay, int[] viewport)
		{
			if (deltax <= 0 || deltay <= 0)
			{
				return;
			}
			GL.Translate((viewport[2] - 2 * ((x + deltax) - viewport[0])) / deltax,
				(viewport[3] - 2 * ((y) - viewport[1])) / deltay, 0);
			GL.Scale(viewport[2] / deltax, viewport[3] / deltay, 1.0);
		}

		public bool hasParent(Transform other)
		{
			Transform tmp = this;
			while (tmp != null)
			{
				tmp = tmp.parent;
				if (tmp != null && (GameBehaviour)tmp == (GameBehaviour)other)
					return (true);
			}
			return (false);
		}

		public bool isMouseOn()
		{
			if (Transform.getSelections == null)
			{
				return (false);
			}
			else
			{
				ImpactInfo impact = Transform.getSelections();
				if (impact != null && impact.hits != null)
				{
					List<Transform> selection = impact.hits;
					for (int i = 0; i < selection.Count; i++)
					{
						if ((GameBehaviour)selection[i] == (GameBehaviour)this)
							return (true);
					}
				}
				return (false);
			}
			return (false);
		}

		public ImpactInfo getMouseImpact()
		{
			return (Transform.getSelections());
		}

		private Quaternion _localRotation = Quaternion.identity;
		private Vector3 _localScale = Vector3.one;
		private Vector3 _localPosition = Vector3.zero;
		private Vector3 _position = Vector3.zero;
		private Quaternion _rotation = Quaternion.identity;
		private Matrix _transformation = Matrix.Identity;
		private static List<Transform> _Selection = new List<Transform>();
		public Texture generateIcon()
		{
			FrameBufferObject tmp = new FrameBufferObject(90, 90);
			tmp.bind();
			GL.Viewport(0, 0, tmp.width, tmp.height);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();
			Matrix4 lookat2;

			Camera cam = new Camera();

			Transform pivotPoint = Transform.zero;
			cam.pivotPoint = pivotPoint;
			float aspectRatio = tmp.width / (float)tmp.height;
			cam.aspectRatio = aspectRatio;
			cam.width = tmp.width;
			cam.height = tmp.height;
			Console.WriteLine(cam.width);
			cam.nearClipPlane = 1;
			cam.farClipPlane = 1000;
			cam.fov = MathHelper.PiOver4.toDeg();
			Matrix4 perspective = cam.perspectiveMatrix.gl();
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadMatrix(ref perspective);
			Transform.render(this);

			Debug.Log("Saved texture plzz!!");
			GL.Flush();
			tmp.unbind();

			System.Threading.Thread.Sleep(1000);
			tmp.texture.save("C:/tmp/testalien2.png", System.Drawing.RotateFlipType.Rotate270FlipY);
			return (tmp.texture);
		}
		private static void render(Transform node)
		{
			OpenTK.Matrix4 m = Transform.zero.transformation.gl();
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			GL.LoadIdentity();
			Matrix4 lookat2;
			Transform cameraTransform = new Transform(new Vector3(12, 0, 0), Quaternion.identity, Vector3.one);
			cameraTransform.LookAt(Vector3.zero, Vector3.up, Space.world);
			lookat2 = cameraTransform.worldToLocalMatrix.gl();
			lookat2.Transpose();
			GL.LoadMatrix(ref lookat2);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.FrontFace(FrontFaceDirection.Ccw);
			GL.LoadIdentity();
			GL.LoadMatrix(ref lookat2);
			m.Transpose();
			GL.Disable(EnableCap.LineStipple);
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			if (node.children != null)
			{
				for (int i = 0; i < node.ChildCount; i++)
				{

					Transform.renderRecursive(node.children[i]);
				}
			}

		}
		private static void renderRecursive(Transform node)
		{
			if (node.isActive)
			{
				Matrix4 m = node.transformation.gl();

				m.Transpose();

				GL.PushMatrix();
				GL.MultMatrix(ref m);
				Color4 layerColor = StandardExtensions.FromIntColor((uint)node.GetHashCode());
				if (node.GetComponent<Goliat.MeshFilter>() != null)
				{

					Assimp.Scene scene = node.m_model;
					List<Goliat.MeshFilter> meshs = node.getComponents<Goliat.MeshFilter>();
					
					foreach (Goliat.MeshFilter myFilter in meshs)
					{
						Goliat.Mesh myMesh = myFilter.sharedMesh;
						Assimp.Mesh mesh = myMesh.importVersion;
						Goliat.MeshRenderer myRenderer = myFilter.GetComponent<Goliat.MeshRenderer>();
						if (myRenderer != null && myRenderer.materials != null)
						{
							myRenderer.materials[0].Bind();
						}

						if (myMesh.hasNormals)
						{
							GL.Enable(EnableCap.Lighting);
						}
						else
						{
							GL.Disable(EnableCap.Lighting);
						}

						/*bool hasColors = mesh.HasVertexColors(0);
						if (myMesh.hasColors)
						{
							GL.Enable(EnableCap.ColorMaterial);
						}
						else
						{
							GL.Disable(EnableCap.ColorMaterial);
						}*/


						//bool hasTexCoords = myMesh.hasTextureCoords(0);

						foreach (Face face in myMesh.faces)
						{
							//Debug.Log("found face");
							BeginMode faceMode;
							switch (face.indices.Count())
							{
								case 1:
									faceMode = BeginMode.Points;
									break;
								case 2:
									faceMode = BeginMode.Lines;
									break;
								case 3:
									faceMode = BeginMode.Triangles;
									break;
								default:
									faceMode = BeginMode.Polygon;
									break;
							}


							GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);

							for (int i = 0; i < face.indices.Count(); i++)
							{
								int indice = face.indices[i];
								GL.Color4(Color.LightGray.gl());
								/*if (myMesh.hasColors)
								{
									Color4 vertColor = FromIntColor(0);
									if (hasColors)
										vertColor = FromColor(mesh.VertexColorChannels[0][indice]);

									GL.Color4(vertColor);
								}*/
								if (myMesh.hasNormals)
								{
									//OpenTK.Vector3 normal = FromVector(mesh.Normals[indice]);
									if (myMesh.normals != null)
									{
										OpenTK.Vector3 normal = myMesh.normals[indice].gl();
										GL.Normal3(normal);
									}
								}
								if (myMesh.hasTextureCoords)
								{
									//OpenTK.Vector3 uvw = FromVector(mesh.TextureCoordinateChannels[0][indice]);
									OpenTK.Vector2 uv = myMesh.uvs[indice].gl();
									GL.TexCoord2(uv.X, uv.Y);
								}
								OpenTK.Vector3 pos = myMesh.vertices[indice].gl();
								GL.Vertex3(pos);
							}

							GL.End();
						}

					}

				}
				if (node.children != null)
				{
					for (int i = 0; i < node.ChildCount; i++)
					{

						Transform.renderRecursive(node.children[i]);
					}
				}
				GL.PopMatrix();
			}
		}

		public static Bounds GetBounds(Transform tr)
		{
			Bounds tmp = new Bounds(Vector3.zero, Vector3.zero);
			if (tr.GetComponent<MeshFilter>() != null && tr.GetComponent<MeshFilter>().sharedMesh != null)
			{
				tmp = new Bounds(tr.GetComponent<MeshFilter>().sharedMesh.bounds);
			}
			for (int i = 0; i < tr.childCount; i++)
			{
				tmp.updateToBigger(Transform.GetBounds(tr.children[i]));
			}
			return (tmp);
		}
		[Goliat.NonSerialized]
		public Bounds bounds
		{
			get
			{
				return (Transform.GetBounds(this));
			}
		}
		public delegate ImpactInfo SelectionHandler();
		public static SelectionHandler getSelections = null;

		public static List<Transform> Selection
		{
			get
			{
				if (Transform._Selection == null)
					Transform._Selection = new List<Transform>();
				return (Transform._Selection);
			}
			set
			{
				List<Transform> selection = (List<Transform>)Transform._Selection.Clone();
				for (int i = 0; i < selection.Count; i++)
				{
					selection[i].isSelected = false;
					Console.WriteLine("DESELECTING THAT SHIT0");
				}
				Transform._Selection = value;
				for (int i = 0; i < Transform._Selection.Count; i++)
				{
					Transform._Selection[i].isSelected = true;
				}
			}
		}
		private static Transform _activeTransform = null;
		public static Transform activeTransform
		{
			get
			{
				return (Transform._activeTransform);
			}
			set
			{
				if (Transform._activeTransform != null && !Transform._Selection.Contains(Transform._activeTransform))
				{
					Console.WriteLine("DESELECTING THAT SHIT 1");
					Transform._activeTransform.isSelected = false;
				}
				Transform._activeTransform = value;
				Transform._activeTransform.isSelected = true;
			}
		}
		private static List<Transform> _Transforms = null;
		public static List<Transform> Transforms
		{
			get
			{
				if (_Transforms == null)
					_Transforms = new List<Transform>();
				return (_Transforms);
			}
			set
			{
				_Transforms = value;
			}
		}
		private bool _isSelected = false;
		public bool isSelected
		{
			get
			{
				return (this._isSelected);
			}
			set
			{
				this._isSelected = value;
				if (this._isSelected && !Transform._Selection.Contains(this))
				{
					Transform._Selection.Add(this);
				}
				else if (!this._isSelected && Transform._Selection.Contains(this))
				{
					Transform._Selection.Remove(this);
				}
			}
		}
		public Matrix transformation
		{
			get
			{
				return (this._transformation);
			}
			set
			{
				Vector3 localPos = Vector3.zero;
				Vector3 localScale = Vector3.one;
				Quaternion localRot = Quaternion.identity;
				this._transformation = value;
				this._transformation.Decompose(out localScale, out localRot, out localPos);
				this._localPosition = localPos;
				this._localRotation = localRot;
				this._localScale = localScale;
				this._updateFromLocal();

			}
		}

		public Vector3 transformDirection(Vector3 direction)
		{
			return (this.transform.rotation * direction);
		}
		public Vector3 inverseTransformDirection(Vector3 direction)
		{
			return (this.transform.rotation.inverse * direction);
		}
		public Vector3 limitedTransformDirection(Vector3 localDirection)
		{
			return (this.transform._localRotation * localDirection);
		}
		public Vector3 lossyScale
		{
			get
			{
				Matrix trs = this.localToWorldMatrix;
				Vector3 scale;
				Quaternion rot;
				Vector3 pos;
				trs.Decompose(out scale, out rot, out pos);
				return (scale);
			}
			set
			{
				Vector3 scale = value;
				Vector3 pos = this.position;
				Matrix tmp = (Matrix.translation(this._localPosition).inverse * this._rotation.inverse.orthogonalMatrix) * this.parent.worldToLocalMatrix;
				Vector3 xvec = (new Vector3(scale.x, 0, 0) + pos) * tmp;
				Vector3 yvec = (new Vector3(0, scale.y, 0) + pos) * tmp; //this.calMatrix;
				Vector3 zvec = (new Vector3(0, 0, scale.z) + pos) * tmp;

				//if (scale.x < 0)

				Vector3 all = xvec + yvec + zvec;
				float x = all.x;
				float y = all.y;
				float z = all.z;
				if (x < 0)
				{
					x = -x;
				}
				if (y < 0)
				{
					y = -y;
				}
				if (z < 0)
				{
					z = -z;
				}
				all = new Vector3(x, y, z);

				this.localScale = all;
			}
		}
		public Vector3 limitedInverseTransformDirection(Vector3 parentDirection)
		{
			return (this.transform._localRotation.inverse * parentDirection);
		}

		private void _updateFromLocal(bool update_physic = true)
		{
			//Debug.Log("hey");
			Matrix translation = Matrix.translation(this._localPosition);
			Matrix rotation = new Matrix(this._localRotation);
			Matrix scale = Matrix.scale(this._localScale);
			this._transformation = translation * rotation * scale;
			if (parent != null)
			{
				this._position = parent.localToWorldMatrix * this._localPosition;
				this._rotation = parent.transformRotation(this._localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			if (this.GetComponent<RigidBody>() != null && update_physic)
			{
				this.GetComponent<RigidBody>().position = this._position;
				this.GetComponent<RigidBody>().rotation = this._rotation;
			}
		}


		private void _updateFromWorld(bool update_physic = true)
		{
			if (this.parent != null)
			{
				Matrix parenttransformation = this.parent.worldToLocalMatrix;
				_localPosition = parenttransformation * this._position;
				_localRotation = parent.inverseTransformRotation(this._rotation);
				Matrix translation = Matrix.translation(this._localPosition);
				Matrix rotation = new Matrix(this._localRotation);
				Matrix scale = Matrix.scale(this._localScale);
				this._transformation = (rotation * translation * scale);
			}
			else
			{
				this._localPosition = this._position;
				this._localRotation = this._rotation;
				Matrix translation = Matrix.translation(this._localPosition);
				Matrix rotation = new Matrix(this._localRotation);
				Matrix scale = Matrix.scale(this.localScale);
				this._transformation = (rotation * translation * scale);
			}
			if (this.GetComponent<RigidBody>() != null && update_physic)
			{
				this.GetComponent<RigidBody>().position = this._position;
				this.GetComponent<RigidBody>().rotation = this._rotation;
			}
		}

		public Vector3 getPosition()
		{
			if (this.parent != null)
			{
				return (this.parent.transformPoint(this._localPosition));
			}
			else
			{
				return (this._localPosition);
			}
		}
		[Goliat.NonSerialized]
		public Vector3 position
		{
			get
			{
				return (this.getPosition());
			}
			set
			{
				this._position = value;
				if (this.parent != null)
					this._localPosition = parent.inverseTransformPoint(this._position);
				else
					this._localPosition = value;
				this._updateFromLocal();
			}
		}
		public Vector3 localScale
		{
			get
			{
				return (this._localScale);
			}
			set
			{
				this._localScale = value;
				this._updateFromLocal();
			}
		}
		public Vector3 localPosition
		{
			get
			{
				return (this._localPosition);
			}
			set
			{
				this._localPosition = value;
				this._updateFromLocal();
			}
		}

		private Goliat.Quaternion getRotation()
		{
			Transform tr = this;
			Goliat.Quaternion quat = tr.localRotation;
			Transform current = tr;
			while (current.parent != null)
			{
				quat = current.parent.localRotation * quat;
				current = current.parent;
			}
			return (quat);
		}
		[Goliat.NonSerialized]
		public Quaternion rotation
		{
			get
			{
				return (this.getRotation());
			}
			set
			{
				if (this.parent != null)
				{
					this._localRotation = this.parent.inverseTransformRotation(value);
				}
				else
				{
					this._localRotation = value;
				}
				this._updateFromLocal();
			}
		}
		public Quaternion localRotation
		{
			get
			{
				return (this._localRotation);
			}
			set
			{
				this._localRotation = value;
				this._updateFromLocal();
			}
		}
		public bool isChildOf(Transform candidate)
		{
			for (int i = 0; i < candidate.childCount; i++)
			{
				if ((GameBehaviour)candidate.children[i] == (GameBehaviour)this)
				{
					return (true);
				}
			}
			return (false);
		}
		[Goliat.NonSerialized]
		public Matrix localToWorldMatrix
		{
			get
			{
				Matrix res = new Matrix(4, 4);
				if (this.parent != null)
					res = this.parent.localToWorldMatrix * (this._transformation);
				else
					res = (this._transformation);
				return (res);
			}
			set
			{
				;
			}
		}
		public List<Transform> parentsRecursive()
		{
			Transform tmp = this.parent;
			List<Transform> res = new List<Transform>();
			while (tmp != null)
			{
				res.Add(tmp);
				tmp = tmp.parent;
			}
			return (res);
		}

		private Matrix getInverseTransformation()
		{
			if (this.transformation.inverse != Matrix.Zero)
			{
				return (this.transformation.inverse);
			}
			else
			{
				Matrix translation = Matrix.translation(Vector3.zero - this._localPosition);
				Matrix rotation = this.localRotation.inverse.orthogonalMatrix;
				Matrix scale = Matrix.scale(new Vector3(1 / this._localScale.x, 1 / this._localScale.y, 1 / this._localScale.z));
				return (translation * rotation * scale);
			}
		}
		[Goliat.NonSerialized]
		public Matrix worldToLocalMatrix
		{
			get
			{
				if (parent != null)
				{
					Matrix res;
					List<Transform> parents = this.parentsRecursive();
					res = parents[parents.Count - 1].getInverseTransformation();
					for (int i = (parents.Count - 2); i >= 0; i--)
					{
						res = parents[i].getInverseTransformation() * res;
					}
					res = this.getInverseTransformation() * res;
					return (res);
				}
				else
				{
					Matrix res = this.getInverseTransformation();
					return (res);
				}
			}
			set
			{
				;
			}
		}
		public Transform root
		{
			get
			{
				if (this.parent == null)
					return (this);
				Transform tmp = this.parent;
				while (tmp.parent != null)
				{
					tmp = tmp.parent;
				}
				return (tmp);
			}
		}
		private Transform _parent = null;
		private int _siblingIndex = -1;
		public int siblingIndex
		{
			get
			{
				return (this._siblingIndex);
			}
			set
			{
				;
			}
		}
		public string name = "";
		public float GetDist(Transform other)
		{
			return (other.position.GetDist(this.position));
		}
		public static void CheckRoots()
		{
			List<Transform> toRemove = new List<Transform>();
			for (int i = 0; i < Transform.roots.Count; i++)
			{
				if (Transform.roots[i].parent == null)
				{
					toRemove.Add(Transform.roots[i]);
				}
			}
			for (int i = 0; i < toRemove.Count; i++)
			{
				Transform.roots.Remove(toRemove[i]);
			}
		}
		public void AddToScene()
		{

			if (!Transform._roots.Contains(this.root))
			{
				Transform._roots.Add(this.root);
			}
			if (!Transform.Transforms.Contains(this.root))
			{
				Transform.Transforms.Add(this.root);
			}
			Console.WriteLine("added:" + this.name + "to scene");
		}
		public void AddToScene(bool forceAdd)
		{
			Transform._roots.Add(this.root);
			if (!Transform.Transforms.Contains(this.root))
			{
				Transform.Transforms.Add(this.root);
			}
		}
		public Transform parent
		{
			get
			{
				return (this._parent);
			}
			set
			{
				if (this._parent == value && (this._parent == null || this._parent.children.Contains(this)))
				{

					if (this._parent != null)
						Debug.Log(this._parent.name + " contains: " + this.name);
					return;
				}
				Vector3 position = this.position;
				Quaternion rotation = this.rotation;
				Vector3 scale = this._localScale;
				Console.WriteLine("TWICE?");
				if (this._parent != null && this._parent.children != null)
				{
					this._parent.children.RemoveOnly(this);
				}
				this._parent = value;
				if (this._parent != null)
				{
					Debug.Log("setting fuckin parent");
					if (parent.children == null)
					{
						this._parent.children = new TransformCollection(this._parent);
					}
					this._siblingIndex = this._parent.children.Count;

					this._parent.children.Add(this);
					this.position = position;
					this.rotation = rotation;
					this.localScale = scale;
				}
				if (this._parent != null)
				{
					Transform._roots.Remove(this);
				}
			}
		}

		[System.NonSerialized]
		[Goliat.NonSerialized]
		public Assimp.Scene m_model;
		private TransformCollection _children;
		public TransformCollection children
		{
			get
			{
				if (this._children == null)
				{
					this._children = new TransformCollection(this);
				}
				return (this._children);
			}
			set
			{
				if (this._children != null)
				{
					if (this._children != null)
					{
						for (int i = 0; i < this._children.Count; i++)
						{
							this._children[i].parent = null;
						}
					}
				}
				this._children = value;
				if (this._children != null)
				{
					for (int i = 0; i < this._children.Count; i++)
					{
						this._children[i].parent = this;
					}
				}
			}
		}
		[Goliat.NonSerialized]
		public Vector3 right
		{
			get
			{
				return (Vector3)(this.rotation * Vector3.right);
			}
			/*set
			{
				this.rotation = Quaternion.LookRotation(value);
			}*/
		}
		[Goliat.NonSerialized]
		public Vector3 up
		{
			get
			{
				return (Vector3)(this.rotation * Vector3.up);
			}
			/*set
			{
				this.rotation = Quaternion.LookRotation(value);
			}*/
		}
		[Goliat.NonSerialized]
		public Vector3 forward
		{
			get
			{
				return (Vector3)(this.rotation * Vector3.forward);
			}
			/*set
			{
				this.rotation = Quaternion.LookRotation(value);
			}*/
		}
		public void RotateAround(Vector3 worldPosition, Vector3 axis, float angl)
		{
			angl = (angl / 360) * (float)Math.PI * 2;
			Quaternion rotation1 = Quaternion.CreateFromAxisAngle(axis, angl);
			transform.rotation = (rotation1 * transform.rotation);
			transform.position = (rotation1 * (transform.position - worldPosition)) + worldPosition;
			this._updateFromWorld();
		}
		[Goliat.NonSerialized]
		public Vector3 localEulerAngles
		{
			get
			{
				return (this.rotation.eulerAngles);
			}
			set
			{
				this._localRotation.eulerAngles = value;
				this._updateFromLocal();
			}
		}
		public int childCount
		{
			get
			{
				if (this.children == null)
					return (0);
				return (this.children.Count);
			}
			set
			{
				;
			}
		}
		public int ChildCount
		{
			get
			{
				if (this.children == null)
					return (0);
				return (this.children.Count);
			}
		}
		public Transform(Vector3 pos, Quaternion rotation, Vector3 scale)
		{
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this.children = new TransformCollection(this);
			this._parent = null;
			this._position = pos;
			this._rotation = rotation;
			this._localScale = scale;
			this.position = new Vector3(pos);
			this.rotation = new Quaternion(rotation);
			this.localScale = new Vector3(scale);
			this._updateFromLocal();
		}
		public Transform(Vector3 pos, Quaternion rotation, Vector3 scale, Space type)
		{
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this.children = new TransformCollection(this);
			this._parent = null;
			this._position = pos;
			this._rotation = rotation;
			this._localScale = scale;
			this.position = new Vector3(pos);
			this.rotation = new Quaternion(rotation);
			this.localScale = new Vector3(scale);
			if (type == Space.world)
				this._updateFromLocal();
			else
			{
				this._localPosition = pos;
				this._localRotation = rotation;
				this._localScale = scale;
				this._updateFromLocal();
			}
		}
		public Transform()
		{
			Debug.Log("YOYO NEW TRANSFORM");// Console.WriteLine("YOYO NEW TRANSFORM");
			if (!Transform.Transforms.Contains(this))
				Transform.Transforms.Add(this);
			this._parent = null;
			this.children = new TransformCollection(this);
			this.position = Vector3.zero;
			this.rotation = Quaternion.identity;
			this.localScale = Vector3.one;
			this._updateFromLocal();
		}
		/*		public void Lookat(Transform target)
				{

			Vector3  f = (target - this.position).normalized;
			Vector3  u = normalize(up);
			Vector3  s = normalize(cross(f, u));
			u = cross(s, f);

			mat4x4 Result(1);
			Result[0][0] = s.x;
			Result[1][0] = s.y;
			Result[2][0] = s.z;
			Result[0][1] = u.x;
			Result[1][1] = u.y;
			Result[2][1] = u.z;
			Result[0][2] =-f.x;
			Result[1][2] =-f.y;
			Result[2][2] =-f.z;
			Result[3][0] =-dot(s, eye);
			Result[3][1] =-dot(u, eye);
			Result[3][2] = dot(f, eye);
			return Result;
				*/
		/*Vector3 upVector = -(target.position - this.position).normalized;
		//This part is tricky...I need to use the original forward vector but clean it up so
		//it is 90 degrees from the new Up Vector. So use Math3d.ProjectVectorOnPlane...get Math3d!
		Vector3 planeNormal = upVector;
		Vector3 vector = this.transform.forward.normalized;
		vector = vector - (Vector3.Dot(vector, planeNormal) * planeNormal);
		//Vector3 forwardVector = Math3d.ProjectVectorOnPlane(upVector, myMesh.transform.forward).normalized;
		this.rotation = Quaternion.FromDirection(vector, upVector);
		//this.myNewMesh.transform.rotation = Quaternion.LookRotation(forwardVector, upVector);
*/
		//	}
		public Transform LookAt(Transform target, Vector3 up)
		{

			/*Vector3 position = target.transform.position;
			Matrix matrix = Matrix.Identity;
			Vector3 forward = position - this.transform.position;
			Vector3 newup = up;
			if (parent != null)
			{
				forward = this.worldToLocalMatrix * forward;
				newup = parent.worldToLocalMatrix * up;
			}
			float length = (float)Mathf.Sqrt( (forward[0] * forward[0]) + (forward[1] * forward[1]) + (forward[2] * forward[2]) );
			if ( length != 0 ) {
				forward[0] /= length;
				forward[1] /= length;
				forward[2] /= length;
			} else {
				forward[0] = Vector3.forward.x;
				forward[1] = Vector3.forward.y;
				forward[2] = Vector3.forward.z;
			}
			length = (float)Mathf.Sqrt( up[0] * up[0] + up[1] * up[1] + up[2] * up[2] );
			if ( length != 0) {
				newup[0] /= length;
				newup[1] /= length;
				newup[2] /= length;
			} else {
				newup[0] = Vector3.up.x;
				newup[1] = Vector3.up.y;
				newup[2] = Vector3.up.z;
			}
			Vector3 left = Vector3.Cross(newup, forward);
			if ( left[0]==0 && left[1]==0 && left[2]==0 ) {
				left[0] = transformation[0];
				left[1] = transformation[1];
				left[2] = transformation[2];
			}
			newup = Vector3.Cross( forward, left );
			matrix[0] = left[0];
			matrix[1] = left[1];
			matrix[2] = left[2];
			matrix[3] = 0; // -Vector3.Dot(left, this.transform.position);
			matrix[4] = newup[0];
			matrix[5] = newup[1];
			matrix[6] = newup[2];
			matrix[7] = 0; // -Vector3.Dot(newup, this.transform.position);
			matrix[8] = forward[0];
			matrix[9] = forward[1];
			matrix[10] = forward[2];
			matrix[11] = 0;// -Vector3.Dot(forward, this.transform.position);
			matrix[12] = 0;
			matrix[13] = 0;
			matrix[14] = 0;
			matrix[15] = 1;*/
			/*matrix[0] = left[0];
		   matrix[4] = left[1];
		   matrix[8] = left[2];
		   matrix[12] = 0.0f;
		   //------------------
		   matrix[1] = up[0];
		   matrix[5] = up[1];
		   matrix[9] = up[2];
		   matrix[13] = 0.0f;
		   //------------------
		   matrix[2] = -forward[0];
		   matrix[6] = -forward[1];
		   matrix[10] = -forward[2];
		   matrix[14] = 0.0f;
		   //------------------
		   matrix[3] = matrix[7] = matrix[11] = 0.0f;
		   matrix[15] = 1.0f;*/
			//Console.WriteLine("matrix:" + matrix.ToString());
			//matrix.Transpose();
			//Matrix translation = Matrix.translation(this._localPosition);
			//this._localRotation = matrix.quaternion;
			//Matrix rotation = matrix;
			//Matrix scale = Matrix.scale(this.localScale);
			Matrix4 rotationmat = Matrix4.LookAt(this.position.gl(), target.position.gl(), up.gl());
			rotationmat.Transpose();
			Matrix matrix = rotationmat.octogon();
			Vector3 scale;// = Vector3.zero;
			Quaternion rot;
			Vector3 pos;
			matrix.Decompose(out scale, out rot, out pos);
			//			this._transformation = translation * rotation * scale;
			//this.localScale = scale;
			//this.position = pos;
			//this.rotation = rot;
			this.rotation = rot.inverse;//new Quaternion(-rot.x, rot.y, -rot.z, rot.w);
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return (this);
		}

		public Quaternion transformRotation(Quaternion direction)
		{
			return (this.transform.rotation * direction);
		}
		public Quaternion inverseTransformRotation(Quaternion direction)
		{
			return (this.transform.rotation.inverse * direction);
		}

		public Transform LookAt(Vector3 worldPosition, Vector3 up)
		{
			Matrix4 rotationmat = Matrix4.LookAt(this.position.gl(), worldPosition.gl(), up.gl());
			rotationmat.Transpose();
			Matrix matrix = rotationmat.octogon();
			Vector3 scale;// = Vector3.zero;
			Quaternion rot;
			Vector3 pos;
			matrix.Decompose(out scale, out rot, out pos);
			//			this._transformation = translation * rotation * scale;
			//this.localScale = scale;
			//this.position = pos;
			//this.rotation = rot;
			this.rotation = rot.inverse;//new Quaternion(-rot.x, rot.y, -rot.z, rot.w);
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return (this);
			/*
			Matrix4 rotationmat = Matrix4.LookAt(this.position.gl(), worldPosition.gl(), up.gl());
			rotationmat.Transpose();
			Matrix matrix = rotationmat.octogon();
			Vector3 scale;// = Vector3.zero;
			Quaternion rot;
			Vector3 pos;
			matrix.Decompose(out scale, out rot, out pos);*/
			//			this._transformation = translation * rotation * scale;
			//this.localScale = scale;
			//this.position = pos;
			//this.rotation = rot;
			/*	this.rotation = rot.inverse;//new Quaternion(-rot.x, rot.y, -rot.z, rot.w);
				if (this.parent != null)
				{
					this._position = parent.transformPoint(this.localPosition);
					this._rotation = parent.transformRotation(this.localRotation);
				}
				else
				{
					this._position = this._localPosition;
					this._rotation = this._localRotation;
				}
				return (this);
				*/
			/*Vector3 position = worldPosition;
			Matrix matrix = Matrix.Identity;
			Vector3 forward = position - this.transform.position;
			Vector3 newup = up;
			if (parent != null)
			{
				forward = parent.worldToLocalMatrix * forward;
				newup = parent.worldToLocalMatrix * up;
			}
			float length = (float)Mathf.Sqrt( (forward[0] * forward[0]) + (forward[1] * forward[1]) + (forward[2] * forward[2]) );
			if ( length != 0 ) {
				forward[0] /= length;
				forward[1] /= length;
				forward[2] /= length;
			} else {
				forward[0] = Vector3.forward.x;
				forward[1] = Vector3.forward.y;
				forward[2] = Vector3.forward.z;
			}
			length = (float)Mathf.Sqrt( up[0] * up[0] + up[1] * up[1] + up[2] * up[2] );
			if ( length != 0 ) {
				newup[0] /= length;
				newup[1] /= length;
				newup[2] /= length;
			} else {
				newup[0] = Vector3.up.x;
				newup[1] = Vector3.up.y;
				newup[2] = Vector3.up.z;
			}
			Vector3 left = Vector3.Cross(newup, forward);
			if ( left[0]==0 && left[1]==0 && left[2]==0 ) {
				left[0] = transformation[0];
				left[1] = transformation[1];
				left[2] = transformation[2];
			}
			newup = Vector3.Cross( forward, left );
			matrix[0] = left[0];
			matrix[1] = left[1];
			matrix[2] = left[2];
			matrix[3] = 0;
			matrix[4] = newup[0];
			matrix[5] = newup[1];
			matrix[6] = newup[2];
			matrix[7] = 0;
			matrix[8] = forward[0];
			matrix[9] = forward[1];
			matrix[10] = forward[2];
			matrix[11] = 0;
			matrix[12] = 0;
			matrix[13] = 0;
			matrix[14] = 0;
			matrix[15] = 1;
			Matrix translation = Matrix.translation(this._localPosition);
			this._localRotation = matrix.quaternion;
			Matrix rotation = matrix;
			Matrix scale = Matrix.scale(this.localScale);
			this._transformation = translation * rotation * scale;
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return (this);*/
		}
		public void LookAt(Vector3 position, Vector3 up, Space type)
		{
			Matrix matrix = Matrix.Identity;
			if (this.transform == null)
			{
				;//MessageBox.Show("nulll");
			}
			Vector3 forward = position - this.transform.position;//
			Vector3 newup = up;
			if (parent != null && type == Space.world)
			{
				forward = parent.worldToLocalMatrix * forward;
				newup = parent.worldToLocalMatrix * up;
			}
			else if (type == Space.self)
			{
				forward = position - this.transform.localPosition;
				newup = up;
			}
			float length = (float)Mathf.Sqrt((forward[0] * forward[0]) + (forward[1] * forward[1]) + (forward[2] * forward[2]));
			if (length != 0)
			{
				forward[0] /= length;
				forward[1] /= length;
				forward[2] /= length;
			}
			else
			{
				forward[0] = Vector3.forward.x;
				forward[1] = Vector3.forward.y;
				forward[2] = Vector3.forward.z;
			}
			length = (float)Mathf.Sqrt(up[0] * up[0] + up[1] * up[1] + up[2] * up[2]);
			if (length != 0)
			{
				newup[0] /= length;
				newup[1] /= length;
				newup[2] /= length;
			}
			else
			{
				newup[0] = Vector3.up.x;
				newup[1] = Vector3.up.y;
				newup[2] = Vector3.up.z;
			}
			Vector3 left = Vector3.Cross(newup, forward);
			if (left[0] == 0 && left[1] == 0 && left[2] == 0)
			{
				left[0] = transformation[0];
				left[1] = transformation[1];
				left[2] = transformation[2];
			}
			newup = Vector3.Cross(forward, left);
			matrix[0] = left[0];
			matrix[1] = left[1];
			matrix[2] = left[2];
			matrix[3] = 0;
			matrix[4] = newup[0];
			matrix[5] = newup[1];
			matrix[6] = newup[2];
			matrix[7] = 0;
			matrix[8] = forward[0];
			matrix[9] = forward[1];
			matrix[10] = forward[2];
			matrix[11] = 0;
			matrix[12] = 0;
			matrix[13] = 0;
			matrix[14] = 0;
			matrix[15] = 1;
			Matrix translation = Matrix.translation(this._localPosition);
			this._localRotation = matrix.quaternion;
			Matrix rotation = matrix;
			Matrix scale = Matrix.scale(this.localScale);
			this._transformation = translation * rotation * scale;
			if (this.parent != null)
			{
				this._position = parent.transformPoint(this.localPosition);
				this._rotation = parent.transformRotation(this.localRotation);
			}
			else
			{
				this._position = this._localPosition;
				this._rotation = this._localRotation;
			}
			return;
		}
		public void goFrontTo(Transform target)
		{
			Vector3 planeNormal = transform.forward;
			Vector3 planeOrigin = target.position;
			if (this.parent != null)
			{
				planeOrigin = parent.inverseTransformPoint(target.position);
				planeNormal = (this.transformation * (-1 * Vector3.forward)).normalized;
			}
			Vector3 rayOrigin = this._localPosition;
			Vector3 rayDirection = planeNormal;
			Vector3 intersection = Mathf.intersectPlane(planeNormal, planeOrigin, rayOrigin, rayDirection);
			Vector3 translation = planeOrigin - intersection;
			this._localPosition += translation;
			this._updateFromLocal();
		}
		public Vector3 transformPoint(Vector3 localPosition)
		{
			Vector3 res = this.localToWorldMatrix * localPosition;
			return (res);
		}
		public Vector3 inverseTransformPoint(Vector3 worldPosition)
		{
			Vector3 res = this.worldToLocalMatrix * worldPosition;
			return (res);
		}
		public Transform(Transform other)
		{
			Transforms.Add(this);
			this.position = other.position;
			this.rotation = other.rotation;
			this.localScale = Vector3.one;
			this._updateFromLocal();
		}
		public static Transform zero
		{
			get
			{
				Transform res = new Transform(Vector3.zero, Quaternion.identity, Vector3.one);
				res.name = "Transform" + res.id;
				return (res);
			}
		}



		/*
			

			Transform();
			Transform(const Transform& copy);
			void			DetachChildren();
			Transform		Find(std::string childName);
			Transform		GetChild(std::size_t index);
			std::size_t		GetSiblingIndex(); //son propre index dans son parent
			Quaternion		*InverseTransformDirection(Quaternion direction);
			Vector3			*InverseTransformPoint(Vector3 position);
			Vector3			*InverseTransformVector(Vector3 vector); // world to local;
			
			void			LookAt(Vector3 worldPosition);
			void			LookAt(Vector3 position, Space space);
			void			Rotate(Vector3 eulerAngles, Space space);
			void			Rotate(Vector3 eulerAngles);
			void			RotateAround(Vector3 point, Vector3 axis, float angle);
			void			SetAsFirstSibling();
			void			SetAsLastSibling();
			void			SetParent(Transform parent);
			void			SetSiblingIndex(int index);
			Quaternion		*TransformDirection(Quaternion *direction); //local to world;
			Vector3			*TransformPosition(Vector3 *direction); //local to world;		
			Quaternion		*TransformVector(Vector3 *vector);
			void			Translate(Vector3 *translation);
		private:
			bool			_updated;
			void			checkUpdate()
			{
				if (this._updated)
				{
					this._updated = false;
					Matrix		*translation = Matrix::Translation(this.localPosition);
					Matrix		*rotation = Matrix::Rotation(this.localRotation);
					Matrix		*scale = Matrix::Scale(this.localScale);
					this.transformation = (*scale) * (*rotation) * (*translation);
				}
			}
			Matrix			*transformation;
	};*/
	}
}
