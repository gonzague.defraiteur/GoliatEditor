﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

using Goliat;
using GL = OpenTK.Graphics.OpenGL.GL;
namespace Goliat
{
	public struct Rect
	{
		private float _x;
		private float _y;
		private float _xMax;
		private float _yMax;
		public float x
		{
			get
			{
				return (this._x);
			}
			set
			{
				this._x = value;
			}
		}
		public float y
		{
			get
			{
				return (this._y);
			}
			set
			{
				this._y = value;
			}
		}
		public float xMax
		{
			get
			{
				return (this._xMax);
			}
			set
			{
				this._xMax = value;
			}
		}
		public float yMax
		{
			get
			{
				return (this._yMax);
			}
			set
			{
				this._yMax = value;
			}
		}
		public Rect(float x, float y, float xMax, float yMax)
		{
			this._x = x;
			this._y = y;
			this._yMax = yMax;
			this._xMax = xMax;
		}
		public float width
		{
			get
			{
				return ((float)Math.Abs(this.xMax - this._x));
			}
		}
		public float height
		{
			get
			{
				return ((float)Math.Abs(this.yMax - this.y));
			}
		}
	}

	public class Camera : GameBehaviour
	{
		public static readonly List<Camera> Cameras = new List<Camera>();
		private float _nearClipPlane;
		private float _farClipPlane;
		private static Camera _main = null;
		private static Camera _current = null;
		public static Camera current
		{
			get
			{
				if (_current == null)
					return (main);
				else
					return (_current);
			}
			set
			{
				_current = value;
			}
		}
		public static Camera main
		{
			get
			{
				if (_main == null)
				{
					if (Cameras.Count > 0)
					{
						_main = Cameras[0];
					}
				}
				return (_main);
			}
			set
			{
				_main = value;
			}
		}
		public Matrix4 projView()
		{
			Matrix4 projectionMatrix = this.perspectiveMatrix.gl(); //Matrix4.CreatePerspectiveFieldOfView(cam.fov.toRad(), cam.aspectRatio, cam.nearClipPlane, cam.farClipPlane);
			Matrix4 modelViewMatrix = transform.worldToLocalMatrix.gl();
			modelViewMatrix.Transpose();
			return (modelViewMatrix * projectionMatrix);
		}
		private void updatePerspective()
		{
			if (this._orhtographic)
			{
				this._perspectiveMatrix = Matrix.Orthographic(0, 0, this.width, this.height, this._nearClipPlane, this._farClipPlane);
			}
			else
			{
				this._perspectiveMatrix = Matrix.Projection(this._fov, this.aspectRatio, this._nearClipPlane, this._farClipPlane);
			}
		}
		public void debug()
		{
			float start = -1;
			float end = 1;
			while (start < end)
			{
				Ray ray = new Ray(this.transform.position, this.viewPortPointToRay(new Vector3(start, start, 0.5f)) - this.transform.position);
				Debug.DrawLine(ray.origin, ray.origin + ray.direction);
				ray = new Ray(this.transform.position, this.viewPortPointToRay(new Vector3(start, (end - start) - 1, 0.5f)) - this.transform.position);
				Debug.DrawLine(ray.origin, ray.origin + ray.direction);
				start += 0.01f;
			}
		}

		public Rect pixelRect;
		private Matrix _perspectiveMatrix;
		public Matrix perspectiveMatrix
		{
			get
			{
				return (this._perspectiveMatrix);
			}
			set
			{
				if (value.width == 4 && value.height == 4)
					this._perspectiveMatrix = value;
			}
		}

		public Matrix projectionMatrix
		{
			get
			{
				return (this._perspectiveMatrix);
			}
			set
			{
				if (value.width == 4 && value.height == 4)
					this._perspectiveMatrix = value;
			}
		}
		public float nearClipPlane
		{
			get
			{
				return (this._nearClipPlane);
			}
			set
			{
				this._nearClipPlane = value;
				this.updatePerspective();
			}
		}
		public float farClipPlane
		{
			get
			{
				return (this._farClipPlane);
			}
			set
			{
				this._farClipPlane = value;
				this.updatePerspective();
			}
		}

		private bool _orhtographic = false;
		public bool orthographic
		{
			get
			{
				return (this._orhtographic);
			}
			set
			{
				this._orhtographic = value;
				this.updatePerspective();
			}

		}
		private float _fov = 60;
		public float fov
		{
			get
			{
				return (this._fov);
			}
			set
			{
				this._fov = value;
				this.updatePerspective();

			}
		}
		private int _width;
		public int width
		{
			get
			{
				return (this._width);
			}
			set
			{
				this._width = value;
				this.updatePerspective();
			}
		}
		public float aspectRatio
		{
			get
			{
				return ((float)this._width / (float)this._height);
			}
			set
			{
				;
			}
		}
		private int _height;
		public int height
		{
			get
			{
				return (this._height);
			}
			set
			{
				this._height = value;
				this.updatePerspective();
			}
		}

		public void PickMatrix(double x, double y, double deltax, double deltay, int[] viewport)
		{
			if (deltax <= 0 || deltay <= 0)
			{
				return;
			}
			GL.Translate((viewport[2] - 2 * ((x + deltax) - viewport[0])) / deltax,
				(viewport[3] - 2 * ((y) - viewport[1])) / deltay, 0);
			GL.Scale(viewport[2] / deltax, viewport[3] / deltay, 1.0);
		}

		public Transform GetSelection(int mouseX, int mouseY, GameWindow game)
		{

			float sw = 1;
			float sh = 1;
			mouseX = mouseX - 1;
			int[] selectBuffer = new int[1024];
			int hits;
			int[] viewport = new int[4];
			//GL.GetInteger(GetPName.Viewport, viewport);
			viewport = new int[] { 0, 0, game.Width, game.Height };
			GL.Viewport(0, 0, game.Width, game.Height);
			GL.SelectBuffer(1024, selectBuffer);
			GL.RenderMode(OpenTK.Graphics.OpenGL.RenderingMode.Select);
			GL.InitNames();
			float aspectRatio = game.Width / (float)game.Height;
			Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, 1, 1000);

			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.LoadIdentity();
			PickMatrix(mouseX, viewport[3] - mouseY, sw, sh, viewport);
			GL.MultMatrix(ref perspective);

			((dynamic)game).OnRenderFrame();
			((dynamic)game).SwapBuffers();
			GL.Flush();
			float distance = -1;
			int selectedIndex = -1;
			hits = GL.RenderMode(RenderingMode.Render);
			if (hits != 0)
			{
				for (int n = 0; n < hits; n++)
				{
					uint newDist = (uint)selectBuffer[n * 4 + 1];
					if (distance >= newDist || distance == -1)
					{
						distance = newDist;
						selectedIndex = selectBuffer[n * 4 + 3];
					}
				}
			}
			if (distance != -1)
			{
				return (((Transform)(GameBehaviour.GameBehaviours[selectedIndex])));
			}

			GL.PopName();
			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
			return (null);
		}

		public List<Transform> GetSelectionList(int mouseX, int mouseY, GameWindow game)
		{
			float sw = 1;
			float sh = 1;
			mouseX = mouseX - 1;
			int[] selectBuffer = new int[1024];
			int hits;
			int[] viewport = new int[4];
			GL.GetInteger(GetPName.Viewport, viewport);
			GL.Viewport(0, 0, game.Width, game.Height);
			GL.SelectBuffer(1024, selectBuffer);
			GL.RenderMode(RenderingMode.Select);

			GL.InitNames();

			float aspectRatio = game.Width / (float)game.Height;
			Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, 1, 1000);

			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.LoadIdentity();
			PickMatrix(mouseX, viewport[3] - mouseY, sw, sh, viewport);
			GL.MultMatrix(ref perspective);

			((dynamic)game).OnRenderFrame();
			((dynamic)game).SwapBuffers();


			GL.Flush();
			hits = GL.RenderMode(RenderingMode.Render);
			List<Transform> res = new List<Transform>();
			if (hits != 0)
			{
				for (int n = 0; n < hits; n++)
				{
					//uint newDist = (uint)selectBuffer[n * 4 + 1];
					res.Add((Transform)GameBehaviour.GameBehaviours[selectBuffer[n * 4 + 3]]);
				}
			}


			GL.PopName();
			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
			return (res);
		}

		public Vector3 viewPortPointToRay(Vector3 viewPortPoint)
		{
			Camera cam = this;
			Matrix projectionMatrix = cam.projectionMatrix;
			Matrix modelViewMatrix = cam.GetComponent<Transform>().localToWorldMatrix;
			Matrix transformMatrix = Matrix.Inverse((projectionMatrix.transposed())); //* modelViewMatrix);
			Vector4 inVector = new Vector4(viewPortPoint.x, viewPortPoint.y, viewPortPoint.z, 1);
			Vector4 res1 = transformMatrix * inVector;
			float mult = 1.0f / res1.w;
			return (modelViewMatrix * new Vector3(res1.x * mult, res1.y * mult, res1.z * mult));
		}

		public Vector3 unProject(Vector3 screenPosition)
		{
			Camera cam = this;
			float winX = screenPosition.x;
			float winY = screenPosition.y;
			float winZ = screenPosition.z;
			Matrix projectionMatrix = cam.projectionMatrix;

			Matrix modelViewMatrix = cam.GetComponent<Transform>().worldToLocalMatrix;
			Vector3 objpos = new Vector3(0, 0, 0);
			//Matrix tmp = projectionMatrix * modelViewMatrix;
			Matrix transformMatrix = Matrix.Inverse((projectionMatrix) * modelViewMatrix);
			Vector4 inVector = new Vector4(
			  ((2.0f * (winX) / Convert.ToSingle(cam.width))) - 1.0f,
			  ((2.0f * (winY) / Convert.ToSingle(cam.height))) - 1.0f,
			  (2 * -winZ - 1.0f),
			  1.0f);
			Vector4 res1 = transformMatrix * inVector;
			float mult = 1.0f / res1.w; /// res1.w;
			objpos = (new Vector3(res1.x * mult, res1.y * mult, res1.z * mult));
			return objpos;
		}
		/*
		 * was correctly working
		 Camera cam = this;
			Matrix4 projectionMatrix = Matrix4.CreatePerspectiveFieldOfView(cam.fov.toRad(), cam.aspectRatio, cam.nearClipPlane, cam.farClipPlane);
			Matrix4 modelViewMatrix = transform.worldToLocalMatrix.gl();
			modelViewMatrix.Transpose();
				//cam.projectionMatrix;
			//Matrix modelViewMatrix = ;
			Vector3 objpos = new Vector3(0, 0, 0);
			//Matrix tmp = projectionMatrix * modelViewMatrix;
			Matrix4 transformMatrix = (modelViewMatrix * projectionMatrix);
			transformMatrix.Invert();
			//Matrix transformMatrix = Matrix.Inverse((projectionMatrix *  cam.getComponent<Transform>().worldToLocalMatrix));
			OpenTK.Vector4 inVector = new OpenTK.Vector4(
			  ((2f * (winX) / cam.width)) - 1f,
			  - ((2f * (winY) / cam.height) - 1f),
			  2f * (winZ) - 1f,
			  1.0f);
			OpenTK.Vector4 res1 = OpenTK.Vector4.Transform(inVector, transformMatrix);
			float mult = 1.0f / res1.W; /// res1.w;
			objpos = (new Vector3(res1.X * mult, res1.Y * mult, res1.Z * mult));
				//modelViewMatrix * 
			objpos = cam.transform.position - objpos;
			return objpos;
		 */
		public Vector3 unProject(float winX, float winY, float winZ)
		{
			Camera cam = this;
			float start = Time.time;
			Matrix4 projectionMatrix = this.perspectiveMatrix.gl(); //Matrix4.CreatePerspectiveFieldOfView(cam.fov.toRad(), cam.aspectRatio, cam.nearClipPlane, cam.farClipPlane);
			Matrix4 modelViewMatrix = transform.worldToLocalMatrix.gl();
			modelViewMatrix.Transpose();

			//Debug.Log("UNPROJt0:" + (Time.time - start));
			start = Time.time;
			//cam.projectionMatrix;
			//Matrix modelViewMatrix = ;
			Vector3 objpos = new Vector3(0, 0, 0);
			//Matrix tmp = projectionMatrix * modelViewMatrix;
			Matrix4 transformMatrix = (modelViewMatrix * projectionMatrix);
			transformMatrix.Invert();
			//Debug.Log("UNPROJt1:" + (Time.time - start));
			//start = Time.time;
			//Matrix transformMatrix = Matrix.Inverse((projectionMatrix *  cam.getComponent<Transform>().worldToLocalMatrix));
			OpenTK.Vector4 inVector = new OpenTK.Vector4(
			  ((2f * (winX) / cam.width)) - 1f,
			  -((2f * (winY) / cam.height) - 1f),
			  2f * (winZ) - 1f,
			  1.0f);
			OpenTK.Vector4 res1 = OpenTK.Vector4.Transform(inVector, transformMatrix);
			//Debug.Log("UNPROJt2:" + (Time.time - start));
			//start = Time.time;
			float mult = 1.0f / res1.W; /// res1.w;
			objpos = (new Vector3(res1.X * mult, res1.Y * mult, res1.Z * mult));
			//modelViewMatrix * 
			objpos = cam.transform.position - objpos;
			//Debug.Log("UNPROJt3:" + (Time.time - start));
			return objpos;
		}

		public Ray ScreenPointToRay(Vector2 screenPosition)
		{
			float start = Time.time;
			float winX = screenPosition.x;
			float winY = screenPosition.y;
			Vector3 result1 = unProject(
			  winX, winY, 0f);
			Vector3 result2 = unProject(
			  winX, winY, 1f);
			result1 = this.transform.position;
			Vector3 direction = result2 - result1;
			//direction = direction.rounded(2);
			//direction = direction.normalized;
			//Debug.Log("STRTEMPS0:" + (Time.time - start));
			Ray res = new Ray(result1, direction);
			//Debug.Log("STR temps:" + (Time.time - start));
			return (res);
		}

		public Vector3 WorldToCameraPosition(Vector3 position)
		{
			Vector3 localPos = position;
			if (this.transform != null)
				localPos = this.transform.inverseTransformPoint(position);
			Vector3 screenPos = localPos * this.perspectiveMatrix;

			Vector2 p1 = new Vector2((screenPos.x / screenPos.w), (screenPos.y / screenPos.w));// , dy * this.nearClipPlane, this.nearClipPlane);
			//Vector3 p2 = new Vector3(dx * this.farClipPlane, dy * this.farClipPlane, this.farClipPlane);
			//p1 += new Vector2
			Vector3 res = new Vector3((p1.x * this.width / 2) + this.width / 2, (p1.y * this.height / 2) + this.height / 2, screenPos.z);
			return (res);
		}

		/*		public Vector2 get2dPoint(Vector3 vector, Matrix viewMatrix, Matrix projectionMatrix, int width, int height)
				{
						Matrix viewProjectionMatrix = projectionMatrix * viewMatrix;
						//transform world to clipping coordinates
						Vector3 point3D = viewProjectionMatrix.multiply(point3D);
						int winX = (int) Math.round((( point3D[0] + 1 ) / 2.0) *
													 width );
						//we calculate -point3D.getY() because the screen Y axis is
						//oriented top->down 
						int winY = (int) Math.round((( 1 - point3D.Y ) / 2.0) *
													 height );
						return (new Vector2(winX, winY));
				}
		*/
		/*public Ray screenPointToRay(Vector2 screenPosition)
		{
			float dx = ((x / (this.width / 2)) -1.0f)/this.aspectRatio;
			if (!this.orthographic)
				dx *= Mathf.tan(this.fov * 0.5f);
			float dy = 1.0f - (y / (this.height / 2));
			if (!this.orthographic)
				dy *= tanf(FOV*0.5f);
			Vector3 p1 = new Vector3(dx * this.nearClipPlane, dy * this.nearClipPlane, this.nearClipPlane);
			Vector3 p2 = new Vector3(dx * this.farClipPlane, dy * this.farClipPlane, this.farClipPlane);
			return (new Ray(p1, p2));
		}*/
		public Camera()
		{
			Camera.Cameras.Add(this);
			//Console.WriteLine("ok");

			this._width = (int)1024;
			this._height = (int)768;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = 1;
			this._farClipPlane = 64;
		}
		private Transform _pivotPoint;
		public Transform pivotPoint
		{
			get
			{
				if (_pivotPoint == null)
					_pivotPoint = Transform.zero;
				return (_pivotPoint);
			}
			set
			{
				_pivotPoint = value;
			}
		}
		public Camera(int width, int height)
		{
			this._width = (int)width;
			this._height = (int)height;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = 1;
			this._farClipPlane = 64;
		}
		public Camera(Vector2 size)
		{
			this._width = (int)size.x;
			this._height = (int)size.y;
			this._fov = (float)(Mathf.PI / 4);
			this._nearClipPlane = 1;
			this._farClipPlane = 64;
		}
		public Camera(Vector2 size, float fov, float nearClipPlane, float farClipPlane)
		{
			this._width = (int)size.x;
			this._height = (int)size.y;
			this._fov = fov;
			this._nearClipPlane = nearClipPlane;
			this._farClipPlane = farClipPlane;
		}
		public Camera(int width, int height, float fov, float nearClipPlane, float farClipPlane)
		{
			this._width = (int)width;
			this._height = (int)height;
			this._fov = fov;
			this._nearClipPlane = nearClipPlane;
			this._farClipPlane = farClipPlane;
		}

		//(MathHelper.PiOver4, aspectRatio, 1, 64);
	}
}
