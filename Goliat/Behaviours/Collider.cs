﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter;
using JMaterial = Jitter.Dynamics.Material;
using Jitter.Collision;
using JOctree = Jitter.Collision.Octree;
using JVector = Jitter.LinearMath.JVector;
using Shape = Jitter.Collision.Shapes.Shape;
using JRigidBody = Jitter.Dynamics.RigidBody;
//using BulletSharp;
using Jitter.Collision.Shapes;
namespace Goliat
{
		public class PhysicMaterial : SharedResource
		{
			private float _bounciness = 0;
			private float _dynamicFriction = 0;
			private float _staticFriction = 0;
			public float bounciness
			{
				get
				{
					return (this._bounciness);
				}
				set
				{
					this._bounciness = value;
					this.jmaterial.Restitution = this._bounciness;
				}
			}
			public float dynamicFriction
			{
				get
				{
					return (this._dynamicFriction);
				}
				set
				{
					this._dynamicFriction = value;
					this.jmaterial.KineticFriction = this._bounciness;
				}
			}
			public float staticFriction
			{
				get
				{
					return (this._staticFriction);
				}
				set
				{
					this._staticFriction = value;
					this.jmaterial.StaticFriction = this._bounciness;
				}
			}

			private JMaterial jmaterial;
			public static List<PhysicMaterial> PhysicMaterials = new List<PhysicMaterial>();
			public PhysicMaterial()
			{
				PhysicMaterial.PhysicMaterials.Add(this);
			}
			public PhysicMaterial(float bounciness, float dynamicFriction, float staticFriction)
			{
				this.bounciness = bounciness;
				this.dynamicFriction = dynamicFriction;
				this.staticFriction = staticFriction;
				this.jmaterial = new JMaterial(); //this.bounciness, this.staticFriction, this.dynamicFriction);
				this.jmaterial.Restitution = this.bounciness;
				this.jmaterial.StaticFriction = this.staticFriction;
				this.jmaterial.KineticFriction = this.dynamicFriction;
			}
		}

		public struct ContactPoint
		{
			public Vector3 normal;
			public Collider otherCollider;
			public Vector3 point;
			public Collider thisCollider;
			public ContactPoint(Vector3 normal, Collider otherCollider, Vector3 point, Collider thisCollider)
			{
				this.normal = normal;
				this.otherCollider = otherCollider;
				this.point = point;
				this.thisCollider = thisCollider;
			}
		}

		public struct Collision
		{
			public Collider collider;
			public ContactPoint[] contacts;
			public GameObject gameObject;
			public Vector3 relativeVelocity;
			public RigidBody rigidbody;
			public Transform transform;
			public Collision(Collider collider, ContactPoint[] contacts, GameObject gameObject, Vector3 relativeVelocity, RigidBody rigidbody, Transform transform)
			{
				this.collider = collider;
				this.contacts = contacts;
				this.gameObject = gameObject;
				this.relativeVelocity = relativeVelocity;
				this.rigidbody = rigidbody;
				this.transform = transform;
			}
		}
		/*	
		attachedRigidbody	The rigidbody the collider is attached to.
		bounds	The world space bounding volume of the collider.
		contactOffset	Contact offset value of this collider.
		enabled	Enabled Colliders will collide with other colliders, disabled Colliders won't.
		isTrigger	Is the collider a trigger?
		material	The material used by the collider.
		sharedMaterial	The shared physic material of this collider.
		gameObject	The game object this component is attached to. A component is always attached to a game object.----------------------------------------
		tag	The tag of this game object.
		transform	The Transform attached to this GameObject (null if there is none attached).
		hideFlags	Should the object be hidden, saved with the scene or modifiable by the user?
		name
		*/
		/*
			
				PHYSIQUE PLAN GLOBAL:
				
				 * comment decide t on a quel moment on update depuis la scene, et a quel moment on update depuis le moteur physique:
				 * tres simple: lors de la modification de la position, rotation, localScale local ce que tu veux du transform, qui eventuellement changerait une position/rot/scale,
				 * on update aussi la meme chose niveau physique, si necessaire, 
				 * inversement, une fois par update, on set un bool dans le physic qui dit que c'est en cours d'update, et on update les positions, etc... depuis la physique.
				 *  
				 * 
				 * pour les rigidbody:
				 * 
				 * je sais pas encore comment éviter l'usage d une shape, mais on verra après, a part ça, la plupart du temps,
				 * il s agit la bas de fonctions qui servent a moduler les variables du jbody, mais rien de bien supplémentaire, par exemple,
				 * les drags réduisent coup aprés coup d'update les velocity, qu'elles soient angular ou linear.
				 * 
				 * celles qu'on ne sait pas trop quoi en faire, on passe pour le moment, genre (conemachin), pas sur que ce soit tout de suite utile.
				 * 
				 * pour les collider:
				 * 
				 * une shape + un rigidbody qui l'utilise.
				 * l'attachedRigidbody n'est qu'un getComponent un peu plus facile d'accès.
				 * 
				 * le trigger: faut faire deux trois tests pour etre sur, tester les isactive tout ça, ou bien simplement ne pas effectuer d'update coté pos/rot/scale pour eux,
				 * et les mettre dans un systeme de gestion de mondes a part.( + une espece de deuxieme version des autres objets, pour qu'ils puissent quand meme collider de leurs cotés et avec les triggers qunad necessaires,
				 * updatés jusqu'en velocity depuis leurs "parents" réels, a chaque tour d'update. genre un TriggerCollider, partageant la shape du "parent")
				 * 
				 *  * j'imagine que "OnTriggerStay" va check que l'objet est toujours en collision, 
				 * //Physic.world.CollisionSystem.Detect()
				//Physic.world.CollisionSystem .Detect(test, test); a mon avis va check si il peut appeler le delegate pour la detection.
				//Dans ce cas, check qu'une liste de collisions est toujours active sur les deux ou un truc du genre.
				// des qu'il detecte plus, collision exit ou trigger exit.
				 *
				 * 
				 * les layers:
				 * un monde par layer, que ce soit en TriggerCollider system ou en Collider system.
				 * 
				 * Physics.World[layer] (dico Layer ou string  => jworld)
				 * un collision system global? ou un par world? ou un par systeme? je pense un global semble suffir.
				 * 
				 * List<string ou Layer> Physics.CollisionLayers;
				 * 
				 * 
				 * 
				 * 
				 * 
				 * 
				 
				//Jitter.Dynamics.SoftBody body = new Jitter.Dynamics.SoftBody.
				JRigidBody test = new JRigidBody(null);
				CollisionSystem customSystem = new CollisionSystemBrute();
				 * */
	public class Physic
	{
		private static CollisionSystem collisionSystem = new CollisionSystemSAP();
		private static void CollisionDetected(JRigidBody body1, JRigidBody body2, JVector point1, JVector point2, JVector normal, float penetration) 
		{

			/*
			 * OnCollisionEnter etc... pas sur colliders, sur behaviours. ^^'.
			 * 
			 */
			GameBehaviour behav1 = (GameBehaviour)body1.Tag;
			GameBehaviour behav2 = (GameBehaviour)body2.Tag;

			ContactPoint[] contacts = new ContactPoint[2];
			ContactPoint contact1 = new ContactPoint(normal.goliat(), behav1.GetComponent<Collider>(), point1.goliat(), behav2.GetComponent<Collider>());
			ContactPoint contact2 = new ContactPoint(normal.goliat(), behav2.GetComponent<Collider>(), point2.goliat(), behav1.GetComponent<Collider>());
			contacts[0] = contact1;
			contacts[1] = contact2;
			Collision collision1 = new Collision(contact1.otherCollider, contacts, contact1.otherCollider.GetComponent<GameObject>(), (contact1.thisCollider.jrigidbody.LinearVelocity - contact1.otherCollider.jrigidbody.LinearVelocity).goliat(), contact1.otherCollider.GetComponent<RigidBody>(), contact1.otherCollider.GetComponent<Transform>());
			Collision collision2 = new Collision(contact2.otherCollider, contacts, contact2.otherCollider.GetComponent<GameObject>(), (contact2.thisCollider.jrigidbody.LinearVelocity - contact2.otherCollider.jrigidbody.LinearVelocity).goliat(), contact2.otherCollider.GetComponent<RigidBody>(), contact2.otherCollider.GetComponent<Transform>());
			//body1.
			behav1.OnCollisionEnter(collision1);
			behav2.OnCollisionEnter(collision2);
		
			//Collision collision = new Collision()  merde, unity gère le fait que plusieurs collisions a la fois arrivent, donc soit on fait comme eux, soit on décide 
			//Console.WriteLine(" Collision detected !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		private static bool _physics_inited = false;
		public static void Init()
		{
			if (!_physics_inited)
			{
				collisionSystem = new CollisionSystemSAP();
				world = new World(collisionSystem);
				world.Gravity = new JVector(0, -10, 0);
				//collisionSystem.CollisionDetected += new CollisionDetectedHandler(CollisionDetected);
				_physics_inited = true;

/*	BulletSharp
 				DefaultCollisionConfiguration collisionConf = new DefaultCollisionConfiguration();
				CollisionDispatcher dispatcher = new CollisionDispatcher(collisionConf);

				DbvtBroadphase broadphase = new DbvtBroadphase();
				_dynamicWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, null, collisionConf);
				_dynamicWorld.Gravity = new Vector3(0, -10, 0).bullet();*/
			}
		}
		static float lastRest = 0;
		public static void Update()
		{
			if (!_physics_inited)
			{
				Init();
			}
			/*float step = 1.0f / 100.0f;
			float deltaTime = Time.deltaTime + lastRest;
			float total = deltaTime - (Mathf.Repeat(deltaTime, step));
			//Console.WriteLine("total:" + total);
			lastRest = deltaTime - total;
			for (float start = 0f; start + step < total; start += step)
			{
				world.Step(step, false);
			}
			*/

			float coolFuckinLittleTardStep = 1.0f / 100.0f;
			//Console.WriteLine("DELTA TIME:" + Time.deltaTime);
			for (float start = 0f; start < Time.deltaTime; start += coolFuckinLittleTardStep)
			{
				world.Step(coolFuckinLittleTardStep, false);
			}
			//world.Step(, false);

			//world.Step(Time.deltaTime, false);
			//collisionSystem.Detect(true);
			/*for (int i = 0; i < GameBehaviour.GameBehaviours.Count; i++)
			{
				if (GameBehaviour.GameBehaviours[i] is Collider || GameBehaviour.GameBehaviours[i] is RigidBody)
				{
					((dynamic)GameBehaviour.GameBehaviours[i]).jrigidbody.Update();
				}
			}*/
		}
		/*private static BulletSharp.DiscreteDynamicsWorld _dynamicWorld = null;
		public static BulletSharp.DiscreteDynamicsWorld dynamicWorld
		{
			get
			{
				return (_dynamicWorld);
			}
			set
			{
				_dynamicWorld = value;
			}
		}*/
		private static World _world = null;
		public static World world
		{
			get
			{
				return (_world);
			}
			private set
			{
				_world = value;
			}
		}
		//Physic.world.CollisionSystem.Raycast() si need fonction raycast.
		

	}
	/*
	angularDrag	The angular drag of the object.
	angularVelocity	The angular velocity vector of the rigidbody.
	centerOfMass	The center of mass relative to the transform's origin.
	collisionDetectionMode	The Rigidbody's collision detection mode.
	constraints	Controls which degrees of freedom are allowed for the simulation of this Rigidbody.
	detectCollisions	Should collision detection be enabled? (By default always enabled).
	drag	The drag of the object.
	freezeRotation	Controls whether physics will change the rotation of the object.
	inertiaTensor	The diagonal inertia tensor of mass relative to the center of mass.
	inertiaTensorRotation	The rotation of the inertia tensor.
	interpolation	Interpolation allows you to smooth out the effect of running physics at a fixed frame rate.
	isKinematic	Controls whether physics affects the rigidbody.
	mass	The mass of the rigidbody.
	maxAngularVelocity	The maximimum angular velocity of the rigidbody. (Default 7) range { 0, infinity }.
	maxDepenetrationVelocity	Maximum velocity of a rigidbody when moving out of penetrating state.
	position	The position of the rigidbody.
	rotation	The rotation of the rigdibody.
	sleepThreshold	The mass-normalized energy threshold, below which objects start going to sleep.
	solverIterationCount	Allows you to override the solver iteration count per rigidbody.
	useConeFriction	Force cone friction to be used for this rigidbody.
	useGravity	Controls whether gravity affects this rigidbody.
	velocity	The velocity vector of the rigidbody.
	worldCenterOfMass	
	*/
	public enum CollisionDetectionMode
	{
		Discrete,
		Continuous,
		ContinuousDynamic
	}
	/*
	 * peut etre un systeme de monde aussi.
	 */
	public enum RigidbodyContraints
	{
		None,
		FreezePositionX,
		FreezePositionY,
		FreezePositionZ,
		FreezeRotationX,
		FreezeRotationY,
		FreezeRotationZ,
		FreezePosition,
		FreezeRotation,
		FreezeAll
	} // change ce qu'il se passe pendant une update n'update pas comme il faut les valeurs concernées, et les reset plutot coté jitter.
	public enum RigidbodyInterpolation
	{
		None,
		Interpolate,
		Extrapolate
	}
	public enum ForceMode
	{
		Force,
		Impulse,
		VelocityChange
	}
	public class RigidBody : GameBehaviour
	{
		public float angularDrag = 0;
		public Vector3 angularVelocity
		{
			get
			{
				return (jrigidbody.AngularVelocity.goliat());
			}
			set
			{
				jrigidbody.AngularVelocity = value.jitter();
				//CollisionDetectionMode.
				//Jitter.Collision.
			}
		}
		//sait pas encore trop on verra
		public Vector3 centerOfMass;
		
		public CollisionDetectionMode collisionDetectionMode;
		public RigidbodyContraints contraints;
		public bool detectCollisions;
		public float drag = 0;
		public bool freezeRotation;
		public Vector3 inertiaTensor
		{
			get
			{
				return (jrigidbody.Inertia.goliat().quaternion.eulerAngles);
			}
			/*set
			{
				Matrix tmp = new Quaternion(value).orthogonalMatrix;
				jrigidbody.Inertia = tmp.jitter;
			}*/
		}
		public Quaternion inertiaTensorRotation
		{
			get
			{
				return (jrigidbody.Inertia.goliat().quaternion);
			}
		}
		/*
		 * d apres unity utilisé pour smooth la physique entre les updates, mais chelou on verra après.
		 * public RigidbodyInterpolation interpolation
		{
			get
			{
				;
			}
			set
			{
				//jrigidbody.Torque
				;
			}
		}
		 */
		public bool isKinematic;
		// encore du mal dans la tete.
		/*
		 * 
		 * a priori il est plus trop affecté par les collisions et les forces ou les "joints" 
		 * 
		 * ca sert a priori pour transformer vite un truc qui n'est pas affecté par des collisions en ragdoll.
		 * 
		 */
		public float mass
		{
			get
			{
				return (jrigidbody.Mass);
			}
			set
			{
				jrigidbody.Mass = value;
			}
		}
		public float maxAngularVelocity = -1;
		public float maxDepenetrationVelocity = -1;
		public Vector3 position
		{
			get
			{
				return (jrigidbody.Position.goliat());
			}
			set
			{
				jrigidbody.Position = value.jitter();
			}
		}
		public Quaternion rotation
		{
			get
			{
				return (Jitter.LinearMath.JQuaternion.CreateFromMatrix(jrigidbody.Orientation).goliat());
			}
			set
			{
				jrigidbody.Orientation = Jitter.LinearMath.JMatrix.CreateFromQuaternion(new Jitter.LinearMath.JQuaternion(value.x, value.y, value.z, value.w));
				//jrigidbody.Orientation = value.orthogonalMatrix.jitter;
			}
		}
		public float sleepThreshold = 0;
		//public int solverIterationCount; //idk
		//public bool useConeFriction; idem
		public bool useGravity
		{
			get
			{
				return (jrigidbody.AffectedByGravity);
			}
			set
			{
				jrigidbody.AffectedByGravity = value;
			}
		}
		public Vector3 velocity
		{
			get
			{
				return (jrigidbody.LinearVelocity.goliat());
			}
			set
			{
				jrigidbody.LinearVelocity = value.jitter();
			}
		}
		public JRigidBody jrigidbody = null;
		//private Shape shape = null;// pas forcément
		
		public Matrix transformation
		{
			get
			{
				return (Matrix.Transformation(this.position, this.rotation, Vector3.one));
			}
		}
		public Vector3 worldCenterOfMass
		{
			get
			{
				Vector3 center = this.transformation * this.jrigidbody.Shape.BoundingBox.Center.goliat();
				// check ici si c'est bien ce qu'on veut. après //!\\
				
				//jrigidbody.Orientation = une matrice representant sa rotation
				return (Vector3.zero);
			}
		}
		public void UpdateBody()
		{
			
			;
		}
		public bool initialized = true;
		public override void Update()
		{
			/*if (!initialized)
			{
				this.jrigidbody.Position = this.transform.position.jitter();
				this.jrigidbody.Orientation = this.transform.rotation.orthogonalMatrix.jitter;
				Console.WriteLine("hey my orientation:" + this.transform.rotation.orthogonalMatrix);
				initialized = true;
			}
			//this.jrigidbody.Update();
			this.jrigidbody.IsStatic = false;
			this.jrigidbody.AffectedByGravity = true;*/
			//this.drag = 70000;
			//this.angularVelocity -= (this.angularVelocity * (this.angularDrag / 100));
			//this.velocity -= (this.velocity * (this.drag / 100));
			//Quaternion rotation = this.jrigidbody.Orientation.goliat().quaternion;
			
			//Console.WriteLine("hey!!");
		}
		// pas de suite.
		public void AddExplosionForce(float explosionForce, Vector3 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode mode = ForceMode.Force)
		{
			Vector3 direction = this.position - explosionPosition;
			
			;
		}
		public void AddForce(Vector3 force, ForceMode mode = ForceMode.Force) //attention aux coordinates system: AddForce le fait depuis une world position, AddRelativeForce le fait depuis le coordinate system du rigidbody.
		{
			if (mode == ForceMode.Force)
			{
				this.jrigidbody.AddForce(force.jitter());
			}
			if (mode == ForceMode.Impulse)
			{
				this.jrigidbody.ApplyImpulse(force.jitter());
			}
			if (mode == ForceMode.VelocityChange)
			{
				this.jrigidbody.LinearVelocity += force.jitter();
			}
			//impulse:
			//this.jrigidbody.ApplyImpulse();
			//force:
			//this.jrigidbody.AddForce();
			//
			//
			//this.accelerations.Add(force)
			// foreach => this.velocity += ou *= j en sais rien
			//
			//
			//this.velocity += force
			;
		}
		public void AddRelativeForce(Vector3 force, ForceMode mode = ForceMode.Force)
		{
			
			;
		}
		public void AddTorque(Vector3 torque, ForceMode mode = ForceMode.Force) //, ForceMode mode = ForceMode.Force)
		{
			this.jrigidbody.AddTorque(torque.jitter());
			//this.jrigidbody.Torque += torque.jitter();
			;
		}
		public void AddRelativeTorque(Vector3 torque)
		{
			// donc la meme qu'au desus, sauf qu'on va faire une transformation avec la position et l orientation actuelle du rigidbody.
			// pareil mais avec le torque
			//
			//this.jrigidbody.
		}
		public Vector3 ClosestPointOnBounds(Vector3 position)
		{
			/*float dist = -1;
			Vector3 closest = Vector3.zero;
			List<GameBehaviour> colliders = this.GetComponent<Collider>();
			for (int i = 0; i < colliders.Count; i++)
			{
				Vector3 newPoint = colliders[i].ClosestPointOnBounds(position);
				float newDist = newPoint.GetDist(position);
				if (newDist < dist)
				{
					dist = newDist;
					closest = newPoint;
				}
			}
			return (closest);*/
			return (Vector3.zero);
		}
		public float GetPointVelocity(Vector3 worldPosition)
		{
			return (0);
		}
		public float GetRelativePointVelocity(Vector3 localPosition)
		{
			return (0);
		}
		public bool isSleeping()
		{
			return (false);
		}
		public void MovePosition(Vector3 position)
		{
			this.position = position;
		}
		public void MoveRotation(Quaternion rotation)
		{
			this.rotation = rotation;
		}
		public void SetDensity(float density)
		{
			//genre un lossyscale version masse en somme j imagine
			;
		}
		public void Sleep()
		{
			;
		}
		public bool SweepTest(Vector3 direction, out RaycastHit hitInfo, float maxDistance = (float)Mathf.Infinity)
		{
			hitInfo = new RaycastHit();
			return (false);
		}
		public RaycastHit[] SweepTestAll(Vector3 direction, float maxDistance = (float)Mathf.Infinity)
		{
			return (null);
		}
		public void WakeUp()
		{



			;
		}
		public override void OnEnable()
		{
			Collider collider = this.GetComponent<Collider>();
			if (collider != null)
			{
				this.jrigidbody = collider.jrigidbody;
			}
			else
			{
				this.jrigidbody = new JRigidBody(new Jitter.Collision.Shapes.BoxShape(1, 1, 1));
				Physic.world.AddBody(this.jrigidbody);
			}
			jrigidbody.IsStatic = false;
			//jrigidbody.IsActive = true;
			//jrigidbody.IsActive = true;
			jrigidbody.Tag = this;
			this.jrigidbody.Position = this.transform.position.jitter();
			this.jrigidbody.Orientation = Jitter.LinearMath.JMatrix.CreateFromQuaternion(this.transform.rotation.jitter());// this.transform.rotation.orthogonalMatrix.jitter;
			this.useGravity = true;
			base.OnEnable();
		}
		public RigidBody()
		{
			
			//this.mass = 1;

			//this.drag = 0;
			//this.angularDrag = 0.05f;
			;
		}

	}
	public class Collider : GameBehaviour
	{
		public bool drawMesh = false;
		public RigidBody attachedRigidBody
		{
			get
			{
				return (this.GetComponent<RigidBody>());
			}
		}
		public Bounds bounds;
		public float contactOffset;
		public bool enabled;
		public bool isTrigger;
		private PhysicMaterial _material = new PhysicMaterial();
		public PhysicMaterial material
		{
			get
			{
				this.material.bounciness = this.jrigidbody.Material.Restitution;
				this.material.dynamicFriction = this.jrigidbody.Material.KineticFriction;
				this.material.staticFriction = this.jrigidbody.Material.StaticFriction;			//= new PhysicMaterial(this.jrigidbody.Material.Restitution, this.jrigidbody.Material.KineticFriction, this.jrigidbody.Material.StaticFriction);
				return (this._material);
			}
			set
			{
				this.jrigidbody.Material = new JMaterial();
				this.jrigidbody.Material.Restitution = value.bounciness;
				this.jrigidbody.Material.StaticFriction = value.staticFriction;
				this.jrigidbody.Material.KineticFriction = value.dynamicFriction;
				this._material = value;
			}
		}
		public PhysicMaterial sharedMaterial;
		//private List<JVector> vertices;
		protected Shape shape = null;
		private List<TriangleVertexIndices> triangleVertexIndices = new List<TriangleVertexIndices>();
		public override void Update()
		{
			/*if (!_init && this.GetComponent<RigidBody>() == null)
			{
				this.jrigidbody.Position = this.transform.position.jitter();
				this.jrigidbody.Orientation = this.transform.rotation.orthogonalMatrix.jitter;
				_init = true;
				Console.WriteLine("MY POSITION:" + this.jrigidbody.Position);
				Console.WriteLine("MY ROTATION:" + this.jrigidbody.Orientation);
			}
			//Console.WriteLine("hey!!");
			//this.jrigidbody.Update();
			if (this.GetComponent<RigidBody>() != null && this.jrigidbody != null && this.GetComponent<RigidBody>().jrigidbody != this.jrigidbody)
			{
				Physic.world.RemoveBody(this.jrigidbody);
				JRigidBody tmp = this.jrigidbody;
				this.jrigidbody = this.GetComponent<RigidBody>().jrigidbody;
				this.jrigidbody.Shape = this.shape;
				this.jrigidbody.IsActive = true;
				this.jrigidbody.IsStatic = false;
				this.jrigidbody.AffectedByGravity = true;
				this.jrigidbody.Material.Restitution = 0;
				
				//Console.WriteLine("its allright dont see me");
				//this.material = this._material;
			} // /!\ attention il faut ici que le matériaux soit retransmit quand on l'enleve aussi, donc ça ne va pas. Il faut des privées de sauvegarde.
			else if (this.GetComponent<RigidBody>() == null)
			{
				//Console.WriteLine("WTF YOU SHOULDNT SEE THAT FOR BOTH:" + this.id);
				this.jrigidbody.Tag = this;
				if (this.jrigidbody.IsStatic == false)
				{
					//this.jrigidbody.Tag = this;
					this.jrigidbody.IsActive = true;
					this.jrigidbody.IsStatic = true;
					this.jrigidbody.AffectedByGravity = false;
					this.jrigidbody.Material.Restitution = 0;
				}
				//this.transform.position = this.jrigidbody.Position.;
				//this.transform.rotation = this.jrigidbody.Orientation;
			}
			
			//this.jrigidbody.IsStatic = true; pour les colliders, pas pour lui;
			// si un collider est dans le meme truc qu'un rigidbody, il y est alors "attaché, et du coup son jrigidbody disparait du world"; a check dans l'update.
			//Physic.world.RemoveBody()*/
		}
		public virtual Vector3 ClosestPointOnBounds(Vector3 worldPosition)
		{
			/*float dist = -1;
			int index = 0;
			List<Vector3> castedPoints = new List<Vector3>();
			//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
			for (int i = 0; i < bounds.faces; i++)
			{
				Face face = bounds.faces[i];
				Vector3 direction = face.normal;
				Ray ray = new Ray(worldPosition, direction);
				Vector3 point;
				if (ray.cast(face, out point))
				{
					float newDist =  point.GetDist(worldPosition);
					if (dist == -1 || newDist < dist)
					{
						dist = newDist;
						index = i;
						castedPoints.Add(point);
					}
				}
			}
			if (dist >= 0)
				return (castedPoints[index]);
			return (Vector3.zero);*/
			return (Vector3.zero);
			//return (bounds.ClosestPoint(worldPosition));
		}
		//Physic.world.CollisionSystem.Raycast()
		public virtual bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{
			/*
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			return (true);*/
			maxDistance = 0;
			return (true);
		}
		public JRigidBody jrigidbody;
		private bool _init = false;
		public override void OnEnable()
		{
			//Console.WriteLine("COLLIDER ON ENABLE");
			this.jrigidbody.Position = this.transform.position.jitter();
			//this.jrigidbody.Orientation = this.transform.rotation.orthogonalMatrix.jitter;
			this.jrigidbody.Orientation = Jitter.LinearMath.JMatrix.CreateFromQuaternion(this.transform.rotation.jitter());
			//throw new System.Exception("OHHHHHHHH");
			//throw new System.Exception("CALLED");
			base.OnEnable();
		}
		public Collider()
		{
			//Console.WriteLine("CREATING SHAPE");
			this.shape = new Jitter.Collision.Shapes.BoxShape(Vector3.one.jitter());
			//Console.WriteLine("creating a body for the collider");
			RigidBody body = this.GetComponent<RigidBody>();
			if (body != null)
			{
				//Console.WriteLine("HAS RIGIDBODY SO WE TAKE THAT.");
				this.jrigidbody = body.jrigidbody;
			}
			else
			{
				//Console.WriteLine("CREATING RIGIDBODY FOR THE COLLIDER");
				this.jrigidbody = new JRigidBody(this.shape);

				//this.jrigidbody.Position = this.transform.position.jitter();
				//Console.WriteLine("SETTING IS STATIC TO:" + true);
				this.jrigidbody.IsStatic = true;

				//this.jrigidbody.Tag = this;
				//Console.WriteLine("SETTING AFFECTEDBYGRAVITY TO:" + false);
				this.jrigidbody.AffectedByGravity = false;
				//Console.WriteLine("ADDING THE RIGIDBODY OF THE COLLIDER TO THE WORLD");
				Physic.world.AddBody(this.jrigidbody);
			}
			
		}
	}


	public class BoxCollider : Collider
	{
		
		public Vector3 center;
		private Vector3 _size;
		public Vector3 size
		{
			get
			{
				return (this._size);
				;
			}
			set
			{
				RigidBody body = this.GetComponent<RigidBody>();
				this._size = value;
				JRigidBody old = this.jrigidbody;
				
				this.jrigidbody = new JRigidBody(new Jitter.Collision.Shapes.BoxShape(_size.jitter()));
				this.jrigidbody.Position = old.Position;
				this.jrigidbody.Orientation = old.Orientation;
				this.jrigidbody.AffectedByGravity = old.AffectedByGravity;
				this.jrigidbody.IsStatic = old.IsStatic;
				//this.jrigidbody.Mass = old.Mass;
				
				//this.jrigidbody.Orientation = old.Orientation;
				Physic.world.RemoveBody(old);
				Physic.world.AddBody(this.jrigidbody);
				this.shape = this.jrigidbody.Shape;
				if (body != null)
				{
					body.jrigidbody = this.jrigidbody;
				}

			}
		}
		private Shape shape;
		public Vector3 ClosestPointOnBounds(Vector3 worldPosition)
		{
			/*float dist = -1;
			int index = 0;
			List<Vector3> castedPoints = new List<Vector3>();
			//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
			for (int i = 0; i < bounds.faces; i++)
			{
				Face face = bounds.faces[i];
				Vector3 direction = face.normal;
				Ray ray = new Ray(worldPosition, direction);
				Vector3 point;
				if (ray.cast(face, out point))
				{
					float newDist =  point.GetDist(worldPosition);
					if (dist == -1 || newDist < dist)
					{
						dist = newDist;
						index = i;
						castedPoints.Add(point);
					}
				}
			}
			if (dist >= 0)
				return (castedPoints[index]);
			return (Vector3.zero);
			//return (bounds.ClosestPoint(worldPosition));*/
			return (Vector3.zero);
		}
		/*public override void Update()
		{
		}*/
		private Primitive cubeMesh;
		/*public override void OnEnable()
		{
			base.OnEnable();
			//this.shape = new BoxShape(this.size.x, this.size.y, this.size.z);
			//this.cubeMesh = Primitive.Cube;
		}*/
		public bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{
			/*
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			return (true);
			 */
			maxDistance = 0;
			return (true);
		}
		public BoxCollider() : base()
		{
			this._size = Vector3.one;
			/*this.jrigidbody.Shape = new Jitter.Collision.Shapes.BoxShape(Vector3.one.jitter());
			
			this.shape = this.jrigidbody.Shape;*/
			;
		}

	}
	public class SphereCollider : Collider
	{
		public override Vector3 ClosestPointOnBounds(Vector3 worldPosition)
		{
		
			//§BulletSharp.RigidBody body = new BulletSharp.RigidBody(new BulletSharp.RigidBodyConstructionInfo());
			//BulletSharp.DynamicsWorld world = new BulletSharp.MultiBodyDynamicsWorld();
			
				/*float dist = -1;
				int index = 0;
				List<Vector3> castedPoints = new List<Vector3>();
				//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
				for (int i = 0; i < bounds.faces; i++)
				{
					Face face = bounds.faces[i];
					Vector3 direction = face.normal;
					Ray ray = new Ray(worldPosition, direction);
					Vector3 point;
					if (ray.cast(face, out point))
					{
						float newDist =  point.GetDist(worldPosition);
						if (dist == -1 || newDist < dist)
						{
							dist = newDist;
							index = i;
							castedPoints.Add(point);
						}
					}
				}
				if (dist >= 0)
					return (castedPoints[index]);
				*/
				return (Vector3.zero);
				//return (bounds.ClosestPoint(worldPosition));
		}
		public override bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
		{

			maxDistance = 0;
			/*float dist = -1;
			int index = -1;
			if ( collision(this, out distance))
			{
				if (dist == -1 || distance < dist)
				{
					dist = distance;
					maxDistance = dist;
					index = i;
				}
			}
			if (index == -1)
				return (false);
			*/
			return (true);
		}
		public SphereCollider()
		{
			this.jrigidbody.Shape = new Jitter.Collision.Shapes.SphereShape(1);
			this.shape = this.jrigidbody.Shape;
			;
		}
		private float _radius = 1f;
		public float radius
		{
			get
			{
				return (_radius);
				;
			}
			set
			{
				_radius = value;
				RigidBody body = this.GetComponent<RigidBody>();
				//this._size = value;
				JRigidBody old = this.jrigidbody;

				this.jrigidbody = new JRigidBody(new Jitter.Collision.Shapes.SphereShape(_radius));
				this.jrigidbody.Position = old.Position;
				this.jrigidbody.Orientation = old.Orientation;
				this.jrigidbody.AffectedByGravity = old.AffectedByGravity;
				this.jrigidbody.IsStatic = old.IsStatic;
				//this.jrigidbody.Mass = old.Mass;

				//this.jrigidbody.Orientation = old.Orientation;
				Physic.world.RemoveBody(old);
				Physic.world.AddBody(this.jrigidbody);
				this.shape = this.jrigidbody.Shape;
				if (body != null)
				{
					body.jrigidbody = this.jrigidbody;
				}
				;
			}
		}
		public override void OnEnable()
		{
			base.OnEnable();
			RigidBody body = this.GetComponent<RigidBody>();
			//this._size = value;
			JRigidBody old = this.jrigidbody;

			this.jrigidbody = new JRigidBody(new Jitter.Collision.Shapes.SphereShape(_radius));
			this.jrigidbody.Position = old.Position;
			this.jrigidbody.Orientation = old.Orientation;
			this.jrigidbody.AffectedByGravity = old.AffectedByGravity;
			this.jrigidbody.IsStatic = old.IsStatic;
			//this.jrigidbody.Mass = old.Mass;

			//this.jrigidbody.Orientation = old.Orientation;
			Physic.world.RemoveBody(old);
			Physic.world.AddBody(this.jrigidbody);
			this.shape = this.jrigidbody.Shape;
			if (body != null)
			{
				body.jrigidbody = this.jrigidbody;
			}
		}
	}
	public class MeshCollider : Collider
		{
			public override Vector3 ClosestPointOnBounds(Vector3 worldPosition)
			{
				/*float dist = -1;
				int index = 0;
				List<Vector3> castedPoints = new List<Vector3>();
				//raycast normal of each bound face starting from pos, on each bound face, then get closest points between;
				for (int i = 0; i < bounds.faces; i++)
				{
					Face face = bounds.faces[i];
					Vector3 direction = face.normal;
					Ray ray = new Ray(worldPosition, direction);
					Vector3 point;
					if (ray.cast(face, out point))
					{
						float newDist =  point.GetDist(worldPosition);
						if (dist == -1 || newDist < dist)
						{
							dist = newDist;
							index = i;
							castedPoints.Add(point);
						}
					}
				}
				if (dist >= 0)
					return (castedPoints[index]);
				*/
				return (Vector3.zero);
				//return (bounds.ClosestPoint(worldPosition));
			}
			public override bool Raycast(Ray ray, RaycastHit hitInfo, out float maxDistance)
			{
				/*maxDistance = 0;
				float dist = -1;
				int index = -1;
				if ( collision(this, out distance))
				{
					if (dist == -1 || distance < dist)
					{
						dist = distance;
						maxDistance = dist;
						index = i;
					}
				}
				if (index == -1)
					return (false);
				return (true);*/
				maxDistance = 0;
				return (true);
			}
			public MeshCollider() : base()
			{
				//JOctree octree = new JOctree();
				//this.jrigidbody.Shape = new Jitter.Collision.Shapes.TriangleMeshShape();
				//this.shape = this.jrigidbody.Shape;
				;
			}
		}
}