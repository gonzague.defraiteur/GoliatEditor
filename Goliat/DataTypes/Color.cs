﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Ce projet appartient au groupe Octogon

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

beware ur ass if u do stupid things.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
namespace Goliat
{
	//[System.Serializable]
	public struct Color : IComparable<Color>, IEquatable<Color>
	{
		public static bool operator ==(Color self, Color other)
		{
			if (other == null || self == null)
			{
				if (other != null || self != null)
				{
					return (false);
				}
				else
				{
					return (true);
				}
			}
			return (self.Equals(other));
		}
		public static bool operator !=(Color self, Color other)
		{
			return (!(self == other));
		}
		public int[] intPtr()
		{
			int[] res = new int[4];
			res[0] = (int)(this.r * 255);
			res[1] = (int)(this.g * 255);
			res[2] = (int)(this.b * 255);
			res[3] = (int)(this.a * 255);
			return (res);
		}
		public override string ToString()
		{
			return ("Color :(" + ((this.r)) + "," + ((this.g)) + "," + ((this.b)) + "," + ((this.a)) + ")");
		}
		public string ToString(bool intColor)
		{
			if (intColor)
				return ("Color :(" + ((int)(this.r * 255)) + "," + ((int)(this.g * 255)) + "," + ((int)(this.b * 255)) + "," + ((int)(this.a * 255)) + ")");
			else
				return (this.ToString());
		}
		public Color(float r, float g, float b, float a)
		{
			this._array = new float[4];
			this[0] = r;
			this[1] = g;
			this[2] = b;
			this[3] = a;
		}
		[Goliat.NonSerialized]
		public Color standardized
		{
			get
			{
				float x = this.x;
				float y = this.y;
				float z = this.z;
				float w = this.w;
				if (x > 1)
					x = 1;
				if (x < 0)
					x = 0;
				if (y > 1)
					y = 1;
				if (y < 0)
					y = 0;
				if (z > 1)
					z = 1;
				if (z < 0)
					z = 0;
				if (w > 1)
					w = 1;
				if (w < 0)
					w = 0;
				return (new Color(x, y, z, w));
			}
		}

		public static Color intColor(int r, int g, int b, int a)
		{

			float x = ((float)r) / 255;
			float y = ((float)g) / 255;
			float z = ((float)b) / 255;
			float w = ((float)a) / 255;

			return (new Color(x, y, z, w));
		}
		public void init()
		{
			if (this._array == null)
				this._array = new float[4];
		}

		public float this[int index]
		{
			get
			{
				this.init();
				return (this._array[index]);
			}
			set
			{
				this.init();
				this._array[index] = value;
			}
		}

		private float[] _array;
		public float[] array
		{
			get
			{
				this.init();
				return ((float[])this._array.Clone());
			}
			set
			{
				this.init();
				this._array = value;
			}
		}
		[Goliat.NonSerialized]
		public float R
		{
			get
			{
				this.init();
				return (this[0]);
			}
			set
			{
				this.init();
				this[0] = value;
			}
		}
		[Goliat.NonSerialized]
		public float G
		{
			get
			{
				this.init();
				return (this[1]);
			}
			set
			{
				this.init();
				this[1] = value;
			}
		}
		[Goliat.NonSerialized]
		public float B
		{
			get
			{
				this.init();
				return (this[2]);
			}
			set
			{
				this.init();
				this[2] = value;
			}
		}
		[Goliat.NonSerialized]
		public float A
		{
			get
			{
				this.init();
				return (this[3]);
			}
			set
			{
				this.init();
				this[3] = value;
			}
		}
		[Goliat.NonSerialized]
		public float X
		{
			get
			{
				this.init();
				return (this[0]);
			}
			set
			{
				this.init();
				this[0] = value;
			}
		}
		[Goliat.NonSerialized]
		public float Y
		{
			get
			{
				this.init();
				return (this[1]);
			}
			set
			{
				this.init();
				this[1] = value;
			}
		}
		[Goliat.NonSerialized]
		public float Z
		{
			get
			{
				this.init();
				return (this[2]);
			}
			set
			{
				this.init();
				this[2] = value;
			}
		}
		[Goliat.NonSerialized]
		public float W
		{
			get
			{
				this.init();
				return (this[3]);
			}
			set
			{
				this.init();
				this[3] = value;
			}
		}

		[Goliat.NonSerialized]
		public float x
		{
			get
			{
				this.init();
				return (this[0]);
			}
			set
			{
				this.init();
				this[0] = value;
			}
		}
		[Goliat.NonSerialized]
		public float y
		{
			get
			{
				this.init();
				return (this[1]);
			}
			set
			{
				this.init();
				this[1] = value;
			}
		}
		[Goliat.NonSerialized]
		public float z
		{
			get
			{
				this.init();
				return (this[2]);
			}
			set
			{
				this.init();
				this[2] = value;
			}
		}
		[Goliat.NonSerialized]
		public float w
		{
			get
			{
				this.init();
				return (this[3]);
			}
			set
			{
				this.init();
				this[3] = value;
			}
		}
		[Goliat.NonSerialized]
		public float r
		{
			get
			{
				this.init();
				return (this[0]);
			}
			set
			{
				this.init();
				this[0] = value;
			}
		}
		[Goliat.NonSerialized]
		public float g
		{
			get
			{
				this.init();
				return (this[1]);
			}
			set
			{
				this.init();
				this[1] = value;
			}
		}
		[Goliat.NonSerialized]
		public float b
		{
			get
			{
				this.init();
				return (this[2]);
			}
			set
			{
				this.init();
				this[2] = value;
			}
		}
		[Goliat.NonSerialized]
		public float a
		{
			get
			{
				this.init();
				return (this[3]);
			}
			set
			{
				this.init();
				this[3] = value;
			}
		}
		public float Length()
		{
			return ((this.x.square() + this.y.square() + this.z.square() + this.w.square()).sqrt());
		}

		public static Color Normalize(Color vector)
		{
			return (vector / vector.Length());
		}
		public static Color operator -(Color self, Color other)
		{
			return (new Color(self.r - other.r, self.g - other.g, self.b - other.b, self.a - other.a));
		}
		public static Color operator *(Color self, float other)
		{
			return (new Color(self.r * other, self.g * other, self.b * other, self.a * other));
		}
		public static Color operator /(Color self, float other)
		{
			return (new Color(self.r / other, self.g / other, self.b / other, self.a / other));
		}
		public static Color operator *(float other, Color self)
		{
			return (new Color(self.r * other, self.g * other, self.b * other, self.a * other));
		}
		public static Color operator *(Color self, Color other)
		{
			return (new Color(self.r * other.r, self.g * other.g, self.b * other.b, self.a * other.a));
		}
		public static Color operator /(Color self, Color other)
		{
			Color rectified = other;
			if (other.r == 0)
			{
				rectified.r = 0.0001f;
			}
			if (other.g == 0)
			{
				rectified.g = 0.0001f;
			}
			if (other.b == 0)
			{
				rectified.b = 0.0001f;
			}
			if (other.a == 0)
			{
				rectified.a = 0.0001f;
			}
			return (new Color(self.r / other.r, self.g / other.g, self.b / other.b, self.a / other.a));
		}
		public static Color operator +(Color self, Color other)
		{
			return (new Color(self.r + other.r, self.g + other.g, self.b + other.b, self.a + other.a));
		}

		public Color4 gl()
		{
			Color4 c;
			//c._array = new float[4];
			c.R = this.R;
			c.G = this.G;
			c.B = this.B;
			c.A = this.A;
			return c;
		}
		public string ToDaeString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(this.R.ToString());
			sb.Append(" ");
			sb.Append(this.G.ToString());
			sb.Append(" ");
			sb.Append(this.B.ToString());
			sb.Append(" ");
			sb.Append(this.A.ToString());
			return (sb.ToString());
			//return ("" + this.R + " " + this.G + " " + this.B + " " + this.A);
		}
		public int rgba
		{
			get
			{
				int r = (int)(this.r * 255f);
				r = r > 255 ? 255 : r;
				int g = (int)(this.g * 255f);
				g = g > 255 ? 255 : g;
				int b = (int)(this.b * 255f);
				b = b > 255 ? 255 : b;
				int a = (int)(this.a * 255f);
				a = a > 255 ? 255 : a;

				int rgba = 0;
				rgba += (a << 0);
				rgba += (b << 8);
				rgba += (g << 16);
				rgba += (r << 24);
				return (rgba);
			}
			set
			{
				
				int rgba = value;
				int r = (rgba >> 24) & 0xFF;
				int g = (rgba >> 16) & 0xFF;
				int b = (rgba >> 8) & 0xFF;
				int a = (rgba >> 0) & 0xFF;
				this.r = (float)r / 255f;
				this.g = (float)g / 255f;
				this.b = (float)b / 255f;
				this.a = (float)a / 255f;
			}
		}

		public int argb
		{
			get
			{
				int r = (int)(this.r * 255f);
				r = r > 255 ? 255 : r;
				int g = (int)(this.g * 255f);
				g = g > 255 ? 255 : g;
				int b = (int)(this.b * 255f);
				b = b > 255 ? 255 : b;
				int a = (int)(this.a * 255f);
				a = a > 255 ? 255 : a;

				int argb = 0;
				argb += (b << 0);
				argb += (g << 8);
				argb += (r << 16);
				argb += (a << 24);
				return (argb);
			}
			set
			{
				int argb = value;
				int a = (argb >> 24) & 0xFF;
				int r = (argb >> 16) & 0xFF;
				int g = (argb >> 8) & 0xFF;
				int b = (argb >> 0) & 0xFF;
				this.r = (float)r / 255f;
				this.g = (float)g / 255f;
				this.b = (float)b / 255f;
				this.a = (float)a / 255f;
			}
		}

		public int bgra
		{
			get
			{
				int r = (int)(this.r * 255f);
				r = r > 255 ? 255 : r;
				int g = (int)(this.g * 255f);
				g = g > 255 ? 255 : g;
				int b = (int)(this.b * 255f);
				b = b > 255 ? 255 : b;
				int a = (int)(this.a * 255f);
				a = a > 255 ? 255 : a;

				int bgra = 0;
				bgra += (a << 0);
				bgra += (r << 8);
				bgra += (g << 16);
				bgra += (b << 24);
				return (bgra);
			}
			set
			{
				int bgra = value;
				int b = (bgra >> 24) & 0xFF;
				int g = (bgra >> 16) & 0xFF;
				int r = (bgra >> 8) & 0xFF;
				int a = (bgra >> 0) & 0xFF;
				this.r = (float)r / 255f;
				this.g = (float)g / 255f;
				this.b = (float)b / 255f;
				this.a = (float)a / 255f;
			}
		}
		public static Color FromArgb(int argb)
		{
			;
			Color res = Color.Black;
			res.argb = argb;
			return (res);
		}

		public static Color FromBgra(int bgra)
		{
			;
			Color res = Color.Black;
			res.bgra = bgra;
			return (res);
		}


		public static Color FromRgba(int rgba)
		{
			;
			Color res = Color.Black;
			res.rgba = rgba;
			return (res);
		}

		public int CompareTo(Color other)
		{
			
			return (this.rgba.CompareTo(other.rgba));
		}

		public bool Equals(Color other)
		{
			
			if (this._array == null || other._array == null)
			{
				return (this._array == other._array);
			}
			return (this.a == other.a && this.b == other.b && this.g == other.g && this.r == other.r);
		}

		public static Color AliceBlue
		{
			get
			{
				return (Color.intColor(240, 248, 255, 255));
			}
		}
		public static Color LightSalmon
		{
			get
			{
				return (Color.intColor(255, 160, 122, 255));
			}
		}
		public static Color AntiqueWhite
		{
			get
			{
				return (Color.intColor(250, 235, 215, 255));
			}
		}
		public static Color LightSeaGreen
		{
			get
			{
				return (Color.intColor(32, 178, 170, 255));
			}
		}
		public static Color Aqua
		{
			get
			{
				return (Color.intColor(0, 255, 255, 255));
			}
		}
		public static Color LightSkyBlue
		{
			get
			{
				return (Color.intColor(135, 206, 250, 255));
			}
		}
		public static Color Aquamarine
		{
			get
			{
				return (Color.intColor(127, 255, 212, 255));
			}
		}
		public static Color LightSlateGray
		{
			get
			{
				return (Color.intColor(119, 136, 153, 255));
			}
		}
		public static Color Azure
		{
			get
			{
				return (Color.intColor(240, 255, 255, 255));
			}
		}
		public static Color LightSteelBlue
		{
			get
			{
				return (Color.intColor(176, 196, 222, 255));
			}
		}
		public static Color Beige
		{
			get
			{
				return (Color.intColor(245, 245, 220, 255));
			}
		}
		public static Color LightYellow
		{
			get
			{
				return (Color.intColor(255, 255, 224, 255));
			}
		}
		public static Color Bisque
		{
			get
			{
				return (Color.intColor(255, 228, 196, 255));
			}
		}
		public static Color Lime
		{
			get
			{
				return (Color.intColor(0, 255, 0, 255));
			}
		}
		public static Color Black
		{
			get
			{
				return (Color.intColor(0, 0, 0, 255));
			}
		}
		public static Color LimeGreen
		{
			get
			{
				return (Color.intColor(50, 205, 50, 255));
			}
		}
		public static Color BlanchedAlmond
		{
			get
			{
				return (Color.intColor(255, 255, 205, 255));
			}
		}
		public static Color Linen
		{
			get
			{
				return (Color.intColor(250, 240, 230, 255));
			}
		}
		public static Color Blue
		{
			get
			{
				return (Color.intColor(0, 0, 255, 255));
			}
		}
		public static Color Magenta
		{
			get
			{
				return (Color.intColor(255, 0, 255, 255));
			}
		}
		public static Color BlueViolet
		{
			get
			{
				return (Color.intColor(138, 43, 226, 255));
			}
		}
		public static Color Maroon
		{
			get
			{
				return (Color.intColor(128, 0, 0, 255));
			}
		}
		public static Color Brown
		{
			get
			{
				return (Color.intColor(165, 42, 42, 255));
			}
		}
		public static Color MediumAquamarine
		{
			get
			{
				return (Color.intColor(102, 205, 170, 255));
			}
		}
		public static Color BurlyWood
		{
			get
			{
				return (Color.intColor(222, 184, 135, 255));
			}
		}
		public static Color MediumBlue
		{
			get
			{
				return (Color.intColor(0, 0, 205, 255));
			}
		}
		public static Color CadetBlue
		{
			get
			{
				return (Color.intColor(95, 158, 160, 255));
			}
		}
		public static Color Chartreuse
		{
			get
			{
				return (Color.intColor(127, 255, 0, 255));
			}
		}
		public static Color MediumOrchid
		{
			get
			{
				return (Color.intColor(186, 85, 211, 255));
			}
		}

		public static Color MediumPurple
		{
			get
			{
				return (Color.intColor(147, 112, 219, 255));
			}
		}
		public static Color Chocolate
		{
			get
			{
				return (Color.intColor(210, 105, 30, 255));
			}
		}
		public static Color MediumSeaGreen
		{
			get
			{
				return (Color.intColor(60, 179, 113, 255));
			}
		}
		public static Color Coral
		{
			get
			{
				return (Color.intColor(255, 127, 80, 255));
			}
		}
		public static Color MediumSlateBlue
		{
			get
			{
				return (Color.intColor(123, 104, 238, 255));
			}
		}
		public static Color CornflowerBlue
		{
			get
			{
				return (Color.intColor(100, 149, 237, 255));
			}
		}
		public static Color MediumSpringGreen
		{
			get
			{
				return (Color.intColor(0, 250, 154, 255));
			}
		}
		public static Color Cornsilk
		{
			get
			{
				return (Color.intColor(255, 248, 220, 255));
			}
		}
		public static Color MediumTurquoise
		{
			get
			{
				return (Color.intColor(72, 209, 204, 255));
			}
		}
		public static Color Crimson
		{
			get
			{
				return (Color.intColor(220, 20, 60, 255));
			}
		}
		public static Color MediumVioletRed
		{
			get
			{
				return (Color.intColor(199, 21, 112, 255));
			}
		}
		public static Color Cyan
		{
			get
			{
				return (Color.intColor(0, 255, 255, 255));
			}
		}
		public static Color MidnightBlue
		{
			get
			{
				return (Color.intColor(25, 25, 112, 255));
			}
		}
		public static Color DarkBlue
		{
			get
			{
				return (Color.intColor(0, 0, 139, 255));
			}
		}
		public static Color MintCream
		{
			get
			{
				return (Color.intColor(245, 255, 250, 255));
			}
		}
		public static Color DarkCyan
		{
			get
			{
				return (Color.intColor(0, 139, 139, 255));
			}
		}
		public static Color MistyRose
		{
			get
			{
				return (Color.intColor(255, 228, 225, 255));
			}
		}
		public static Color DarkGoldenrod
		{
			get
			{
				return (Color.intColor(184, 134, 11, 255));
			}
		}
		public static Color Moccasin
		{
			get
			{
				return (Color.intColor(255, 228, 181, 255));
			}
		}
		public static Color DarkGray
		{
			get
			{
				return (Color.intColor(169, 169, 169, 255));
			}
		}
		public static Color NavajoWhite
		{
			get
			{
				return (Color.intColor(255, 222, 173, 255));
			}
		}
		public static Color DarkGreen
		{
			get
			{
				return (Color.intColor(0, 100, 0, 255));
			}
		}
		public static Color Navy
		{
			get
			{
				return (Color.intColor(0, 0, 128, 255));
			}
		}
		public static Color DarkKhaki
		{
			get
			{
				return (Color.intColor(189, 183, 107, 255));
			}
		}
		public static Color OldLace
		{
			get
			{
				return (Color.intColor(253, 245, 230, 255));
			}
		}
		public static Color DarkMagenta
		{
			get
			{
				return (Color.intColor(139, 0, 139, 255));
			}
		}
		public static Color Olive
		{
			get
			{
				return (Color.intColor(128, 128, 0, 255));
			}
		}
		public static Color DarkOliveGreen
		{
			get
			{
				return (Color.intColor(85, 107, 47, 255));
			}
		}
		public static Color OliveDrab
		{
			get
			{
				return (Color.intColor(107, 142, 45, 255));
			}
		}
		public static Color DarkOrange
		{
			get
			{
				return (Color.intColor(255, 140, 0, 255));
			}
		}
		public static Color Orange
		{
			get
			{
				return (Color.intColor(255, 165, 0, 255));
			}
		}
		public static Color DarkOrchid
		{
			get
			{
				return (Color.intColor(153, 50, 204, 255));
			}
		}
		public static Color OrangeRed
		{
			get
			{
				return (Color.intColor(255, 69, 0, 255));
			}
		}
		public static Color DarkRed
		{
			get
			{
				return (Color.intColor(139, 0, 0, 255));
			}
		}
		public static Color Orchid
		{
			get
			{
				return (Color.intColor(218, 112, 214, 255));
			}
		}
		public static Color DarkSalmon
		{
			get
			{
				return (Color.intColor(233, 150, 122, 255));
			}
		}
		public static Color PaleGoldenrod
		{
			get
			{
				return (Color.intColor(238, 232, 170, 255));
			}
		}
		public static Color DarkSeaGreen
		{
			get
			{
				return (Color.intColor(143, 188, 143, 255));
			}
		}
		public static Color PaleGreen
		{
			get
			{
				return (Color.intColor(152, 251, 152, 255));
			}
		}
		public static Color DarkSlateBlue
		{
			get
			{
				return (Color.intColor(72, 61, 139, 255));
			}
		}
		public static Color PaleTurquoise
		{
			get
			{
				return (Color.intColor(175, 238, 238, 255));
			}
		}
		public static Color DarkSlateGray
		{
			get
			{
				return (Color.intColor(40, 79, 79, 255));
			}
		}
		public static Color PaleVioletRed
		{
			get
			{
				return (Color.intColor(219, 112, 147, 255));
			}
		}
		public static Color DarkTurquoise
		{
			get
			{
				return (Color.intColor(0, 206, 209, 255));
			}
		}
		public static Color PapayaWhip
		{
			get
			{
				return (Color.intColor(255, 239, 213, 255));
			}
		}
		public static Color DarkViolet
		{
			get
			{
				return (Color.intColor(148, 0, 211, 255));
			}
		}
		public static Color PeachPuff
		{
			get
			{
				return (Color.intColor(255, 218, 155, 255));
			}
		}
		public static Color DeepPink
		{
			get
			{
				return (Color.intColor(255, 20, 147, 255));
			}
		}
		public static Color Peru
		{
			get
			{
				return (Color.intColor(205, 133, 63, 255));
			}
		}
		public static Color DeepSkyBlue
		{
			get
			{
				return (Color.intColor(0, 191, 255, 255));
			}
		}
		public static Color Pink
		{
			get
			{
				return (Color.intColor(255, 192, 203, 255));
			}
		}
		public static Color DimGray
		{
			get
			{
				return (Color.intColor(105, 105, 105, 255));
			}
		}
		public static Color Plum
		{
			get
			{
				return (Color.intColor(221, 160, 221, 255));
			}
		}
		public static Color DodgerBlue
		{
			get
			{
				return (Color.intColor(30, 144, 255, 255));
			}
		}
		public static Color PowderBlue
		{
			get
			{
				return (Color.intColor(176, 224, 230, 255));
			}
		}
		public static Color Firebrick
		{
			get
			{
				return (Color.intColor(178, 34, 34, 255));
			}
		}
		public static Color Purple
		{
			get
			{
				return (Color.intColor(128, 0, 128, 255));
			}
		}
		public static Color FloralWhite
		{
			get
			{
				return (Color.intColor(255, 250, 240, 255));
			}
		}
		public static Color Red
		{
			get
			{
				return (Color.intColor(255, 0, 0, 255));
			}
		}
		public static Color ForestGreen
		{
			get
			{
				return (Color.intColor(34, 139, 34, 255));
			}
		}
		public static Color RosyBrown
		{
			get
			{
				return (Color.intColor(188, 143, 143, 255));
			}
		}
		public static Color Fuschia
		{
			get
			{
				return (Color.intColor(255, 0, 255, 255));
			}
		}
		public static Color RoyalBlue
		{
			get
			{
				return (Color.intColor(65, 105, 225, 255));
			}
		}
		public static Color Gainsboro
		{
			get
			{
				return (Color.intColor(220, 220, 220, 255));
			}
		}
		public static Color SaddleBrown
		{
			get
			{
				return (Color.intColor(139, 69, 19, 255));
			}
		}
		public static Color GhostWhite
		{
			get
			{
				return (Color.intColor(248, 248, 255, 255));
			}
		}
		public static Color Salmon
		{
			get
			{
				return (Color.intColor(250, 128, 114, 255));
			}
		}
		public static Color Gold
		{
			get
			{
				return (Color.intColor(255, 215, 0, 255));
			}
		}
		public static Color SandyBrown
		{
			get
			{
				return (Color.intColor(244, 164, 96, 255));
			}
		}
		public static Color Goldenrod
		{
			get
			{
				return (Color.intColor(218, 165, 32, 255));
			}
		}
		public static Color SeaGreen
		{
			get
			{
				return (Color.intColor(46, 139, 87, 255));
			}
		}
		public static Color Gray
		{
			get
			{
				return (Color.intColor(128, 128, 128, 255));
			}
		}
		public static Color Seashell
		{
			get
			{
				return (Color.intColor(255, 245, 238, 255));
			}
		}
		public static Color Green
		{
			get
			{
				return (Color.intColor(0, 128, 0, 255));
			}
		}
		public static Color Sienna
		{
			get
			{
				return (Color.intColor(160, 82, 45, 255));
			}
		}
		public static Color GreenYellow
		{
			get
			{
				return (Color.intColor(173, 255, 47, 255));
			}
		}
		public static Color Silver
		{
			get
			{
				return (Color.intColor(192, 192, 192, 255));
			}
		}
		public static Color Honeydew
		{
			get
			{
				return (Color.intColor(240, 255, 240, 255));
			}
		}
		public static Color SkyBlue
		{
			get
			{
				return (Color.intColor(135, 206, 235, 255));
			}
		}
		public static Color HotPink
		{
			get
			{
				return (Color.intColor(255, 105, 180, 255));
			}
		}
		public static Color SlateBlue
		{
			get
			{
				return (Color.intColor(106, 90, 205, 255));
			}
		}
		public static Color IndianRed
		{
			get
			{
				return (Color.intColor(205, 92, 92, 255));
			}
		}
		public static Color SlateGray
		{
			get
			{
				return (Color.intColor(112, 128, 144, 255));
			}
		}
		public static Color Indigo
		{
			get
			{
				return (Color.intColor(75, 0, 130, 255));
			}
		}
		public static Color Snow
		{
			get
			{
				return (Color.intColor(255, 250, 250, 255));
			}
		}
		public static Color Ivory
		{
			get
			{
				return (Color.intColor(255, 240, 240, 255));
			}
		}
		public static Color SpringGreen
		{
			get
			{
				return (Color.intColor(0, 255, 127, 255));
			}
		}
		public static Color Khaki
		{
			get
			{
				return (Color.intColor(240, 230, 140, 255));
			}
		}
		public static Color SteelBlue
		{
			get
			{
				return (Color.intColor(70, 130, 180, 255));
			}
		}
		public static Color Lavender
		{
			get
			{
				return (Color.intColor(230, 230, 250, 255));
			}
		}
		public static Color Tan
		{
			get
			{
				return (Color.intColor(210, 180, 140, 255));
			}
		}
		public static Color LavenderBlush
		{
			get
			{
				return (Color.intColor(255, 240, 245, 255));
			}
		}
		public static Color Teal
		{
			get
			{
				return (Color.intColor(0, 128, 128, 255));
			}
		}
		public static Color LawnGreen
		{
			get
			{
				return (Color.intColor(124, 252, 0, 255));
			}
		}
		public static Color Thistle
		{
			get
			{
				return (Color.intColor(216, 191, 216, 255));
			}
		}
		public static Color LemonChiffon
		{
			get
			{
				return (Color.intColor(255, 250, 205, 255));
			}
		}
		public static Color Tomato
		{
			get
			{
				return (Color.intColor(253, 99, 71, 255));
			}
		}
		public static Color Transparent
		{
			get
			{
				return (Color.intColor(0, 0, 0, 0));
			}
		}
		public static Color LightBlue
		{
			get
			{
				return (Color.intColor(173, 216, 230, 255));
			}
		}
		public static Color Turquoise
		{
			get
			{
				return (Color.intColor(64, 224, 208, 255));
			}
		}
		public static Color LightCoral
		{
			get
			{
				return (Color.intColor(240, 128, 128, 255));
			}
		}
		public static Color Violet
		{
			get
			{
				return (Color.intColor(238, 130, 238, 255));
			}
		}
		public static Color LightCyan
		{
			get
			{
				return (Color.intColor(224, 255, 255, 255));
			}
		}
		public static Color Wheat
		{
			get
			{
				return (Color.intColor(245, 222, 179, 255));
			}
		}
		public static Color LightGoldenrodYellow
		{
			get
			{
				return (Color.intColor(250, 250, 210, 255));
			}
		}
		public static Color White
		{
			get
			{
				return (Color.intColor(255, 255, 255, 255));
			}
		}
		public static Color LightGreen
		{
			get
			{
				return (Color.intColor(144, 238, 144, 255));
			}
		}
		public static Color WhiteSmoke
		{
			get
			{
				return (Color.intColor(245, 245, 245, 255));
			}
		}
		public static Color LightGray
		{
			get
			{
				return (Color.intColor(211, 211, 211, 255));
			}
		}
		public static Color Yellow
		{
			get
			{
				return (Color.intColor(255, 255, 0, 255));
			}
		}
		public static Color LightPink
		{
			get
			{
				return (Color.intColor(255, 182, 193, 255));
			}
		}
		public static Color YellowGreen
		{

			get
			{
				return (Color.intColor(154, 205, 50, 255));
			}
		}

		internal Color clamped()
		{
			float r = this.r;
			float g = this.g;
			float b = this.b;
			float a = this.a;
			if (r > 1f)
				r = 1f;
			if (g > 1f)
				g = 1f;
			if (b > 1f)
				b = 1f;
			if (a > 1f)
				a = 1f;
			if (r < 0)
				r = 0;
			if (g < 0)
				g = 0;
			if (b < 0)
				b = 0;
			if (a < 0)
				a = 0;
			return (new Goliat.Color(r, g, b, a));
		}
	}
}
