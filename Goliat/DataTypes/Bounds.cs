﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goliat
{
	[System.Serializable]
	public class Bounds2D
	{
		public Bounds2D(Bounds2D other)
		{
			this._center = other.center;
			this._extents = other.extents;
			this._max = other.max;
			this._min = other.min;
		}
		private Vector2 _center = Vector2.zero;
		public Vector2 center
		{
			get
			{
				return (this._center);
			}
			set
			{
				this._center = value;

			}
		}
		private Vector2 _extents = Vector2.zero;
		public Vector2 extents
		{
			get
			{
				return (this._extents);
			}
			set
			{
				this._extents = value;
			}
		}
		private Vector2 _max = Vector2.zero;
		public Vector2 max
		{
			get
			{
				return (this._max);
			}
			set
			{
				this._max = value;
				Vector2 diff = (this._max - this._center) - (this._min - this._center);
				this._extents = diff;
			}
		}
		private Vector2 _min = Vector2.zero;
		public Vector2 min
		{
			get
			{
				return (this._min);
			}
			set
			{
				this._max = value;
				Vector2 diff = (this._max - this._center) - (this._min - this._center);
				this._extents = diff;
				//Vector2 diff = this._max - this._min;
				//this._extents
			}
		}
		public Vector2 size
		{
			get
			{
				return (this._extents * 2);
			}
			set
			{
				this._extents = value / 2;
			}
		}
		public Bounds2D(Vector2 center, Vector2 size)
		{
			this._extents = size / 2;
			//this._min = 
			//this._max = 
			this._center = center;
			this._min = this._center - this._extents;
			this._max = this._center + this._extents;
			//this._size = size;
		}
		public static Bounds2D generateBounds(List<Vector2> vectors)
		{
			Vector2 min = Vector2.zero;
			Vector2 max = Vector2.zero;
			if (vectors.Count > 0)
			{
				min = vectors[0];
				max = vectors[0];
			}
			for (int i = 0; i < vectors.Count; i++)
			{
				if (vectors[i].x < min.x)
				{
					min.x = vectors[i].x;
				}
				if (vectors[i].y < min.y)
				{
					min.y = vectors[i].y;
				}
				if (vectors[i].x > max.x)
				{
					max.x = vectors[i].x;
				}
				if (vectors[i].y > max.y)
				{
					max.y = vectors[i].y;
				}
			}
			Vector2 median = (max / 2) + (min / 2);
			Vector2 size = max - min;
			return (new Bounds2D(median, size));
		}
	}
	[System.Serializable]
	public class Bounds
	{
		public Bounds(Bounds other)
		{
			this._center = other.center;
			this._extents = other.extents;
			this._max = other.max;
			this._min = other.min;
		}
		private Vector3 _center = Vector3.zero;
		public Vector3 center
		{
			get
			{
				return (this._center);
			}
			set
			{
				this._center = value;

			}
		}
		private Vector3 _extents = Vector3.zero;
		public Vector3 extents
		{
			get
			{
				return (this._extents);
			}
			set
			{
				this._extents = value;
			}
		}
		private Vector3 _max = Vector3.zero;
		public Vector3 max
		{
			get
			{
				return (this._max);
			}
			set
			{
				this._max = value;
				Vector3 diff = (this._max - this._center) - (this._min - this._center);
				this._extents = diff;
			}
		}
		private Vector3 _min = Vector3.zero;
		public Vector3 min
		{
			get
			{
				return (this._min);
			}
			set
			{
				this._max = value;
				Vector3 diff = (this._max - this._center) - (this._min - this._center);
				this._extents = diff;
				//Vector3 diff = this._max - this._min;
				//this._extents
			}
		}
		public Vector3 size
		{
			get
			{
				return (this._extents * 2);
			}
			set
			{
				this._extents = value / 2;
			}
		}
		public Bounds(Vector3 center, Vector3 size)
		{
			this._extents = size / 2;
			//this._min = 
			//this._max = 
			this._center = center;
			this._min = this._center - this._extents;
			this._max = this._center + this._extents;
			//this._size = size;
		}
	}
}
