﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat;
using GL = OpenTK.Graphics.OpenGL.GL;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Goliat
{
	public class Shader : SharedResource
	{

		/// <summary>
		/// Type of Shader
		/// </summary>
		public enum Type
		{
			Vertex = 0x1,
			Fragment = 0x2,
			Both = 0x3
		}

		/// <summary>
		/// Get Whether the Shader function is Available on this Machine or not
		/// </summary>
		public static bool IsSupported
		{
			get
			{
				return (new Version(GL.GetString(StringName.Version).Substring(0, 3)) >= new Version(2, 0) ? true : false);
			}
		}
		public static Shader FromInfo(ShaderInfo info)
		{
			if ((Shader)SharedResource.getRessource(info.guid) != null)
			{
				return ((Shader)SharedResource.getRessource(info.guid));
			}
			return (new Shader(info.vertexShaderSource, info.fragmentShaderSource));
		}
		public int Program = 0;
		private Dictionary<string, int> Variables = new Dictionary<string, int>();

		/// <summary>
		/// Create a new Shader
		/// </summary>
		/// <param name="source">Vertex or Fragment Source</param>
		/// <param name="type">Type of Source Code</param>
		public Shader(string source, Type type)
		{
			if (!IsSupported)
			{
				Console.WriteLine("Failed to create Shader." +
					Environment.NewLine + "Your system doesn't support Shader.", "Error");
				return;
			}

			if (type == Type.Vertex)
				Compile(source, "");
			else
				Compile("", source);
		}

		private int GetVariableLocation(string name)
		{
			if (Variables.ContainsKey(name))
				return Variables[name];

			int location = GL.GetUniformLocation(Program, name);

			if (location != -1)
				Variables.Add(name, location);
			else
				Console.WriteLine("Failed to retrieve Variable Location." +
					Environment.NewLine + "Variable Name not found.", "Error");

			return location;
		}

		/// <summary>
		/// Change a value Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="x">Value</param>
		public void SetVariable(string name, float x)
		{
			if (Program > 0)
			{
				int oldProgram = GL.GetInteger(GetPName.CurrentProgram);
				Renderer.Call(() => GL.UseProgram(Program));

				int location = GetVariableLocation(name);
				if (location != -1)
					Renderer.Call(() => GL.Uniform1(location, x));

				Renderer.Call(() => GL.UseProgram(oldProgram));
			}
		}

		/// <summary>
		/// Change a 2 value Vector Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="x">First Vector Value</param>
		/// <param name="y">Second Vector Value</param>
		public void SetVariable(string name, float x, float y)
		{
			if (Program > 0)
			{
				int oldProgram = GL.GetInteger(GetPName.CurrentProgram);
				Renderer.Call(() => GL.UseProgram(Program));

				int location = GetVariableLocation(name);
				if (location != -1)
					Renderer.Call(() => GL.Uniform2(location, x, y));

				Renderer.Call(() => GL.UseProgram(oldProgram));
			}
		}

		/// <summary>
		/// Change a 3 value Vector Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="x">First Vector Value</param>
		/// <param name="y">Second Vector Value</param>
		/// <param name="z">Third Vector Value</param>
		public void SetVariable(string name, float x, float y, float z)
		{
			if (Program > 0)
			{
				int oldProgram = GL.GetInteger(GetPName.CurrentProgram);
				Renderer.Call(() => GL.UseProgram(Program));

				int location = GetVariableLocation(name);
				if (location != -1)
					Renderer.Call(() => GL.Uniform3(location, x, y, z));

				Renderer.Call(() => GL.UseProgram(oldProgram));
			}
		}

		/// <summary>
		/// Change a 4 value Vector Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="x">First Vector Value</param>
		/// <param name="y">Second Vector Value</param>
		/// <param name="z">Third Vector Value</param>
		/// <param name="w">Fourth Vector Value</param>
		public void SetVariable(string name, float x, float y, float z, float w)
		{
			if (Program > 0)
			{
				int oldProgram = GL.GetInteger(GetPName.CurrentProgram);
				Renderer.Call(() => GL.UseProgram(Program));

				int location = GetVariableLocation(name);
				if (location != -1)
					Renderer.Call(() => GL.Uniform4(location, x, y, z, w));

				Renderer.Call(() => GL.UseProgram(oldProgram));
			}
		}

		/// <summary>
		/// Change a Matrix4 Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="matrix">Matrix</param>
		public void SetVariable(string name, Matrix4 matrix)
		{
			if (Program > 0)
			{
				int oldProgram = GL.GetInteger(GetPName.CurrentProgram);
				Renderer.Call(() => GL.UseProgram(Program));

				int location = GetVariableLocation(name);
				if (location != -1)
				{
					// Well cannot use ref on lambda expression Lol
					// So we need to call Check error manually
					GL.UniformMatrix4(location, false, ref matrix);
					Renderer.CheckError();
				}

				Renderer.Call(() => GL.UseProgram(oldProgram));
			}
		}

		/// <summary>
		/// Change a 2 value Vector Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="vector">Vector Value</param>
		public void SetVariable(string name, Vector2 vector)
		{
			SetVariable(name, vector.X, vector.Y);
		}

		/// <summary>
		/// Change a 3 value Vector Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="vector">Vector Value</param>
		public void SetVariable(string name, Vector3 vector)
		{
			SetVariable(name, vector.X, vector.Y, vector.Z);
		}

		/// <summary>
		/// Change a Color Variable of the Shader
		/// </summary>
		/// <param name="name">Variable Name</param>
		/// <param name="color">Color Value</param>
		public void SetVariable(string name, Color color)
		{
			SetVariable(name, color.R, color.G, color.B, color.A);
		}


		/// <summary>
		/// Create a new Shader
		/// </summary>
		/// <param name="source">Vertex or Fragment Source</param>
		/// <param name="type">Type of Source Code</param>
		public Shader(string vsource, string fsource)
		{
			if (!IsSupported)
			{
				Console.WriteLine("Failed to create Shader." +
					Environment.NewLine + "Your system doesn't support Shader.", "Error");
				return;
			}

			Compile(vsource, fsource);
		}

		public static void Bind(Shader shader)
		{
			if (shader != null && shader.Program > 0)
			{
				Renderer.Call(() => GL.UseProgram(shader.Program));
			}
			else
			{
				Renderer.Call(() => GL.UseProgram(0));
			}
		}

		public static void UnBind()
		{
			Renderer.Call(() => GL.UseProgram(0));
		}

		public void Dispose()
		{
			if (Program != 0)
				Renderer.Call(() => GL.DeleteProgram(Program));
		}

		private void BindTexture(ref int textureId, TextureUnit textureUnit, string UniformName)
		{
			GL.ActiveTexture(textureUnit);
			GL.BindTexture(TextureTarget.Texture2D, textureId);
			GL.Uniform1(GL.GetUniformLocation(this.Program, UniformName), textureUnit - TextureUnit.Texture0);
		}
		private static string oneColorFSource = @"
		uniform vec4 color;
		void main()
		{
			gl_FragColor = color;
		}
		
";
		private static string oneColorVSource = @"
		void main(){
		gl_Position = ftransform();
		}
";
		private static string uvVSource = @"
varying vec2 UV;

void main() {
	UV = vec2(gl_MultiTexCoord0.x, gl_MultiTexCoord0.y);
	gl_Position = ftransform();
}";

		private static string uvFSource = @"
varying vec2 UV;

void main()
{
	gl_FragColor = vec4(UV.x, UV.y, 0, 0); 
}"; 
		private static string terrainVSource = @"
varying float pixelon;
uniform vec2 mouseposition;
uniform mat4 node_matrix;
uniform sampler2D heightmap;
uniform mat4 view_matrix;
varying vec4 color;

varying vec2 UV;
varying vec2 size;
float square(float nb)
{
	float res = nb * nb;
	return (res);
}


void main() {
	UV = vec2(gl_MultiTexCoord0.x, gl_MultiTexCoord0.y);
	//gl_ProjectionMatrix * view_matrix * model_matrix * vertex_pos;
	//gl_Position = ftransform();
	size = textureSize2D(heightmap, 0);

	float offset = texture2D(heightmap, vec2(UV.x, UV.y)).x;
	color = texture2D(heightmap, vec2(UV.x, UV.y));
	gl_Vertex = vec4(gl_Vertex.x, gl_Vertex.y + offset * 45f, gl_Vertex.z, 1);
	vec4 clip = gl_ProjectionMatrix * view_matrix * ((node_matrix * gl_Vertex)); //+ Texture2D(heightmap, gl_TexCoord[0].st));
	
	//gl_Position = vec4(clip.xyz / clip.w + vec3(convertexPix, 0.0), 1.0);
	clip = vec4(clip.xyz / clip.w, 1);
	if (clip.x >= -1 && clip.x <= 1 && clip.y >= -1 && clip.y <= 1 && clip.z >= -1 && clip.z <= 1)
		gl_Position = vec4(clip.x , clip.y, clip.z , 1.0);
	else
		gl_Position = ftransform();
	pixelon = 0.0;
	vec3 trueposition = node_matrix * gl_Vertex;
	if (sqrt(square(mouseposition.x - trueposition.x) + square(mouseposition.y - trueposition.z)) < 5)
		pixelon = 1.0;
	
}";
		private static string terrainFSource = @"
varying vec2 UV;
varying vec2 size;
varying vec4 color;
varying float pixelon;

void main()
{
	//vec4 color;
	//color = vec4(UV.x, UV.y, 0, 0);
	
	if (pixelon == 0.0)
	{	
		//vec4 color = texture2D(test, gl_TexCoord[0].st);
		//gl_FragColor = vec4(color.x, color.y, color.z, color.w);
		gl_FragColor = vec4(UV.x, UV.y, 0, 0); 
		//gl_FragColor = Texture2D(colormap, gl_TexCoord[0].st);
	}
	else
	{
		color = (vec4(UV.x, UV.y, 0, 0) * 0.50) + vec4(0.0, 0.0, 0.9, 0) * 0.5;
		gl_FragColor = color;
	}
}";
		public static Shader terrainShader = new Shader(terrainVSource, terrainFSource);
		public static Shader uvShader = new Shader(uvVSource, uvFSource);
		public static Shader oneColorShader = new Shader(oneColorVSource, oneColorFSource);
		private bool Compile(string vertexSource = "", string fragmentSource = "")
		{
			int status_code = -1;
			string info = "";

			if (vertexSource == "" && fragmentSource == "")
			{
				Console.WriteLine("Failed to compile Shader." +
					Environment.NewLine + "Nothing to Compile.", "Error");
				return false;
			}

			if (Program > 0)
				Renderer.Call(() => GL.DeleteProgram(Program));

			Variables.Clear();

			Program = GL.CreateProgram();

			if (vertexSource != "")
			{
				int vertexShader = GL.CreateShader(ShaderType.VertexShader);
				Renderer.Call(() => GL.ShaderSource(vertexShader, vertexSource));
				Renderer.Call(() => GL.CompileShader(vertexShader));
				Renderer.Call(() => GL.GetShaderInfoLog(vertexShader, out info));
				Renderer.Call(() => GL.GetShader(vertexShader, ShaderParameter.CompileStatus, out status_code));

				if (status_code != 1)
				{
					Console.WriteLine("Failed to Compile Vertex Shader Source." +
						Environment.NewLine + info + Environment.NewLine + "Status Code: " + status_code.ToString());

					Renderer.Call(() => GL.DeleteShader(vertexShader));
					Renderer.Call(() => GL.DeleteProgram(Program));
					Program = 0;

					return false;
				}

				Renderer.Call(() => GL.AttachShader(Program, vertexShader));
				Renderer.Call(() => GL.DeleteShader(vertexShader));
			}

			if (fragmentSource != "")
			{
				int fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
				Renderer.Call(() => GL.ShaderSource(fragmentShader, fragmentSource));
				Renderer.Call(() => GL.CompileShader(fragmentShader));
				Renderer.Call(() => GL.GetShaderInfoLog(fragmentShader, out info));
				Renderer.Call(() => GL.GetShader(fragmentShader, ShaderParameter.CompileStatus, out status_code));

				if (status_code != 1)
				{
					Console.WriteLine("Failed to Compile Fragment Shader Source." +
						Environment.NewLine + info + Environment.NewLine + "Status Code: " + status_code.ToString());

					Renderer.Call(() => GL.DeleteShader(fragmentShader));
					Renderer.Call(() => GL.DeleteProgram(Program));
					Program = 0;

					return false;
				}

				Renderer.Call(() => GL.AttachShader(Program, fragmentShader));
				Renderer.Call(() => GL.DeleteShader(fragmentShader));
			}

			Renderer.Call(() => GL.LinkProgram(Program));
			Renderer.Call(() => GL.GetProgramInfoLog(Program, out info));
			Renderer.Call(() => GL.GetProgram(Program, GetProgramParameterName.LinkStatus, out status_code));

			if (status_code != 1)
			{
				Console.WriteLine("Failed to Link Shader Program." +
					Environment.NewLine + info + Environment.NewLine + "Status Code: " + status_code.ToString());

				Renderer.Call(() => GL.DeleteProgram(Program));
				Program = 0;

				return false;
			}

			return true;
		}
	}
}
