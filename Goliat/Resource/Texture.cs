﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using TextureType = Assimp.TextureType;
using TextureSlot = Assimp.TextureSlot;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Color = Goliat.Color;
using Goliat.Serialization;
//TextureType.Ambient;
//TextureType.Diffuse;
//TextureType.Displacement;
//TextureType.Emissive;
//TextureType.Height;
//TextureType.Lightmap;
//TextureType.Normals;
//TextureType.Opacity;
//TextureType.Reflection;
//TextureType.Shininess;
//TextureType.Specular;

namespace Goliat
{
	[Goliat.Serializable]
	public class Texture : SharedResource, Goliat.ISerializable
	{
		public Texture(MappedRessourceInfo info)
			: base(info)
		{
			Console.WriteLine("regenerating:" + info.filePath);
			this.LoadFrom(info.filePath);
		}
		public static Texture FromInfo(TextureInfo info)
		{
			if (SharedResource.getRessource(info.guid) != null)
			{
				return ((Texture)SharedResource.getRessource(info.guid));
			}
			else
				return (new Texture(info));
		}
		private Bitmap _imageBitmap = null;
		public Bitmap imageBitmap
		{
			get
			{
				return (this._imageBitmap);
			}
			private set
			{
				this._imageBitmap = value;
			}
		}
		public bool disposed = false;
		public override void Dispose()
		{
			base.Dispose();
			GL.DeleteTexture(this._glID);
			disposed = true;
			if (this.guid == "DA")
			{
				Console.WriteLine("DISPOSING TEXTURE");
			}
		}
		public override string ToString()
		{
			string res = "";
			res += "Texture:" + "width:" + this.width + "height:" + this.height + "\n";
			Color[,] test = this.getPixelRect(Vector2.zero, new Vector2(this.width, this.height));
			for (int i = 0; i < this.height; i++)
			{
				for (int j = 0; j < this.width; j++)
				{
					res += test[i, j].ToString() + "\n";
				}
			}
			return (res);
		}
		public void OnSerialize(Serializer.SerializationInfo info)
		{
			string path = "" + this.guid + ".bmp";
			this.save(path);
			info.AddValue("filePath", path);
			info.AddValue("guid", this.guid);
		}
		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			string path = (string)info.GetValue("filePath");
			Debug.Log(CustomProperty.logPath, "texture path:" + path);
			this.guid = (string)info.GetValue("guid");
			if (SharedResource.getRessource(this.guid) != null)
			{
				Texture text = (Texture)SharedResource.getRessource(this.guid);
				this._glID = text.glID;
				this.loaded = true;
				//this.width = text.width;
				//this.height = text.height;
				this.imageBitmap = text.imageBitmap;
			}
			else
			{
				//if (SharedResource.)
				Debug.Log(CustomProperty.logPath, "here my path:" + path);
				this.LoadFrom(path);
			}
		}
		private Bitmap ResizeBitmap(Bitmap sourceBMP, int width, int height)
		{
			//System.Drawing.Drawing2D.InterpolationMode test = System.Drawing.Drawing2D.InterpolationMode.
			Bitmap result = new Bitmap(width, height);
			using (Graphics g = Graphics.FromImage(result))
			{
				g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				g.DrawImage(sourceBMP, 0, 0, width, height);
			}
			return result;
		}
		public int width
		{
			get
			{
				bool check = false;
				float id;
				GL.GetFloat(GetPName.TextureBinding2D, out id);
				if ((int)id == this.glID)
					check = true;
				float width;
				this.Bind();

				GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureWidth, out width);

				if (!check)
					this.UnBind();
				return ((int)width);
			}
			set
			{
				bool check = false;
				float id;
				GL.GetFloat(GetPName.TextureBinding2D, out id);
				if ((int)id == this.glID)
					check = true;
				float width = value;
				this.Bind();
				this._imageBitmap = ResizeBitmap(this._imageBitmap, value, this.height);
				BitmapData TextureData =
					this._imageBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, (int)width, height),
					System.Drawing.Imaging.ImageLockMode.ReadOnly,
					System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, (int)width, height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);
				//if (!check)
				this.UnBind();
			}
		}
		public int height
		{
			get
			{
				bool check = false;
				float id;
				GL.GetFloat(GetPName.TextureBinding2D, out id);
				if ((int)id == this.glID)
					check = true;
				float height;
				this.Bind();
				GL.GetTexLevelParameter(TextureTarget.Texture2D, 0, GetTextureParameter.TextureHeight, out height);
				if (!check)
					this.UnBind();
				//				this.UnBind();
				return ((int)height);
			}
			set
			{
				/*bool check = false;
				float id;
				GL.GetFloat(GetPName.TextureBinding2D, out id);
				float height = value;
				if ((int)id == this.glID)
					check = true;*/
				this.Bind();
				this._imageBitmap = ResizeBitmap(this._imageBitmap, (int)width, (int)height);
				BitmapData TextureData =
					this._imageBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, (int)width, (int)height),
					System.Drawing.Imaging.ImageLockMode.ReadOnly,
					System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, (int)width, (int)height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);
				//if (!check)
				this.UnBind();
			}
		}

		public PixelFormat format
		{
			get
			{
				int format;
				this.Bind();
				GL.GetTexParameter(TextureTarget.Texture2D, GetTextureParameter.TextureInternalFormat, out format);

				return ((PixelFormat)format);
			}
		}
		public static List<Texture> SharedTextures = new List<Texture>();
		public String path;
		private bool _loaded = false;
		public bool loaded
		{
			get
			{
				return (this._loaded);
			}
			private set
			{
				this._loaded = value;
			}

		}
		public void Reload()
		{
			this.Delete();
			this.LoadFrom(path);
		}
		public void Delete()
		{
			if (loaded)
			{
				GL.DeleteTexture(this._glID);
				loaded = false;
			}
		}
		private int _glID;
		public int glID
		{
			get
			{
				if (!loaded)
					Reload();
				if (!loaded)
					return (-1);
				return (this._glID);
			}
			private set
			{
				this._glID = value;
			}
		}
		public Texture(String path)
		{
			SharedTextures.Add(this);
			this.LoadFrom(path);
		}

		public Texture(int width, int height)
		{
			Bitmap textureBitmap = new Bitmap(width, height);
			BitmapData TextureData =
					textureBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, width, height),
					System.Drawing.Imaging.ImageLockMode.ReadWrite,
					System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);
			int _glID = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, _glID);
			this._glID = _glID;
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);
			//textureBitmap.UnlockBits(TextureData);
			float maxAniso;
			GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAniso);
			float aniso = maxAniso;
			if (2 < maxAniso)
				aniso = 2;
			GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			//GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.ClearColor(Color.Black.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit);

			//GL.BindTexture(TextureTarget.Texture2D, 0);
			/*if (this.info == null)
				this.info = new TextureInfo();
			info.guid = this.guid;
			((TextureInfo)info).anisoLevel = new Range((int)maxAniso, 0, maxAniso);
			*/
			//GL.BindTexture(TextureTarget.Texture2D, 0);
			//TextureInfo tmp = (TextureInfo)this.info;
			//tmp.filePath = "";
			//tmp.imageBitmap = textureBitmap;
			this.imageBitmap = textureBitmap;
			loaded = true;
			this.UnBind();
			if (SharedResource.getRessource(this.guid) == null)
			{
				SharedResource.addRessource(this);
			}
		}
		public Texture(int width, int height, Color backColor)
		{

			Bitmap textureBitmap = new Bitmap(width, height);
			BitmapData TextureData =
					textureBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, width, height),
					System.Drawing.Imaging.ImageLockMode.ReadWrite,
					System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);
			IntPtr ptr = TextureData.Scan0;



			// Declare an array to hold the bytes of the bitmap.
			int bytes = Math.Abs(TextureData.Stride) * textureBitmap.Height;
			//Console.WriteLine("STRIDE:" + Math.Abs(TextureData.Stride));
			byte[] rgbaValues = new byte[bytes];

			// Copy the RGB values into the array.
			System.Runtime.InteropServices.Marshal.Copy(ptr, rgbaValues, 0, bytes);

			// Set every third value to 255. A 24bpp bitmap will look red.  
			for (int counter = 0; counter < rgbaValues.Length; counter += 4)
			{

				rgbaValues[counter] = (byte)((int)(backColor.r * 255));
				rgbaValues[counter + 1] = (byte)((int)(backColor.g * 255));
				rgbaValues[counter + 2] = (byte)((int)(backColor.b * 255));
				rgbaValues[counter + 3] = (byte)((int)(backColor.a * 255));
			}
			System.Runtime.InteropServices.Marshal.Copy(rgbaValues, 0, ptr, bytes);
			int _glID = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, _glID);
			this._glID = _glID;
			//Console.WriteLine("WIDTH:" + width);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);
			textureBitmap.UnlockBits(TextureData);




			/*textureBitmap = new Bitmap(width, height);
			TextureData =
					textureBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, width, height),
					System.Drawing.Imaging.ImageLockMode.ReadWrite,
					System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);
			 * ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(bytes);



			// Declare an array to hold the bytes of the bitmap.

			// Copy the RGB values into the array.

			// Set every third value to 255. A 24bpp bitmap will look red.  
			for (int counter = 0; counter < rgbaValues.Length; counter += 4)
			{

				rgbaValues[counter] = (byte)((int)(Color.White.r * 255));
				rgbaValues[counter + 1] = (byte)((int)(Color.White.g * 255));
				rgbaValues[counter + 2] = (byte)((int)(Color.White.b * 255));
				rgbaValues[counter + 3] = (byte)((int)(Color.White.a * 255));
			}
			System.Runtime.InteropServices.Marshal.Copy(rgbaValues, 0, ptr, bytes);
			GL.BindTexture(TextureTarget.Texture2D, _glID);
			this._glID = _glID;
			Console.WriteLine("WIDTH:" + width);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, ptr);
			textureBitmap.UnlockBits(TextureData);
		*/

			//textureBitmap.UnlockBits(TextureData);
			float maxAniso;
			GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAniso);
			float aniso = maxAniso;
			if (2 < maxAniso)
				aniso = 2;
			//GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			//GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

			int width2 = 0;
			int height2 = 0;


			//GL.BindTexture(TextureTarget.Texture2D, 0);
			if (this.info == null)
				this.info = new TextureInfo();
			info.guid = this.guid;
			((TextureInfo)info).anisoLevel = new Range((int)maxAniso, 0, maxAniso);
			TextureInfo tmp = (TextureInfo)this.info;
			tmp.filePath = "";
			tmp.imageBitmap = textureBitmap;
			loaded = true;
			this.UnBind();
		}
		public Texture(TextureInfo info)
		{

			Bitmap textureBitmap = info.imageBitmap;
			BitmapData TextureData =
					textureBitmap.LockBits(
					new System.Drawing.Rectangle(0, 0, width, height),
					System.Drawing.Imaging.ImageLockMode.ReadOnly,
					System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);
			int _glID = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, _glID);
			this._glID = _glID;
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);
			//textureBitmap.UnlockBits(TextureData);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)info.wrapMode);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)info.wrapMode);
			float maxAniso;
			GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAniso);
			float aniso = maxAniso;
			if (maxAniso > info.anisoLevel)
				aniso = info.anisoLevel;
			GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);

			GL.ClearColor(Color.Black.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit);
			//GL.BindTexture(TextureTarget.Texture2D, 0);
			this.info = info;
			TextureInfo tmp = (TextureInfo)this.info;
			tmp.anisoLevel = new Range((int)maxAniso, 0, maxAniso);
			loaded = true;
			this.UnBind();
			this.guid = info.guid;
			this.updateDictionary();
		}
		private static Texture _activeTexture;
		public static Texture activeTexture
		{
			get
			{
				return (Texture._activeTexture);
			}
			private set
			{
				Texture._activeTexture = value;
			}
		}
		public Texture(int glID)
		{
			this._glID = glID;
			this._loaded = true;
		}

		public void setPixel(Vector2 position, Color color)
		{

			GL.TexSubImage2D(TextureTarget.Texture2D, 0, (int)position.x, (int)(position.y), this.width, this.height, this.format, PixelType.UnsignedByte, color.intPtr());
		}

		public void setPixel(float x, float y, Color color)
		{
			GL.TexSubImage2D(TextureTarget.Texture2D, 0, (int)x, (int)y, this.width, this.height, this.format, PixelType.UnsignedByte, color.intPtr());
		}
		public void setPixelRect(Vector2 position, Vector2 size, Color color)
		{
			this.Bind();
			int bytes = 4 * this.width * this.height;
			byte[] rgbaValues = new byte[bytes];
			GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Bgra, PixelType.UnsignedByte, rgbaValues);
			int width = (int)size.x;
			int height = (int)size.y;
			int thiswidth = this.width;
			int thisheight = this.height;

			for (int y = (int)position.y; y < (int)(position.y + height); y++)
			{
				for (int x = (int)position.x; x < (int)(position.x + width); x++)
				{
					int y2 = y - (int)position.y;
					int x2 = x - (int)position.x;
					if (x2 >= 0 && y2 >= 0 && x2 < thiswidth && y2 < thisheight)
					{
						rgbaValues[(y2 * thiswidth * 4) + (x2 * 4)] = (byte)((int)(color.r * 255));
						rgbaValues[(y2 * thiswidth * 4) + (x2 * 4) + 1] = (byte)((int)(color.g * 255));
						rgbaValues[(y2 * thiswidth * 4) + (x2 * 4) + 2] = (byte)((int)(color.b * 255));
						rgbaValues[(y2 * thiswidth * 4) + (x2 * 4) + 3] = (byte)((int)(color.a * 255));
					}
				}
			}
			IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(bytes);


			System.Runtime.InteropServices.Marshal.Copy(rgbaValues, 0, ptr, bytes);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, thiswidth, thisheight, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, ptr);
			Renderer.Call(() => GL.TexSubImage2D(TextureTarget.Texture2D, 0, 0, 0, thiswidth, thisheight, PixelFormat.Bgra, PixelType.UnsignedByte, ptr));
			System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);
			this.UnBind();
		}
		public void setPixelRect(Vector2 position, Color[,] pixelRect)
		{
			this.Bind();
			int bytes = 4 * this.width * this.height;
			byte[] rgbaValues = new byte[bytes];
			GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Bgra, PixelType.UnsignedByte, rgbaValues);
			int width = (int)pixelRect.GetLength(1);
			int height = (int)pixelRect.GetLength(0);
			int thiswidth = this.width;
			int thisheight = this.height;

			for (int y = (int)position.y; y < (int)(position.y + height); y++)
			{
				for (int x = (int)position.x; x < (int)(position.x + width); x++)
				{
					int y2 = y - (int)position.y;
					int x2 = x - (int)position.x;
					if (x >= 0 && y >= 0 && x < thiswidth && y < thisheight)
					{
						rgbaValues[(y * thiswidth * 4) + (x * 4)] = (byte)((int)(pixelRect[y2, x2].r * 255));
						rgbaValues[(y * thiswidth * 4) + (x * 4) + 1] = (byte)((int)(pixelRect[y2, x2].g * 255));
						rgbaValues[(y * thiswidth * 4) + (x * 4) + 2] = (byte)((int)(pixelRect[y2, x2].b * 255));
						rgbaValues[(y * thiswidth * 4) + (x * 4) + 3] = (byte)((int)(pixelRect[y2, x2].a * 255));
					}
				}
			}
			IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(bytes);
			System.Runtime.InteropServices.Marshal.Copy(rgbaValues, 0, ptr, bytes);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, thiswidth, thisheight, 0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, ptr);
			Renderer.Call(() => GL.TexSubImage2D(TextureTarget.Texture2D, 0, 0, 0, thiswidth, thisheight, PixelFormat.Bgra, PixelType.UnsignedByte, ptr));
			System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);
			this.UnBind();
		}

		public Color[,] getPixelRect(Vector2 position, Vector2 size)
		{
			this.Bind();
			int width = this.width;
			int height = this.height;
			int bytes = 4 * width * height;
			byte[] rgbaValues = new byte[bytes];
			int resbytes = 4 * (int)size.x * (int)size.y;
			Color[,] res = new Color[(int)size.x, (int)size.y];
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					res[i, j] = Color.Black;
				}
			}
			GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Bgra, PixelType.UnsignedByte, rgbaValues);
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					if (x >= (int)position.x && y >= (int)position.y && x - (int)position.x < (int)size.x && y - (int)position.y < (int)size.y)
					{
						float b = (float)rgbaValues[(y * width * 4) + (x * 4)] / 255f;
						float g = (float)rgbaValues[(y * width * 4) + (x * 4) + 1] / 255f;
						float r = (float)rgbaValues[(y * width * 4) + (x * 4) + 2] / 255f;
						float a = (float)rgbaValues[(y * width * 4) + (x * 4) + 3] / 255f;
						res[y - (int)position.y, x - (int)position.x] = new Color(r, g, b, a);
					}
				}
			}
			this.UnBind();
			return (res);
		}

		public Color getPixel(Vector2 position)
		{
			// ! 
			return (Color.Black);
		}
		public Color getPixel(int x, int y)
		{
			// !
			return (Color.Black);
		}
		public void drawCircle(Vector2 center, float radius, Color color)
		{
			int x0 = (int)center.x;
			int y0 = (int)center.y;
			int x = 0;
			int y = (int)radius;
			int delta = 2 - 2 * (int)radius;
			int error = 0;

			while (y >= 0)
			{
				setPixel(x0 + x, y0 - y, color);
				setPixel(x0 - x, y0 - y, color);
				error = 2 * (delta + y) - 1;
				if (delta < 0 && error <= 0)
				{
					++x;
					delta += 2 * x + 1;
					continue;
				}
				error = 2 * (delta - x) - 1;
				if (delta > 0 && error > 0)
				{
					--y;
					delta += 1 - 2 * y;
					continue;
				}
				++x;
				delta += 2 * (x - y);
				--y;
			}
		}
		public void save(string path)
		{
			if (File.Exists(path))
			{
				File.Delete(path);
			}
			/*Color[,] pixels = this.getPixelRect(Vector2.zero, new Vector2(this.width, this.height));
			for (int i = 0; i < pixels.GetLength(0); i++ )
			{
				for (int j = 0; j < pixels.GetLength(1); j++)
				{
					if (path.Contains("Meta"))
					{
						Console.WriteLine("" + pixels[i, j]);
					}
				}
			}*/
			Debug.Log(CustomProperty.logPath, "hey! here my path:" + path);
			int width = this.width;
			int height = this.height;
			Bitmap textureBitmap = new Bitmap(width, height);
			this.Bind();
			BitmapData TextureData =
						textureBitmap.LockBits(
						new System.Drawing.Rectangle(0, 0, textureBitmap.Width, textureBitmap.Height),
						System.Drawing.Imaging.ImageLockMode.ReadWrite,
						System.Drawing.Imaging.PixelFormat.Format32bppArgb
					);
			GL.GetTexImage(TextureTarget.Texture2D, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);

			textureBitmap.UnlockBits(TextureData);
			this.imageBitmap = textureBitmap;
			try
			{
				textureBitmap.Save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), path), ImageFormat.Bmp);
			}
			catch (Exception e)
			{
				this.save(path);
			}
		}


		public void save(string path, RotateFlipType rotateFlipType)
		{
			if (File.Exists(path))
			{
				File.Delete(path);
			}
			/*Color[,] pixels = this.getPixelRect(Vector2.zero, new Vector2(this.width, this.height));
			for (int i = 0; i < pixels.GetLength(0); i++ )
			{
				for (int j = 0; j < pixels.GetLength(1); j++)
				{
					if (path.Contains("Meta"))
					{
						Console.WriteLine("" + pixels[i, j]);
					}
				}
			}*/
			Debug.Log(CustomProperty.logPath, "hey! here my path:" + path);
			int width = this.width;
			int height = this.height;
			Bitmap textureBitmap = new Bitmap(width, height);
			this.Bind();
			BitmapData TextureData =
						textureBitmap.LockBits(
						new System.Drawing.Rectangle(0, 0, textureBitmap.Width, textureBitmap.Height),
						System.Drawing.Imaging.ImageLockMode.ReadWrite,
						System.Drawing.Imaging.PixelFormat.Format32bppArgb
					);
			GL.GetTexImage(TextureTarget.Texture2D, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);

			textureBitmap.UnlockBits(TextureData);

			((Image)textureBitmap).RotateFlip(rotateFlipType);
			this.imageBitmap = textureBitmap;
			try
			{
				textureBitmap.Save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), path), ImageFormat.Bmp);
			}
			catch (Exception e)
			{
				this.save(path);
			}
		}

		private void LoadFrom(String fileName)
		{
			//Console.WriteLine("hey! here my path oy:" + fileName);
			//Debug.Log(Serializer.CustomProperty.logPath, "FileName:" + fileName);
			this.path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
			if (!File.Exists(fileName) || !SharedResource.isEnvironnementLoaded)
			{
				if (SharedResource.isEnvironnementLoaded)
					loaded = true;
				loaded = false;
				//Console.WriteLine("i dont have path");

				return;
			}
			else
			{
				GL.Enable(EnableCap.Texture2D);
				Bitmap textureBitmap = new Bitmap(fileName);
				BitmapData TextureData =
						textureBitmap.LockBits(
						new System.Drawing.Rectangle(0, 0, textureBitmap.Width, textureBitmap.Height),
						System.Drawing.Imaging.ImageLockMode.ReadOnly,
						System.Drawing.Imaging.PixelFormat.Format32bppArgb
					);
				int _glID = GL.GenTexture();
				GL.BindTexture(TextureTarget.Texture2D, _glID);
				this._glID = _glID;
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, textureBitmap.Width, textureBitmap.Height, 0,
					OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, TextureData.Scan0);
				textureBitmap.UnlockBits(TextureData);

				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
				loaded = true;
			}
			GL.Disable(EnableCap.Texture2D);
		}
		public void Bind()
		{
			if (!loaded)
				this.Reload();
			GL.BindTexture(TextureTarget.Texture2D, this._glID);
			Texture._activeTexture = this;
		}
		public void UnBind()
		{
			GL.BindTexture(TextureTarget.Texture2D, 0);
			Texture._activeTexture = null;
		}
	}
}
