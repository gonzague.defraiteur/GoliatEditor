﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goliat
{
	public class RotationKey
	{
		public Quaternion key = Quaternion.identity;
		public float timeOffset = 0.0f;
		public AnimationClip parent = null;
		public int index = 0;
		public RotationKey(AnimationClip parent, Quaternion key, int index, float timeOffset)
		{
			this.index = index;
			this.parent = parent;
			this.key = key;
			this.timeOffset = timeOffset;
		}
	}

	public class PositionKey
	{
		public Vector3 key = Vector3.zero;
		public float timeOffset = 0.0f;
		public AnimationClip parent = null;
		public int index = 0;
		public PositionKey(AnimationClip parent, Vector3 key, int index, float timeOffset)
		{
			this.index = index;
			this.parent = parent;
			this.key = key;
			this.timeOffset = timeOffset;
		}
	}

	public class ScaleKey
	{
		public Vector3 key = Vector3.zero;
		public float timeOffset = 0.0f;
		public AnimationClip parent = null;
		public int index = 0;
		public ScaleKey(AnimationClip parent, Vector3 key, int index, float timeOffset)
		{
			this.index = index;
			this.parent = parent;
			this.key = key;
			this.timeOffset = timeOffset;
		}
	}

	public struct AnimationStep
	{
		public readonly Quaternion fromRotation;
		public readonly Quaternion toRotation;
		public readonly Vector3 fromPosition;
		public readonly Vector3 toPosition;
		public readonly Vector3 fromScale;
		public readonly Vector3 toScale;
		public readonly Quaternion rotation;
		public readonly Vector3 position;
		public readonly Vector3 scale;
		public readonly AnimationBlendMode blendMode;
		public readonly float time;
		public readonly float weight;
		public AnimationStep(float time, AnimationKeys keys, AnimationBlendMode blendMode, float weight)
		{
			this.weight = weight;
			Vector3 indexs = keys.indexs(time);
			fromPosition = keys.PositionKeys[(int)indexs.x % keys.PositionKeys.Count].key;
			toPosition = keys.PositionKeys[((int)indexs.x + 1) % keys.PositionKeys.Count].key;
			fromRotation = keys.RotationKeys[(int)indexs.y % keys.RotationKeys.Count].key;
			toRotation = keys.RotationKeys[((int)indexs.y + 1) % keys.RotationKeys.Count].key;
			fromScale = keys.ScaleKeys[(int)indexs.z % keys.ScaleKeys.Count].key;
			toScale = keys.ScaleKeys[((int)indexs.z + 1) % keys.ScaleKeys.Count].key;
			//Console.WriteLine("here is my weight:" + weight);
			// donc:
			/*
			 * probleme au niveau de l'update du temps qui devient négative très loin, et probleme au niveau de la weight qui devrait etre a 1.
			 * 
			 * 
			 */
			position = keys.PositionKeys[(int)indexs.x % keys.PositionKeys.Count].key;
			scale = keys.ScaleKeys[(int)indexs.z % keys.ScaleKeys.Count].key;
			rotation = keys.RotationKeys[(int)indexs.y % keys.RotationKeys.Count].key;
			this.blendMode = blendMode;
			this.time = time;
			return;


				///////
			Console.WriteLine("this weight:" + weight);
			Console.WriteLine("current time:" + time);
			float rotFactor = (time - keys.RotationKeys[(int)indexs.y % keys.RotationKeys.Count].timeOffset) / (keys.RotationKeys[((int)indexs.y + 1) % keys.RotationKeys.Count].timeOffset - keys.RotationKeys[(int)indexs.y % keys.RotationKeys.Count].timeOffset);
			Console.WriteLine("rot factor:" + rotFactor);
			float posFactor = (time - keys.PositionKeys[(int)indexs.x % keys.PositionKeys.Count].timeOffset) / (keys.PositionKeys[((int)indexs.x + 1) % keys.PositionKeys.Count].timeOffset - keys.PositionKeys[(int)indexs.x % keys.PositionKeys.Count].timeOffset);
			float scaleFactor = (time - keys.ScaleKeys[(int)indexs.z % keys.ScaleKeys.Count].timeOffset) / (keys.ScaleKeys[((int)indexs.z + 1) % keys.ScaleKeys.Count].timeOffset - keys.ScaleKeys[(int)indexs.z % keys.ScaleKeys.Count].timeOffset);
			rotation = Quaternion.Slerp(fromRotation, toRotation, rotFactor);
			position = Vector3.Lerp(fromPosition, toPosition, posFactor);
			scale = Vector3.Lerp(fromScale, toScale, scaleFactor);
			this.blendMode = blendMode;
			this.time = time;
		}
	}
	public class AnimationKeys
	{
		public List<RotationKey> RotationKeys = new List<RotationKey>();
		public List<PositionKey> PositionKeys = new List<PositionKey>();
		public List<ScaleKey> ScaleKeys = new List<ScaleKey>();
		public AnimationClip parent = null;
		public string boneName = "";
		/// <summary>
		/// Return index of actually played keys in a Vector : (res.x = positionIndex, res.y = rotationIndex, res.z = scaleIndex)
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		/// 
		public Vector3 indexs(float time)
		{
			return (new Vector3(findPosIndex(time), findRotIndex(time), findScaleIndex(time)));
			;
		}
		public int findPosIndex(float time)
		{
			for (int i = 0; i < this.PositionKeys.Count; i++ )
			{
				if (time <= 0)
				{
					return (0);
				}
				if (this.PositionKeys[i].timeOffset > time)
				{
					return (i - 1);
				}
			}
			return (this.PositionKeys.Count - 1);
		}
		public int findRotIndex(float time)
		{
			for (int i = 0; i < this.RotationKeys.Count; i++)
			{
				if (time <= 0)
				{
					return (0);
				}
				if (this.RotationKeys[i].timeOffset > time)
				{
					return (i - 1);
				}
			}
			return (this.RotationKeys.Count - 1);
		}
		public int findScaleIndex(float time)
		{
			for (int i = 0; i < this.ScaleKeys.Count; i++)
			{
				if (time <= 0)
				{
					return (0);
				}
				if (this.ScaleKeys[i].timeOffset > time)
				{
					return (i - 1);
				}
			}
			return (this.ScaleKeys.Count - 1);
		}
		public AnimationKeys(AnimationClip parent, Assimp.NodeAnimationChannel channel)
		{
			this.boneName = channel.NodeName;
			this.parent = parent;
			for (int i = 0; i < channel.PositionKeyCount; i++)
			{
				PositionKeys.Add(new PositionKey(this.parent, channel.PositionKeys[i].Value.octogon(), i, (float)channel.PositionKeys[i].Time));
			}
			for (int i = 0; i < channel.ScalingKeyCount; i++)
			{
				ScaleKeys.Add(new ScaleKey(this.parent, channel.ScalingKeys[i].Value.octogon(), i, (float)channel.ScalingKeys[i].Time));
			}
			for (int i = 0; i < channel.RotationKeyCount; i++)
			{
				RotationKeys.Add(new RotationKey(this.parent, channel.RotationKeys[i].Value.octogon(), i, (float)channel.RotationKeys[i].Time));
			}
		}
	}
	public class AnimationClip : SharedResource
	{
		//
		// ou non: ce qu'on fait, il s'agit d'une resource, c'est dans le serializer qu'on gère cette idée directement (d'ailleurs me demande si c'est pas déja le cas).
		//
		public readonly float frameRate = 0.0f;
		public readonly float length = 0.0f;
		public WrapMode wrapMode = WrapMode.Default;
		public List<string> affectedBones = new List<string>();
		public Dictionary<string, AnimationKeys> animationDictionary = new Dictionary<string, AnimationKeys>();
		public AnimationClip(Assimp.Animation animation, Assimp.Scene m_model)
		{
			this.name = animation.Name;
			for (int i = 0; i < animation.NodeAnimationChannelCount; i++)
			{
				affectedBones.Add(animation.NodeAnimationChannels[i].NodeName);
				Assimp.NodeAnimationChannel channel = animation.NodeAnimationChannels[i];
				AnimationKeys keys = new AnimationKeys(this, animation.NodeAnimationChannels[i]);
				animationDictionary.Add(animation.NodeAnimationChannels[i].NodeName, keys);
			}
			this.frameRate = (float)animation.TicksPerSecond;
			this.length = (float)animation.DurationInTicks;
		}
		public AnimationClip(Assimp.Animation animation, Assimp.Scene m_model, MappedRessourceInfo info) : base(info)
		{
			this.name = animation.Name;
			for (int i = 0; i < animation.NodeAnimationChannelCount; i++)
			{
				affectedBones.Add(animation.NodeAnimationChannels[i].NodeName);
				Assimp.NodeAnimationChannel channel = animation.NodeAnimationChannels[i];
				AnimationKeys keys = new AnimationKeys(this, animation.NodeAnimationChannels[i]);
				animationDictionary.Add(animation.NodeAnimationChannels[i].NodeName, keys);
			}
			this.frameRate = (float)animation.TicksPerSecond;
			this.length = (float)animation.DurationInTicks;
		}
	}
}
