﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using TextureType = Assimp.TextureType;
using TextureSlot = Assimp.TextureSlot;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Color = Goliat.Color;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using Goliat.Serialization;
using System.Xml;

namespace Goliat
{

	// ce que je pense: fuck unity, on mettra un attribut qui dit que la ressource n'est pas visible.
	/*
	 * 
	 * 
	 * AnimationInfo : animation en mode sharé
	 * 
	 * 
	 * SceneBone : les bones mais pour les scenes.
	 * 
	 * 
	 * 
	 */
	public interface INamed
	{
		string name
		{
			get;
			set;
		}
	}

	
	public class MeshInfo : SharedResource
	{
		public bool isSkinned = false;
		public string name = "";
		public List<Material> materials = new List<Material>();
		public Mesh mesh = null;
		public override void Dispose()
		{
			base.Dispose();
			this.mesh.Dispose();
			for (int i = 0; i < this.materials.Count; i++)
			{
				this.materials[i].Dispose();
			}
		}
		public Scene sceneNode;
		public MeshInfo(Scene parentNode)
		{
			this.sceneNode = parentNode;
			;
		}
	}

	public class BoneInfo
	{
		public MeshInfo affectedMesh = null;
		public Assimp.Bone bone = null;
		public string name = "";
		public Scene meshNode = null;
		public int subMeshIndice = 0;
		public BoneInfo(MeshInfo mesh, Assimp.Bone bone, Scene meshNode, int subMeshIndice)
		{
			this.name = bone.Name;
			this.affectedMesh = mesh;
			this.bone = bone;
			this.meshNode = meshNode;
			this.subMeshIndice = subMeshIndice;
		}
	}
	public class AnimationInfo
	{
		public Dictionary<string, List<Mesh>> boneMeshs = new Dictionary<string, List<Mesh>>();
		public List<SceneBone> mappedBones = new List<SceneBone>();
		public List<BoneInfo> assimpBones = new List<BoneInfo>();
		public AnimationState this[string animationName]
		{
			get
			{
				return (this.states.ContainsKey(animationName) ? this.states[animationName] : null);
			}
			private set
			{
				this.states.AddOrReplace(animationName, value);
			}
		}
		public List<AnimationClip> clips
		{
			get
			{
				List<AnimationClip> clips = new List<AnimationClip>();
				foreach (string key in this.states.Keys)
				{
					clips.Add(this.states[key].clip);
				}
				return (clips);
			}
		}
		public AnimationInfo(Assimp.Scene m_model)
		{
			for (int i = 0; i < m_model.AnimationCount; i++ )
			{
				AnimationClip clip = new AnimationClip(m_model.Animations[i], m_model);
				AnimationState state = new AnimationState(clip);
				this.states.Add(clip.name, state);
			}
		}
		public AnimationInfo(Assimp.Scene m_model, SharedResource.MappedRessourceInfo info)
		{
			for (int i = 0; i < m_model.AnimationCount; i++)
			{
				AnimationClip clip = new AnimationClip(m_model.Animations[i], m_model, info);
				AnimationState state = new AnimationState(clip);
				this.states.Add(clip.name, state);
			}
		}
		private Dictionary<string, AnimationState> states = new Dictionary<string, AnimationState>();
		//private List<AnimationClip> clips = new List<AnimationClip>();
		public bool animatePhysics
		{
			get;
			set;
		}
		private AnimationState _current;
		public AnimationClip clip // le current? ouai.
		{
			get
			{
				return (this._current.clip);
			}
			set
			{
				this._current = (value != null && states.ContainsKey(value.name)) ? states[value.name] : this._current;
			}
		}

		public AnimationCullingType cullingType = AnimationCullingType.BasedOnRenderers;
		private bool _isPlaying = false;
		public bool isPlaying
		{
			get
			{
				return(this._isPlaying);
			}
			set
			{
				this._isPlaying = value;
			}
		}
		public Bounds localBounds
		{
			get
			{
				return (new Bounds(Vector3.zero, Vector3.one));
			}
		}
		private bool _playAutomatically = false;
		public bool playAutomatically
		{
			get
			{
				return (this._playAutomatically);
			}
			set
			{
				this._playAutomatically = value;
			}
		}
		public WrapMode wrapMode = WrapMode.Default;
		public void Blend(string animation, float targetWeight = 1.0f, float fadeLength = 0.3f)
		{
			;
		}
		public void CrossFade(string animation, float fadeLength = 0.3f, PlayMode mode = PlayMode.StopSameLayer)
		{
			;
		}
		public void CrossFadeQueued(string animation, float fadeLength = 0.3f, PlayMode mode = PlayMode.StopSameLayer, QueueMode queue = QueueMode.CompleteOthers)
		{
			;
		}
		public bool IsPlaying(string animation) //  d ou la classe animation state, qui dit, par rapport a des clips qui eux sont simplement des resources, des stats. a savoir ou en est on de ce clip, est il playing, etc...
		{
			if (this.clip.name == animation)
				return (true);
			return (false);
		}
		public void Play(string animation, PlayMode mode = PlayMode.StopSameLayer)
		{
			if (mode == PlayMode.StopSameLayer || mode == PlayMode.StopAll)
			{
				this.Stop();
			}
			else if (mode == PlayMode.Continue)
			{
				if (this.states.ContainsKey(animation))
				{
					this.states[animation].time = 0;
					this.states[animation].enabled = true;
				}
			}
			else if (mode == PlayMode.Resume)
			{
				if (this.states.ContainsKey(animation))
				{
					this.states[animation].enabled = true;
				}
			}
			this._isPlaying = true; // si le wrapMode == default, bah on joue suivant le wrap mode du clip en cours. sinon on joue selon notre wrapmode.
		}
		public void Play(PlayMode mode = PlayMode.StopSameLayer)
		{
			this._isPlaying = true;
		}
		public void AddClip(AnimationClip clip)
		{
			if (!this.states.ContainsKey(clip.name))
			{
				this.states.Add(clip.name, new AnimationState(clip));
			}
		}
		public void RemoveClip(AnimationClip clip)
		{
			if (this.states.ContainsKey(clip.name))
			{
				this.states.Remove(clip.name);
			}
			;
		}
		public void Stop()
		{
			this.pausedStates = null;
			foreach (string key in this.states.Keys)
			{
				this.states[key].time = 0;
				this.states[key].enabled = false;
			}
		}
		public void Pause()
		{
			if (this.pausedStates != null)
			{
				int count = pausedStates.Count;
				foreach (string key in this.states.Keys)
				{
					if (this.states[key].enabled && !this.pausedStates.Contains(key))
					{
						this.pausedStates = new List<string>();
						break;
					}
					else if (this.states[key].enabled)
					{
						count--;
					}
					this.states[key].enabled = false;
				}
				if (count != 0)
				{
					this.pausedStates = new List<string>();
				}
				else
				{
					return;
				}
			}
			this.pausedStates = new List<string>();
			this._isPlaying = false;
			foreach (string key in this.states.Keys)
			{
				if (this.states[key].enabled)
				{
					this.pausedStates.Add(key);
				}
				this.states[key].enabled = false;
			}
		}
		public void Resume()
		{
			for (int i = 0; i < this.pausedStates.Count; i++)
			{
				this.states[this.pausedStates[i]].enabled = true;
			}
			this._isPlaying = true;
		}
		private List<string> pausedStates = null;
	}
	public struct WeightedVertex
	{
		public float weight;
		public Vector3 value;
		public WeightedVertex(VertexWeight weight, Vector3 value)
		{
			this.weight = weight.weight;
			this.value = value;
		}
	}
	public struct VertexWeight
	{
		public int index;
		public float weight;
		public VertexWeight(int index, float weight)
		{
			this.index = index;
			this.weight = weight;
		}
	}
	public class SceneBone : Scene
	{
		public Dictionary<MeshInfo, List<VertexWeight>> weights = new Dictionary<MeshInfo, List<VertexWeight>>();
		public Matrix OffsetMatrix(SkinnedMeshFilter filter)
		{
			return (Matrix.Identity);
		}
		public Dictionary<MeshInfo, Matrix> offsetMatrixs = new Dictionary<MeshInfo, Matrix>();
		public SceneBone rootBone
		{
			get
			{
				if (this.parent != null && this.parent.GetType() == this.GetType())
				{
					return (((SceneBone)this.parent).rootBone);
				}
				else
				{
					return (this);
				}
			}
		}
		private string _originalName = "";
		public string originalName
		{
			get
			{
				if (_originalName == "")
				{
					this._originalName = this.name;
				}
				return (_originalName);
			}
		}
		public Matrix offsetMatrix = Matrix.Identity;
		public bool drawBone = false;
		public readonly Vector3 originalPosition = Vector3.zero;
		public readonly Quaternion originalRotation = Quaternion.identity;
		public readonly Vector3 originalScale = Vector3.one;
		public SceneBone(MappedRessourceInfo info, int sibling) : base(info, sibling)
		{
			;
		}
		public SceneBone(int sibling) : base(sibling)
		{
			;
		}
	}
	public class Scene : SharedResource
	{
		public List<int> getSibling()
		{
			//List<int> reversed = new List<int>();
			List<int> res = new List<int>();

			Scene current = this;
			while (current.parent != null)
			{
				res.Insert(0, current.sibling);
				current = current.parent;
			}
			return (res);
		}
		public AnimationInfo animation = null;
		public List<MeshInfo> meshs = new List<MeshInfo>();
		public List<Scene> children = new List<Scene>();
		public Vector3 localPosition = Vector3.zero;
		public Quaternion localRotation = Quaternion.identity;
		public Vector3 localScale = Vector3.one;
		public Scene parent = null;
		public string name = "";
		public int sibling
		{
			get;
			protected set;
		}

		private Matrix getTransformation()
		{
			Matrix translation = Matrix.translation(this.localPosition);
			Matrix rotation = new Matrix(this.localRotation);
			Matrix scale = Matrix.scale(this.localScale);
			Matrix res = translation * rotation * scale;
			return (res);
		}
		private int getVerticesNb()
		{
			int res = 0;
			if (this.meshs == null)
			{
				Console.WriteLine("i got null meshs");
			}
			for (int i = 0; i < this.meshs.Count; i++)
			{
				if (this.meshs[i] == null)
				{
					Console.WriteLine("mesh nb:" + this.meshs[i].mesh.guid + "; is null");
				}
				else if (this.meshs[i].mesh.vertices == null)
				{
					Console.WriteLine("mesh nb: " + this.meshs[i].mesh.guid + "; has no vertices");
				}
				res += this.meshs[i].mesh.vertices.Length;
			}
			for (int i = 0; i < this.children.Count; i++)
			{
				res += this.children[i].getVerticesNb();
			}
			return (res);
		}
		private Vector3 getMedian()
		{
			Vector3 median = Vector3.zero;
			int totalLen = this.getVerticesNb();
			for (int i = 0; i < this.meshs.Count; i++)
			{
				for (int j = 0; j < this.meshs[i].mesh.vertices.Length; j++)
				{
					median += this.meshs[i].mesh.vertices[j] / totalLen;
				}
			}
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].getMedianRecursive(ref median, totalLen, this);
			}
			return (median);
		}
		private Matrix getLocalToRootMatrix(Scene root)
		{
			if (this == root)
				return (Matrix.Identity);
			Matrix res = new Matrix(4, 4);
			if (this.parent != null && this.parent != root)
				res = this.parent.getLocalToRootMatrix(root) * (this.getTransformation());
			else
				res = (this.getTransformation());
			return (res);
		}
		private Vector3 getMedianRecursive(ref Vector3 median, int totalVerticesLen, Scene root)
		{
			Matrix parentMatrix = this.getLocalToRootMatrix(root);

			for (int i = 0; i < this.meshs.Count; i++)
			{
				for (int j = 0; j < this.meshs[i].mesh.vertices.Length; j++)
				{
					median += (this.meshs[i].mesh.vertices[j] * parentMatrix) / totalVerticesLen;
				}
			}
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].getMedianRecursive(ref median, totalVerticesLen, root);
			}
			return (median);
		}
		private Vector3[] GetPolarities()
		{

			Vector3 center = this.getMedian(); // a check
			Vector3 farerFromMedian = Vector3.zero;
			Vector3[] res = new Vector3[3] { Vector3.zero, Vector3.zero, Vector3.zero };
			for (int i = 0; i < this.meshs.Count; i++)
			{
				for (int j = 0; j < this.meshs[i].mesh.vertices.Length; j++)
				{
					if ((this.meshs[i].mesh.vertices[j] - center).magnitude > farerFromMedian.magnitude)
					{
						farerFromMedian = this.meshs[i].mesh.vertices[j] - center;
					}
				}
			}
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].GetFarerPointRecursive(ref farerFromMedian, center, this); // a check
			}
			farerFromMedian = farerFromMedian + center;
			Vector3 polarity2 = Vector3.zero;
			for (int i = 0; i < this.meshs.Count; i++)
			{
				for (int j = 0; j < this.meshs[i].mesh.vertices.Length; j++)
				{
					if ((this.meshs[i].mesh.vertices[j] - farerFromMedian).magnitude > polarity2.magnitude)
					{
						polarity2 = this.meshs[i].mesh.vertices[j] - farerFromMedian;
					}
				}
			}
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].GetFarerPointRecursive(ref polarity2, farerFromMedian, this);
			}
			res[0] = farerFromMedian;
			res[1] = farerFromMedian + polarity2;
			res[2] = (farerFromMedian + res[1]) / 2;
			return (res);
		}
		private void GetFarerPointRecursive(ref Vector3 current, Vector3 from, Scene root)
		{
			Matrix parentMatrix = this.getLocalToRootMatrix(root);
			for (int i = 0; i < this.meshs.Count; i++)
			{
				for (int j = 0; j < this.meshs[i].mesh.vertices.Length; j++)
				{
					Vector3 transformed = this.meshs[i].mesh.vertices[j] * parentMatrix;
					if ((transformed - from).magnitude > current.magnitude)
					{
						current = transformed - from;
					}
				}
			}
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].GetFarerPointRecursive(ref current, from, root);
			}
		}
		public override void generateIcon()
		{
			FrameBufferObject tmp = new FrameBufferObject(90, 90);
			tmp.bind();

			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();

			Camera cam = new Camera();

			Transform pivotPoint = Transform.zero;
			cam.pivotPoint = pivotPoint;
			float aspectRatio = tmp.width / (float)tmp.height;
			cam.aspectRatio = aspectRatio;
			cam.width = tmp.width;
			cam.height = tmp.height;
			cam.nearClipPlane = 1;
			cam.farClipPlane = 1000;
			cam.fov = MathHelper.PiOver4.toDeg();
			Matrix4 perspective = cam.perspectiveMatrix.gl();

			GL.MatrixMode(MatrixMode.Projection);
			GL.Viewport(0, 0, tmp.width, tmp.height);
			GL.LoadMatrix(ref perspective);
			this.render(cam);

			GL.Flush();
			tmp.unbind();
			this.icon = tmp.texture;
			return;
		}
		private void render(Camera cam)
		{
			//Debug.Log("OK");
			Vector3[] polarities = this.GetPolarities();
			OpenTK.Matrix4 m = Transform.zero.transformation.gl();
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			GL.Light(LightName.Light0, LightParameter.Diffuse, mat_specular);
			GL.Light(LightName.Light0, LightParameter.Specular, mat_specular);
			GL.LoadIdentity();
			Matrix4 lookat2;
			Vector3 center = polarities[2];
			Vector3 farer = polarities[0];
			Vector3 farer2 = polarities[1];
			Vector3 cross = Vector3.Cross(farer - center, Vector3.one).normalized;
			float dist = (farer - center).magnitude / (float)Math.Tan(cam.fov.toRad() / 2);
			Transform cameraTransform = new Transform(center + (dist * cross), Quaternion.identity, Vector3.one);
			cameraTransform.LookAt(center, Vector3.up);
			lookat2 = cameraTransform.worldToLocalMatrix.gl();
			lookat2.Transpose();
			GL.LoadMatrix(ref lookat2);
			GL.ClearColor(Color.CadetBlue.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.FrontFace(FrontFaceDirection.Ccw);
			GL.LoadIdentity();
			GL.LoadMatrix(ref lookat2);
			m.Transpose();
			GL.Disable(EnableCap.LineStipple);
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			Material current = null;
			foreach (MeshInfo myFilter in this.meshs)
			{
				Goliat.Mesh myMesh = myFilter.mesh;
				Assimp.Mesh mesh = myMesh.importVersion;
				if (myFilter.materials.Count > 0)
				{
					myFilter.materials[0].Bind();
				}

				if (myMesh.hasNormals)
				{
					GL.Enable(EnableCap.Lighting);
				}
				else
				{
					GL.Disable(EnableCap.Lighting);
				}

				/*bool hasColors = mesh.HasVertexColors(0);
				if (myMesh.hasColors)
				{
					GL.Enable(EnableCap.ColorMaterial);
				}
				else
				{
					GL.Disable(EnableCap.ColorMaterial);
				}*/


				//bool hasTexCoords = myMesh.hasTextureCoords(0);

				foreach (Face face in myMesh.faces)
				{
					if (current != myFilter.materials[face.materialIndex])
					{
						current = myFilter.materials[face.materialIndex];
						current.Bind();
					}
					//Debug.Log("found face");
					BeginMode faceMode;
					switch (face.indices.Count())
					{
						case 1:
							faceMode = BeginMode.Points;
							break;
						case 2:
							faceMode = BeginMode.Lines;
							break;
						case 3:
							faceMode = BeginMode.Triangles;
							break;
						default:
							faceMode = BeginMode.Polygon;
							break;
					}


					GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);

					for (int i = 0; i < face.indices.Count(); i++)
					{
						int indice = face.indices[i];
						GL.Color4(Color.LightGray.gl());
						/*if (myMesh.hasColors)
						{
							Color4 vertColor = FromIntColor(0);
							if (hasColors)
								vertColor = FromColor(mesh.VertexColorChannels[0][indice]);

							GL.Color4(vertColor);
						}*/
						if (myMesh.hasNormals)
						{
							if (myMesh.normals != null)
							{
								OpenTK.Vector3 normal = myMesh.normals[indice].gl();
								GL.Normal3(normal);
							}
						}
						if (myMesh.hasTextureCoords)
						{
							OpenTK.Vector2 uv = myMesh.uvs[indice].gl();
							GL.TexCoord2(uv.X, uv.Y);
						}
						OpenTK.Vector3 pos = myMesh.vertices[indice].gl();
						GL.Vertex3(pos);
					}

					GL.End();
				}

			}
			if (this.children != null)
			{
				for (int i = 0; i < this.children.Count; i++)
				{

					this.children[i].renderRecursive();
				}
			}

		}

		private void renderRecursive()
		{
			Matrix4 m = this.getTransformation().gl();
			m.Transpose();
			GL.PushMatrix();
			GL.MultMatrix(ref m);
			if (this.meshs.Count > 0)
			{
				foreach (MeshInfo myFilter in this.meshs)
				{
					Material current = null;
					Goliat.Mesh myMesh = myFilter.mesh;
					Assimp.Mesh mesh = myMesh.importVersion;
					if (myFilter.materials.Count > 0)
					{
						myFilter.materials[0].Bind();
					}

					if (myMesh.hasNormals)
					{
						GL.Enable(EnableCap.Lighting);
					}
					else
					{
						GL.Disable(EnableCap.Lighting);
					}

					/*bool hasColors = mesh.HasVertexColors(0);
					if (myMesh.hasColors)
					{
						GL.Enable(EnableCap.ColorMaterial);
					}
					else
					{
						GL.Disable(EnableCap.ColorMaterial);
					}*/


					//bool hasTexCoords = myMesh.hasTextureCoords(0);

					foreach (Face face in myMesh.faces)
					{
						if (current != myFilter.materials[face.materialIndex])
						{
							current = myFilter.materials[face.materialIndex];
							current.Bind();
						}
						//Debug.Log("found face");
						BeginMode faceMode;
						switch (face.indices.Count())
						{
							case 1:
								faceMode = BeginMode.Points;
								break;
							case 2:
								faceMode = BeginMode.Lines;
								break;
							case 3:
								faceMode = BeginMode.Triangles;
								break;
							default:
								faceMode = BeginMode.Polygon;
								break;
						}


						GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);

						for (int i = 0; i < face.indices.Count(); i++)
						{
							int indice = face.indices[i];
							GL.Color4(Color.LightGray.gl());
							if (myMesh.hasNormals)
							{
								if (myMesh.normals != null)
								{
									OpenTK.Vector3 normal = myMesh.normals[indice].gl();
									GL.Normal3(normal);
								}
							}
							if (myMesh.hasTextureCoords)
							{
								OpenTK.Vector2 uv = myMesh.uvs[indice].gl();
								GL.TexCoord2(uv.X, uv.Y);
							}
							OpenTK.Vector3 pos = myMesh.vertices[indice].gl();
							GL.Vertex3(pos);
						}

						GL.End();
					}

				}

			}
			if (this.children != null)
			{
				for (int i = 0; i < this.children.Count; i++)
				{
					this.children[i].renderRecursive();
				}
			}
			GL.PopMatrix();
		}
		public Scene getRoot()
		{
			Scene res = this;
			while (res.parent != null)
			{
				res = res.parent;
			}
			return (res);
		}
		private string filePath;
		public Scene(string filePath)
		{
			Assimp.Scene m_model;
			this.filePath = filePath;
			try
			{
				m_model = importer.ImportFile(filePath, Assimp.PostProcessPreset.TargetRealTimeMaximumQuality);
			}
			catch (Exception e)
			{
				Console.WriteLine("empty scene");
				return;
			}
			Assimp.Node current = m_model.RootNode;
			this.name = current.Name;
			Assimp.Vector3D scaling;
			Assimp.Vector3D translation;
			Assimp.Quaternion rotation;

			current.Transform.Decompose(out scaling, out rotation, out translation);
			this.localRotation = new Goliat.Quaternion(rotation);
			this.localPosition = new Goliat.Vector3(translation);
			this.localScale = new Goliat.Vector3(scaling);
			if (m_model.AnimationCount > 0)
			{
				this.animation = new AnimationInfo(m_model);
			}
			if (current.Children != null)
			{
				for (int i = 0; i < current.Children.Count; i++)
				{
					Scene tmp = new Scene(i);
					appendNodeRecursive(current.Children[i], this, m_model, i);
				}
			}
			if (this.animation != null)
			{
				/*
				 * pour le traficotage:
				 * 
				 * le blem: 
				 * maintenant on itere sur chaque assigned, mais deux assigned peuvent avoir le meme mesh,
				 * ce qu'il faut, c 'est soit les reunir quand c'est le cas, soit gerer leur subMeshIndice sur le tas.
				 * 
				 * 
				 */
				Console.WriteLine("scn01");
				for (int i = 0; i < this.animation.mappedBones.Count; i++) // ici pour le traficotage:
				{
					SceneBone bone = this.animation.mappedBones[i];
					List<BoneInfo> assigned = new List<BoneInfo>();
					for (int j = 0; j < this.animation.assimpBones.Count; j++)
					{
						if (this.animation.assimpBones[j].name == this.animation.mappedBones[i].name)
						{
							assigned.Add(this.animation.assimpBones[j]);
						}
					}
					for (int j = 0; j < assigned.Count; j++)
					{

						int subMeshOffset = assigned[j].affectedMesh.mesh.getSubMeshOffset(assigned[j].subMeshIndice);
						bone.weights.AddOrRetrieve(assigned[j].affectedMesh, new List<VertexWeight>());
						// suppose qu'on puisse de l exterieur de mesh savoir l'emplacement de depart d'un submesh sinon ça va pas.
						for (int k = 0; k < assigned[j].bone.VertexWeightCount; k++)
						{

							bone.weights[assigned[j].affectedMesh].Add(new VertexWeight(assigned[j].bone.VertexWeights[k].VertexID + subMeshOffset, assigned[j].bone.VertexWeights[k].Weight));
						}
						//bone.offsetMatrixs.AddOrReplace(assigned[j].affectedMesh, assigned[j].affectedMesh.sceneNode.getLocalToRootMatrix(this) * bone.getLocalToRootMatrix(this).inverse);
						bone.offsetMatrixs.AddOrReplace(assigned[j].affectedMesh, new Matrix(assigned[j].bone.OffsetMatrix));
						/*
						 * 
						 * 
						 * pour débug:
						 * 1 : test avec la conversion de leur offsetmatrix directement.
						 *  
						 * 2 : verifier l'ordre, ici et aussi du coté du rendu.
						 * 
						 * 3 dés que 2 ok : verifier que c'est bien fait les localtoroot, et que c'est vraiment localtoroot qu'il faut.
						 * 
						 */
					}
				}
				Console.WriteLine("scn1");
			}
			
		}
		public Transform generateTransform()
		{
			return (new Transform(this));
		}
		private static Scene appendAsset(Assimp.Scene model)
		{
			Assimp.Node current = model.RootNode;
			return (appendNodeRecursive(current, null, model, 0));
		}

		private static Scene AppendAsset(MappedRessourceInfo info)
		{
			if (File.Exists(info.filePath))
			{
				Assimp.Scene m_model = importer.ImportFile(info.filePath, Assimp.PostProcessPreset.TargetRealTimeMaximumQuality);
				return (appendAsset(m_model, info));
			}
			else
			{
				Debug.Log("mince");
			}
			return (null);
		}
		public override void Dispose()
		{
			base.Dispose();
			for (int i = 0; i < this.children.Count; i++)
			{
				this.children[i].Dispose();
			}
			for (int i = 0; i < this.meshs.Count; i++)
			{
				this.meshs[i].Dispose();
			}
		}
		private static Scene appendAsset(Assimp.Scene model, MappedRessourceInfo info)
		{
			Assimp.Node current = model.RootNode;
			return (appendNodeRecursive(current, null, model, info, 0));
		}

		public Scene(MappedRessourceInfo info, int sibling)
			: base(info)
		{
			if (info.parent == null)
			{
				this.filePath = info.filePath;
				if (File.Exists(info.filePath))
				{

					Assimp.Scene m_model = importer.ImportFile(info.filePath, Assimp.PostProcessPreset.TargetRealTimeMaximumQuality);
					for (int i = 0; i < m_model.AnimationCount; i++)
					{
						AnimationClip clip = new AnimationClip(m_model.Animations[i], m_model);
						Assimp.NodeAnimationChannel channel = m_model.Animations[i].FindNodeAnim(m_model.RootNode);
					}
					Assimp.Node current = m_model.RootNode;
					this.name = current.Name;
					Assimp.Vector3D scaling;
					Assimp.Vector3D translation;
					Assimp.Quaternion rotation;

					current.Transform.Decompose(out scaling, out rotation, out translation);
					this.localRotation = new Goliat.Quaternion(rotation);
					this.localPosition = new Goliat.Vector3(translation);
					this.localScale = new Goliat.Vector3(scaling);
					if (m_model.AnimationCount > 0)
					{
						this.animation = new AnimationInfo(m_model, info);
					}
					if (current.Children != null)
					{
						for (int i = 0; i < current.Children.Count; i++)
						{
							appendNodeRecursive(current.Children[i], this, m_model, info.sons[i], 0);
						}
					}

					if (this.animation != null)
					{
						for (int i = 0; i < this.animation.mappedBones.Count; i++)
						{
							SceneBone bone = this.animation.mappedBones[i];
							List<BoneInfo> assigned = new List<BoneInfo>();
							for (int j = 0; j < this.animation.assimpBones.Count; j++)
							{
								if (this.animation.assimpBones[j].name == this.animation.mappedBones[i].name)
								{
									assigned.Add(this.animation.assimpBones[j]);
								}
							}
							for (int j = 0; j < assigned.Count; j++)
							{
								int subMeshOffset = assigned[j].affectedMesh.mesh.getSubMeshOffset(assigned[j].subMeshIndice);
								bone.weights.AddOrRetrieve(assigned[j].affectedMesh, new List<VertexWeight>());
								for (int k = 0; k < assigned[j].bone.VertexWeightCount; k++)
								{
									bone.weights[assigned[j].affectedMesh].Add(new VertexWeight(assigned[j].bone.VertexWeights[k].VertexID + subMeshOffset, assigned[j].bone.VertexWeights[k].Weight));
								}
								assigned[j].bone.OffsetMatrix.Decompose(out scaling, out rotation, out translation);
								Matrix transl = Matrix.translation(translation.octogon());
								Matrix rot = new Matrix(rotation.octogon());
								Matrix sc = Matrix.scale(scaling.octogon());
								Matrix transformation = transl * rot * sc;
								//Console.WriteLine("hey MATRIX HEREEEE!!!:" + assigned[j].bone.OffsetMatrix);
								bone.offsetMatrixs.AddOrReplace(assigned[j].affectedMesh, transformation);//new Matrix(assigned[j].bone.OffsetMatrix));
								//bone.offsetMatrixs.AddOrReplace(assigned[j].affectedMesh, new Matrix(assigned[j].bone.OffsetMatrix));

								
							}
						}
					}
				}
				else
				{
					;
				}
			}

		}

		private static Scene getScene(Assimp.Node node, Scene parent, Assimp.Scene m_model, MappedRessourceInfo info, int sibling)
		{
			Scene root = parent.getRoot();
			Scene tr;// = new Scene(info);
			bool isBone = false;
			if (root.animation != null)
			{
				for (int i = 0; i < root.animation.clips.Count; i++)
				{
					if (root.animation.clips[i].affectedBones.Contains(node.Name))
					{
						isBone = true;
					}
				}
			}
			if (isBone)
			{
				tr = new SceneBone(info, sibling);
				root.animation.mappedBones.Add((SceneBone)tr);
			}
			else
			{
				tr = new Scene(info, sibling);
			}
			tr.parent = parent;
			parent.children.Add(tr);
			tr.name = node.Name;
			Assimp.Vector3D scaling;
			Assimp.Vector3D translation;
			Assimp.Quaternion rotation;

			node.Transform.Decompose(out scaling, out rotation, out translation);
			tr.localRotation = new Goliat.Quaternion(rotation);
			tr.localPosition = new Goliat.Vector3(translation);
			tr.localScale = new Goliat.Vector3(scaling);
			if (node.MeshIndices.Count > 0)
			{
				MeshInfo tmp = new MeshInfo(tr);
				tr.meshs.Add(tmp);
				tmp.mesh = new Mesh(info.meshs[0]);
				tmp.materials = new List<Goliat.Material>();
				for (int i = 0; i < node.MeshIndices.Count; i++)
				{

					Assimp.Mesh mesh = m_model.Meshes[node.MeshIndices[i]];
					if (mesh.BoneCount > 0)
					{
						for (int j = 0; j < mesh.BoneCount; j++)
						{
							if (!root.animation.boneMeshs.ContainsKey(mesh.Bones[j].Name))
							{
								root.animation.boneMeshs.Add(mesh.Bones[j].Name, new List<Mesh>());
							}
							tmp.isSkinned = true;
							root.animation.boneMeshs[mesh.Bones[j].Name].Add(tmp.mesh);
							root.animation.assimpBones.Add(new BoneInfo(tmp, mesh.Bones[j], tr, i));
						}
					}
					Assimp.Material mat = m_model.Materials[mesh.MaterialIndex];
					for (int j = 0; j < mat.GetMaterialTextures(TextureType.Diffuse).Length; j++)
					{
						Assimp.TextureSlot test;
						mat.GetMaterialTexture(TextureType.Diffuse, j, out test);
						Material material = new Goliat.Material(mat, j);
						string parentPath = tr.getRoot().filePath;
						if (test.FilePath != "" && test.FilePath != null && parentPath != "" && parentPath != null)
						{
							string texturePath = Path.Combine(Path.GetDirectoryName(parentPath), Path.GetFileName(test.FilePath));
							if (File.Exists(texturePath))
							{
								string metaFile = texturePath + ".meta";
								MappedRessourceInfo info2;
								if (!File.Exists(metaFile))
								{
									info2 = new MappedRessourceInfo(texturePath);
								}
								else
								{
									info2 = (MappedRessourceInfo)(Serializer.UnSerialize(metaFile));
								}
								info2 = MappedRessourceInfo.RessourceMap[info2.guid];
								material.textures[0] = (Texture)info2.Regenerate();
							}
						}
						tmp.materials.Add(material);
					}
					if (mat.GetMaterialTextures(TextureType.Diffuse).Length == 0)
					{
						int j = 0;

						Assimp.TextureSlot test;
						mat.GetMaterialTexture(TextureType.Diffuse, j, out test);
						Material material = new Goliat.Material(mat, j);
						string parentPath = tr.getRoot().filePath;
						if (test.FilePath != "" && test.FilePath != null && parentPath != "" && parentPath != null)
						{
							string texturePath = Path.Combine(Path.GetDirectoryName(parentPath), Path.GetFileName(test.FilePath));
							if (File.Exists(texturePath))
							{
								string metaFile = texturePath + ".meta";
								MappedRessourceInfo info2;
								if (!File.Exists(metaFile))
								{
									info2 = new MappedRessourceInfo(texturePath);
								}
								else
								{
									info2 = (MappedRessourceInfo)(Serializer.UnSerialize(metaFile));
								}
								info2 = MappedRessourceInfo.RessourceMap[info2.guid];
								material.textures[0] = (Texture)info2.Regenerate();
							}
						}
						tmp.materials.Add(material);
					}

					if (tmp.mesh == null)
						tmp.mesh = new Mesh(info.meshs[0]);
					tmp.mesh.Assimp_Import(m_model.Meshes[node.MeshIndices[i]], m_model, true);
				}
			}
			return (tr);
		}
		private static Scene appendNodeRecursive(Assimp.Node node, Scene current, Assimp.Scene m_model, MappedRessourceInfo info, int sibling)
		{
			Scene tr = getScene(node, current, m_model, info, sibling);
			Scene children = null;
			if (current == null)
				children = tr;
			else
				children = current.children[current.children.Count - 1];
			if (node.Children != null)
			{
				for (int i = 0; i < node.Children.Count; i++)
				{
					appendNodeRecursive(node.Children[i], children, m_model, info.sons[i], sibling);
				}
			}
			return (tr);
		}

		/*private static Scene appendNodeRecursive(Goliat.Node node, Scene current, Goliat.Node m_model, int sibling)
		{

		}*/

		private static Scene appendNodeRecursive(Assimp.Node node, Scene current, Assimp.Scene m_model, int sibling)
		{
			Scene tr = getScene(node, current, m_model, sibling);
			Scene children = null;
			if (current == null)
				children = tr;
			else
			{
				children = current.children[current.children.Count - 1];
			}
			if (node.Children != null)
			{
				for (int i = 0; i < node.Children.Count; i++)
				{
					appendNodeRecursive(node.Children[i], children, m_model, i);
				}
			}
			return (tr);
		}
		public Scene(int sibling)
		{
			this.sibling = sibling;
			;
		}
		private static Scene getScene(Assimp.Node node, Scene parent, Assimp.Scene m_model, int sibling)
		{
			Scene root = parent.getRoot();
			Scene tr;
			bool isBone = false;
			if (root.animation != null)
			{
				for (int i = 0; i < root.animation.clips.Count; i++)
				{
					if (root.animation.clips[i].affectedBones.Contains(node.Name))
					{
						isBone = true;
					}
				}
			}
			if (isBone)
			{
				tr = new SceneBone(sibling);
				root.animation.mappedBones.Add((SceneBone)tr);
			}
			else
			{
				tr = new Scene(sibling);
			}
			tr.parent = parent;
			tr.name = node.Name;
			parent.children.Add(tr);
			Assimp.Vector3D scaling;
			Assimp.Vector3D translation;
			Assimp.Quaternion rotation;
			node.Transform.Decompose(out scaling, out rotation, out translation);
			tr.localRotation = new Goliat.Quaternion(rotation);
			tr.localPosition = new Goliat.Vector3(translation);
			tr.localScale = new Goliat.Vector3(scaling);
			if (node.MeshIndices.Count > 0)
			{
				MeshInfo tmp = new MeshInfo(tr);
				tmp.name = tr.name;
				tr.meshs.Add(tmp);
				tmp.mesh = new Mesh();
				tmp.materials = new List<Goliat.Material>();
				for (int i = 0; i < node.MeshIndices.Count; i++)
				{
					Assimp.Mesh mesh = m_model.Meshes[node.MeshIndices[i]];
					if (mesh.BoneCount > 0)
					{
						for (int j = 0; j < mesh.BoneCount; j++)
						{
							if (!root.animation.boneMeshs.ContainsKey(mesh.Bones[j].Name))
							{
								root.animation.boneMeshs.Add(mesh.Bones[j].Name, new List<Mesh>());
							}
							tmp.isSkinned = true;
							root.animation.boneMeshs[mesh.Bones[j].Name].Add(tmp.mesh);
							root.animation.assimpBones.Add(new BoneInfo(tmp, mesh.Bones[j], tr, i));
						}
					}
					Assimp.Material mat = m_model.Materials[mesh.MaterialIndex];
					for (int j = 0; j < mat.GetMaterialTextures(TextureType.Diffuse).Length; j++)
					{
						Assimp.TextureSlot test;
						mat.GetMaterialTexture(TextureType.Diffuse, j, out test);
						Material material = new Goliat.Material(mat, j);
						string parentPath = tr.getRoot().filePath;

						if (test.FilePath != "" && test.FilePath != null && parentPath != "" && parentPath != null)
						{
							string texturePath = Path.Combine(Path.GetDirectoryName(parentPath), Path.GetFileName(test.FilePath));
							if (File.Exists(texturePath))
							{
								string metaFile = texturePath + ".meta";
								MappedRessourceInfo info;
								if (!File.Exists(metaFile))
								{
									info = new MappedRessourceInfo(texturePath);
								}
								else
								{
									info = (MappedRessourceInfo)(Serializer.UnSerialize(metaFile));
								}
								info = MappedRessourceInfo.RessourceMap[info.guid];
								material.textures[0] = (Texture)info.Regenerate();
							}

						}
						tmp.materials.Add(material);
					}
					if (mat.GetMaterialTextures(TextureType.Diffuse).Length == 0)
					{
						int j = 0;
						Assimp.TextureSlot test;
						mat.GetMaterialTexture(TextureType.Diffuse, j, out test);
						Material material = new Goliat.Material(mat, j);
						string parentPath = tr.getRoot().filePath;

						if (test.FilePath != "" && test.FilePath != null && parentPath != "" && parentPath != null)
						{
							string texturePath = Path.Combine(Path.GetDirectoryName(parentPath), Path.GetFileName(test.FilePath));
							if (File.Exists(texturePath))
							{
								string metaFile = texturePath + ".meta";
								MappedRessourceInfo info;
								if (!File.Exists(metaFile))
								{
									info = new MappedRessourceInfo(texturePath);
								}
								else
								{
									info = (MappedRessourceInfo)(Serializer.UnSerialize(metaFile));
								}
								info = MappedRessourceInfo.RessourceMap[info.guid];
								material.textures[0] = (Texture)info.Regenerate();
							}

						}
						tmp.materials.Add(material);
					}

					if (tmp.mesh == null)
						tmp.mesh = new Mesh();
					tmp.mesh.Assimp_Import(m_model.Meshes[node.MeshIndices[i]], m_model, true);
				}
			}
			return (tr);
		}
		public void GetRessourceDictionaryRecursive(Dictionary<Type, List<SharedResource>> resources)
		{
			Dictionary<Type, List<SharedResource>> res = resources;
			Console.WriteLine("hey wtf");
			for (int i = 0; i < this.meshs.Count; i++)
			{
				res.AddOrRetrieve(typeof(Mesh), new List<SharedResource>());
				res[typeof(Mesh)].Add(this.meshs[i].mesh);
				res.AddOrRetrieve(typeof(MeshInfo), new List<SharedResource>());
				res[typeof(MeshInfo)].Add(this.meshs[i]);
				Console.WriteLine("one");
				for (int j = 0; j < this.meshs[i].materials.Count; j++)
				{
					res.AddOrRetrieve(typeof(Material), new List<SharedResource>());
					if (!res[typeof(Material)].Contains(this.meshs[i].materials[j]))
						res[typeof(Material)].Add(this.meshs[i].materials[j]);
				}
				Console.WriteLine("two");
			}
			Console.WriteLine("oh");
			if (this.GetType().IsSameOrSubclass(typeof(SceneBone)))
			{
				res.AddOrRetrieve(typeof(SceneBone), new List<SharedResource>());
				res[typeof(SceneBone)].Add(this);
			}

			res.AddOrRetrieve(typeof(Scene), new List<SharedResource>());
			res[typeof(Scene)].Add(this);
			for (int i = 0; i < this.children.Count; i++)
			{
				Console.WriteLine("????");
				this.children[i].GetRessourceDictionaryRecursive(res);
			}
		}
		public Dictionary<Type, List<SharedResource>> GetResourceDictionary()
		{

			Dictionary<Type, List<SharedResource>> res = new Dictionary<Type, List<SharedResource>>();
			this.GetRessourceDictionaryRecursive(res);
			return (res);
		}
		/// <summary>
		/// Export Transform collada style but with goliat features, and users scripts serialization attached.
		/// </summary>
		/// <param name="filePath"> Where does the exported file sould go. </param>
		/// 

		public Scene(Transform tr)
		{
			//for (int i = 0; i )
			/*
			 * 
			 * on map ça en scene,
			 * on repasse par chaque noeud, sans le ré écrire,
			 * on y ajoute un userScript_instance qui contient un userscrit et son "regeneration_pattern" pour chaque composants qui le nécessite, en se servant des
			 * ids de ressources / noeuds ou pour ces derniers d'un accés hierarchique. (ce truc pour l'instant comme avant)
			 * 
			 * 
			 * ou ata
			 * 
			 * on ajoute une reference directement au serializer d'ici, a chaque passage qqpart, et on ajoute la meme reference,
			 * 
			 * 
			 * 
			 * 
			 * 
			 */
		}

		public void ExportGoliat(Transform tr, string filePath)
		{
			/*
			 * alors : en passant partout, on mappe une premiere fois un dicitonnaire reference_obj_dechezgoliat / id_scene, os, ou autre.
			 * on repasse partout, on sérialize les scripts en xml mais avec a chaque objet goliat retrouve, un par exemple <GoliatScene source="guid_de_la_scene"></> 
			 * 
			 */
			
			//Glrp Goliat Regeneration Pattern
			Scene self = new Scene(tr);
			Dictionary<Type, List<SharedResource>> resources = this.GetResourceDictionary();// = new Dictionary<Type, SharedResource>(); // on laisse meshInfo pour une info de mesh, pour pas perdre les données qu'on a besoin. ou pas.
			Console.WriteLine("hey on a le dico");
			#region GLRP_Export
			System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings()
			{
				Indent = true,
				IndentChars = "\t",
				NewLineOnAttributes = false
			};
			using (System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(filePath, xmlWriterSettings))
			{
				writer.WriteStartDocument();
				//var ns = System.Xml.Linq.XNamespace.Get("http://www.collada.org/2005/11/COLLADASchema");
				writer.WriteStartElement("COLLADA", "http://www.collada.org/2005/11/COLLADASchema");
				//var root = new System.Xml.Linq.XElement(ns + "COLLADA", new System.Xml.Linq.XElement(ns + "a", "b"), new System.Xml.Linq.XAttribute("version", "1.4.1"));
				#region initialization
				//root.WriteTo(writer);

				//writer.WriteNode(root, true);
				//writer.WriteAttributeString("xmlns", "http://www.collada.org/2005/11/COLLADASchema", null);
				writer.WriteAttributeString("version", "1.4.1");
				writer.WriteStartElement("asset");
				writer.WriteStartElement("unit");
				writer.WriteAttributeString("meter", "1");
				writer.WriteAttributeString("name", "meter");
				writer.WriteEndElement(); //unit
				writer.WriteElementString("up_axis", "Y_UP");
				writer.WriteEndElement(); //asset
				#endregion

				writer.WriteStartElement("library_images");
				#region library_images
				if (resources.ContainsKey(typeof(Texture)))
				{
					foreach (Texture texture in resources[typeof(Texture)])
					{
						writer.WriteStartElement("image");
						writer.WriteAttributeString("id", texture.guid);
						writer.WriteAttributeString("name", texture.name);
						writer.WriteElementString("init_from", texture.filePath);
						writer.WriteEndElement();
					}
				}
				writer.WriteEndElement();
				#endregion

				writer.WriteStartElement("library_effects");
				#region library_effects
				if (resources.ContainsKey(typeof(Material)))
				{
					foreach (Material mat in resources[typeof(Material)])
					{
						writer.WriteStartElement("image");

						writer.WriteAttributeString("id", mat.guid + "-fx");
						writer.WriteAttributeString("name", mat.name);


						writer.WriteStartElement("profile_COMMON");

						writer.WriteStartElement("technique");

						writer.WriteAttributeString("sid", "standard");
						writer.WriteStartElement("phong");

						writer.WriteStartElement("emission");
						if (mat.emissiveMap != null)
						{
							writer.WriteStartElement("texture");
							writer.WriteAttributeString("texture", mat.emissiveMap.guid);
							writer.WriteEndElement();
						}
						else
							writer.WriteElementString("color", mat.emissiveColor.ToDaeString());
						writer.WriteEndElement(); //emission


						writer.WriteStartElement("ambient");
						writer.WriteElementString("color", mat.ambientColor.ToDaeString());
						writer.WriteEndElement(); //ambient

						writer.WriteStartElement("diffuse");
						if (mat.diffuseMap != null)
						{
							writer.WriteStartElement("texture");
							writer.WriteAttributeString("texture", mat.diffuseMap.guid);
							writer.WriteEndElement();
						}
						else
							writer.WriteElementString("color", mat.diffuseColor.ToDaeString());
						writer.WriteEndElement(); //diffuse

						writer.WriteStartElement("specular");
						if (mat.specularMap != null)
						{
							writer.WriteStartElement("texture");
							writer.WriteAttributeString("texture", mat.specularMap.guid);
							writer.WriteEndElement();
						}
						else
							writer.WriteElementString("color", mat.specularColor.ToDaeString());
						writer.WriteEndElement(); //specular

						writer.WriteEndElement(); // phong

						writer.WriteEndElement(); // tech

						writer.WriteEndElement(); // prof

						writer.WriteEndElement(); // image
					}
				}
				writer.WriteEndElement();
				#endregion

				writer.WriteStartElement("library_materials");
				#region library_materials
				if (resources.ContainsKey(typeof(Material)))
				{
					foreach (Material mat in resources[typeof(Material)])
					{
						writer.WriteStartElement("material");
						writer.WriteAttributeString("id", mat.guid);
						writer.WriteAttributeString("name", mat.name + mat.guid);

						writer.WriteStartElement("instance_effect");
						writer.WriteAttributeString("url", "#" + mat.guid + "-fx");
						writer.WriteEndElement();//instance_effect

						writer.WriteEndElement();//material
					}
				}
				writer.WriteEndElement();
				#endregion

				writer.WriteStartElement("library_geometries");
				#region library_geometries
				if (resources.ContainsKey(typeof(MeshInfo)))
				{
					foreach (MeshInfo meshInfo in resources[typeof(MeshInfo)])
					{

						Mesh mesh = meshInfo.mesh;
						writer.WriteStartElement("geometry");

						writer.WriteAttributeString("id", mesh.guid);
						writer.WriteAttributeString("name", mesh.name);
						writer.WriteStartElement("mesh");
						#region positions
						///// POSITIONS //////
						writer.WriteStartElement("source");
						writer.WriteAttributeString("id", mesh.guid + "-positions");
						writer.WriteStartElement("float_array");
						writer.WriteAttributeString("id", mesh.guid + "-positions" + "-array");
						int len = mesh.vertices.Length * 3;
						writer.WriteAttributeString("count", "" + len);

						StringBuilder sb = new StringBuilder();
						for (int i = 0; i < mesh.vertices.Length; i++)
						{

							sb.Append(mesh.vertices[i].x);
							sb.Append(" ");
							sb.Append(mesh.vertices[i].y);
							sb.Append(" ");
							sb.Append(mesh.vertices[i].z);
							if (i + 1 < mesh.vertices.Length)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());

						writer.WriteEndElement(); // float_array

						writer.WriteStartElement("technique_common");
						writer.WriteStartElement("accessor");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-positions" + "-array");
						writer.WriteAttributeString("count", mesh.vertices.Length.ToString());
						writer.WriteAttributeString("stride", "3");


						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "X");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Y");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Z");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param


						writer.WriteEndElement();//accessor
						writer.WriteEndElement();//technique_common

						writer.WriteEndElement(); // source0
						#endregion
						#region normals
						////// NORMALS //////

						writer.WriteStartElement("source");
						writer.WriteAttributeString("id", mesh.guid + "-normals");
						writer.WriteStartElement("float_array");
						writer.WriteAttributeString("id", mesh.guid + "-normals" + "-array");
						len = mesh.normals.Length * 3;
						writer.WriteAttributeString("count", "" + len);

						sb = new StringBuilder();
						for (int i = 0; i < mesh.normals.Length; i++)
						{

							sb.Append(mesh.normals[i].x);
							sb.Append(" ");
							sb.Append(mesh.normals[i].y);
							sb.Append(" ");
							sb.Append(mesh.normals[i].z);
							if (i + 1 < mesh.vertices.Length)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());

						writer.WriteEndElement(); // float_array

						writer.WriteStartElement("technique_common");
						writer.WriteStartElement("accessor");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-normals" + "-array");
						writer.WriteAttributeString("count", mesh.normals.Length.ToString());
						writer.WriteAttributeString("stride", "3");


						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "X");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Y");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Z");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param


						writer.WriteEndElement();//accessor
						writer.WriteEndElement();//technique_common

						writer.WriteEndElement(); // source0
						#endregion
						#region uvs
						////// MAP0 //////

						writer.WriteStartElement("source");
						writer.WriteAttributeString("id", mesh.guid + "-uvs");
						writer.WriteStartElement("float_array");
						writer.WriteAttributeString("id", mesh.guid + "-uvs" + "-array");
						len = mesh.uvs.Length * 2;
						writer.WriteAttributeString("count", "" + len);

						sb = new StringBuilder();
						for (int i = 0; i < mesh.uvs.Length; i++)
						{

							sb.Append(mesh.uvs[i].x);
							sb.Append(" ");
							sb.Append(mesh.uvs[i].y);
							if (i + 1 < mesh.vertices.Length)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());

						writer.WriteEndElement(); // float_array

						writer.WriteStartElement("technique_common");
						writer.WriteStartElement("accessor");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-uvs" + "-array");
						writer.WriteAttributeString("count", mesh.uvs.Length.ToString());
						writer.WriteAttributeString("stride", "2");


						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "S");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "T");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param


						writer.WriteEndElement();//accessor
						writer.WriteEndElement();//technique_common

						writer.WriteEndElement(); // source0
						#endregion
						#region vertices
						writer.WriteStartElement("vertices");
						writer.WriteAttributeString("id", mesh.guid + "-vertices");
						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "POSITION");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-positions");

						writer.WriteEndElement();//input
						writer.WriteEndElement();//vertices
						#endregion
						#region polylist
						//deux problèmes possibles pr l instant : triés en sens inverse, pas counterclockwise, ou pas clockwise.
						/*
						 * ou bien le matériaux qu'on a pas cité.
						 * 
						 */
						// 

						/*
						 * une source de recherche : j'ai beaucoup plus de vertices, peut etre forcer assimp a n'importer que l'essentiel sans trafiquer le truc.
						 * 
						 */
						writer.WriteStartElement("polylist");
						writer.WriteAttributeString("count", "" + (mesh.faces.Length));
						writer.WriteAttributeString("material", meshInfo.materials[0].name + meshInfo.materials[0].guid);
						#region inputs
						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "VERTEX");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-vertices");
						writer.WriteAttributeString("offset", "" + 0);
						writer.WriteEndElement();//input


						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "NORMAL");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-normals");
						writer.WriteAttributeString("offset", "" + 1);
						writer.WriteEndElement();//input


						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "TEXCOORD");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-uvs");
						writer.WriteAttributeString("offset", "" + 2);
						writer.WriteAttributeString("set", "0");
						writer.WriteEndElement();//input
						#endregion
						#region vcount
						writer.WriteStartElement("vcount");
						len = mesh.faces.Length;
						sb = new StringBuilder();

						for (int i = 0; i < len; i++)
						{
							sb.Append(mesh.faces[i].indices.Length);
							if (i + 1 < len)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());
						sb.Clear();
						writer.WriteEndElement(); //vcount
						#endregion
						#region polygons
						writer.WriteStartElement("p");
						for (int i = 0; i < len; i++)
						{
							for (int j = 0; j < mesh.faces[i].indices.Length; j++)
							{
								sb.Append(mesh.faces[i].indices[j]);
								sb.Append(" ");
								sb.Append(mesh.faces[i].indices[j]);
								sb.Append(" ");
								sb.Append(mesh.faces[i].indices[j]);
								if (j + 1 < mesh.faces[i].indices.Length)
								{
									sb.Append(" ");
								}
							}
							if (i + 1 < len)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());
						writer.WriteEndElement();//p
						#endregion

						writer.WriteEndElement(); //polylist
						#endregion

						writer.WriteEndElement(); //mesh

						writer.WriteEndElement();// geometry
					}
				}
				writer.WriteEndElement(); //geometries
				#endregion //atention ici faut gerer que ce soit des meshinfo d utilisés plutot.- //attention faudra gerer des meshinfo a la place

				writer.WriteStartElement("library_controllers");
				#region library_controllers
				if (resources.ContainsKey(typeof(MeshInfo)))
				{
					foreach (MeshInfo info in resources[typeof(MeshInfo)])
					{
						if (info.isSkinned)
						{
							writer.WriteStartElement("controller");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller");
							writer.WriteStartElement("skin");
							writer.WriteAttributeString("source", info.mesh.guid);
							writer.WriteStartElement("bind_shape_matrix");
							writer.WriteString(info.sceneNode.getTransformation().inverse.array.ToDAEString());
							writer.WriteEndElement(); //bind_shape_matrix
							writer.WriteStartElement("source");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-joints");
							List<int>[] weights = new List<int>[info.mesh.vertices.Length];
							List<int>[] bones = new List<int>[info.mesh.vertices.Length];
							List<Matrix> offsets = new List<Matrix>();
							List<float> orderedWeights = new List<float>();
							if (resources.ContainsKey(typeof(SceneBone)))
							{
								int i = 0;
								int j = 0;
								writer.WriteStartElement("Name_array");
								writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-joints" + "-array");
								StringBuilder sb = new StringBuilder();
								foreach (SceneBone bone in resources[typeof(SceneBone)])
								{
									if (bone.weights.ContainsKey(info))
									{
										foreach (VertexWeight weight in bone.weights[info])
										{
											if (weights[weight.index] == null)
											{
												weights[weight.index] = new List<int>();
												bones[weight.index] = new List<int>();
											}
											weights[weight.index].Add(j);
											bones[weight.index].Add(i);
											orderedWeights.Add(weight.weight);
											j++;
										}
										offsets.Add(bone.offsetMatrixs[info]);
										sb.Append(bone.name);
										i++;
									}
								}
								writer.WriteAttributeString("count", "" + i);
								writer.WriteString(sb.ToString());
								writer.WriteEndElement(); //Name_array
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-joints" + "-array");
								writer.WriteAttributeString("count", "" + i);
								writer.WriteStartElement("param");
								writer.WriteAttributeString("type", "name");
								writer.WriteEndElement(); // param
								writer.WriteEndElement(); //accessor
								writer.WriteEndElement(); //technique_common
							}
							//							foreach (SceneBone bone in )
							writer.WriteEndElement(); //source0
							writer.WriteStartElement("source");//1
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-matrices");
							writer.WriteStartElement("float_array");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-matrices" + "-array");
							writer.WriteAttributeString("count", "" + offsets.Count);
							StringBuilder sb1 = new StringBuilder();
							for (int i = 0; i < offsets.Count; i++)
							{
								sb1.Append(offsets[i].array.ToDAEString());
								if (i + 1 < offsets.Count)
								{
									sb1.Append("\n");
								}
							}
							writer.WriteString(sb1.ToString());
							writer.WriteEndElement(); //float_array

							writer.WriteStartElement("technique_common");
							writer.WriteStartElement("accessor");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-matrices" + "-array");
							writer.WriteAttributeString("count", "" + offsets.Count);
							writer.WriteAttributeString("stride", "16");
							writer.WriteStartElement("param");
							writer.WriteAttributeString("type", "float4x4");
							writer.WriteEndElement(); //param

							writer.WriteEndElement(); //accessor
							writer.WriteEndElement(); // technique_common



							writer.WriteEndElement(); //source1

							writer.WriteStartElement("source"); //2
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-weights");
							writer.WriteStartElement("float_array");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-weights" + "-array");
							writer.WriteAttributeString("count", "" + orderedWeights.Count);
							writer.WriteString("" + orderedWeights.ToArray().ToDAEString());
							writer.WriteEndElement(); // float_array

							writer.WriteStartElement("technique_common");
							writer.WriteStartElement("accessor");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-weights" + "-array");
							writer.WriteAttributeString("count", "" + orderedWeights.Count);
							writer.WriteStartElement("param");
							writer.WriteAttributeString("type", "float");
							writer.WriteEndElement(); //param

							writer.WriteEndElement(); //accessor
							writer.WriteEndElement(); // technique_common
							writer.WriteEndElement(); //source 2
							writer.WriteStartElement("joints");

							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "JOINT");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-joints");
							writer.WriteEndElement(); //input

							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "INV_BIND_MATRIX");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-matrices");
							writer.WriteEndElement(); //input
							writer.WriteEndElement();//joints
							writer.WriteStartElement("vertex_weights");
							writer.WriteAttributeString("count", "" + weights.Length);
							int[] lengths = new int[weights.Length];
							for (int i = 0; i < weights.Length; i++)
							{
								lengths[i] = weights[i].Count;
							}
							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "JOINT");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-joints");
							writer.WriteAttributeString("offset", "0");
							writer.WriteEndElement(); //input

							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "WEIGHT");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-weights");
							writer.WriteAttributeString("offset", "1");
							writer.WriteEndElement(); //input

							writer.WriteStartElement("vcount");
							writer.WriteString("" + lengths.ToDAEString());
							writer.WriteEndElement(); //vcount
							writer.WriteStartElement("v");
							StringBuilder sb3 = new StringBuilder();
							for (int i = 0; i < weights.Length; i++)
							{
								for (int j = 0; j < lengths[i]; j++)
								{
									sb3.Append("" + bones[i][j] + " " + weights[i][j]);
									if (j + 1 < lengths[i])
										sb3.Append(" ");
								}
								if (i + 1 < weights.Length)
								{
									sb3.Append(" ");
								}
							}
							writer.WriteString(sb3.ToString());
							writer.WriteEndElement(); //v
							writer.WriteEndElement(); //vertex_weights

							writer.WriteEndElement(); // skin
							writer.WriteEndElement(); // controller.
						}
					}
				}

				writer.WriteEndElement(); // library_controllers.
				#endregion
				if (this.getRoot() != null && this.getRoot().animation != null)
				{
					writer.WriteStartElement("library_animations");

					#region library_animations
					AnimationInfo anim = this.getRoot().animation;
					float[] startTimes = new float[anim.clips.Count];
					float[] endTimes = new float[anim.clips.Count];
					float totalTime = 0;
					for (int i = 0; i < anim.clips.Count; i++)
					{
						if (i > 0)
						{
							totalTime += anim.clips[i - 1].length;
							startTimes[i] = totalTime;
							endTimes[i] = totalTime + anim.clips[i].length;
						}
						else
						{
							startTimes[i] = totalTime;
							endTimes[i] = anim.clips[i].length;
						}
					}
					if (resources.ContainsKey(typeof(SceneBone)))
					{
						for (int i = 0; i < anim.clips.Count; i++)
						{
							float timeOffset = startTimes[i];
							StringBuilder sb = new StringBuilder();
							foreach (SceneBone bone in resources[typeof(SceneBone)])
							{
								#region build_bone_dae_compatible_keys
								#region register_each_available_times
								List<PositionKey> positionKeys = anim.clips[i].animationDictionary[bone.name].PositionKeys;
								List<ScaleKey> scaleKeys = anim.clips[i].animationDictionary[bone.name].ScaleKeys;
								List<RotationKey> rotationKeys = anim.clips[i].animationDictionary[bone.name].RotationKeys;

								//anim.clips[i].animationDictionary[bone.name].
								// un on check que les trois ont des temps identiques et un nombre de frames identiques.
								// deux, si c est le cas on export en faisant des transformations pour chaque, sinon bah on doit faire des putains d'animation step

								/*
								 * dae file format only accept matrixs as animations outputs, but some file formats accepts separated keys (location keys, rotation keys, scale keys), so we interpolate if necessary.
								 * 
								 * Here we register keys by time, to check later if a time contains only loc/ only rot /only scale, and convert it to interpolated matrix.
								 * 
								 */
								Dictionary<float, LocRotScale> finalKeys = new Dictionary<float, LocRotScale>();

								for (int k = 0; k < positionKeys.Count; k++)
								{
									finalKeys.AddOrRetrieve(positionKeys[k].timeOffset, new LocRotScale());
									finalKeys[positionKeys[k].timeOffset].loc = positionKeys[k].key;

								}

								for (int k = 0; k < rotationKeys.Count; k++)
								{
									finalKeys.AddOrRetrieve(rotationKeys[k].timeOffset, new LocRotScale());
									finalKeys[rotationKeys[k].timeOffset].rot = rotationKeys[k].key;
								}

								for (int k = 0; k < positionKeys.Count; k++)
								{
									finalKeys.AddOrRetrieve(positionKeys[k].timeOffset, new LocRotScale());
									finalKeys[positionKeys[k].timeOffset].scale = scaleKeys[k].key;
								}
								#endregion
								#region sort_and_convert
								List<LocRotScale> locRotScales = new List<LocRotScale>();
								//List<AnimationStep> steps = new List<AnimationStep>();
								foreach (float key in finalKeys.Keys)
								{
									#region convert
									/*
								 * 
								 * dae file format only accept matrixs as animations outputs, but some file formats accepts separated keys (location keys, rotation keys, scale keys), so we interpolate if necessary.
								 * 
								 * Here we interpolate for a key that may not have location / rotaiton / or scale
								 * 
								 */
									finalKeys[key].time = key;
									if (!finalKeys[key].rotSet || !finalKeys[key].locSet || !finalKeys[key].scaleSet)
									{
										AnimationStep step = new AnimationStep(key, anim.clips[i].animationDictionary[bone.name], AnimationBlendMode.Blend, 1);
										finalKeys[key].scale = step.scale;
										finalKeys[key].rot = step.rotation;
										finalKeys[key].loc = step.position;
									}
									#endregion
									#region sort_keys_by_time
									if (locRotScales.Count == 0)
									{
										locRotScales.Add(finalKeys[key]);
									}
									for (int k = 0; k < locRotScales.Count; k++)
									{
										if (k == 0 && locRotScales[k].time >= key)
										{
											locRotScales.Insert(k, finalKeys[key]);
										}
										else if (locRotScales[k].time <= key && k + 1 < finalKeys.Keys.Count && locRotScales[k + 1].time >= key)
										{
											locRotScales.Insert(k + 1, finalKeys[key]);
										}
										else if (locRotScales[k].time <= key && k + 1 >= finalKeys.Keys.Count)
										{
											locRotScales.Add(finalKeys[key]);
										}
									}
									#endregion
								}
								#region retrieve_converted_data
								StringBuilder sb5 = new StringBuilder();
								StringBuilder sb4 = new StringBuilder();
								for (int k = 0; k < locRotScales.Count; k++)
								{
									sb4.Append(locRotScales[k].matrix.array.ToDAEString());
									sb5.Append(locRotScales[k].time);
									if (k + 1 < locRotScales.Count)
									{
										sb4.Append("\n");
										sb5.Append(" ");
									}
								}
								#endregion
								#endregion
								#endregion
								#region write_dae_animation
								writer.WriteStartElement("animation");
								writer.WriteAttributeString("id", bone.name + "-anim");
								writer.WriteAttributeString("name", bone.name);
								writer.WriteStartElement("animation"); //1

								#region times_source
								writer.WriteStartElement("source");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-input");
								writer.WriteStartElement("float_array");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-input" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);

								writer.WriteString(sb5.ToString());

								writer.WriteEndElement(); //float_array;
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-input" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);
								writer.WriteStartElement("param");
								writer.WriteAttributeString("name", "TIME");
								writer.WriteAttributeString("type", "float");
								writer.WriteEndElement();//param
								writer.WriteEndElement();//accessor
								writer.WriteEndElement();//technique_common

								writer.WriteEndElement();//source


								#endregion
								#region transformations_source
								writer.WriteStartElement("source");//1
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-output" + "-transform");
								writer.WriteStartElement("float_array");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-output" + "-transform" + "-array");
								writer.WriteAttributeString("count", "" + (locRotScales.Count * 16));

								writer.WriteString(sb4.ToString());

								writer.WriteEndElement(); //float_array;
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-output" + "-transform" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);
								writer.WriteAttributeString("stride", "16");
								writer.WriteStartElement("param");
								writer.WriteAttributeString("type", "float4x4");
								writer.WriteEndElement();//param
								writer.WriteEndElement();//accessor
								writer.WriteEndElement();//technique_common

								writer.WriteEndElement();//source1
								#endregion

								#region interpolations_source
								writer.WriteStartElement("source");//2
								writer.WriteAttributeString("id", bone.name + "-interpolations");
								writer.WriteStartElement("Name_array");
								writer.WriteAttributeString("id", bone.name + "-interpolations" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);

								sb4.Clear();
								for (int k = 0; k < locRotScales.Count; k++)
								{
									sb4.Append("LINEAR");
									if (k + 1 < locRotScales.Count)
									{
										sb4.Append(" ");
									}
								}
								//writer.WriteString(sb4.ToString());

								writer.WriteEndElement(); //float_array;
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + bone.name + "-interpolations" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);
								writer.WriteStartElement("param");
								writer.WriteAttributeString("type", "name");
								writer.WriteEndElement();//param
								writer.WriteEndElement();//accessor
								writer.WriteEndElement();//technique_common

								writer.WriteEndElement();//source2
								#endregion

								#region sampler_and_channel
								writer.WriteStartElement("sampler");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-transform");
								writer.WriteStartElement("input");
								writer.WriteAttributeString("semantic", "INPUT");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-input");
								writer.WriteEndElement(); //input
								writer.WriteStartElement("input");//2
								writer.WriteAttributeString("semantic", "OUTPUT");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-output");
								writer.WriteEndElement();//input2

								writer.WriteStartElement("input");//3
								writer.WriteAttributeString("semantic", "INTERPOLATION");
								writer.WriteAttributeString("source", "#" + bone.name + "-interpolations");
								writer.WriteEndElement();//input3
								writer.WriteEndElement();//sampler
								writer.WriteStartElement("channel");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-transform");
								writer.WriteAttributeString("target", bone.name + "/matrix");
								writer.WriteEndElement();//channel

								writer.WriteEndElement(); //animation1
								writer.WriteEndElement(); // animation
								sb4.Clear();
								sb5.Clear();
								#endregion

								#endregion
							}
						}
					}

					writer.WriteEndElement(); //library_animations
					#endregion

					#region library_animation_clips

					/*
				writer.WriteStartElement("library_animation_clips");
				writer.WriteStartElement("AnimationClip");
				
				writer.WriteEndElement(); //AnimationClip
				writer.WriteEndElement(); //library_animaion_clips
				*/
					#endregion
				}

				writer.WriteStartElement("library_visual_scenes");
				#region library_visual_scenes
				writer.WriteStartElement("visual_scene");
				writer.WriteAttributeString("id", this.guid);
				writer.WriteAttributeString("name", this.name);

				// au pire: on exporte un genre de collada, mais on l'appel pas collada, et du coup bah on saura importer des collada, importer des glurp ou autre nom, et exporter glurp et collada, mais nos collada exportés, perdront leurs scripts et tout ça
				foreach (Scene subScene in this.children)
				{
					WriteNodesRecursive(writer, subScene);
				}
				writer.WriteEndElement(); //visual_scene

				writer.WriteEndElement(); //library visual scenes
				#endregion

				writer.WriteStartElement("scene");
				#region scene
				writer.WriteStartElement("instance_visual_scene");
				writer.WriteAttributeString("url", "#" + this.guid);
				writer.WriteEndElement(); //instance
				writer.WriteEndElement();//scene
				#endregion

				writer.WriteEndElement(); //collada

				writer.WriteEndDocument();
				//library_animations
				//animation id= nom_os + -animation va contenir temps et matrices sur temps dans des array de float
				//library_animations_clips
				/*
				 * id = nom du clip
				 * start = le temps de depart du clip sur le temps total
				 * end = le temps de fin du clip sur le temps total
				 * 
				 * instance_animation pour chaque animation d'os utilisé dans l'animationclip (ou bien en gros, chaque os)
				 * 
				 * 
				 */
				//library_controllers
				/*
				 * 
				 * contient des controller avec id idmesh + -skin (par convention)
				 * controller qui contient:
				 * des skin
				 * avec source= id_du_mesh_affecté
				 * bind_shape_matrix <bind_shape_matrix>0 0 -0.823226 -127.093 -0.804999 0.172274 0 -393.418 0.172274 0.804999 0 597.2 0 0 0 1</bind_shape_matrix> position inverse du mesh ou position du mesh
				 * un tableau de noms d'os convention : idmesh + -controller + -joints + -array 
				 * <Name_array id = idmesh + "-controller" + "-joints" + "-array" count=65>
				 * NomOs NomOs2 NomOs3, etc... </Name_array>
				 * 
				 * juste apres un float array avec les offset matrix de chaque os. dans l ordre ou c est venu
				 * 
				 * 
				 * juste apres un float array avec les weights par vertices de chauqe os non
				 * 
				 * <vcount></vcount> un nombre d os qui l'affecte pour chaque vertice
				 * <v> 38 1 -> index os suivi de 1 a max_weights genre
				 * 
				 * j'ai 5 os qui affecte le premier vertice,
				 * les 5 premieres paires: index_os_quil'affecte index_weight_quil'affecte ... etc... etc...
				 * 
				 * 
				 * 
				 * 
				 */
			}
			#endregion

		}
		public void ExportCollada(string filePath)
		{
			Dictionary<Type, List<SharedResource>> resources = this.GetResourceDictionary();// = new Dictionary<Type, SharedResource>(); // on laisse meshInfo pour une info de mesh, pour pas perdre les données qu'on a besoin. ou pas.
			Console.WriteLine("hey on a le dico");
			#region DAE_Export
			System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings()
			{
				Indent = true,
				IndentChars = "\t",
				NewLineOnAttributes = false
			};
			using (System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(filePath, xmlWriterSettings))
				{
				writer.WriteStartDocument();
				//var ns = System.Xml.Linq.XNamespace.Get("http://www.collada.org/2005/11/COLLADASchema");
				writer.WriteStartElement("COLLADA", "http://www.collada.org/2005/11/COLLADASchema");
				//var root = new System.Xml.Linq.XElement(ns + "COLLADA", new System.Xml.Linq.XElement(ns + "a", "b"), new System.Xml.Linq.XAttribute("version", "1.4.1"));
				#region initialization
				//root.WriteTo(writer);
				
				//writer.WriteNode(root, true);
				//writer.WriteAttributeString("xmlns", "http://www.collada.org/2005/11/COLLADASchema", null);
				writer.WriteAttributeString("version", "1.4.1");
				writer.WriteStartElement("asset");
				writer.WriteStartElement("unit");
				writer.WriteAttributeString("meter", "1");
				writer.WriteAttributeString("name", "meter");
				writer.WriteEndElement(); //unit
				writer.WriteElementString("up_axis", "Y_UP");
				writer.WriteEndElement(); //asset
				#endregion

				writer.WriteStartElement("library_images");
				#region library_images
				if (resources.ContainsKey(typeof(Texture)))
				{
					foreach (Texture texture in resources[typeof(Texture)])
					{
						writer.WriteStartElement("image");
						writer.WriteAttributeString("id", texture.guid);
						writer.WriteAttributeString("name", texture.name);
						writer.WriteElementString("init_from", texture.filePath);
						writer.WriteEndElement();
					}
				}
				writer.WriteEndElement();
				#endregion
				
				writer.WriteStartElement("library_effects");
				#region library_effects
				if (resources.ContainsKey(typeof(Material)))
				{
					foreach (Material mat in resources[typeof(Material)])
					{
						writer.WriteStartElement("image");

						writer.WriteAttributeString("id", mat.guid + "-fx");
						writer.WriteAttributeString("name", mat.name);
						
						
						writer.WriteStartElement("profile_COMMON");
						
						writer.WriteStartElement("technique");

						writer.WriteAttributeString("sid", "standard");
						writer.WriteStartElement("phong");

						writer.WriteStartElement("emission");
						if (mat.emissiveMap != null)
						{
							writer.WriteStartElement("texture");
							writer.WriteAttributeString("texture", mat.emissiveMap.guid);
							writer.WriteEndElement();
						}
						else
							writer.WriteElementString("color", mat.emissiveColor.ToDaeString());
						writer.WriteEndElement(); //emission


						writer.WriteStartElement("ambient");
						writer.WriteElementString("color", mat.ambientColor.ToDaeString());
						writer.WriteEndElement(); //ambient

						writer.WriteStartElement("diffuse");
						if (mat.diffuseMap != null)
						{
							writer.WriteStartElement("texture");
							writer.WriteAttributeString("texture", mat.diffuseMap.guid);
							writer.WriteEndElement();
						}
						else
							writer.WriteElementString("color", mat.diffuseColor.ToDaeString());
						writer.WriteEndElement(); //diffuse

						writer.WriteStartElement("specular");
						if (mat.specularMap != null)
						{
							writer.WriteStartElement("texture");
							writer.WriteAttributeString("texture", mat.specularMap.guid);
							writer.WriteEndElement();
						}
						else
							writer.WriteElementString("color", mat.specularColor.ToDaeString());
						writer.WriteEndElement(); //specular
					
						writer.WriteEndElement(); // phong

						writer.WriteEndElement(); // tech

						writer.WriteEndElement(); // prof
						
						writer.WriteEndElement(); // image
					}
				}
				writer.WriteEndElement();
				#endregion

				writer.WriteStartElement("library_materials");
				#region library_materials
				if (resources.ContainsKey(typeof(Material)))
				{
					foreach (Material mat in resources[typeof(Material)])
					{
						writer.WriteStartElement("material");
						writer.WriteAttributeString("id", mat.guid);
						writer.WriteAttributeString("name", mat.name + mat.guid);
						
						writer.WriteStartElement("instance_effect");
						writer.WriteAttributeString("url", "#" + mat.guid + "-fx");
						writer.WriteEndElement();//instance_effect
						
						writer.WriteEndElement();//material
					}
				}
				writer.WriteEndElement();
				#endregion

				writer.WriteStartElement("library_geometries");
				#region library_geometries
				if (resources.ContainsKey(typeof(MeshInfo)))
				{
					foreach (MeshInfo meshInfo in resources[typeof(MeshInfo)])
					{

						Mesh mesh = meshInfo.mesh;
						writer.WriteStartElement("geometry");
					
						writer.WriteAttributeString("id", mesh.guid);
						writer.WriteAttributeString("name", mesh.name);
						writer.WriteStartElement("mesh");
						#region positions
						///// POSITIONS //////
						writer.WriteStartElement("source");
						writer.WriteAttributeString("id", mesh.guid + "-positions");
						writer.WriteStartElement("float_array");
						writer.WriteAttributeString("id", mesh.guid + "-positions" + "-array");
						int len = mesh.vertices.Length * 3;
						writer.WriteAttributeString("count", "" + len);
						
						StringBuilder sb = new StringBuilder();
						for (int i = 0; i < mesh.vertices.Length; i++ )
						{

							sb.Append(mesh.vertices[i].x);
							sb.Append(" ");
							sb.Append(mesh.vertices[i].y);
							sb.Append(" ");
							sb.Append(mesh.vertices[i].z);
							if (i + 1 < mesh.vertices.Length)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());

						writer.WriteEndElement(); // float_array
						
						writer.WriteStartElement("technique_common");
						writer.WriteStartElement("accessor");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-positions" + "-array");
						writer.WriteAttributeString("count", mesh.vertices.Length.ToString());
						writer.WriteAttributeString("stride", "3");


						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "X");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Y");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Z");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param


						writer.WriteEndElement();//accessor
						writer.WriteEndElement();//technique_common

						writer.WriteEndElement(); // source0
						#endregion
						#region normals
						////// NORMALS //////

						writer.WriteStartElement("source");
						writer.WriteAttributeString("id", mesh.guid + "-normals");
						writer.WriteStartElement("float_array");
						writer.WriteAttributeString("id", mesh.guid + "-normals" + "-array");
						len = mesh.normals.Length * 3;
						writer.WriteAttributeString("count", "" + len);

						sb = new StringBuilder();
						for (int i = 0; i < mesh.normals.Length; i++)
						{

							sb.Append(mesh.normals[i].x);
							sb.Append(" ");
							sb.Append(mesh.normals[i].y);
							sb.Append(" ");
							sb.Append(mesh.normals[i].z);
							if (i + 1 < mesh.vertices.Length)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());

						writer.WriteEndElement(); // float_array

						writer.WriteStartElement("technique_common");
						writer.WriteStartElement("accessor");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-normals" + "-array");
						writer.WriteAttributeString("count", mesh.normals.Length.ToString());
						writer.WriteAttributeString("stride", "3");


						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "X");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Y");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "Z");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param


						writer.WriteEndElement();//accessor
						writer.WriteEndElement();//technique_common

						writer.WriteEndElement(); // source0
						#endregion
						#region uvs
						////// MAP0 //////

						writer.WriteStartElement("source");
						writer.WriteAttributeString("id", mesh.guid + "-uvs");
						writer.WriteStartElement("float_array");
						writer.WriteAttributeString("id", mesh.guid + "-uvs" + "-array");
						len = mesh.uvs.Length * 2;
						writer.WriteAttributeString("count", "" + len);

						sb = new StringBuilder();
						for (int i = 0; i < mesh.uvs.Length; i++)
						{

							sb.Append(mesh.uvs[i].x);
							sb.Append(" ");
							sb.Append(mesh.uvs[i].y);
							if (i + 1 < mesh.vertices.Length)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());

						writer.WriteEndElement(); // float_array

						writer.WriteStartElement("technique_common");
						writer.WriteStartElement("accessor");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-uvs" + "-array");
						writer.WriteAttributeString("count", mesh.uvs.Length.ToString());
						writer.WriteAttributeString("stride", "2");


						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "S");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param

						writer.WriteStartElement("param");
						writer.WriteAttributeString("name", "T");
						writer.WriteAttributeString("type", "float");
						writer.WriteEndElement();// param


						writer.WriteEndElement();//accessor
						writer.WriteEndElement();//technique_common

						writer.WriteEndElement(); // source0
						#endregion
						#region vertices
						writer.WriteStartElement("vertices");
						writer.WriteAttributeString("id", mesh.guid + "-vertices");
						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "POSITION");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-positions");
						
						writer.WriteEndElement();//input
						writer.WriteEndElement();//vertices
						#endregion
						#region polylist
						 //deux problèmes possibles pr l instant : triés en sens inverse, pas counterclockwise, ou pas clockwise.
						/*
						 * ou bien le matériaux qu'on a pas cité.
						 * 
						 */
						// 

						/*
						 * une source de recherche : j'ai beaucoup plus de vertices, peut etre forcer assimp a n'importer que l'essentiel sans trafiquer le truc.
						 * 
						 */
						writer.WriteStartElement("polylist");
						writer.WriteAttributeString("count", "" + (mesh.faces.Length));
						writer.WriteAttributeString("material", meshInfo.materials[0].name + meshInfo.materials[0].guid);
						#region inputs
						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "VERTEX");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-vertices");
						writer.WriteAttributeString("offset", "" + 0);
						writer.WriteEndElement();//input


						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "NORMAL");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-normals");
						writer.WriteAttributeString("offset", "" + 1);
						writer.WriteEndElement();//input


						writer.WriteStartElement("input");
						writer.WriteAttributeString("semantic", "TEXCOORD");
						writer.WriteAttributeString("source", "#" + mesh.guid + "-uvs");
						writer.WriteAttributeString("offset", "" + 2);
						writer.WriteAttributeString("set", "0");
						writer.WriteEndElement();//input
						#endregion
						#region vcount
						writer.WriteStartElement("vcount");
						len = mesh.faces.Length;
						sb = new StringBuilder();

						for (int i = 0; i < len; i++ )
						{
							sb.Append(mesh.faces[i].indices.Length);
							if (i + 1 < len)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());
						sb.Clear();
						writer.WriteEndElement(); //vcount
						#endregion
						#region polygons
						writer.WriteStartElement("p");
						for (int i = 0; i < len; i++ )
						{
							for (int j = 0; j < mesh.faces[i].indices.Length; j++)
							{
								sb.Append(mesh.faces[i].indices[j]);
								sb.Append(" ");
								sb.Append(mesh.faces[i].indices[j]);
								sb.Append(" ");
								sb.Append(mesh.faces[i].indices[j]);
								if (j + 1 < mesh.faces[i].indices.Length)
								{
									sb.Append(" ");
								}
							}
							if (i + 1 < len)
							{
								sb.Append(" ");
							}
						}
						writer.WriteString(sb.ToString());
						writer.WriteEndElement();//p
						#endregion

						writer.WriteEndElement(); //polylist
						#endregion

						writer.WriteEndElement(); //mesh

						writer.WriteEndElement();// geometry
					}
				}
				writer.WriteEndElement(); //geometries
				#endregion //atention ici faut gerer que ce soit des meshinfo d utilisés plutot.- //attention faudra gerer des meshinfo a la place

				writer.WriteStartElement("library_controllers");
				#region library_controllers
				if (resources.ContainsKey(typeof(MeshInfo)))
				{
					foreach (MeshInfo info in resources[typeof(MeshInfo)])
					{
						if (info.isSkinned)
						{
							writer.WriteStartElement("controller");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller");
							writer.WriteStartElement("skin");
							writer.WriteAttributeString("source", info.mesh.guid);
							writer.WriteStartElement("bind_shape_matrix");
							writer.WriteString(info.sceneNode.getTransformation().inverse.array.ToDAEString());
							writer.WriteEndElement(); //bind_shape_matrix
							writer.WriteStartElement("source");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-joints");
							List<int>[] weights = new List<int>[info.mesh.vertices.Length];
							List<int>[] bones = new List<int>[info.mesh.vertices.Length];
							List<Matrix> offsets = new List<Matrix>();
							List<float> orderedWeights = new List<float>();
							if (resources.ContainsKey(typeof(SceneBone)))
							{
								int i = 0;
								int j = 0;
								writer.WriteStartElement("Name_array");
								writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-joints" + "-array");
								StringBuilder sb = new StringBuilder();
								foreach (SceneBone bone in resources[typeof(SceneBone)])
								{
									if (bone.weights.ContainsKey(info))
									{
										foreach (VertexWeight weight in bone.weights[info])
										{
											if (weights[weight.index] == null)
											{
												weights[weight.index] = new List<int>();
												bones[weight.index] = new List<int>();
											}
											weights[weight.index].Add(j);
											bones[weight.index].Add(i);
											orderedWeights.Add(weight.weight);
											j++;
										}
										offsets.Add(bone.offsetMatrixs[info]);
										sb.Append(bone.name);
										i++;
									}
								}
								writer.WriteAttributeString("count", "" + i);
								writer.WriteString(sb.ToString());
								writer.WriteEndElement(); //Name_array
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-joints" + "-array");
								writer.WriteAttributeString("count", "" + i);
								writer.WriteStartElement("param");
								writer.WriteAttributeString("type", "name");
								writer.WriteEndElement(); // param
								writer.WriteEndElement(); //accessor
								writer.WriteEndElement(); //technique_common
							}
//							foreach (SceneBone bone in )
							writer.WriteEndElement(); //source0
							writer.WriteStartElement("source");//1
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-matrices");
							writer.WriteStartElement("float_array");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-matrices" + "-array");
							writer.WriteAttributeString("count", "" + offsets.Count);
							StringBuilder sb1 = new StringBuilder();
							for (int i = 0; i < offsets.Count; i++ )
							{
								sb1.Append(offsets[i].array.ToDAEString());
								if (i + 1 < offsets.Count)
								{
									sb1.Append("\n");
								}
							}
							writer.WriteString(sb1.ToString());
							writer.WriteEndElement(); //float_array

							writer.WriteStartElement("technique_common");
							writer.WriteStartElement("accessor");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-matrices" + "-array");
							writer.WriteAttributeString("count", "" + offsets.Count);
							writer.WriteAttributeString("stride", "16");
							writer.WriteStartElement("param");
							writer.WriteAttributeString("type", "float4x4");
							writer.WriteEndElement(); //param

							writer.WriteEndElement(); //accessor
							writer.WriteEndElement(); // technique_common



							writer.WriteEndElement(); //source1

							writer.WriteStartElement("source"); //2
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-weights" );
							writer.WriteStartElement("float_array");
							writer.WriteAttributeString("id", info.mesh.guid + "-controller" + "-weights" + "-array");
							writer.WriteAttributeString("count", "" +orderedWeights.Count);
							writer.WriteString("" + orderedWeights.ToArray().ToDAEString());
							writer.WriteEndElement(); // float_array

							writer.WriteStartElement("technique_common");
							writer.WriteStartElement("accessor");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-weights" + "-array");
							writer.WriteAttributeString("count", "" + orderedWeights.Count);
							writer.WriteStartElement("param");
							writer.WriteAttributeString("type", "float");
							writer.WriteEndElement(); //param

							writer.WriteEndElement(); //accessor
							writer.WriteEndElement(); // technique_common
							writer.WriteEndElement(); //source 2
							writer.WriteStartElement("joints");
							
							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "JOINT");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-joints");
							writer.WriteEndElement(); //input

							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "INV_BIND_MATRIX");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-matrices");
							writer.WriteEndElement(); //input
							writer.WriteEndElement();//joints
							writer.WriteStartElement("vertex_weights");
							writer.WriteAttributeString("count", "" + weights.Length);
							int[] lengths = new int[weights.Length];
							for (int i = 0; i < weights.Length; i++ )
							{
								lengths[i] = weights[i].Count;
							}
							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "JOINT");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-joints");
							writer.WriteAttributeString("offset", "0");
							writer.WriteEndElement(); //input

							writer.WriteStartElement("input");
							writer.WriteAttributeString("semantic", "WEIGHT");
							writer.WriteAttributeString("source", "#" + info.mesh.guid + "-controller" + "-weights");
							writer.WriteAttributeString("offset", "1");
							writer.WriteEndElement(); //input

							writer.WriteStartElement("vcount");
							writer.WriteString("" + lengths.ToDAEString());
							writer.WriteEndElement(); //vcount
							writer.WriteStartElement("v");
							StringBuilder sb3 = new StringBuilder();
							for (int i = 0; i < weights.Length; i++ )
							{
								for (int j = 0; j < lengths[i]; j++)
								{
									sb3.Append("" + bones[i][j] + " " + weights[i][j]);
									if (j + 1 < lengths[i])
										sb3.Append(" ");
								}
								if (i + 1 < weights.Length)
								{
									sb3.Append(" ");
								}
							}
							writer.WriteString(sb3.ToString());
							writer.WriteEndElement(); //v
							writer.WriteEndElement(); //vertex_weights

							writer.WriteEndElement(); // skin
							writer.WriteEndElement(); // controller.
						}
					}
				}
				
				writer.WriteEndElement(); // library_controllers.
				#endregion
				if (this.getRoot() != null && this.getRoot().animation != null)
				{
				writer.WriteStartElement("library_animations");
				
					#region library_animations
					AnimationInfo anim = this.getRoot().animation;
					float[] startTimes = new float[anim.clips.Count];
					float[] endTimes = new float[anim.clips.Count];
					float totalTime = 0;
					for (int i = 0; i < anim.clips.Count; i++)
					{
						if (i > 0)
						{
							totalTime += anim.clips[i - 1].length;
							startTimes[i] = totalTime;
							endTimes[i] = totalTime + anim.clips[i].length;
						}
						else
						{
							startTimes[i] = totalTime;
							endTimes[i] = anim.clips[i].length;
						}
					}
					if (resources.ContainsKey(typeof(SceneBone)))
					{
						for (int i = 0; i < anim.clips.Count; i++)
						{
							float timeOffset = startTimes[i];
							StringBuilder sb = new StringBuilder();
							foreach (SceneBone bone in resources[typeof(SceneBone)])
							{
								#region build_bone_dae_compatible_keys
								#region register_each_available_times
								List<PositionKey> positionKeys = anim.clips[i].animationDictionary[bone.name].PositionKeys;
								List<ScaleKey> scaleKeys = anim.clips[i].animationDictionary[bone.name].ScaleKeys;
								List<RotationKey> rotationKeys = anim.clips[i].animationDictionary[bone.name].RotationKeys;

								//anim.clips[i].animationDictionary[bone.name].
								// un on check que les trois ont des temps identiques et un nombre de frames identiques.
								// deux, si c est le cas on export en faisant des transformations pour chaque, sinon bah on doit faire des putains d'animation step

								/*
								 * dae file format only accept matrixs as animations outputs, but some file formats accepts separated keys (location keys, rotation keys, scale keys), so we interpolate if necessary.
								 * 
								 * Here we register keys by time, to check later if a time contains only loc/ only rot /only scale, and convert it to interpolated matrix.
								 * 
								 */
								Dictionary<float, LocRotScale> finalKeys = new Dictionary<float, LocRotScale>();

								for (int k = 0; k < positionKeys.Count; k++)
								{
									finalKeys.AddOrRetrieve(positionKeys[k].timeOffset, new LocRotScale());
									finalKeys[positionKeys[k].timeOffset].loc = positionKeys[k].key;

								}

								for (int k = 0; k < rotationKeys.Count; k++)
								{
									finalKeys.AddOrRetrieve(rotationKeys[k].timeOffset, new LocRotScale());
									finalKeys[rotationKeys[k].timeOffset].rot = rotationKeys[k].key;
								}

								for (int k = 0; k < positionKeys.Count; k++)
								{
									finalKeys.AddOrRetrieve(positionKeys[k].timeOffset, new LocRotScale());
									finalKeys[positionKeys[k].timeOffset].scale = scaleKeys[k].key;
								}
								#endregion
								#region sort_and_convert
								List<LocRotScale> locRotScales = new List<LocRotScale>();
								//List<AnimationStep> steps = new List<AnimationStep>();
								foreach (float key in finalKeys.Keys)
								{
									#region convert
									/*
								 * 
								 * dae file format only accept matrixs as animations outputs, but some file formats accepts separated keys (location keys, rotation keys, scale keys), so we interpolate if necessary.
								 * 
								 * Here we interpolate for a key that may not have location / rotaiton / or scale
								 * 
								 */
									finalKeys[key].time = key;
									if (!finalKeys[key].rotSet || !finalKeys[key].locSet || !finalKeys[key].scaleSet)
									{
										AnimationStep step = new AnimationStep(key, anim.clips[i].animationDictionary[bone.name], AnimationBlendMode.Blend, 1);
										finalKeys[key].scale = step.scale;
										finalKeys[key].rot = step.rotation;
										finalKeys[key].loc = step.position;
									}
									#endregion
									#region sort_keys_by_time
									if (locRotScales.Count == 0)
									{
										locRotScales.Add(finalKeys[key]);
									}
									for (int k = 0; k < locRotScales.Count; k++)
									{
										if (k == 0 && locRotScales[k].time >= key)
										{
											locRotScales.Insert(k, finalKeys[key]);
										}
										else if (locRotScales[k].time <= key && k + 1 < finalKeys.Keys.Count && locRotScales[k + 1].time >= key)
										{
											locRotScales.Insert(k + 1, finalKeys[key]);
										}
										else if (locRotScales[k].time <= key && k + 1 >= finalKeys.Keys.Count)
										{
											locRotScales.Add(finalKeys[key]);
										}
									}
									#endregion
								}
								#region retrieve_converted_data
								StringBuilder sb5 = new StringBuilder();
								StringBuilder sb4 = new StringBuilder();
								for (int k = 0; k < locRotScales.Count; k++)
								{
									sb4.Append(locRotScales[k].matrix.array.ToDAEString());
									sb5.Append(locRotScales[k].time);
									if (k + 1 < locRotScales.Count)
									{
										sb4.Append("\n");
										sb5.Append(" ");
									}
								}
								#endregion
								#endregion
								#endregion
								#region write_dae_animation
								writer.WriteStartElement("animation");
								writer.WriteAttributeString("id", bone.name + "-anim");
								writer.WriteAttributeString("name", bone.name);
								writer.WriteStartElement("animation"); //1

								#region times_source
								writer.WriteStartElement("source");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-input");
								writer.WriteStartElement("float_array");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-input" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);

								writer.WriteString(sb5.ToString());

								writer.WriteEndElement(); //float_array;
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-input" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);
								writer.WriteStartElement("param");
								writer.WriteAttributeString("name", "TIME");
								writer.WriteAttributeString("type", "float");
								writer.WriteEndElement();//param
								writer.WriteEndElement();//accessor
								writer.WriteEndElement();//technique_common

								writer.WriteEndElement();//source


								#endregion
								#region transformations_source
								writer.WriteStartElement("source");//1
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-output" + "-transform");
								writer.WriteStartElement("float_array");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-output" + "-transform" + "-array");
								writer.WriteAttributeString("count", "" + (locRotScales.Count * 16));

								writer.WriteString(sb4.ToString());

								writer.WriteEndElement(); //float_array;
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-output" + "-transform" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);
								writer.WriteAttributeString("stride", "16");
								writer.WriteStartElement("param");
								writer.WriteAttributeString("type", "float4x4");
								writer.WriteEndElement();//param
								writer.WriteEndElement();//accessor
								writer.WriteEndElement();//technique_common

								writer.WriteEndElement();//source1
								#endregion

								#region interpolations_source
								writer.WriteStartElement("source");//2
								writer.WriteAttributeString("id", bone.name + "-interpolations");
								writer.WriteStartElement("Name_array");
								writer.WriteAttributeString("id", bone.name + "-interpolations" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);

								sb4.Clear();
								for (int k = 0; k < locRotScales.Count; k++)
								{
									sb4.Append("LINEAR");
									if (k + 1 < locRotScales.Count)
									{
										sb4.Append(" ");
									}
								}
								//writer.WriteString(sb4.ToString());

								writer.WriteEndElement(); //float_array;
								writer.WriteStartElement("technique_common");
								writer.WriteStartElement("accessor");
								writer.WriteAttributeString("source", "#" + bone.name + "-interpolations" + "-array");
								writer.WriteAttributeString("count", "" + locRotScales.Count);
								writer.WriteStartElement("param");
								writer.WriteAttributeString("type", "name");
								writer.WriteEndElement();//param
								writer.WriteEndElement();//accessor
								writer.WriteEndElement();//technique_common

								writer.WriteEndElement();//source2
								#endregion

								#region sampler_and_channel
								writer.WriteStartElement("sampler");
								writer.WriteAttributeString("id", bone.name + "-matrix" + "-animation" + "-transform");
								writer.WriteStartElement("input");
								writer.WriteAttributeString("semantic", "INPUT");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-input");
								writer.WriteEndElement(); //input
								writer.WriteStartElement("input");//2
								writer.WriteAttributeString("semantic", "OUTPUT");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-output");
								writer.WriteEndElement();//input2

								writer.WriteStartElement("input");//3
								writer.WriteAttributeString("semantic", "INTERPOLATION");
								writer.WriteAttributeString("source", "#" + bone.name + "-interpolations");
								writer.WriteEndElement();//input3
								writer.WriteEndElement();//sampler
								writer.WriteStartElement("channel");
								writer.WriteAttributeString("source", "#" + bone.name + "-matrix" + "-animation" + "-transform");
								writer.WriteAttributeString("target", bone.name + "/matrix");
								writer.WriteEndElement();//channel

								writer.WriteEndElement(); //animation1
								writer.WriteEndElement(); // animation
								sb4.Clear();
								sb5.Clear();
								#endregion

								#endregion
							}
						}
					}

					writer.WriteEndElement(); //library_animations
					#endregion
			
					#region library_animation_clips
				
				/*
				writer.WriteStartElement("library_animation_clips");
				writer.WriteStartElement("AnimationClip");
				
				writer.WriteEndElement(); //AnimationClip
				writer.WriteEndElement(); //library_animaion_clips
				*/
				#endregion
				}

				writer.WriteStartElement("library_visual_scenes");
				#region library_visual_scenes
				writer.WriteStartElement("visual_scene");
				writer.WriteAttributeString("id", this.guid);
				writer.WriteAttributeString("name", this.name);
			
				// au pire: on exporte un genre de collada, mais on l'appel pas collada, et du coup bah on saura importer des collada, importer des glurp ou autre nom, et exporter glurp et collada, mais nos collada exportés, perdront leurs scripts et tout ça
				foreach (Scene subScene in this.children)
				{
					WriteNodesRecursive(writer, subScene);
				}
				writer.WriteEndElement(); //visual_scene

				writer.WriteEndElement(); //library visual scenes
				#endregion
			
				writer.WriteStartElement("scene");
				#region scene
				writer.WriteStartElement("instance_visual_scene");
				writer.WriteAttributeString("url", "#" + this.guid);
				writer.WriteEndElement(); //instance
				writer.WriteEndElement();//scene
				#endregion

				writer.WriteEndElement(); //collada

				writer.WriteEndDocument();
				//library_animations
				//animation id= nom_os + -animation va contenir temps et matrices sur temps dans des array de float
				//library_animations_clips
				/*
				 * id = nom du clip
				 * start = le temps de depart du clip sur le temps total
				 * end = le temps de fin du clip sur le temps total
				 * 
				 * instance_animation pour chaque animation d'os utilisé dans l'animationclip (ou bien en gros, chaque os)
				 * 
				 * 
				 */
				//library_controllers
					/*
					 * 
					 * contient des controller avec id idmesh + -skin (par convention)
					 * controller qui contient:
					 * des skin
					 * avec source= id_du_mesh_affecté
					 * bind_shape_matrix <bind_shape_matrix>0 0 -0.823226 -127.093 -0.804999 0.172274 0 -393.418 0.172274 0.804999 0 597.2 0 0 0 1</bind_shape_matrix> position inverse du mesh ou position du mesh
					 * un tableau de noms d'os convention : idmesh + -controller + -joints + -array 
					 * <Name_array id = idmesh + "-controller" + "-joints" + "-array" count=65>
					 * NomOs NomOs2 NomOs3, etc... </Name_array>
					 * 
					 * juste apres un float array avec les offset matrix de chaque os. dans l ordre ou c est venu
					 * 
					 * 
					 * juste apres un float array avec les weights par vertices de chauqe os non
					 * 
					 * <vcount></vcount> un nombre d os qui l'affecte pour chaque vertice
					 * <v> 38 1 -> index os suivi de 1 a max_weights genre
					 * 
					 * j'ai 5 os qui affecte le premier vertice,
					 * les 5 premieres paires: index_os_quil'affecte index_weight_quil'affecte ... etc... etc...
					 * 
					 * 
					 * 
					 * 
					 */
				}
				#endregion
		}
		public class LocRotScale
		{
			private Vector3 _loc = Vector3.zero;
			public Vector3 loc
			{
				get
				{
					return (_loc);
				}
				set
				{
					_loc = value;
					locSet = true;
				}
			}
			private Quaternion _rot = Quaternion.identity;
			public Quaternion rot
			{
				get
				{
					return (_rot);
				}
				set
				{
					_rot = value;
					rotSet = true;
				}
			}
			private Vector3 _scale = Vector3.zero;
			public Vector3 scale
			{
				get
				{
					return (_scale);
				}
				set
				{
					_scale = value;
					locSet = true;
				}
			}
			public bool locSet = false;
			public bool rotSet = false;
			public bool scaleSet = false;
			public float time = 0;
			public Matrix matrix
			{
				get
				{
					return (Matrix.Transformation(loc, rot, scale));
				}
			}
		}
		private void WriteNodesRecursive(System.Xml.XmlWriter writer, Scene current)
		{
			writer.WriteStartElement("node");
			writer.WriteAttributeString("id", current.guid);
			writer.WriteAttributeString("name", current.name);
			writer.WriteAttributeString("type", "NODE");
			writer.WriteStartElement("matrix");
			writer.WriteAttributeString("sid", "transform");
			writer.WriteString(current.getTransformation().array.ToDAEString());
			writer.WriteEndElement();//matrix
			for (int i = 0; i < current.meshs.Count; i++)
			{
				writer.WriteStartElement("instance_geometry");
				writer.WriteAttributeString("url", "#" + current.meshs[i].mesh.guid);
				writer.WriteStartElement("bind_material");

				writer.WriteStartElement("technique_common");
				writer.WriteStartElement("instance_material");
				writer.WriteAttributeString("symbol", current.meshs[i].materials[0].name);
				writer.WriteAttributeString("target", "#" + current.meshs[i].materials[0].guid);
				writer.WriteEndElement();
				writer.WriteEndElement();// tech
				writer.WriteEndElement();// bind mat
				writer.WriteEndElement();//instance_geometry
			}
			writer.WriteEndElement();//node

		}

		[System.NonSerializedAttribute]
		private static Assimp.AssimpContext importer = new Assimp.AssimpContext();
		private static Scene AppendAsset(String path)
		{

			if (File.Exists(path))
			{
				Assimp.Scene m_model = importer.ImportFile(path, Assimp.PostProcessPreset.TargetRealTimeFast);
				return (appendAsset(m_model));
			}
			else
			{
				;
			}
			return (null);
		}


		/*
		public class ColladaDocument
		{
			public ColladaDocument()
			{
				;
			}
			public List<AssetNode> asset = null;
			//... etc...
		}*/

		public class XmlnsAttr
		{
			private string _name = "";
			public string name
			{
				get
				{
					return (this._name);
				}
			}
			private string _value = "";
			public string value
			{
				get
				{
					return (this._value);
				}
			}
			public XmlnsAttr(string name, string value)
			{
				this._name = name;
				this._value = value;
			}
		}
		// exemple de classe générée.
		/*
		public class AssetNode
		{
			public AssetNode(XmlNode node)
			{
				if (node.Attributes != null)
				{

					foreach (XmlNode attribute in node.Attributes)
					{
						string attributeName = attribute.Name;
						string attributeValue = attribute.Value;
						if (attributeName == "xmlns")
						{
							this.xmlns = new XmlnsAttr(attribute.Name, attribute.Value);
						}
						else if (attributeName == "etc...")
						{
							;
						}
					}
				}
				if (node.ChildNodes != null)
				{
					foreach (XmlNode subNode in node.ChildNodes)
					{
						if (subNode.Name == "contributor")
						{
							if (this.contributorNodeList == null)
							{
								this.contributorNodeList = new List<ContributorNode>();
							}
							this.contributorNodeList.Add(new ContributorNode(subNode));
						}
						else if (subNode.Name == "created")
						{
							if (this.createdNodeList == null)
							{
								this.createdNodeList = new List<CreatedNode>();
							}
							this.createdNodeList.Add(new CreatedNode(subNode));
						}
						else if (subNode.Name == "modified")
						{
							this.modifiedNodeList.Add(new ModifiedNode(subNode));
						}
						//etc..
					}
				}
				this.Text = node.InnerText;
			}
			public XmlnsAttr xmlns = null;
			public List<ContributorNode> contributorNodeList = null;
			public ContributorNode contributor
			{
				get
				{
					if (contributorNodeList.Count == 0)
						return (null);
					return (contributorNodeList[0]);
				}
			}
			public List<CreatedNode> createdNodeList = null;
			public List<ModifiedNode> modifiedNodeList = null;
			public List<UnitNode> unitNodeList = null;
			public List<Up_AxisNode> up_axisNodeList = null;
			public string Text = "";
		}*/
		public static void ImporteNode(XmlNode node)
		{
			string nodeName = node.Name;
			//Console.WriteLine("FOUND NODE:" + node.Name);
			string nodeValue = node.Value;
			string textContent = node.InnerText;
			XmlAttributeCollection attributes = node.Attributes;
			if (attributes != null)
			{
				foreach (XmlAttribute attribute in attributes)
				{
					string attributeName = attribute.Name;
					//Console.WriteLine("attribute:" + attributeName);
					string attributeValue = attribute.Value;
				}
			}
		
			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					//Console.WriteLine("has child:" + subNode.Name);
					ImporteNode(subNode);
				}
			}
		}
		public static Scene ImportCollada(string path)
		{
			//XmlReader reader = XmlReader.Create(path);
			XmlDocument node = new XmlDocument();
			node.Load(path);
			//ClassFromXMLGenerator generator = new ClassFromXMLGenerator(node, "Collada");
			//Console.WriteLine(generator.stringDoc);
			
			//List<string> attributes = new List<string>();
			//List<XmlNode> nodes = new List<XmlNode>();
			//XmlNode node = node.FirstChild;

			string nodeName = node.Name;
			Console.WriteLine("FOUND NODE:" + node.Name);
			string nodeValue = node.Value;
			string textContent = node.InnerText;
			XmlAttributeCollection attributes = node.Attributes;
			if (attributes != null)
			{
				foreach (XmlAttribute attribute in attributes)
				{
					string attributeName = attribute.Name;
					Console.WriteLine("attribute:" + attributeName);
					string attributeValue = attribute.Value;
				}
			}

			if (node.ChildNodes != null)
			{
				foreach (XmlNode subNode in node.ChildNodes)
				{
					Console.WriteLine("has child:" + subNode.Name);
					ImporteNode(subNode);
				}
			}
			//reader.MoveToContent();
			//StringBuilder parsedDebug = new StringBuilder()
			// Parse the file and display each of the nodes.
			/*while (reader.Read())
			{
				switch (reader.NodeType)
				{
					// e.g: <Collada>
					case XmlNodeType.Element:
						reader.
						Console.Write("<{0}>", reader.Name);
						break;
					// e.g: <SomeTextSpan> hello i am the text </SomeTextSpan>
					case XmlNodeType.Text:
						Console.Write(reader.Value);
						break;
					// ignore.
					case XmlNodeType.CDATA:
						//Console.Write("<![CDATA[{0}]]>", reader.Value);
						break;
					// ignore 
					case XmlNodeType.ProcessingInstruction:
						Console.Write("<?{0} {1}?>", reader.Name, reader.Value);
						break;
					// ignore complet.
					case XmlNodeType.Comment:
						Console.Write("<!--{0}-->", reader.Value);
						break;
					// ignore pour le moment? ou log de warning si version superieure? version du xml utilisé.
					case XmlNodeType.XmlDeclaration:
						Console.Write("<?xml version='1.0'?>");
						break;
					// ignore.
					case XmlNodeType.Document:
						break;
					// ignore.
					case XmlNodeType.DocumentType:
						Console.Write("<!DOCTYPE {0} [{1}]", reader.Name, reader.Value);
						break;
					// pas sur qu'il y en ai en collada.
					case XmlNodeType.EntityReference:
						throw new System.Exception("found entity ref:" + reader.Name);
					//Console.Write(reader.Name);
						break;
					// e.g: </Collada>
					case XmlNodeType.EndElement:
						Console.Write("</{0}>", reader.Name);
						break;
				}
			}*/
			return (null);
		}
	}
}
